import React, { useState, useEffect, useCallback } from "react";
import Image from 'next/image'
import { connect, useDispatch, useSelector } from "react-redux";
import { useRouter } from 'next/router';
import { HANDLE_SUCCESS, HANDLE_ERROR } from "../../provider/ApiProvider";
import { IMAGE_URL,NO_IMAGE_ERROR } from "../../provider/EndPoints";
import { changeDateFormat,capitalizeFirstLetter } from "../../provider/helper";
import Loader from "../../Components/loader";
import { getAllAssistance,getAllAssistanceFilter,createAssistanceType,getAllAssistanceType,getAssistanceTypeDetails,updateAssistanceType,deleteAssistanceType,createAssistanceFare,getAssistanceFareDetails,updateAssistanceFare,deleteAssistanceFare,deleteProblemType,replyAssistanceRequest} from "../../Redux/Action/assistance";
import { createHub,getAllHubs,allCityHubs,deleteHub,updateHub} from "../../Redux/Action/hubs";
import { getAllCities} from "../../Redux/Action/cities"

/* ant design */
import 'antd/dist/antd.css';
import { Table,Tabs,Row, Divider,Card  ,Col, Tag, Space, Modal, Form, Input, Radio, Upload, message, Button, Descriptions, DatePicker,Select,Switch } from 'antd';
import { EditOutlined, DeleteOutlined, InboxOutlined, EyeOutlined, SearchOutlined } from '@ant-design/icons'
// formik import 
import * as Yup from "yup";
import { VALIDATION_SCHEMA, FORMIK } from "../../provider/FormikProvider";
import FormList from "antd/lib/form/FormList";

const AssistanceComp = (props) => {
  const router = useRouter();
  const [loader, setLoader] = useState(false);
  const dispatch = useDispatch();
  const { isLoggedIn,user} = useSelector(state => state.auth);

  const [user_role, setuser_role] = useState(props.user_role);  
  const [user_city_id, setuser_city_id] = useState(props.user_hub_id);
  const [user_hub_id, setuser_hub_id] = useState(props.user_city_id);

  const [form] = Form.useForm();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [modalTitle,setModalTitle]= useState('Add');
  const [buttonText,setButtonText]= useState('Save');
  const [isModalVisibleDetails, setIsModalVisibleDetails] = useState(false);
  const [dataSource, setDataSource] = useState([]);
  const [assistanceDetails,setAssistanceDeatils] = useState(null)
  const [dataSourceAssistanceType, setDataSourceAssistanceType] = useState([]);
  const [dataSourceProblemType, setDataSourceProblemType] = useState([]);
  const [dataSourceAssistanceFare, setDataSourceAssistanceFare] = useState([]);
  const [userKycDeatils, setUserKycDeatils] = useState([]);
  const [cityDataFilter, setCityDataFilter] = useState([]);
  const [hubDataFilter, setHubDataFilter] = useState([]);
  const [hubDefaultValue, setHubDefaultValue] = useState('');
  const [filterArray, setFilter] = useState({'city_id':props.user_city_id?props.user_city_id[0]:'','hub_id':(props.user_role == 'FAE_HUB_ADMIN' || props.user_role == 'FAE_HUB_MANAGER') && props.user_hub_id?props.user_hub_id[0]:'','searchKey':'','start_date':'','end_date':''});
  const [searchKey, setSearchKey] = useState(null);
  const [isFilter, setIsFilter] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [editId, setEditId] = useState('');
  const [activeTab, setActiveTab] = useState(1);
  const { Search,TextArea } = Input;
  const { Option } = Select;
  const { TabPane } = Tabs;
  const { RangePicker } = DatePicker;
  const setCityStatus = 0;
  useEffect(() => {
    if (!isLoggedIn) {
      router.push('/login');
    } else {
      setuser_role(props.user_role);
      setuser_hub_id(props.user_hub_id)
      setuser_city_id(props.user_city_id);  
       
      getAllCityAPI();
      if(props.user_city_id){        
        getAllCityHubsAPI(props.user_city_id[0]);
      } 

      if(props.user_role == 'FAE_HUB_ADMIN' || props.user_role == 'FAE_HUB_MANAGER'){          
        setFilter({...filterArray, hub_id: props.user_hub_id[0]})
      }else{        
        // setFilter({...filterArray, city_id: props.user_city_id[0] ,hub_id: ""})
      }
     
      // getAllAssistanceData();
    }
  }, [])
 
  useEffect(() => {
    if(cityDataFilter.length >0){
      if(props.user_role == 'FAE_MAIN_ADMIN' || props.user_role == 'FAE_ADMIN'){
        const getCity = cityDataFilter.filter((i)=> props.user_city_id.includes(i._id));
        
        if(getCity.length >0){        
          setFilter({...filterArray, city_id: props.user_city_id[0]})
        }else{
          setFilter({...filterArray, city_id: "" ,hub_id: ""})
        }
      }
    }   
  }, [cityDataFilter.length >0 &&  setCityStatus == 0])

  useEffect(() => {
    getAllAssistanceData();
  }, [filterArray])
  useEffect(() => {
    getAllAssistanceData();
  }, [activeTab])

  const getAllCityAPI=()=>{
    try {
      setLoader(true);      
      dispatch(getAllCities())
      .then((res) => {
        console.log('res.data)',res.data)
        setLoader(false);
        setCityDataFilter(res.data);
      })
      .catch((error) => {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });
    } catch (error) {
      setLoader(false);
      // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }

  
  /* formik code start */   
  var initialValuesFare = {
    city_id:user_city_id?user_city_id[0]:'',
    assistance_type_id:'',
    // problem_type_id:'',
    price:'',
    gst:'',
    term_conditions:'',
    created:new Date(),
    updated:new Date()
  };
  const validationSchemaFare= Yup.object() 
  .shape({
    city_id: Yup.string().default(null).nullable()
    .required('Please select City'),
    assistance_type_id: Yup.string().default(null).nullable()
    .required('Please select assistance type'),
    // problem_type_id: Yup.string().default(null).nullable()
    // .required('Please select problem type'),   
    price: Yup.string().default(null).nullable()
    .required('Price is required').matches(/^\d{0,9}(\.\d{1,4})?$/, "Only numeric value are allowed for this field "),
    gst: Yup.string().default(null).nullable()
    .required('GST is required'),
    // term_conditions: Yup.string().default(null).nullable()
    // .required('Terms conditions is required'),
  }); 
  const submitFormFare =(data)=>{  
    callAPI(data,'assistance_Fare');     
  }
  const formikFare = FORMIK(initialValuesFare,validationSchemaFare,submitFormFare);
  formikFare.handleChange = (e) => {        
    formikFare.setFieldValue(e.target.name,e.target.value);  
  };

  // formikProblemType
  var initialValuesProblemType = {
    problem_type:'',
    type:'problem_type',
    created:new Date(),
    updated:new Date()
  };
  const validationSchemaProblemType = Yup.object() 
  .shape({
    problem_type: Yup.string().default(null).nullable()
    .required('Problem Type is required'),   
    
  }); 
  const submitFormProblemType =(data)=>{  
      callAPI(data,'problem_type');   
  }
  const formikProblemType = FORMIK(initialValuesProblemType,validationSchemaProblemType,submitFormProblemType);

  // formikAssistanceType
  var initialValuesAssistanceType = {
    assistance_type:'',
    type:'assistance_type',
    created:new Date(),
    updated:new Date()
  };
  const validationSchemaAssistanceType = Yup.object() 
  .shape({
    assistance_type: Yup.string().default(null).nullable()
    .required('Assistance Type is required'),   
    
  }); 
  const submitFormAssistanceType =(data)=>{  
      callAPI(data,'assistance_type');   
  }
  const formikAssistanceType = FORMIK(initialValuesAssistanceType,validationSchemaAssistanceType,submitFormAssistanceType);

  /* formikRequest  */
  var initialValuesRequest = {
    _id:'',
    status:0,
    assistance_status:'open',
    statusText:'Open',
    amount:0,
    additional_amount:0,
    comment:'',
    additional_comment:'',
    updated:new Date()
  };
  const validationSchemaRequest = Yup.object() 
  .shape({
    assistance_status: Yup.string().required('Status is required'), 
    additional_amount: Yup.string()
    .matches('^(([0-9]*)|(([0-9]*)\.([0-9]*)))$', "Amount should be number or decimal!").default(0), 
    // comment:Yup.string().required('Comment is required'),  
  }); 
  const submitFormRequest =(data)=>{  
    UpdateAssistanceRequest(data);   
  }
  const formikRequest = FORMIK(initialValuesRequest,validationSchemaRequest,submitFormRequest);

  const UpdateAssistanceRequest=(data)=>{
    const dataParse = JSON.parse(data)
    const editId =dataParse._id;    
    try {
      setLoader(true);      
      dispatch(replyAssistanceRequest(data,editId))
      .then((res) => {
          setLoader(false);      
          if(res.status == true){
              HANDLE_SUCCESS(res.message);                                
              setIsModalVisibleDetails(false)           
              setIsEditing(false)           
              formikRequest.resetForm(); 
              getAllAssistanceData();
          }else{
            HANDLE_ERROR(res.message);
          }
      })
      .catch((error) => {
          setLoader(false);
          // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });
    } catch (error) {
        setLoader(false);
        HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }

    
  const callAPI= (data,type)=> {    
    if(type == 'assistance_type'){
      if(isEditing){
        const dataParse = JSON.parse(data)
        const editId =dataParse._id;
        var ApiPath = updateAssistanceType(data,editId);
      }else{
        var ApiPath = createAssistanceType(data)
      }
    }else if(type == 'problem_type'){
      if(isEditing){
        const dataParse = JSON.parse(data)
        const editId =dataParse._id;
        var ApiPath = updateAssistanceType(data,editId);
      }else{
        var ApiPath = createAssistanceType(data)
      }
    }else if(type == 'assistance_Fare'){
      if(isEditing){
        const dataParse = JSON.parse(data)
        const editId =dataParse._id;
        var ApiPath = updateAssistanceFare(data,editId);
      }else{
        var ApiPath = createAssistanceFare(data)
      }
    }
    try {
      setLoader(true);      
      dispatch(ApiPath)
      .then((res) => {
          setLoader(false);      
          if(res.status === true){
              setButtonText("Save");
              HANDLE_SUCCESS(res.message);                
              getAllAssistanceData();                  
              setIsModalVisible(false)           
              setIsEditing(false)           
              formikAssistanceType.resetForm(); 
              formikProblemType.resetForm(); 
              formikFare.resetForm(); 
          }else{
            HANDLE_ERROR(res.message);
          }
      })
      .catch((error) => {
          setLoader(false);
          // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });
    } catch (error) {
        setLoader(false);
        HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  
  const onAdd=(type)=>{
    setIsModalVisible(true)
    if(type == 'assistance_fare'){
      formikFare.setValues(initialValuesFare);
      setModalTitle("Add New Fare");
    }

    if(type == 'problem_type'){
      formikProblemType.setValues(initialValuesProblemType);
      setModalTitle("Add New");
    }

    if(type == 'assistance_type'){
      formikAssistanceType.setValues(initialValuesAssistanceType);
      setModalTitle("Add New ");
    }   
  }
  const  onEdit=(record,type)=>{
    setIsEditing(true);
    setButtonText("Save");
    if(type == 'problem_type'){
      formikProblemType.setValues(record);
      formikProblemType.setFieldValue("type",'problem_type');
    }
    if(type == 'assistance_type'){
      formikAssistanceType.setValues(record);
      formikAssistanceType.setFieldValue("type","assistance_type");
    }
    
    if(type == 'assistance_Fare'){
      setIsModalVisible(true) 
      setModalTitle("Edit Fare");
      formikFare.setValues({
        city_id:record.city_id._id,
        assistance_type_id:record.assistance_type_id._id,
        _id:record._id,
        // problem_type_id:record.problem_type_id._id,
        gst:record.gst,
        price:record.price,
        term_conditions:record.term_conditions,
        status:record.status,
        assistance_status:record.assistance_status,
      });
      formikFare.setFieldValue("type","assistance_Fare");
    }
    
  }

  const onDelete = (record,type) => {
    // showModal();
    Modal.confirm({
      title: 'Are you sure, you want to delete this record?',
      okText: "Yes",
      okType: "danger",
      onOk: () => {       
        deleteCallApi(record,type);
      }
    })
  }
  const deleteCallApi = (record,type) => {    
    if(type == 'problem_type'){      
      var ApiPath = deleteProblemType(record._id);

    }else if(type == 'assistance_type'){   
      var ApiPath = deleteAssistanceType(record._id)

    }else if(type == 'assistance_Fare'){   
      var ApiPath = deleteAssistanceFare(record._id)
    }
    
    try {
      setLoader(true);      
      dispatch(ApiPath)
      .then((res) => {
        setLoader(false); 
        if(res.status == true){          
          HANDLE_SUCCESS(res.message);   
          console.log('type',type)       
          // setDataSourceAssistanceType(res.assistance_typeData);
          if(type == 'problem_type'){
            setDataSourceProblemType(pre=>{
              return pre.filter((client)=>client._id!=record._id);
            }) 
          }else if(type == 'assistance_type'){
            setDataSourceAssistanceType(pre=>{
              return pre.filter((client)=>client._id!=record._id);
            })
          }else if(type == 'assistance_Fare'){
            setDataSourceAssistanceFare(pre=>{
              return pre.filter((client)=>client._id!=record._id);
            })
          }  

        }else{
          HANDLE_ERROR(res.message);
        }  
      })
      .catch((error) => {
        console.log('error',error)
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });     
    } catch (error) {
      setLoader(false);
      HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  const getAllAssistanceData=()=>{
    try {
        setLoader(true);      
        dispatch(getAllAssistanceFilter(filterArray))
        .then((res) => {
          setLoader(false);
          // console.log('activeTab',activeTab)
          if(setCityStatus == 0){           
            setCityDataFilter(res.citiesData)
            setCityStatus = 1;
          }
                   
          setDataSource(res.data.assistanceRequestData);
          setDataSourceAssistanceFare(res.data.assistance_fareData)
          setDataSourceAssistanceType(res.data.assistance_typeData);
          setDataSourceProblemType(res.data.problem_typeData);
                   
        })
        .catch((error) => {
          setLoader(false);
          HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
        });
      } catch (error) {
        setLoader(false);
        HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      }
  }

  const getAllCityHubsAPI=(city_id)=>{
    try {
      setLoader(true);      
      dispatch(allCityHubs(city_id))
      .then((res) => {
        setLoader(false);
        setHubDataFilter(res.data);
      })
      .catch((error) => {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });
    } catch (error) {
      setLoader(false);
      // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  function onChangeActive(checked) {
    // console.log(`switch to ${checked}`);
  }
  const columns = [
    {
      key: "1",
      title: 'Customer Name',
      render: (record)=>{
        return record.user_id.name; 
      },
      
    },
    {
      key: "3",
      title: 'Phone Number',
      render: (record)=>{
        return record.user_id.phone; 
      },
        
    },
    {
      key: "4",
      title: 'Email',
      render: (record) => {
        return record.user_id.email;;
      },
      
    },
    {
      key: "5",
      title: 'Address',
      render: (record) => {
        return record.user_id.address1;
      },
      
    },
    {
      key: "6",
      title: 'City',
      render: (record) => {
        return record.city_id.name;
      }
    },
    {
      key: "7",
      title: 'Query',
      render: (record) => {
        return record.comment;
      }
    },
    {
      key: "8",
      title: 'Updated By',
      render: (record) => {
        return changeDateFormat(record.updated);
      }
    },
    {
      key: "9",
      title: 'Current Status',
      render: (record) => {        
        return <>
          <Tag style={{'textTransform':'capitalize'}} color={record.assistance_status=='payment_requested'?'red':'green'}>{record.assistance_status=='payment_requested'?'Payment Requested':record.assistance_status}</Tag>
          {/* <Tag color={record.status?'green':'red'}>{record.status==1?'Payment':record.status==2?'Resolved':'Open'}</Tag> */}
        </>
      }
    },
    (user_role != 'FAE_STAFF')?
    {
      
      key: "10",
      title: 'Action',
      render: (record) => {
        return <Button type="danger" size="small" onClick={()=>{
          formikRequest.setFieldValue('status',record.status);
          formikRequest.setFieldValue('assistance_status',record.assistance_status);
          
          if(record.assistance_status == 'payment_requested'){
            var statusText="Payment Requested" ;
          }else{
            var statusText=record.assistance_status ;
          }
          
          formikRequest.setFieldValue('statusText',capitalizeFirstLetter(statusText)); 
          formikRequest.setFieldValue('_id',record._id); 
          formikRequest.setFieldValue('additional_amount',record.additional_amount); 
          formikRequest.setFieldValue('additional_comment',record.additional_comment); 
          // set amount from dataSourceAssistanceFare
          if(dataSourceAssistanceFare.length>0){
            const getIndexAmt = dataSourceAssistanceFare.findIndex((val,key)=>{
              return (val.city_id._id==record.city_id._id)
            })
            
            var getAmount = 0;
            if(getIndexAmt >=0){
              const getFareData= dataSourceAssistanceFare[getIndexAmt];
              // getAmount = parseFloat(getFareData.gst)+parseFloat(getFareData.price);
              const getAmountGst = (parseFloat(getFareData.gst)*parseFloat(getFareData.price))/100;
              getAmount= parseFloat(getAmountGst)+parseFloat(getFareData.price);
              
            }
            formikRequest.setFieldValue('amount',getAmount);
          }
          setAssistanceDeatils(record);
          
          setIsModalVisibleDetails(true)
        }} >View More</Button>;
      }
    }
    :{}   
  ];
  /* column for setup fare */
  const columnsFare = [
    {
      key: "1",
      title: 'City Name',
      render: (record) => {
        return record.city_id.name;
      },
    },
    {
        key: "2",
        title: 'Assistance Type',
        render: (record) => {
          return record.assistance_type_id.assistance_type;
        },
    },
    // {
    //   key: "3",
    //   title: 'Problem Type',
    //   render: (record) => {
    //     return record.problem_type_id.problem_type;
    //   },
    // },
    {
      key: "4",
      title: 'Price',
      render: (record) => {
        return record.price;
      },
    },
    {
      key: "5",
      title: 'GST (%)',
      render: (record) => {
        return record.gst;
      },
    },
    {
      key: "7",
      title: 'Action',
      // dataIndex: '_id',
      render: (record) => {
        return <>   
         <i className="fa fa-edit me-2"  style={{'color': 'blue'}} onClick={() => {
               onEdit(record,'assistance_Fare')    
          }} ></i> 
          <i className="fa fa-trash me-2"  style={{'color': 'red'}} onClick={() => {
               onDelete(record,'assistance_Fare')    
          }}></i>
        </>
      }
    },    
  ]
  
  /* column for problem type */
  const columnsProblemType= [
    {
      key: "1",
      title: 'Problem type',
      render: (record) => {
        return record.problem_type;
      }
    },
    {
      key: "7",
      title: 'Action',
      // dataIndex: '_id',
      render: (record) => {
        return <>  
          <i className="fa fa-edit me-3" onClick={() => {
               onEdit(record,'problem_type')    
          }}  style={{'color': 'blue'}}></i>   

          <i className="fa fa-trash me-2" onClick={() => {
               onDelete(record,'problem_type')    
          }}  style={{'color': 'red'}}></i>
        </>
      }
    },
  ];

  /* column for Assistance type */
  const columnsAssistanceType = [
    {
      key: "1",
      title: 'Assistance type',
      render: (record) => {
        return record.assistance_type;
      }
    },
    {
      key: "7",
      title: 'Action',
      // dataIndex: '_id',
      render: (record) => {
        return <>  
          <i className="fa fa-edit me-3" onClick={() => {
               onEdit(record,'assistance_type')    
          }}  style={{'color': 'blue'}}></i>   

          <i className="fa fa-trash me-2" onClick={() => {
               onDelete(record,'assistance_type')    
          }}  style={{'color': 'red'}}></i>
        </>
      }
    },
  ];

  /* table data onchange function */
  function onChange(pagination, filters, sorter, extra) {
    // console.log('params', pagination, filters, sorter, extra);
    const filterData ={
      sortField: sorter.field,
      sortOrder: sorter.order,
      pagination,
      ...filters,
    }
  }

  /* select chane function */   
  function handleChangeSelect(value,selectType) {
    if(selectType == 'assistance_status'){
      formikRequest.setFieldValue(selectType,value);
           
      if(value == 'payment_requested'){
        var statusText="Payment Requested" ;
      }else{
        var statusText=value ;
      }
      formikRequest.setFieldValue('statusText',capitalizeFirstLetter(statusText)); 
    }else{
      if(selectType == 'start_date'){ 
        if(value != null){
          setFilter({...filterArray,  start_date:changeDateFormat(value._d,"YYYY-MM-DD")})
        }else{
          setFilter({...filterArray,  start_date:""})
        }
  
      }else if(selectType == 'end_date'){ 
        if(value != null){
          setFilter({...filterArray,  end_date: changeDateFormat(value._d,"YYYY-MM-DD")})
        }else{
          setFilter({...filterArray,  end_date:""})
        }
  
      }else if(selectType == 'city'){  
        setFilter({...filterArray,  city_id: value, hub_id: ''})
        setHubDefaultValue('') 
        setHubDataFilter([]); 
        if(value){
          getAllCityHubsAPI(value);
        } 

      }else if(selectType == 'hub'){  
        setHubDefaultValue(value) 
        setFilter({...filterArray,  hub_id: value})
      }
      else {
        formikFare.setFieldValue(selectType,value);
      }
       
    }
    
  }
  const searchOnEnter=(e)=>{
    const value = e.target.value;
    console.log('value',value);
    if(value.length >0){
      setSearchKey(value);
      setFilter({...filterArray,searchKey:value})
    }else{
      setSearchKey('null');
      setFilter({...filterArray,searchKey:""})
    }
  }
  
  const onSearch = value => {
    if(value.length >0){
      setSearchKey(value);
      setFilter({...filterArray,searchKey:value})

    }else{
      setSearchKey('null');
      setFilter({...filterArray,searchKey:""})
    }
  };
  /* code for tabs */
  function onChangeTab(key) {
    setActiveTab(key)
  }
  return (
    <>
      <Loader loading={loader} />
      {/* <h2 className="mb-4 text-danger fw-bold fs-3">Assistance</h2> */}
      <div className="card-container">
      <Tabs defaultActiveKey="support_assistance_key" size="large" onChange={onChangeTab}  type="card" >
        {/* show all list of assistance start */}
        <TabPane tab="Support & Assistance Request" key="support_assistance_key">
            <Row className="mb-2 mt-3">
              <Col span={8} className="mb-3">
                <Search placeholder="Search by Email, Phone, Booking Id or bike registration number" name="support_serch" allowClear  onSearch={onSearch} onPressEnter={(e)=>searchOnEnter(e)}   />
              </Col>
              {/* {user_role != 'FAE_HUB_ADMIN' && user_role != 'FAE_HUB_MANAGER'? */}
              <Col span={5} className="mb-3">
                
                <Select  value={filterArray.city_id} name="city_filter" className="w-100" id="city_filter" onChange={(e)=>handleChangeSelect(e,'city')}>
                {(user_role == 'FAE_MAIN_ADMIN' || user_role == 'FAE_ADMIN'?
                <Option value="">Select City</Option>:'')}
                
                {
                  cityDataFilter.map(function(item, i){                  
                    if((user_role == 'FAE_CITY_ADMIN' || user_role == 'FAE_HUB_ADMIN' || user_role == 'FAE_HUB_MANAGER') && item._id == user_city_id[0]){
                      return <Option  key={i} value={item._id} >{item.name}</Option>
                    }else if(user_role == 'FAE_MAIN_ADMIN'  || user_role == 'FAE_ADMIN'  || user_role == 'FAE_STAFF'){
                      return <Option key={i}  value={item._id} >{item.name}</Option>
                    }                       
                  })
                  
                }
              </Select>             
              </Col>
               {/* :''} */}
              <Col span={5} className="mb-3">
                <Select value={filterArray.hub_id} style={{ width: 120 }} className="w-100" name="hub_filter" id="hub_filter"  onChange={(e)=>handleChangeSelect(e,'hub')}>
                  {
                  (user_role == 'FAE_MAIN_ADMIN' || user_role == 'FAE_ADMIN'|| user_role == 'FAE_CITY_ADMIN')?( <Option value="">Select Hub</Option>):""}
                  {
                    hubDataFilter.map(function(item, i){
                      if(user_role == 'FAE_MAIN_ADMIN' || user_role == 'FAE_ADMIN'){
                        return <Option key={i} value={item._id} >{item.title}</Option>
                      }else{  
                        if((user_role == 'FAE_HUB_ADMIN' || user_role == 'FAE_HUB_MANAGER')){
                          if(user_hub_id  && user_hub_id.includes(item._id) ){                  
                            return <Option key={i} value={item._id} >{item.title}</Option>  
                          }
                        }else{
                          return <Option key={i} value={item._id} >{item.title}</Option>
                        } 
                      }                      
                    })
                  }
                </Select>
              </Col>
              <Col span={6} className="mb-3 ">               
                <DatePicker   format='DD-MM-YYYY'  placeholder="Start Date"  className="" name="start_date_filter" id="start_date_filter" onClear={(e)=>handleChangeSelect(e,'start_date')} onChange={(e)=>handleChangeSelect(e,'start_date')} />
                <DatePicker  format='DD-MM-YYYY' placeholder="End  Date"   className=""  name="end_date_filter" id="end_date_filter" onClear={(e)=>handleChangeSelect(e,'end_date')}  onChange={(e)=>handleChangeSelect(e,'end_date')} />
              </Col>
              
            </Row>
            <Row className="mt-1">
              <Col span={24} className="mb-3">
                <Table  scroll={{x: 1000}} loading={isLoading} onChange={onChange} columns={columns} dataSource={dataSource} />
              </Col>
            </Row>
            {/* rowSelection={{ type: 'checkbox' }} */}
            {/* View  assistance details modal start */}
            <Modal
              title={"View Details"}
              destroyOnClose={true}
              centered
              visible={isModalVisibleDetails}
              onOk={()=>{
                // setIsModalVisibleDetails(false)
                formikRequest.submitForm()                  
              }} 
              onCancel={()=>{
                setIsModalVisibleDetails(false)
                formikRequest.resetForm();
              // Modal.destroyAll();
              }}
              okType="danger"
              okText={formikRequest.values.assistance_status=="payment_requested"?"Payment Request":"Send"}
              cancelText="Close"
              width={1000}       
            >
              <form onSubmit={formikRequest.handleSubmit} className="" > 
                  <Row className="mb-3">
                    <Col span={8} className="mb-3"> 
                      <label className="mb-3 fw-bold">Action</label>                     
                      <Select name="assistance_status" placeholder="" className={"w-100 rounded-4"} value={formikRequest.values.statusText} id="assistance_status" onBlur={formikRequest.handleBlur}  onChange={(e)=>handleChangeSelect(e,'assistance_status')} > 
                          {/* <Option value="" >select</Option>                         */}
                          <Option value="open" >Open</Option>                        
                          <Option value="picked" >Picked</Option>                        
                          <Option value="resolved" >Resolved</Option> 
                          <Option value="invalid" >Invalid</Option> 
                          <Option value="payment_requested" >Payment Requested</Option>                        
                      </Select> 
                      {formikRequest.errors.assistance_status && formikRequest.touched.status && (
                        <div className="input-feedback invailid_feedback">{formikRequest.errors.assistance_status}</div>
                      )}
                    </Col>
                    <Col span={8} className="mb-3"> 
                      <label className="mb-3 fw-bold" >Updated By</label>                     
                      <p>{assistanceDetails?changeDateFormat(assistanceDetails.updated):''}</p>
                    </Col>
                    <Col span={8} className="mb-3"> 
                      <label className="mb-3 fw-bold" >Booking ID</label>                     
                      <p>{assistanceDetails?assistanceDetails.booking_id._id:''}</p>
                    </Col>
                  </Row>
                  <Row className="mb-3">
                    
                    <Col span={6} className="mb-3"> 
                      <label className="mb-3 fw-bold" >Base Rate</label>                     
                      {/* <p>{assistanceDetails?assistanceDetails.amount>0?assistanceDetails.amount:formikRequest.values.amount:formikRequest.values.amount}</p> */}
                      <p>{formikRequest.values.amount}</p>
                    </Col>
                    <Col span={6} className="mb-3"> 
                      <label className="mb-3 fw-bold" >Current Status</label>                     
                      <p>{
                        <Tag style={{'text-transform':'capitalize'}} color={assistanceDetails && assistanceDetails.assistance_status=='payment_requested'?'red':'green'}>{assistanceDetails && assistanceDetails.assistance_status=='payment_requested'?'Payment Requested':assistanceDetails && assistanceDetails.assistance_status}</Tag>
                      // assistanceDetails && assistanceDetails.status == 1?'Payment':assistanceDetails && assistanceDetails.status == 2?'Resolved':'Open'
                      }</p>
                    </Col>
                  </Row>
                  <Row className="mb-3">
                    <Col span={24} className="mb-3"> 
                      <label className="mb-3 fw-bold" >Query</label>                     
                      <p>{assistanceDetails?assistanceDetails.comment:''}</p>
                    </Col>                    
                  </Row>
                  <Row className="mb-3">
                    <Col span={24} className="mb-3"> 
                      <label className="mb-3 fw-bold" >Additional Amount</label>                     
                      <Input type="text" name="additional_amount" id="additional_amount" value={formikRequest.values.additional_amount}  onBlur={formikRequest.handleBlur}  onChange={formikRequest.handleChange}   />

                      {formikRequest.errors.additional_amount && formikRequest.touched.additional_amount && (
                        <div className="input-feedback invailid_feedback">{formikRequest.errors.additional_amount}</div>
                      )}
                    </Col>                    
                  </Row>
                  <Row className="mb-3">
                    <Col span={24} className="mb-3"> 
                      <label className="mb-3 fw-bold" >Additional Comment</label>                     
                      <TextArea placeholder="Additional Comment" name="additional_comment" id="additional_comment" value={formikRequest.values.additional_comment} allowClear  onBlur={formikRequest.handleBlur}  onChange={formikRequest.handleChange} showCount maxLength={250} style={{ height:'120px !important'}}  />

                      {formikRequest.errors.additional_comment && formikRequest.touched.additional_comment && (
                        <div className="input-feedback invailid_feedback">{formikRequest.errors.coadditional_commentmment}</div>
                      )}
                    </Col>                    
                  </Row>
              </form>
            </Modal>        
            {/* View  assistance details modal end */}
        </TabPane>
        {/* show all list of assistance end */}
        {/* set up Fares start */}
        {(user_role != 'FAE_HUB_ADMIN' && user_role != 'FAE_HUB_MANAGER' && user_role != 'FAE_STAFF' )?
        <TabPane tab="Set Up Fares" key="set_up_fares_key">
            <Row className="mb-2 mt-3">             
              <Col span={11} className="mb-3">
                <Row>
                  {(user_role == 'FAE_MAIN_ADMIN' && user_role == 'FAE_ADMIN')?
                  <Col span={12} className="mb-3">                   
                  <Search placeholder="Search by City Name" name="fare_serch"  id="fare_serch" allowClear  onSearch={onSearch} onPressEnter={(e)=>searchOnEnter(e)}   />                   
                  </Col>
                     :""  }
                  <Col span={12} className="mb-3">
                  {user_role != 'FAE_HUB_ADMIN' && user_role != 'FAE_HUB_MANAGER'?
                    <Select  value={filterArray.city_id} name="city_filter" className="w-100" id="city_filter" onChange={(e)=>handleChangeSelect(e,'city')}>
                    {(user_role == 'FAE_MAIN_ADMIN' || user_role == 'FAE_ADMIN'?
                    <Option value="">Select City</Option>:'')}
                    
                    {
                      cityDataFilter.map(function(item, i){                 
                        if(user_role == 'FAE_CITY_ADMIN' && user_city_id.includes(item._id)){
                          return <Option value={item._id} >{item.name}</Option>
                        }else if(user_role != 'FAE_CITY_ADMIN'){
                          return <Option value={item._id} >{item.name}</Option>
                        }                      
                      })
                      
                    }
                  </Select>
                  :''}
                    </Col>
                </Row>                
              </Col>
              <Col span={5} className="mb-3">
                {/* <Select value={filterArray.hub_id} style={{ width: 120 }} className="w-100" name="hub_filter" id="hub_filter"  onChange={(e)=>handleChangeSelect(e,'hub')}>
                  <Option value="">Select Hub</Option>
                  {
                    hubDataFilter.map(function(item, i){
                      if(user_hub_id.includes(item._id)){
                        return <Option value={item._id} >{item.title}</Option>
                      }else if(user_role == 'FAE_MAIN_ADMIN' || user_role == 'FAE_ADMIN'){
                        return <Option value={item._id} >{item.title}</Option>
                      }                      
                    })
                  }
                </Select> */}
              </Col>
              <Col span={8} className="mb-3">
                {/* add new fare start */}
                <Button type="danger" className="float-end" onClick={()=>{onAdd('assistance_fare')}} htmlType="button">
                  Add
                </Button>  
                {/* show all list all fares */}
              </Col>
            </Row>
            <Row className="mt-1">
              <Col span={24} className="mb-3">
                <Table  scroll={{x: 1000}} loading={isLoading} onChange={onChange} columns={columnsFare} dataSource={dataSourceAssistanceFare} />
                {/* rowSelection={{ type: 'checkbox' }} */}
              </Col>
            </Row>
          {/* Edit modal start */}
          <Modal
            title={modalTitle}
            destroyOnClose="true"
            centered
            visible={isModalVisible}
            onOk={()=>{
              formikFare.submitForm()                  
            }} 
            onCancel={()=>{
              setIsModalVisible(false)
              formikFare.resetForm();
            // Modal.destroyAll();
            }}
            okType="danger"
            okText="Save"
            cancelText="Close"
            width={1000}       
          >
            <form onSubmit={formikFare.handleSubmit} className="" > 
                <Row className="mb-3">
                  <Col span={24} className="mb-3">
                    <Select name="city_id" placeholder="For the City" className={"w-100 rounded-4"} value={formikFare.values.city_id} id="city_id" onBlur={formikFare.handleBlur}  onChange={(e)=>handleChangeSelect(e,'city_id')} > 
                        {(user_role == 'FAE_MAIN_ADMIN' || user_role == 'FAE_ADMIN'?
                          <Option value="">Select City</Option>:'')
                        }                       
                        {
                          cityDataFilter.map(function(item, i){
                            if(user_role == 'FAE_CITY_ADMIN'  && user_city_id.includes(item._id)){
                              return <Option value={item._id} >{item.name}</Option>
                            }else if(user_role != 'FAE_CITY_ADMIN'){
                              return <Option value={item._id} >{item.name}</Option>
                            } 
                          })
                        }
                    </Select>                       
                    {formikFare.errors.city_id && formikFare.touched.city_id && (
                        <div className="input-feedback invailid_feedback">{formikFare.errors.city_id}</div>
                    )}
                  </Col>
                  <Col span={24} className="mb-3">
                    <Row className="mb-3">  
                      <Col span={24}>
                        <Select name="assistance_type_id" placeholder="Assistance Type" className={"w-100 rounded-4"}  id="assistance_type_id" value={formikFare.values.assistance_type_id} onBlur={formikFare.handleBlur}  onChange={(e)=>handleChangeSelect(e,'assistance_type_id')}>
                         <Option value="" >Assistance Type</Option>  
                          {
                            dataSourceAssistanceType.map(function(item, i){
                                return <Option  value={item._id} >{item.assistance_type}</Option>
                            })
                          }
                        </Select>
                        {formikFare.errors.assistance_type_id && formikFare.touched.assistance_type_id && (
                            <div className="input-feedback invailid_feedback">{formikFare.errors.assistance_type_id}</div>
                        )}
                      </Col>
                      {/* <Col span={12}>
                        <Select name="problem_type_id" placeholder="Problem Type" className={"w-100 rounded-4"}  id="problem_type_id" value={formikFare.values.problem_type_id} onBlur={formikFare.handleBlur} onChange={(e)=>handleChangeSelect(e,'problem_type_id')}>  
                          <Option value="" >Problem Type</Option>                                
                          {
                            dataSourceProblemType.map(function(item, i){
                                return <Option  value={item._id} >{item.problem_type}</Option>
                            })
                          }
                        </Select>
                        {formikFare.errors.problem_type_id && formikFare.touched.problem_type_id && (
                            <div className="input-feedback invailid_feedback">{formikFare.errors.problem_type_id}</div>
                        )}
                      </Col> */}
                      </Row>  
                    </Col>
                    <Col span={24} className="mb-3">
                    <Row className="mb-3">  
                      <Col span={12}>
                        <Input
                              type="text"                  
                              id="price"
                              name="price"
                              placeholder="Price"
                              value={formikFare.values.price}
                              onChange={formikFare.handleChange}
                              onBlur={formikFare.handleBlur}
                              className={
                              formikFare.errors.price && formikFare.touched.price
                                  ? "text-input error form-control rounded-4 "
                                  : "text-input form-control rounded-4 "
                              }
                          />
                          {formikFare.errors.price && formikFare.touched.price && (
                            <div className="input-feedback invailid_feedback">{formikFare.errors.price}</div>
                          )}
                      </Col>
                      <Col span={12}>
                        <Input
                                type="text"                  
                                id="gst"
                                name="gst"
                                placeholder="GST (%)"
                                value={formikFare.values.gst}
                                onChange={formikFare.handleChange}
                                onBlur={formikFare.handleBlur}
                                className={
                                formikFare.errors.gst && formikFare.touched.gst
                                    ? "text-input error form-control rounded-4 "
                                    : "text-input form-control rounded-4 "
                                }
                            />
                            {formikFare.errors.gst && formikFare.touched.gst && (
                              <div className="input-feedback invailid_feedback">{formikFare.errors.gst}</div>
                            )}
                      </Col>
                    </Row>  
                    </Col>
                    <Col span={24} className="mb-3">
                    <Row className="mb-3">  
                      <Col span={24}>
                        <Input
                              type="text"                  
                              id="term_conditions"
                              name="term_conditions"
                              placeholder="Term & Conditions"
                              value={formikFare.values.term_conditions}
                              onChange={formikFare.handleChange}
                              onBlur={formikFare.handleBlur}
                              className={
                              formikFare.errors.term_conditions && formikFare.touched.term_conditions
                                  ? "text-input error form-control rounded-4 "
                                  : "text-input form-control rounded-4 "
                              }
                          />
                          {formikFare.errors.term_conditions && formikFare.touched.term_conditions && (
                            <div className="input-feedback invailid_feedback">{formikFare.errors.term_conditions}</div>
                          )}
                      </Col>
                     
                    </Row>  
                    </Col>
                </Row>
            </form>
          </Modal>        
          {/* Edit modal end */}
          
        </TabPane>  
        :''} 
        {/* set up Fares end */} 
        {/* Add Assistance type start */}
        {(user_role == "FAE_MAIN_ADMIN" || user_role == "FAE_ADMIN")?
        <TabPane tab="Assistance Type" key="assistance_type_key">
          <Row>
            <Col span={8}>
              <Row>
                <Col span={22} className="mt-1">
                <div className="site-card-border-less-wrapper">
                  <Card title="" bordered={false} >
                    <form onSubmit={()=>{
                      formikAssistanceType.handleSubmit;
                      }} className="" > 
                      <Input status={formikAssistanceType.errors.assistance_type && formikAssistanceType.touched.assistance_type?"error":''} id="assistance_type" name="assistance_type"  placeholder="Assistance Type"  value={formikAssistanceType.values.assistance_type} onChange={formikAssistanceType.handleChange}  onBlur={formikAssistanceType.handleBlur} />
                      {formikAssistanceType.errors.assistance_type && formikAssistanceType.touched.assistance_type && (
                          <div className="input-feedback invailid_feedback">{formikAssistanceType.errors.assistance_type}</div>
                      )}  
                      <Button type="danger" className="mt-3 float-end" onClick={()=>{formikAssistanceType.submitForm()}} htmlType="button">
                      {buttonText}
                      </Button>           
                    </form>
                  </Card>
                </div>
                </Col>
              </Row>
            </Col>
                      
            <Col span={16}>
              <Table  scroll={{x: 1000}} loading={isLoading} onChange={onChange} columns={columnsAssistanceType} dataSource={dataSourceAssistanceType} />
            </Col>
          </Row>          
        </TabPane>  
        :''}  
        {/* Add Assistance type end */}   
        {/* Add Assistance type start */}
        {/* {(user_role == "FAE_MAIN_ADMIN" || user_role == "FAE_ADMIN")?
        <TabPane tab="Problem Type" key="problem_type_key">
          <Row>
            <Col span={8}>
              <Row>
                <Col span={22} className="mt-1">
                <div className="site-card-border-less-wrapper">
                  <Card title="" bordered={false} >
                    <form onSubmit={()=>{
                      formikProblemType.handleSubmit;
                      }} className="" > 
                      <Input status={formikProblemType.errors.problem_type && formikProblemType.touched.problem_type?"error":''} id="problem_type" name="problem_type"  placeholder="Problem Type"  value={formikProblemType.values.problem_type} onChange={formikProblemType.handleChange}  onBlur={formikProblemType.handleBlur} />
                      {formikProblemType.errors.problem_type && formikProblemType.touched.problem_type && (
                          <div className="input-feedback invailid_feedback">{formikProblemType.errors.problem_type}</div>
                      )}  
                      <Button type="danger" className="mt-3 float-end" onClick={()=>{formikProblemType.submitForm()}} htmlType="button">
                      {buttonText}
                      </Button>           
                    </form>
                  </Card>
                </div>
                </Col>
              </Row>
            </Col>
                      
            <Col span={16}>
              <Table  scroll={{x: 1000}} loading={isLoading} onChange={onChange} columns={columnsProblemType} dataSource={dataSourceProblemType} />
            </Col>
          </Row>          
        </TabPane> 
        :''}   */}
        {/* Add Assistance type end */}  
        
      </Tabs>
      </div>
    </>
  );
}
const mapStateToProps = (state, ownProps = {}) => {  
  const { isLoggedIn,user,user_city_id,user_hub_id,user_role  } = state.auth;
  const { inventoryData } = state.inventory;
  return {
    user,
    isLoggedIn,
    user_city_id,
    user_hub_id,
    user_role,
    inventoryData
  };
}

export default connect(mapStateToProps)(AssistanceComp);

