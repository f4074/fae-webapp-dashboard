import React from "react";
import Header from "../Header";
import Footer from "../Footer";
import Sidebar from "../Sidebar";

export default function Layout({ children }) {
  return (
    <>
      <Header />
      <div id="layoutSidenav">
        <Sidebar />
        <div id="layoutSidenav_content">
          <main className="p-5">{children}</main>
          <Footer />
        </div>
      </div>
    </>
  );
}
