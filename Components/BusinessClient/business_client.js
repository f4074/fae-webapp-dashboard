import React , { useState, useEffect, useCallback } from "react";
import { connect,useDispatch, useSelector } from "react-redux";
import { useRouter } from 'next/router'

import { HANDLE_SUCCESS, HANDLE_ERROR, HTTP_SERVICE_CALL } from "../../provider/ApiProvider";
import  {SET_DATA_LOCALSTORAGE,REMEMBER_PASSWORD,GET_DATA_LOCALSTORAGE} from "../../provider/LocalStorageProvider"
import { BASE_URL, POST, IS_LOADING,ADMIN_SIGN_IN } from "../../provider/EndPoints";
import { changeDateFormat } from "../../provider/helper";
import Loader from "../../Components/loader";
import Spinner from "../../Components/Spinner/Spinner";
import { create,getAll,getAllwithFilter,deleteClient,updateClient } from "../../Redux/Action/business_client";
import { createHub,getAllHubs,allCityHubs,deleteHub,updateHub} from "../../Redux/Action/hubs";

import 'antd/dist/antd.css';
import { Table, Tag, Space,Modal, Form, Input, Radio,Row,Col,Select, Button } from 'antd';
import {EditOutlined,DeleteOutlined} from '@ant-design/icons'
// formik import 
import * as Yup from "yup";
import {VALIDATION_SCHEMA,FORMIK} from "../../provider/FormikProvider";
function onChange(pagination, filters, sorter, extra) {
  console.log('params', pagination, filters, sorter, extra);
}

const BusinessClientComp=(props)=> {
  const router = useRouter();  
  const [loader, setLoader] = useState(false);  
  const dispatch = useDispatch();
  const { isLoggedIn} = useSelector(state => state.auth);
  const { clientData } = useSelector(state => state.business_client);

  const [form] = Form.useForm();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [cityDataFilter, setCityDataFilter] = useState([]);
  const [hubDataFilter, setHubDataFilter] = useState([]);
  const [editingClient, setEditingClient] = useState(null);
  const [dataSource,setDataSource]= useState([]);
  const [filterArray, setFilter] = useState({'searchKey':'','start_date':'','end_date':'','city_id':'','hub_id':''});
  
  const [modalTitle,setModalTitle]= useState('Add Client');

  const [user_role, setuser_role] = useState('');  
  const [user_city_id, setuser_city_id] = useState('');

  const { Search,TextArea } = Input;

  useEffect(()=>{
    if (!isLoggedIn) {
      router.push('/login');
    }else{
      setuser_role(props.user_role);
      setuser_city_id(props.user_city_id);

      // if(props.user_role == 'FAE_HUB_ADMIN' || props.user_role == 'FAE_HUB_MANAGER'){          
      //   // setFilter({...filterArray, hub_id: props.user_hub_id[0]})
      // }else{
      //   setFilter({...filterArray, city_id: props.user_city_id[0] ,hub_id: ""})
      // }

      // if(props.user_city_id){        
      //   getAllCityHubsAPI(props.user_city_id[0]);
      // }

      getAllBusinessClients();
    }
    // const formatDate = changeDateFormat('2016-01-04T10:34:23');
    // console.log('formatDate',formatDate);
      
  },[])
  
  useEffect(()=>{
    getAllBusinessClients();      
  },[filterArray])
  /* formik code start */
  var initialValues = {
    business_client:null,
    contact_person: null,
    phone_number: null,
    email: null,
    address: null,
    city: null,
    city_id: null,
    status:false,
    created:new Date(),
    updated:new Date()
  };
  const validationSchema = Yup.object() 
  .shape({
    business_client: Yup.string().default(null).nullable()
    .min(2, 'Must be greater then 2 or equal characters')
    .required('Business Client Name is required'),
    contact_person: Yup.string().default(null).nullable()
    .min(2, 'Must be greater then 2 or equal characters')
    .required('Contact person name is required'),
    phone_number: Yup.number().default(0).nullable()
    .min(10, 'Must be 10 numbers')
    .required('Phone Number is required'), 
    email: Yup.string().default(null).nullable()
    .email('Invalid Email Address!')
    .required('Email is required'),
    address: Yup.string().default(null).nullable()
    .min(2, 'Must be greater then 2 or equal characters')
    .required('Address is required'),
    city: Yup.string().default(null).nullable()
    .required('City is required'),
  });
  
  const submitForm =(data)=>{
    // console.log('data',data); 
    if(isEditing){
      ClientUpdate(data);
    }else{
      ClientSubmit(data); 
    } 
  }
  const ClientSubmit = async (data) => {
    let dataObj = JSON.parse(data); 
    try {
      setLoader(true);      
      dispatch(create(data))
      .then((res) => {
        setLoader(false); 
        console.log('res home ' ,res); 
        if(res.status == true){
          setIsModalVisible(false);
          HANDLE_SUCCESS(res.message);  
          // setNewData(res.data);
          setDataSource(res.BusinessClientsDataAll);
          formik.resetForm();
        }else{
          HANDLE_ERROR(res.message);
        }          
      })
      .catch((error) => {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });     
    } catch (error) {
      setLoader(false);
      HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  };
  const ClientUpdate = async (data) => { 
    const updateData = JSON.parse(data);    
    try {
      setLoader(true);      
      dispatch(updateClient(data,updateData._id))
      .then((res) => {
        setLoader(false); 
        // console.log('res home ' ,res); 
        if(res.status == true){
          setIsModalVisible(false);
          HANDLE_SUCCESS(res.message);
          const index = dataSource.findIndex((client)=>client._id==updateData._id);
          let updateArrData = [...dataSource];          
          updateArrData[index] = res.data;           
          setDataSource(updateArrData);          
          // setNewData(res.data);
          formik.resetForm();
        }else{
          HANDLE_ERROR(res.message);
        }  
      })
      .catch((error) => {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });
     
    } catch (error) {
      setLoader(false);
      HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }

  };
  const formik = FORMIK(initialValues,validationSchema,submitForm); 
  // console.log('formik',formik);
  /* formik code end */

  const setNewData=(data)=>{
    const newClient={
      _id:data._id,
      business_client:data.business_client,
      contact_person: data.contact_person,
      phone_number: data.phone_number,
      email: data.email,
      address: data.address,
      city: data.city,
      created:data.created,
      updated:data.updated
    }
    setDataSource(pre=>{
      return [...pre,newClient]
    })
  }
  const columns = [
    {

      key:"1",
      title: 'Business Client',
      dataIndex: 'business_client',     
      // sorter: (a, b) => a.business_client - b.business_client,
      // sortDirections: ['descend'],
    },    
    {
      key:"2",
      title: 'Contact Person',
      dataIndex: 'contact_person', 
      // defaultSortOrder: 'descend',
      // sorter: (a, b) => a.contact_person - b.contact_person,      
    },
    {
      key:"3",
      title: 'Phone Number',
      dataIndex: 'phone_number',
      // defaultSortOrder: 'descend',
      // sorter: (a, b) => a.phone_number - b.phone_number,     
    },
    {
      key:"4",
      title: 'Email',
      dataIndex: 'email', 
      // defaultSortOrder: 'descend',
      // sorter: (a, b) => a.email - b.email,    
    },
    {
      key:"5",
      title: 'Address',
      dataIndex: 'address',
      // defaultSortOrder: 'descend',
      // sorter: (a, b) => a.address - b.address,     
    },
    {
      key:"6",
      title: 'Created Date',
      dataIndex: 'created',
      render:(createdDate)=>{
        return changeDateFormat(createdDate) ;    
      }, 
      // defaultSortOrder: 'descend',
      // sorter: (a, b) => a.created - b.created,
    },
    (user_role != 'FAE_STAFF')?
    {
      key:"7",
      title: 'Actions',
      render:(record)=>{
        return <>
          <EditOutlined  onClick={()=>{
            onEditClients(record)
          }} />
          <DeleteOutlined  onClick={()=>{
            onDeleteClients(record)
          }} style={{color:'red',marginLeft:12}} />
        </>
      }
    }
    :{}
  ];
  /* get all hubs by city_id */
  const getAllCityHubsAPI=(city_id)=>{
    try {
      setLoader(true);      
      dispatch(allCityHubs(city_id))
      .then((res) => {
        setLoader(false);
        setHubDataFilter(res.data);
      })
      .catch((error) => {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });
    } catch (error) {
      setLoader(false);
      // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }

  const onAddClient=()=>{
    setIsEditing(false);
    setModalTitle("Add Client");
    setIsModalVisible(true);  
    setEditingClient(null); 
    formik.setValues(initialValues);   
  }
  const onDeleteClients=(record)=>{
    // showModal();
    Modal.confirm({
      title:'Are you sure, you want to delete this client record?',
      okText:"Yes",
      okType:"danger",
      onOk:()=>{
        deleteCallApi(record);       
      }
    })
  }
  const deleteCallApi=(record)=>{
    try {
      setLoader(true);      
      dispatch(deleteClient(record._id))
      .then((res) => {
        setLoader(false); 
        if(res.status == true){          
          HANDLE_SUCCESS(res.message);         
          setDataSource(pre=>{
            return pre.filter((client)=>client._id!=record._id);
          })  
        }else{
          HANDLE_ERROR(res.message);
        } 
      })
      .catch((error) => {
        setLoader(false);
        HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });     
    } catch (error) {
      setLoader(false);
      HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  const onEditClients=(record)=>{
    setIsEditing(true);
    setIsModalVisible(true);
    setEditingClient({...record});
    setModalTitle("Edit Client");
    formik.setValues(record);
    /* formik.setValues({
      id:record._id,
      business_client:record.business_client,
      contact_person: record.contact_person,
      phone_number: record.phone_number,
      email: record.email,
      address: record.address,
      city: record.city,
      created: record.created,
      updated:new Date()
    }); */

  }
  const getAllBusinessClients=()=>{
    try {
      setLoader(true);      
      dispatch(getAllwithFilter(filterArray))
      .then((res) => {
        setLoader(false);
        if(res.status == true){
          // HANDLE_SUCCESS(res.message);         
            setDataSource(res.data);
            setCityDataFilter(res.citiesData)  
        }else{
          // HANDLE_ERROR(res.message);
        }  
      })
      .catch((error) => {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });     
    } catch (error) {
      setLoader(false);
      // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  /* search data on enter button */
  const searchOnEnter=(e)=>{
    const value = e.target.value;
    if(value.length >0){
      setFilter({...filterArray,  searchKey: value})
    }else{
      setFilter({...filterArray,  searchKey: ''})
    }
  }
  /* search data on click search button */
  const onSearch = value => {
    if(value.length >0){
      setFilter({...filterArray,  searchKey: value})
    }else{
      setFilter({...filterArray,  searchKey: ''})
    }
  };
  /* select chane function */   
  function handleChangeSelect(value,selectType) {
    if(selectType == 'city'){  
      setFilter({...filterArray,  city_id: value, hub_id: ''})
      // setHubDefaultValue('') 
      setHubDataFilter([]); 
      if(value){
        getAllCityHubsAPI(value);
      } 

    }else if(selectType == 'hub'){  
      // setHubDefaultValue(value) 
      setFilter({...filterArray,  hub_id: value})
    }
    
  }
  return (
    <>
      <Loader loading={loader} />
      <h2 className="mb-4 text-danger fw-bold fs-3">Business Clients</h2>
      <div className="p-4 shadow rounded-6 bg-white">
          <div className="row">
            <Row className="mb-2 mt-3">
              <Col span={6} className="mb-3">
                <Search placeholder="Search by Business Client name" allowClear  onSearch={onSearch} onPressEnter={(e)=>searchOnEnter(e)}   />
              </Col>
              <Col span={6} className="mb-3">
                  {/* {user_role != 'FAE_HUB_ADMIN' && user_role != 'FAE_HUB_MANAGER'?
                  <Select  value={filterArray.city_id} name="city_filter" className="w-100" id="city_filter" onChange={(e)=>handleChangeSelect(e,'city')}>
                 
                  {
                    cityDataFilter.map(function(item, i){
                     
                      if(user_role == 'FAE_CITY_ADMIN' && user_city_id.includes(item._id)){
                        return <Option value={item._id} >{item.name}</Option>
                      }else if(user_role != 'FAE_CITY_ADMIN'){
                        return <Option value={item._id} >{item.name}</Option>
                      }                      
                    })
                    
                  }
                </Select>
                :''} */}
              </Col>
              <Col span={6} className="mb-3">
                {/* <Select value={filterArray.hub_id} style={{ width: 120 }} className="w-100" name="hub_filter" id="hub_filter"  onChange={(e)=>handleChangeSelect(e,'hub')}>
                  <Option value="">Select Hub</Option>
                  {
                    hubDataFilter.map(function(item, i){
                      if(user_hub_id.includes(item._id)){
                        return <Option value={item._id} >{item.title}</Option>
                      }else if(user_role == 'FAE_MAIN_ADMIN' || user_role == 'FAE_ADMIN'){
                        return <Option value={item._id} >{item.title}</Option>
                      }                      
                    })
                  }
                </Select> */}
              </Col>
              <Col span={6} className="mb-3">
                {(user_role != 'FAE_STAFF'  && user_role != 'FAE_HUB_MANAGER')?
                <button type="button" className="btn btn-danger float-end mb-3 " onClick={()=>onAddClient()} >Add New</button>
                :'' }
              </Col>
            </Row>
          </div>
        
        <div>
          <Table  scroll={{x: 1000}} onChange={onChange} columns={columns} dataSource={dataSource} />
        </div>
      </div>      
      <Modal title="Edit Client"  
      visible={isModalVisible}
      destroyOnClose="true"
      centered="true"
      okText="Save"
      okType="danger"
      onOk={()=>{  
        formik.submitForm(); 
      }} 
      onCancel={()=>{
        setIsModalVisible(false)
        form.resetFields();
       // Modal.destroyAll();
      }} >
          <form onSubmit={formik.handleSubmit} className="">
              <div className="form-floating mb-3">
                <input
                  type="text"                  
                  id="business_client"
                  placeholder="Business Client"
                  value={formik.values.business_client}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  className={
                    formik.errors.business_client && formik.touched.business_client
                      ? "text-input error form-control rounded-4 border-dark shadow"
                      : "text-input form-control rounded-4 border-dark shadow"
                  }
                />
                <label htmlFor="floatingInput">Business Client </label>
                {formik.errors.business_client && formik.touched.business_client && (
                  <div className="input-feedback invailid_feedback">{formik.errors.business_client}</div>
                )}
              </div>
              <div className="form-floating mb-3">
                <input
                  type="text"                  
                  id="contact_person"
                  placeholder="Contact Person"
                  value={formik.values.contact_person}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  className={
                    formik.errors.contact_person && formik.touched.contact_person
                      ? "text-input error form-control rounded-4 border-dark shadow"
                      : "text-input form-control rounded-4 border-dark shadow"
                  }
                />
                <label htmlFor="floatingInput">Contact Person </label>
                {formik.errors.contact_person && formik.touched.contact_person && (
                  <div className="input-feedback invailid_feedback">{formik.errors.contact_person}</div>
                )}
              </div>
              <div className="form-floating mb-3">
                <input
                  type="text"                  
                  id="phone_number"
                  placeholder="Phone Number"
                  value={formik.values.phone_number}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  className={
                    formik.errors.phone_number && formik.touched.phone_number
                      ? "text-input error form-control rounded-4 border-dark shadow"
                      : "text-input form-control rounded-4 border-dark shadow"
                  }
                />
                <label htmlFor="floatingInput">Phone Number </label>
                {formik.errors.phone_number && formik.touched.phone_number && (
                  <div className="input-feedback invailid_feedback">{formik.errors.phone_number}</div>
                )}
              </div>
              <div className="form-floating mb-3">
                <input
                  type="email"                  
                  id="email"
                  placeholder="Email"
                  value={formik.values.email}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  className={
                    formik.errors.email && formik.touched.email
                      ? "text-input error form-control rounded-4 border-dark shadow"
                      : "text-input form-control rounded-4 border-dark shadow"
                  }
                />
                <label htmlFor="floatingInput">Email </label>
                {formik.errors.email && formik.touched.email && (
                  <div className="input-feedback invailid_feedback">{formik.errors.email}</div>
                )}
              </div>
              <div className="form-floating mb-3">
                <textarea                  
                  id="address"
                  placeholder="Address"
                  value={formik.values.address}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  className={
                    formik.errors.address && formik.touched.address
                      ? "text-input error form-control rounded-4 border-dark shadow"
                      : "text-input form-control rounded-4 border-dark shadow"
                  }
                >{formik.values.address}</textarea>
                <label htmlFor="floatingInput">Address </label>
                {formik.errors.address && formik.touched.address && (
                  <div className="input-feedback invailid_feedback">{formik.errors.address}</div>
                )}
              </div>
              <div className="form-floating mb-3">
                <input
                  type="text"
                  id="city"
                  placeholder="city"
                  value={formik.values.city}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  className={
                    formik.errors.city && formik.touched.city
                      ? "text-input error form-control rounded-4 border-dark shadow"
                      : "text-input form-control rounded-4 border-dark shadow"
                  }
                />
                {/* <select  name="city_id" className={
                    formik.errors.city_id && formik.touched.city_id
                      ? "text-input error form-control rounded-4 border-dark shadow"
                      : "text-input form-control rounded-4 border-dark shadow"
                  } id="city_id" value={formik.values.city_id} onChange={formik.handleChange} onBlur={formik.handleBlur} >
                  <option value="">Select</option>                
                {
                  cityDataFilter.map(function(item, i){
                    if(user_role == 'FAE_CITY_ADMIN' && user_city_id.includes(item._id)){
                      return <option value={item._id} >{item.name}</option>
                    }else if(user_role != 'FAE_CITY_ADMIN'){
                      return <option value={item._id} >{item.name}</option>
                    }                  
                  })
                  
                }
              </select> */}
                <label htmlFor="floatingCity">City</label>
                {formik.errors.city && formik.touched.city && (
                  <div className="input-feedback invailid_feedback">{formik.errors.city}</div>
                )}
              </div>
              {/* <button
                className="w-100 mb-2 btn btn-lg rounded-4 btn-outline-danger fae-login"
                type="submit" >
                Save
              </button> */}
              
            </form>
      </Modal>
    </>
  );
}

const mapStateToProps = (state, ownProps = {}) => {
  console.log(state) // state
   /*console.log(ownProps) // {}*/
  const { isLoggedIn,user,user_city_id,user_role } = state.auth; 
  const { clientData } = state.business_client; 
  return {
    user,
    isLoggedIn,
    user_city_id,
    user_role,
    clientData
  };
}

export default connect(mapStateToProps)(BusinessClientComp);
