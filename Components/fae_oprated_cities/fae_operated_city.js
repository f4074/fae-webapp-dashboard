import React , { useState, useEffect, useCallback } from "react";
import { connect,useDispatch, useSelector } from "react-redux";
import { useRouter } from 'next/router';
import { HANDLE_SUCCESS, HANDLE_ERROR, HTTP_SERVICE_CALL } from "../../provider/ApiProvider";
import  {SET_DATA_LOCALSTORAGE,REMEMBER_PASSWORD,GET_DATA_LOCALSTORAGE} from "../../provider/LocalStorageProvider"
import { BASE_URL, POST, IS_LOADING,ADMIN_SIGN_IN } from "../../provider/EndPoints";
import { changeDateFormat } from "../../provider/helper";
import Loader from "../../Components/loader";
import Spinner from "../../Components/Spinner/Spinner";
import { create,getAll,deleteData,update} from "../../Redux/Action/FaeOpratedCities";
import { createCity,getAllCities,deleteCityData,updateCity} from "../../Redux/Action/cities";
import { createHub,getAllHubs,allCityHubs,deleteHub,deleteMultipleHub,updateHub} from "../../Redux/Action/hubs";
/* ant design */
import 'antd/dist/antd.css';
import {Table, Tag, Space, Modal, Form, Input, Radio, Upload, message, Button, Descriptions, DatePicker,Select } from 'antd';
import {EditOutlined,DeleteOutlined} from '@ant-design/icons'
// formik import 
import * as Yup from "yup";
import {VALIDATION_SCHEMA,FORMIK} from "../../provider/FormikProvider";
// import { createAction } from "@reduxjs/toolkit";

function onChange(pagination, filters, sorter, extra) {
  console.log('params', pagination, filters, sorter, extra);
}

const FaeOpratedCitiesComp=(props)=> {
  // console.log('props',props) 
  const router = useRouter();  
  const [loader, setLoader] = useState(false);  
  const dispatch = useDispatch();
  const { isLoggedIn} = useSelector(state => state.auth);
  const { supplierData } = useSelector(state => state.business_supplier);

  const [form] = Form.useForm();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [editingData, setEditingData] = useState(null);
  const [dataSource,setDataSource]= useState([]);
  const [modalTitle,setModalTitle]= useState('Add Supplier');
  const [isModalShowCity,setIsModalShowCity]= useState({'display':'none'});
  const [isModalShowHub,setIsModalShowHub]= useState({'display':'none'});
  const [modelShowType,setModalShowType]= useState('');
  const [cityData,setCityData]= useState([]);
  const [cityDefaultValue,setCityDefaultValue]= useState('');
  const { Option } = Select;
  const [isDeleteButtonShow,seIsDeleteButtonShow]=useState(false);
  const [selectedRow,setSelectedRow]=useState([]);
  const [deleteIds,setDeleteIds]=useState([]);

  const [user_role, setuser_role] = useState(props.user_role);  
  const [user_city_id, setuser_city_id] = useState(props.user_hub_id);
  const [user_hub_id, setuser_hub_id] = useState(props.user_city_id);
  var setCityStatus =0;
  useEffect(()=>{
    seIsDeleteButtonShow(false)
    if (!isLoggedIn) {
        router.push('/login');
    }else{
      setuser_role(props.user_role);
      setuser_city_id(props.user_city_id);
      setuser_hub_id(props.user_hub_id);
      if(props.user_city_id){
        // handleChangeSelect(props.user_city_id[0],'city')
      }      
      getAllFaeOpratedCities();
    }
      
  },[])
  useEffect(() => {
    if(cityData.length >0){
      const getCity = cityData.filter((i)=> props.user_city_id.includes(i._id));
      console.log('cityData',cityData)
      console.log('getCity',getCity)
      if(getCity.length >0){
        handleChangeSelect(props.user_city_id[0],'city')     
      }else{
        setCityDefaultValue("");
      } 
    }   
  }, [cityData.length >0 &&  setCityStatus == 0])
  
  useEffect(()=>{

  },[deleteIds])

  const onSelectChange = (selectedRowKeys,selectedRows) => {
    console.log('selectedRowKeys',selectedRowKeys);
    setSelectedRow(selectedRows)
    if(selectedRows.length>0){
      seIsDeleteButtonShow(true);
      var ids = selectedRows.map(function(i) {
        return i._id;
      }); 

      setDeleteIds(ids)      
    }else{
      seIsDeleteButtonShow(false);
      setDeleteIds(selectedRows)
      
    }    
  };

  const rowSelection = {
    type:'checkbox',
    onChange: onSelectChange,   
  };

  /* formik code start city */
  var initialValues_city = {
    _id:null,
    name:null,
    short_name: null,
    latitude: null,
    longitude: null,  
    created:new Date(),
    updated:new Date()
  };
  const validationSchemaCity = Yup.object() 
  .shape({
    name: Yup.string().default(null).nullable()
    .min(2, 'Must be greater then 2 or equal characters')
    .required('City name is required'),
    short_name: Yup.string().default(null).nullable()
    .required('Short name of city is required'),
    latitude: Yup.string().default(null).nullable()
    .required('Latitude is required'), 
    longitude: Yup.string().default(null).nullable()
    .required('Longitude is required')
  });   

  const submitFormCity =(data)=>{
    if(isEditing){
      UpdateDataSubmit(data,"city");
    }else{
      AddDataSubmit(data,"city"); 
    } 
  }
  const formikCity = FORMIK(initialValues_city,validationSchemaCity,submitFormCity);
  /* formik code city */
  
  /* formik code start hub */
  var initialValues_hub = {
    city_id:null,
    title:null,
    description: null,
    isCentralHub: 'no',
    created:new Date(),
    updated:new Date()
  };
  const validationSchemaHub = Yup.object() 
  .shape({
    // city_id: Yup.string().default(null).nullable()
    // .required('City is required'),
    title: Yup.string().default(null).nullable()
    .min(2, 'Must be greater then 2 or equal characters')
    .required('Title is required'),
    description: Yup.string().default(null).nullable()
    .required('Description is required'),
    // isCentralHub: Yup.string().nullable()
    // .required('Please check CentralHub!'),
  });

  const submitFormHub =(data)=>{ 
    console.log('submitFormHub isEditing',isEditing);
    if(isEditing){
      UpdateDataSubmit(data,"hub");
    }else{
      AddDataSubmit(data,"hub"); 
    } 
  }
  const formikHub = FORMIK(initialValues_hub,validationSchemaHub,submitFormHub); 

  useEffect(()=>{
    if(formikHub){
      formikHub.setFieldValue("city_id",cityDefaultValue);
    }    
  },[cityDefaultValue]);
// console.log('formikHub',formikHub)
  const AddDataSubmit = async (data,type) => {
    if(type == 'city'){
      var createAction = createCity(data);
    }else{
      var createAction = createHub(data);
    }  
    try {
      setLoader(true);      
      dispatch(createAction)
      .then((res) => {
        setLoader(false); 
        if(res.status == true){
          setIsModalVisible(false);
          HANDLE_SUCCESS(res.message);  
          //       
          if(type == 'city'){
            // handleChangeSelect(props.user_city_id[0],'city')
            formikCity.resetForm();          
            getAllFaeOpratedCities();
            // getAllCityHubs(cityDefaultValue);
          }else{
            setNewData(res.data);    
            formikHub.resetForm();
            formikHub.setFieldValue("city_id",cityDefaultValue);
          }
        }else{
          HANDLE_ERROR(res.message);
        }          
      })
      .catch((error) => {
        setLoader(false);
        HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });     
    } catch (error) {
      setLoader(false);
      HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }

  };
  const UpdateDataSubmit = async (data,type) => { 
    const updateDataSet = JSON.parse(data);
    if(type == 'city'){
        var EditAction = updateCity(data,updateDataSet._id);
    }else{
        var EditAction = updateHub(data,updateDataSet._id);
    }  
    try {
      setLoader(true);      
      dispatch(EditAction)
      .then((res) => {
        console.log('res',res);
        setLoader(false); 
        if(res.status == true){
          setIsModalVisible(false);
          HANDLE_SUCCESS(res.message);
          // const index = dataSource.findIndex((client)=>client._id==updateDataSet._id);
          // let updateArrData = [...dataSource];          
          // updateArrData[index] = res.data;           
          // setDataSource(updateArrData);          
          // setNewData(res.data);
          getAllFaeOpratedCities();
          getAllCityHubs(cityDefaultValue);
          formikCity.resetForm();
          formikHub.resetForm();
        }else{
          HANDLE_ERROR(res.message);
        }  
      })
      .catch((error) => {
        console.log('error,',error)
        setLoader(false);
        HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });
    } catch (error) {
      setLoader(false);
      HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  };
 
  /* formik code end city */

  const setNewData=(data)=>{    
    setDataSource(pre=>{
      return [...pre,data]
    })
  }
  const columns = [   
    {

      key:"1",
      title: 'City',
      dataIndex: 'city_name',
    },    
    {
      key:"2",
      title: 'Central Hub',
      dataIndex: 'title',     
    },
    {
      key:"3",
      title: 'Latitude',
      dataIndex: 'city_lat',     
    },
    {
      key:"4",
      title: 'Longitude',
      dataIndex: 'city_lng',     
    },
    (user_role != 'FAE_STAFF' && user_role != 'FAE_HUB_MANAGER')?
    {
      key:"5",
      title: 'Actions',
      render:(record)=>{
        return <>
          <EditOutlined  onClick={()=>{
            onEdit(record)
          }} />
          <DeleteOutlined  onClick={()=>{
            onDelete(record)
          }} style={{color:'red',marginLeft:12}} />
        </>
      }
    }
    :{}
  ];

  const onAdd=(type)=>{
    setIsEditing(false);
    setModalShowType(type);
    if(type=='city'){  
        formikCity.resetForm();      
        setModalTitle("Add City");
        setIsModalShowCity({'display':''});
        setIsModalShowHub({'display':'none'});        
    }else if(type=='hub'){
        formikHub.resetForm();
        if(!cityDefaultValue){
          HANDLE_ERROR("please select city!");
          return;
        }
        formikHub.setFieldValue("city_id",cityDefaultValue);

        setModalTitle("Add Hub");
        setIsModalShowCity({'display':'none'});
        setIsModalShowHub({'display':''});
    }else{
        setModalTitle("Add");
        setIsModalShowCity({'display':'none'});
        setIsModalShowHub({'display':'none'});
    }
    setIsModalVisible(true);  
    setEditingData(null); 
    formikCity.setValues(initialValues_city);   
  }
  const onDelete=(record)=>{
    // showModal();
    Modal.confirm({
      title:'Are you sure, you want to delete this record?',
      okText:"Yes",
      okType:"danger",
      onOk:()=>{
        deleteCallApi(record,'single');       
      }
    })
  }
  const onDeleteMultiple=()=>{
    // showModal();
    Modal.confirm({
      title:'Are you sure, you want to delete this record?',
      okText:"Yes",
      okType:"danger",
      onOk:()=>{
        deleteCallApi(deleteIds,'multiple');       
      }
    })
  }
  const deleteCallApi=(deleteIds,deleteType)=>{ 
    
    if(deleteType == 'multiple'){
      var deleteApi = deleteMultipleHub(deleteIds);
    }else{
      const record = deleteIds;
      var deleteApi = deleteHub(record._id);
    }
    try {
      setLoader(true);      
      dispatch(deleteApi)
      .then((res) => {
        setLoader(false); 
        if(res.status == true){          
          HANDLE_SUCCESS(res.message);  
          if(deleteType == 'multiple'){
            // deleteIds
            setDataSource(pre=>{
              return pre.filter((client)=>!(deleteIds.includes(client._id)));
            }) 
          }else{
            setDataSource(pre=>{
              return pre.filter((client)=>client._id!=record._id);
            }) 
          }       
          
        }else{
          HANDLE_ERROR(res.message);
        }  
      })
      .catch((error) => {
        setLoader(false);
        HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });     
    } catch (error) {
      setLoader(false);
      HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  const onDeleteCity=()=>{
    if(!cityDefaultValue){
      HANDLE_ERROR("please select city!");
      return;
    }
    // showModal();
    Modal.confirm({
      title:'Are you sure, you want to delete this city?',
      okText:"Yes",
      okType:"danger",
      onOk:()=>{
        deleteCityCallApi();       
      }
    })
  }
  const deleteCityCallApi=()=>{ 
    
    var deleteApi = deleteCityData(cityDefaultValue);
    try {
      setLoader(true);      
      dispatch(deleteApi)
      .then((res) => {
        setLoader(false); 
        if(res.status == true){          
          HANDLE_SUCCESS(res.message);  
          setCityDefaultValue('');
          // if(cityData.length>0){
          //   setCityDefaultValue(cityData[0]._id);
          // }
          // setDataSource([]);
          getAllFaeOpratedCities();      
          
        }else{
          HANDLE_ERROR(res.message);
        }  
      })
      .catch((error) => {
        setLoader(false);
        HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });     
    } catch (error) {
      setLoader(false);
      HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  const onEditCity=()=>{
    if(!cityDefaultValue){
      HANDLE_ERROR("please select city!");
      return;
    }
    setModalShowType('cityEdit');
    setIsEditing(true);
    setIsModalVisible(true);
    // setEditingData({...record});

    const getCityDetails = cityData.filter(function(val, index){
      return val._id == cityDefaultValue;
    })
    // console.log('getCityDetails',getCityDetails[0]);
    if(getCityDetails.length >0){
      formikCity.setValues(getCityDetails[0]);
    }    
    setModalTitle("Edit City");
    setIsModalShowCity({'display':''});
    setIsModalShowHub({'display':'none'});  
    

    // formikCity.setValues(record);
  }
  const onEdit=(record)=>{
    setModalShowType('edit_hub');
    setIsEditing(true);
    setIsModalVisible(true);
    setEditingData({...record});
    // setModalTitle("Edit City");    
    // formikCity.setValues(record);

    setModalTitle("Edit Hub");
    setIsModalShowCity({'display':'none'});
    setIsModalShowHub({'display':''});
    formikHub.setValues(record);
  }
  const getAllCityHubs=(city_id)=>{
    try {
      setLoader(true);      
      dispatch(allCityHubs(city_id))
      .then((res) => {
        setLoader(false);        
        if(res.status == true){
          // HANDLE_SUCCESS(res.message);   
            if((user_role == "FAE_HUB_ADMIN" || user_role == "FAE_HUB_MANAGER"))  {
              let getData = res.data;
              getData =  getData.filter(function(item){
                return item._id == user_hub_id[0];
              })
              setDataSource(getData); 
            }else{
              setDataSource(res.data); 
            }
                       
        }else{
          // HANDLE_ERROR(res.message);
        }  
      })
      .catch((error) => {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });
    } catch (error) {
      setLoader(false);
      // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  const getAllFaeOpratedCities=(getType=null)=>{
    try {
      setLoader(true);      
      dispatch(getAllHubs())
      .then((res) => {
        setLoader(false);          
        setCityData(res.data.citiesList);    
        setCityStatus =1;  
        if(res.status == true){
          // HANDLE_SUCCESS(res.message);         
            // setDataSource(res.data.faeOpratedCities);            
        }else{
          // HANDLE_ERROR(res.message);
        }  
      })
      .catch((error) => {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });
    } catch (error) {
      setLoader(false);
      // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  function handleChangeSelect(value,selectType) {
    // console.log('setCityDefaultValue',value)
    setCityDefaultValue(value);
    getAllCityHubs(value);    
    // cityData.length>0?cityData[0]._id:""
  }
  return (
    <>
      <Loader loading={loader} />
      <h2 className="mb-4 text-danger fw-bold fs-3">Fae Operated Cities</h2>
      <div className="p-4 shadow rounded-6 bg-white">
        <div className="col-2 ms-2 float-start">
          {(user_role != "FAE_HUB_ADMIN" && user_role != "FAE_HUB_MANAGER")?
          <Select  value={cityDefaultValue}   name="city_filter" className="w-100" id="city_filter" style={{ width: 120 }} onChange={(e)=>handleChangeSelect(e,'city')}>
            {(props.user_role == 'FAE_MAIN_ADMIN' || props.user_role == 'FAE_ADMIN')?
            <Option value="">Select City</Option>:""}
            {
              cityData.map(function(item, i){
                if(user_role == 'FAE_CITY_ADMIN' && user_city_id.includes(item._id)){
                  return <Option value={item._id} >{item.name}</Option>
                }else if(user_role != 'FAE_CITY_ADMIN'){
                  return <Option value={item._id} >{item.name}</Option>
                }                       
              })
            }
          </Select>
          :''}
        </div>       
        {/* <button type="button" className="btn btn-danger float-end mb-3 " onClick={()=>onAdd('hub')} >Add New Hub</button> */}

        {isDeleteButtonShow?
        <Button type="danger" className={"float-end mb-3 ms-3"}  onClick={()=>onDeleteMultiple()} >Delete Rows</Button>
        :''}

        {
         (user_role == "FAE_MAIN_ADMIN" || user_role == 'FAE_ADMIN')?
           <Button type="danger" className="float-end mb-3" onClick={()=>onAdd('city')} >Add New City</Button>:''
        }

        {(user_role != "FAE_STAFF" && user_role != "FAE_HUB_MANAGER" && user_role != "FAE_HUB_ADMIN")?
        <Button type="danger"  className="mb-3  ms-3 " onClick={()=>onAdd('hub')}>Add New Hub </Button>
        :''}
       
        {/* className={isDeleteButtonShow?'float-end mb-3 me-3 ':'float-end mb-3 me-3 d-none'} */}
        
        {(user_role == "FAE_MAIN_ADMIN" || user_role == 'FAE_ADMIN')?
          <>
          <Button type="danger"  className="mb-3  ms-3 " onClick={()=>onEditCity('city')}>Edit City </Button>
          <Button type="danger"  className="mb-3  ms-3 " onClick={()=>onDeleteCity('city')}>Delete City</Button>
          </>
        :''}
          
        <div>
          <Table  scroll={{x: 1000}} rowSelection={(user_role != "FAE_STAFF" && user_role != "FAE_HUB_MANAGER" )?rowSelection:''}  onChange={onChange} columns={columns} dataSource={dataSource} />
        </div>
      </div>      
      <Modal title={modalTitle}  
      visible={isModalVisible}
      destroyOnClose="true"
      centered="true"
      okText="Save"
      okType="danger"
      onOk={()=>{ 
        (modelShowType == 'city' || modelShowType=='cityEdit')?formikCity.submitForm():formikHub.submitForm();         
      }} 
      onCancel={()=>{
        setIsModalVisible(false)
        form.resetFields();
       // Modal.destroyAll();
      }} >
        <form onSubmit={formikCity.handleSubmit} className="" style={isModalShowCity}>
            <div className="form-floating mb-3">
              <input
                type="text"                  
                id="name"
                placeholder="City Name"
                value={formikCity.values.name}
                onChange={formikCity.handleChange}
                onBlur={formikCity.handleBlur}
                className={
                  formikCity.errors.name && formikCity.touched.name
                    ? "text-input error form-control rounded-4 border-dark shadow"
                    : "text-input form-control rounded-4 border-dark shadow"
                }
              />
              <label htmlFor="floatingInput">City Name </label>
              {formikCity.errors.name && formikCity.touched.name && (
                <div className="input-feedback invailid_feedback">{formikCity.errors.name}</div>
              )}
            </div>
            <div className="form-floating mb-3">
              <input
                type="text"                  
                id="short_name"
                placeholder="Short Name"
                value={formikCity.values.short_name}
                onChange={formikCity.handleChange}
                onBlur={formikCity.handleBlur}
                className={
                  formikCity.errors.short_name && formikCity.touched.short_name
                    ? "text-input error form-control rounded-4 border-dark shadow"
                    : "text-input form-control rounded-4 border-dark shadow"
                }
              />
              <label htmlFor="floatingInput">Short Name </label>
              {formikCity.errors.short_name && formikCity.touched.short_name && (
                <div className="input-feedback invailid_feedback">{formikCity.errors.short_name}</div>
              )}
            </div>
            <div className="form-floating mb-3">
              <input
                type="text"                  
                id="latitude"
                placeholder="Latitude"
                value={formikCity.values.latitude}
                onChange={formikCity.handleChange}
                onBlur={formikCity.handleBlur}
                className={
                  formikCity.errors.latitude && formikCity.touched.latitude
                    ? "text-input error form-control rounded-4 border-dark shadow"
                    : "text-input form-control rounded-4 border-dark shadow"
                }
              />
              <label htmlFor="floatingInput">Latitude </label>
              {formikCity.errors.latitude && formikCity.touched.latitude && (
                <div className="input-feedback invailid_feedback">{formikCity.errors.latitude}</div>
              )}
            </div>
            <div className="form-floating mb-3">
              <input
                type="text"                  
                id="longitude"
                placeholder="Longitude"
                value={formikCity.values.longitude}
                onChange={formikCity.handleChange}
                onBlur={formikCity.handleBlur}
                className={
                  formikCity.errors.longitude && formikCity.touched.longitude
                    ? "text-input error form-control rounded-4 border-dark shadow"
                    : "text-input form-control rounded-4 border-dark shadow"
                }
              />
              <label htmlFor="floatingInput">Longitude </label>
              {formikCity.errors.longitude && formikCity.touched.longitude && (
                <div className="input-feedback invailid_feedback">{formikCity.errors.longitude}</div>
              )}
            </div>
            
            {/* <button
              className="w-100 mb-2 btn btn-lg rounded-4 btn-outline-danger fae-login"
              type="submit" >
              Save
            </button> */}
            
        </form>
        <form onSubmit={formikHub.handleSubmit} className="" style={isModalShowHub}>
            <div className="form-floating mb-3">
              <input
                type="text"                  
                id="title"
                name="title"
                placeholder="Hub Title"
                value={formikHub.values.title}
                onChange={formikHub.handleChange}
                onBlur={formikHub.handleBlur}
                className={
                  formikHub.errors.title && formikHub.touched.title
                    ? "text-input error form-control rounded-4 border-dark shadow"
                    : "text-input form-control rounded-4 border-dark shadow"
                }
              />
              <label htmlFor="floatingInput">Hub Title </label>
              {formikHub.errors.title && formikHub.touched.title && (
                <div className="input-feedback invailid_feedback">{formikHub.errors.title}</div>
              )}
            </div>
            <div className="form-floating mb-3">
              <textarea  
                name="description"
                className={
                  formikHub.errors.description && formikHub.touched.description
                    ? "error form-control"
                    : "form-control"
                } value={formikHub.values.description}
                onChange={formikHub.handleChange}
                onBlur={formikHub.handleBlur} placeholder="Hub Description" id="description" ></textarea>
              <label for="Hub Description">Hub Description</label>
              {formikHub.errors.description && formikHub.touched.description && (
                <div className="input-feedback invailid_feedback">{formikHub.errors.description}</div>
              )}
            </div>
            {/* <div className="form-floating mb-3">
              <div htmlFor="floatingInput" className=" mb-2">Is it a Central Hub? </div>
              <div class="form-check form-check-inline">
                <input className={
                  formikHub.errors.isCentralHub && formikHub.touched.isCentralHub
                    ? "error form-check-input"
                    : "form-check-input"
                } value="yes"
                checked={formikHub.values.isCentralHub == 'yes'}
                onChange={formikHub.handleChange}
                onBlur={formikHub.handleBlur} type="radio" name="isCentralHub" id="isCentralHubYes"  />
                <label class="form-check-label" for="inlineRadio1">Yes</label>
              </div>
              <div class="form-check form-check-inline" >
                <input className={
                  formikHub.errors.isCentralHub && formikHub.touched.isCentralHub
                    ? "error form-check-input"
                    : "form-check-input"
                } type="radio" name="isCentralHub" id="isCentralHubNo" value="no"
                checked={formikHub.values.isCentralHub == 'no'}
                onChange={formikHub.handleChange}
                onBlur={formikHub.handleBlur} />
                <label class="form-check-label" for="inlineRadio2">NO</label>
              </div>              
              {formikHub.errors.isCentralHub && formikHub.touched.isCentralHub && (
                <div className="input-feedback invailid_feedback">{formikHub.errors.isCentralHub}</div>
              )}
            </div> */}
            
            {/* <button
              className="w-100 mb-2 btn btn-lg rounded-4 btn-outline-danger fae-login"
              type="submit" >
              Save
            </button> */}
            
        </form>
      </Modal>
    </>
  );
}
const mapStateToProps = (state, ownProps = {}) => {
  console.log(state) // state
   /*console.log(ownProps) // {}*/
  const {  isLoggedIn,user,user_city_id,user_role,user_hub_id } = state.auth; 
  const { supplierData } = state.business_supplier; 
  return {
    user,
    isLoggedIn,
    user_city_id,
    user_hub_id,
    user_role,
    supplierData
  };
}

export default connect(mapStateToProps)(FaeOpratedCitiesComp);

