import React, { useState, useEffect, useCallback } from "react";
import { connect, useDispatch, useSelector } from "react-redux";
import { useRouter } from 'next/router';
import { HANDLE_SUCCESS, HANDLE_ERROR } from "../../provider/ApiProvider";
import { IMAGE_URL,NO_IMAGE_ERROR,NO_IMAGE_ERROR_BASE64 } from "../../provider/EndPoints";
import { changeDateFormat } from "../../provider/helper";
import Loader from "../../Components/loader";
import { createUser,getAllUsers,deleteUser,deleteMultipleUsers,updateUser,getKycDetails,getAllKycList} from "../../Redux/Action/users";
import { createHub,getAllHubs,allCityHubs,deleteHub,updateHub} from "../../Redux/Action/hubs";
import { getAllCities} from "../../Redux/Action/cities"

/* ant design */
import 'antd/dist/antd.css';
import { Table,Image, Tag, Space, Modal, Form, Input, Radio, Upload, message, Button, Descriptions, DatePicker,Select } from 'antd';
import { EditOutlined, DeleteOutlined, InboxOutlined, EyeOutlined, SearchOutlined } from '@ant-design/icons'
// formik import 
import * as Yup from "yup";
import { VALIDATION_SCHEMA, FORMIK } from "../../provider/FormikProvider";
import FormList from "antd/lib/form/FormList";

const KycRequestComp = (props) => {
  const router = useRouter();
  const [loader, setLoader] = useState(false);
  const dispatch = useDispatch();
  const { isLoggedIn } = useSelector(state => state.auth);
  const [form] = Form.useForm();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isModalVisibleDetails, setIsModalVisibleDetails] = useState(false);
  const [dataSource, setDataSource] = useState([]);
  const [userKycDeatils, setUserKycDeatils] = useState([]);
  const [cityDataFilter, setCityDataFilter] = useState([]);
  const [hubDataFilter, setHubDataFilter] = useState([]);
  const [filterArray, setFilter] = useState({'city_id':props.user_city_id?props.user_city_id[0]:'','hub_id':(props.user_role == 'FAE_HUB_ADMIN' || props.user_role == 'FAE_HUB_MANAGER') && props.user_hub_id?props.user_hub_id[0]:'','kyc_status_filter':'0',"searchKey":""});
  const [searchKey, setSearchKey] = useState(null);
  const [isFilter, setIsFilter] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [editId, setEditId] = useState('');
  const { Search } = Input;
  const { Option } = Select;
  const [user_role, setuser_role] = useState(props.user_role);  
  const [user_city_id, setuser_city_id] = useState(props.user_hub_id);
  const [user_hub_id, setuser_hub_id] = useState(props.user_city_id);
  const setCityStatus = 0;

  useEffect(() => {
    if (!isLoggedIn) {
      router.push('/login');
    } else {
      // setuser_role(props.user_role);
      // setuser_hub_id(props.user_hub_id)
      // setuser_city_id(props.user_city_id);
      
      // if(props.user_role == 'FAE_MAIN_ADMIN' || props.user_role == 'FAE_ADMIN'){
      //   const getCity = cityDataFilter.filter((i)=> [props.user_city_id].includes(i));
      //   if(getCity.length >0){
      //     setuser_city_id(getCity);         
      //     setFilter({...filterArray, city_id: getCity[0] ,hub_id: ""})
      //   } 
      // }else{
         
      //   if(props.user_role == 'FAE_HUB_ADMIN' || props.user_role == 'FAE_HUB_MANAGER'){          
      //     setFilter({...filterArray, hub_id: props.user_hub_id[0]})
      //   }else{
      //     setFilter({...filterArray, city_id: props.user_city_id?props.user_city_id[0]:'',hub_id: ""})
         
      //   }
      // }
      // if(props.user_city_id){        
      //   getAllCityHubsAPI(props.user_city_id[0]);
      // } 
      // getAllRequestUsersData();

      setuser_role(props.user_role);
      setuser_hub_id(props.user_hub_id)
      setuser_city_id(props.user_city_id); 
      
      getAllCityAPI();
      if(props.user_city_id){
        getAllCityHubsAPI(props.user_city_id[0]);
      } 
      
      // if(props.user_role == 'FAE_MAIN_ADMIN' || props.user_role == 'FAE_ADMIN'){
      //   const getCity = cityDataFilter.filter((i)=> [props.user_city_id].includes(i));
      //   if(getCity.length >0){        
      //     // setFilter({...filterArray, city_id: getCity[0] ,hub_id: ""})
      //   }else{
      //     setFilter({...filterArray, city_id: "" ,hub_id: ""})
      //   }
      // } 
    }
  }, [])

  useEffect(() => {
    getAllRequestUsersData();
  }, [filterArray])

  useEffect(() => {
    if(props.user_role == 'FAE_MAIN_ADMIN' || props.user_role == 'FAE_ADMIN'){
      if(cityDataFilter.length >0){
        const getCity = cityDataFilter.filter((i)=> props.user_city_id.includes(i._id));
        
        if(getCity.length >0){        
          setFilter({...filterArray, city_id: props.user_city_id[0]})
        }else{
          setFilter({...filterArray, city_id: "" ,hub_id: ""})
        }
      }  
    } 
  }, [cityDataFilter])
  

  const getAllCityAPI=()=>{
    try {
      setLoader(true);      
      dispatch(getAllCities())
      .then((res) => {
        console.log('res.data)',res.data)
        setLoader(false);
        setCityDataFilter(res.data);
      })
      .catch((error) => {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });
    } catch (error) {
      setLoader(false);
      // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  /* formik code start */   
  var initialValues = {
    kyc_status:'1',
    profile_verified:true,
    reject_reason:'',
    updated:new Date()
  };
  const validationSchema = Yup.object() 
  .shape({
    kyc_status: Yup.string().default('1')
    .required('status is required'),
  }); 
  const submitForm =(data)=>{  
      callAPI(data);   
  }
  const formik = FORMIK(initialValues,validationSchema,submitForm);
  
  formik.handleChange = (e) => {
    formik.setFieldValue(e.target.name,e.target.value);
    if(e.target.name == 'kyc_status'){ 
      if(e.target.value == '1'){
        formik.setFieldValue("profile_verified",true); 
      }else{
        formik.setFieldValue("profile_verified",false); 
      }
    }
};

  const callAPI= (data)=> {
    try {
      setLoader(true);      
      dispatch(updateUser(data,editId))
      .then((res) => {
        setLoader(false);      
        if(res.status === true){
          HANDLE_SUCCESS(res.message);          
          // setDataSource(res.data.allKycRequestData);
          getAllRequestUsersData();                   
          setIsModalVisibleDetails(false)           
          formik.resetFields();  
          formik.setValues(initialValues); 
        }else{
          HANDLE_ERROR(res.message);
          // setIsModalVisibleDetails(true)
        }
      })
      .catch((error) => {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });
    } catch (error) {
      setLoader(false);
      // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }

  const onDelete = (record) => {
    // showModal();
    Modal.confirm({
      title: 'Are you sure, you want to delete this record?',
      okText: "Yes",
      okType: "danger",
      onOk: () => {
        // deleteCallApi(record);
      }
    })
  }
  const deleteCallApi = (record) => {
    /* try {
      setLoader(true);      
      dispatch(deleteHub(record._id))
      .then((res) => {
        setLoader(false); 
        if(res.status == true){          
          HANDLE_SUCCESS(res.message);         
          setDataSource(pre=>{
            return pre.filter((client)=>client._id!=record._id);
          })  
        }else{
          HANDLE_ERROR(res.message);
        }  
      })
      .catch((error) => {
        setLoader(false);
        HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });     
    } catch (error) {
      setLoader(false);
      HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    } */
  }
  const getAllRequestUsersData=()=>{
    try {
        setLoader(true);      
        dispatch(getAllKycList(filterArray))
        .then((res) => {
          setLoader(false);
          setDataSource(res.data);
          // setCityDataFilter(res.citylist)
        })
        .catch((error) => {
          setLoader(false);
          // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
        });
      } catch (error) {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      }
  }
  const getUsersKycDetails=(user_id)=>{
    try {
        setLoader(true);      
        dispatch(getKycDetails(user_id))
        .then((res) => {
          setLoader(false);
          setUserKycDeatils(res.data);
          if(res.data && res.data.user_id){
            formik.setValues(initialValues);
            formik.setFieldValue("kyc_status",'1'); 
          }
          
        })
        .catch((error) => {
          setLoader(false);
          // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
        });
      } catch (error) {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      }
  }
  const getAllCityHubsAPI=(city_id)=>{
    try {
      setLoader(true);      
      dispatch(allCityHubs(city_id))
      .then((res) => {
        setLoader(false);
        setHubDataFilter(res.data);
      })
      .catch((error) => {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });
    } catch (error) {
      setLoader(false);
      // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  const columns = [
    {
      key: "1",
      title: 'Customer Name',
      dataIndex: 'name',
    },
    /* {
      key: "2",
      title: 'Contact Person',
      dataIndex: 'contact_persion',
    }, */
    {
        key: "3",
        title: 'Phone Number',
        dataIndex: 'phone',
    },
    {
      key: "4",
      title: 'Email',
      dataIndex: 'email',
      render: (email) => {
        return email;
      },
    },
    {
      key: "4",
      title: 'Status',
      render: (record) => {
        return  <>
        <Tag color={record.kyc_status==2?"red":record.kyc_status==1?"green":"blue"}>{record.kyc_status==2?"Rejected":record.kyc_status==1?"Approved":"Requested"}</Tag>
        
        </>
      }
    },
    {
      key: "5",
      title: 'View KYC',
      dataIndex: '_id',
      render: (user_id) => {
        return <>
          
          <button className="btn btn-outline-secondary btn-sm" size="small" onClick={() => {
            setIsModalVisibleDetails(true)
            setEditId(user_id);
            getUsersKycDetails(user_id)
          }} >
            <i className="fa fa-eye me-2"></i>
            View 
          </button>

        </>
      }
    },
  ];
  function onChange(pagination, filters, sorter, extra) {
    console.log('params', pagination, filters, sorter, extra);
    const filterData ={
      sortField: sorter.field,
      sortOrder: sorter.order,
      pagination,
      ...filters,
    }
  }
   
  function handleChangeSelect(value,selectType) {
    console.log('value',value)
    if(selectType == 'city'){
      setHubDataFilter([]);
      setFilter({...filterArray,  city_id: value,hub_id: ''})
      getAllCityHubsAPI(value);
      
    }else if(selectType == 'hub'){
      setFilter({...filterArray,  hub_id: value})

    } else{
      setFilter({...filterArray,  kyc_status_filter: value})
    } 
  }

  const searchOnEnter=(e)=>{
    const value = e.target.value;
    if(value.length >0){
      setSearchKey(value);
      setFilter({...filterArray,"searchKey":value});
    }else{
      setSearchKey('null');
      setFilter({...filterArray,"searchKey":""});
    }
  }
  const onSearch = value => {
    if(value.length >0){
      setSearchKey(value);
      setFilter({...filterArray,"searchKey":value});
    }else{
      setSearchKey('null');
      setFilter({...filterArray,"searchKey":""});
    }
    
  };

  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    },
    getCheckboxProps: (record) => ({      
      // Column configuration not to be checked
      name: record.name,
    }),
  };
  return (
    <>
      <Loader loading={loader} />
      <h2 className="mb-4 text-danger fw-bold fs-3">KYC Requests</h2>
      <div className="col-12 d-flex mb-3">
        <div className="col-3">
          <Search placeholder="Search by Phone Number" allowClear  onSearch={onSearch} onPressEnter={(e)=>searchOnEnter(e)}   />
        </div>
        <div className="col-6 d-flex">
          <div className="col-4 ms-2">
             <Select  name="kyc_status_filter" className="w-100" id="kyc_status_filter" onChange={(e)=>handleChangeSelect(e,'kyc_status_filter')} defaultValue={filterArray.kyc_status_filter}>
              <Option value="" >All</Option>
              <Option value="0">Requested</Option>
              <Option value="1">Approved</Option>
              <Option value="2">Rejected</Option>              
            </Select>
          </div>
          {user_role != 'FAE_HUB_ADMIN' && user_role != 'FAE_HUB_MANAGER'?
          <div className="col-4 ms-2">
            <Select value={filterArray.city_id} name="city_filter" className="w-100" id="city_filter" onChange={(e)=>handleChangeSelect(e,'city')}>
            {(user_role == 'FAE_MAIN_ADMIN' || user_role == 'FAE_ADMIN'?
                <Option value="">Select City</Option>:'')}
              {
                cityDataFilter.map(function(item, i){
                  if(user_role == 'FAE_CITY_ADMIN' && user_city_id.includes(item._id)){
                    return <Option key={item._id} value={item._id} >{item.name}</Option>
                  }else if(user_role != 'FAE_CITY_ADMIN'){
                    return <Option key={item._id} value={item._id} >{item.name}</Option>
                  } 
                })
              }
            </Select>
          </div>
           :''}
          <div className="col-4 ms-2">
            <Select value={filterArray.hub_id} style={{ width: 120 }} className="w-100" name="hub_filter" id="hub_filter"  onChange={(e)=>handleChangeSelect(e,'hub')}>
            {
                (user_role == 'FAE_MAIN_ADMIN' || user_role == 'FAE_ADMIN' || user_role == 'FAE_CITY_ADMIN')?( <Option value="">Select Hub</Option>):""}
              {
                hubDataFilter.map(function(item, i){
                  if(user_role == 'FAE_MAIN_ADMIN' || user_role == 'FAE_ADMIN'){
                    return <Option value={item._id} >{item.title}</Option>
                  }else{  
                    if((user_role == 'FAE_HUB_ADMIN' || user_role == 'FAE_HUB_MANAGER')){
                      if(user_hub_id && user_hub_id.includes(item._id) ){                  
                        return <Option value={item._id} >{item.title}</Option>  
                      }
                    }else{
                      return <Option value={item._id} >{item.title}</Option>
                    } 
                  }  
                })
              }
            </Select>
          </div>
        </div>
        <div className="col-3">
         
        </div>
      </div>    
      
      <Table  scroll={{x: 1000}} loading={isLoading} onChange={onChange} columns={columns} dataSource={dataSource} />      
      {/* view modal details start */}
      {/* rowSelection={{
          type: 'checkbox',
          ...rowSelection,
        }}  */}
      <Modal
        title="User Details"
        destroyOnClose="true"
        centered
        visible={isModalVisibleDetails}
        onOk={()=>{
          formik.submitForm()                  
        }} 
        onCancel={()=>{
          setIsModalVisibleDetails(false)
          form.resetFields();
        // Modal.destroyAll();
        }}
        okType="danger"
        okText="Save"
        cancelText="Close"
        width={1000}
        // footer={<button type="button" onClick={() => setIsModalVisibleDetails(false)} class="ant-btn ant-btn-default"><span>Close</span></button>}
      >
          <form onSubmit={formik.handleSubmit} className="" >
           <div className="bg-white">
            <div className="row">
              <div className="col-md-12">
                <div className="p-3 border rounded-6 d-flex flex-row shadow-sm">
                  <p className="mb-0 border-end me-4 pe-4">
                    <span className="fw-bold me-2">Customer Name:</span>{userKycDeatils && userKycDeatils.user_id?userKycDeatils.user_id.name:''}
                  </p>
                  <p className="mb-0">
                    <span className="fw-bold me-2">Email:</span>{userKycDeatils&&userKycDeatils.user_id?userKycDeatils.user_id.email:''}
                  </p>
                </div>
              </div>
            </div>
            <Image.PreviewGroup>
            <div className="row">
              <div className="col-md-3 col-sm-6 text-center py-3 ">
                 <h6>Adhar Front</h6>
                 <div className="shadow-sm p-1 border rounded-6" style={{'height':'120px'}}>                  
                  <div className="id-img h-100 " >                   
                    <Image
                      className=' rounded-6 w-100 h-100 '             
                      src={userKycDeatils?IMAGE_URL+userKycDeatils.aadhar_front:NO_IMAGE_ERROR_BASE64}
                      fallback={NO_IMAGE_ERROR_BASE64}
                    />
                  </div>                 
                </div>
                {/* <div className="mt-3">
                  <button className="btn btn-danger btn-sm me-2">
                    <i className=" text-white fa fa-download" />
                  </button>
                  <button className="btn btn-danger btn-sm">
                    <i className=" text-white fa fa-eye me-2" /> View Full
                    Size
                  </button>
                </div> */}
              </div>
              <div className="col-md-3 col-sm-6 text-center py-3">
                <h6>Adhar Back</h6>
                <div className="shadow-sm p-1 border rounded-6" style={{'height':'120px'}}>                  
                  <div className="id-img h-100 " >
                    <Image
                      className=' rounded-6 w-100 h-100 '                                         
                      src={userKycDeatils?IMAGE_URL+userKycDeatils.aadhar_back:NO_IMAGE_ERROR_BASE64}
                      fallback={NO_IMAGE_ERROR_BASE64}
                    />
                  </div>                  
                </div>
                {/* <div className="mt-3">
                  <button className="btn btn-danger btn-sm me-2">
                    <i className=" text-white fa fa-download" />
                  </button>
                  <button className="btn btn-danger btn-sm">
                    <i className=" text-white fa fa-eye me-2" /> View Full
                    Size
                  </button>
                </div> */}
              </div>
              <div className="col-md-3 col-sm-6 text-center py-3">
                <h6>PAN Card</h6>
                <div className="shadow-sm p-1 border rounded-6" style={{'height':'120px'}}>                  
                  <div className="id-img h-100 " > 
                    <Image
                      className=' rounded-6 w-100 h-100 '                     
                      src={userKycDeatils?IMAGE_URL+userKycDeatils.pan_card:NO_IMAGE_ERROR_BASE64}
                      fallback={NO_IMAGE_ERROR_BASE64}
                    />
                  </div>
                </div>
                {/* <div className="mt-3">
                  <button className="btn btn-danger btn-sm me-2">
                    <i className=" text-white fa fa-download" />
                  </button>
                  <button className="btn btn-danger btn-sm">
                    <i className=" text-white fa fa-eye me-2" /> View Full
                    Size
                  </button>
                </div> */}
              </div>
              <div className="col-md-3 col-sm-6 text-center py-3">
                <h6>Driving Liscence</h6>
                <div className="shadow-sm p-1 border rounded-6" style={{'height':'120px'}}>                  
                  <div className="id-img h-100 " >
                    <Image
                      className=' rounded-6 w-100 h-100 '                     
                      src={userKycDeatils?IMAGE_URL+userKycDeatils.driving_licence:NO_IMAGE_ERROR_BASE64}
                      fallback={NO_IMAGE_ERROR_BASE64}
                    />                    
                  </div>                  
                </div>
               {/*  <div className="mt-3">
                  <button className="btn btn-danger btn-sm me-2">
                    <i className=" text-white fa fa-download" />
                  </button>
                  <button className="btn btn-danger btn-sm">
                    <i className=" text-white fa fa-eye me-2" /> View Full
                    Size
                  </button>
                </div> */}
              </div>
            </div>
            </Image.PreviewGroup>
            <div className="row">
              <div className="col-md-12">
                <div className="card rounded-6 shadow-sm mt-4">
                  <div className="card-header d-flex flex-row bg-white border-bottom bottom-0">
                    <span className="me-4 me-auto">To: {userKycDeatils&&userKycDeatils.user_id?userKycDeatils.user_id.email:''}</span>
                    <div className="d-flex flex-row">
                      {/* <select
                        className="form-select form-select-sm me-3 rounded-6"
                        aria-label="Default select example"
                      >
                        <option selected>Select Reason</option>
                        <option value={1}>One</option>
                        <option value={2}>Two</option>
                        <option value={3}>Three</option>
                      </select> */}
                      <div className="form-check me-3 mt-1">
                        <input
                          className="form-check-input"
                          type="radio"
                          name="kyc_status"
                          id="kycApproveStatusApprove"          
                          checked={formik.values.kyc_status=="1"?true:false}
                          value={'1'}
                          onChange={formik.handleChange}
                          onBlur={formik.handleBlur}
                        />
                        <label
                          className="form-check-label"
                          htmlFor="flexRadioDefault1"
                        >
                          Approve
                        </label>
                      </div>
                      <div className="form-check mt-1">
                        <input
                          className="form-check-input"
                          type="radio"
                          name="kyc_status"
                          checked={formik.values.kyc_status=="2"?true:false}
                          id="kycApproveStatusReaject"
                          value='2'
                          onChange={formik.handleChange}
                          onBlur={formik.handleBlur}
                        />
                        <label
                          className="form-check-label"
                          htmlFor="flexRadioDefault2"
                        >
                          Reject
                        </label>
                      </div>
                    </div>
                  </div>
                  <div className="card-body">
                    <div className="mb-3">
                      <textarea
                        className="form-control rounded-6"
                        id="reject_reason"
                        name="reject_reason"
                        rows={5}
                        placeholder="Type a message"
                        value={formik.values.reject_reason}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                      />                     
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </form>
      </Modal>
      {/* view modal details end */}
      
    </>
  );
}
const mapStateToProps = (state, ownProps = {}) => {
  console.log(state) // state
  /*console.log(ownProps) // {}*/
  const {  isLoggedIn,user,user_city_id,user_role,user_hub_id } = state.auth;
  const { inventoryData } = state.inventory;
  return {
    user,isLoggedIn,
    user_city_id,
    user_hub_id,
    user_role,
    inventoryData
  };
}

export default connect(mapStateToProps)(KycRequestComp);

