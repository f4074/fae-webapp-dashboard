import React, { useState, useEffect, useCallback } from "react";
import { connect, useDispatch, useSelector } from "react-redux";
import { useRouter } from 'next/router';
import { HANDLE_SUCCESS, HANDLE_ERROR } from "../../provider/ApiProvider";
import { changeDateFormat } from "../../provider/helper";
import Loader from "../../Components/loader";
import { createInventory, getAllInventory,getAllInventoryFilter, getDetailsInventory, deleteInventory, updateInventory } from "../../Redux/Action/inventory";
import { createHub,getAllHubs,allCityHubs,deleteHub,updateHub} from "../../Redux/Action/hubs";
/* ant design */
import 'antd/dist/antd.css';
import { Table, Tag, Space, Modal, Form, Input, Radio, Upload, message, Button, Descriptions, DatePicker,Select } from 'antd';
import { EditOutlined, DeleteOutlined, InboxOutlined, EyeOutlined, SearchOutlined } from '@ant-design/icons'
// formik import 
import * as Yup from "yup";
import { VALIDATION_SCHEMA, FORMIK } from "../../provider/FormikProvider";

const Vehicle_detailsComp = (props) => {
  console.log('props', props)
  const router = useRouter();
  const [loader, setLoader] = useState(false);
  const dispatch = useDispatch();
  const { isLoggedIn } = useSelector(state => state.auth);
  const { supplierData } = useSelector(state => state.business_supplier);

  const [form] = Form.useForm();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isModalVisibleDetails, setIsModalVisibleDetails] = useState(false);
  const [isModalVisibleTimeline, setIsModalVisibleTimeline] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [editingData, setEditingData] = useState(null);
  const [dataSource, setDataSource] = useState([]);
  const [allVehiclesDataFilter, setAllVehiclesDataFilter] = useState([]);
  const [businessSupplierDataFilter, setBusinessSupplierDataFilter] = useState([]);
  const [cityDataFilter, setCityDataFilter] = useState([]);
  const [hubDataFilter, setHubDataFilter] = useState([]);
  const [dataVehicleTimeline, setDataVehicleTimeline] = useState([]);
  const [inventoryDetails, setInventoryDetails] = useState('');
  const [filterInventory, setFilterInventory] = useState({'vehicle_id':'','business_supplier_id':'','city_id':'','hub_id':''});
  const [isFilter, setIsFilter] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const { Search } = Input;
  const { Option } = Select;

  useEffect(() => {
    if (!isLoggedIn) {
      router.push('/login');
    } else {
      // getAllInventoryData();
    }
  }, [])

  useEffect(() => {
    if(filterInventory.vehicle_id.length > 0 || filterInventory.business_supplier_id.length > 0 || filterInventory.city_id.length > 0  ||filterInventory.hub_id.length > 0 ){          
      setIsFilter(true);
    }else{      
      setIsFilter(false);
    }      
    getAllInventoryData(); 
  }, [isFilter,filterInventory])

  /* useEffect(() => {  
    
  }, [isFilter]) */

  useEffect(() => {  
    if(props && props.inventoryData)    
    setDataSource(props.inventoryData.inventoryData); 
  }, [dataSource])

  const onDelete = (record) => {
    console.log('record', record);
    // showModal();
    Modal.confirm({
      title: 'Are you sure, you want to delete this record?',
      okText: "Yes",
      okType: "danger",
      onOk: () => {
        deleteCallApi(record);
      }
    })
  }
  const deleteCallApi = (record) => {
    /* try {
      setLoader(true);      
      dispatch(deleteHub(record._id))
      .then((res) => {
        setLoader(false); 
        if(res.status == true){          
          HANDLE_SUCCESS(res.message);         
          setDataSource(pre=>{
            return pre.filter((client)=>client._id!=record._id);
          })  
        }else{
          HANDLE_ERROR(res.message);
        }  
      })
      .catch((error) => {
        setLoader(false);
        HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });     
    } catch (error) {
      setLoader(false);
      HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    } */
  }

  const getAllInventoryData = () => {    
    if(isFilter){
      var apiAction =  getAllInventoryFilter(filterInventory)
    }else{
      var apiAction =  getAllInventory()
    }
    // 
    try {
      setLoader(true);
      dispatch(apiAction)
        .then((res) => {
          console.log('res.data)', res.data)
          setLoader(false);
          setCityDataFilter(res.data.citiesList);
          setBusinessSupplierDataFilter(res.data.businessSupplierList);
          setAllVehiclesDataFilter(res.data.allVehicles);
          // setHubData(res.data.allHubs);
          if (res.status == true) {
            // HANDLE_SUCCESS(res.message);         
            setDataSource(res.data.inventoryData);
          } else {
            // HANDLE_ERROR(res.message);
          }
        })
        .catch((error) => {
          setLoader(false);
          // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
        });
    } catch (error) {
      setLoader(false);
      // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }

  const getAllCityHubsAPI=(city_id)=>{
    console.log('city_id',city_id)
    try {
      setLoader(true);      
      dispatch(allCityHubs(city_id))
      .then((res) => {
        console.log('res.data)',res.data)
        setLoader(false);
        setHubDataFilter(res.data);
      })
      .catch((error) => {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });
    } catch (error) {
      setLoader(false);
      // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  const getVehicleDetails = (data) => {
    try {
      setLoader(true);
      dispatch(getDetailsInventory(data._id))
        .then((res) => {
          console.log('res.data)', res.data)
          setLoader(false);
          if (res.status == true) {
            // HANDLE_SUCCESS(res.message);         
            setInventoryDetails(res.data);
          } else {
            // HANDLE_ERROR(res.message);
          }
        })
        .catch((error) => {
          setLoader(false);
          // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
        });
    } catch (error) {
      setLoader(false);
      // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  const columnsVehicle = [
    {

      key: "1",
      title: 'Date',
      dataIndex: 'date',
    },
    {
      key: "2",
      title: 'Title',
      dataIndex: 'title',
    },
    {
      key: "3",
      title: 'Description',
      dataIndex: 'description',
      render: (data) => {
        //return changeDateFormat(createdDate) ;    
      },
    },
    {
      key: "4",
      title: 'Status',
      dataIndex: 'status',
      render: (data) => {
        // return changeDateFormat(createdDate) ;    
      },
    },
  ]
  const columns = [
    {

      key: "1",
      title: 'Vehicle Name',
      dataIndex: 'vehicle_name',
    },
    {
      key: "2",
      title: 'Registration Number',
      dataIndex: 'vehicle_regNo',
    },
    {
      key: "3",
      title: 'Created On',
      dataIndex: 'created',
      render: (created) => {
        return changeDateFormat(created);
      },
    },
    {
      key: "4",
      title: 'Inventory details',
      render: (record) => {
        return <>
          <button type="button" className="btn btn-outline-secondary btn-sm" onClick={() => {
            getVehicleDetails(record)
            setIsModalVisibleDetails(true)

          }} >
            <i className="fa fa-eye me-2"></i>
            View Details
          </button>

        </>
      }
    },
    {
      key: "5",
      title: 'Vehicle Timeline',
      render: (record) => {
        return <>
          <button className="btn btn-outline-secondary btn-sm" size="small" onClick={() => {
            getVehicleDetails(record);
            setIsModalVisibleTimeline(true)

          }} >
            <i className="fa fa-eye me-2"></i>
            View Details
          </button>

        </>
      }
    },
    {
      key: "6",
      title: 'Status',
      dataIndex: 'status',
      render: (status) => {
        if (status == 2) {
          return " Not Available";
        } else if (status == 3) {
          return "Transfered";
        } else {
          return "Available";
        }
      }
    },
    {
      key: "7",
      title: 'Assigned To',
      dataIndex: 'assignto',
      render: (data) => {
        return "None";
      },
    },
  ];
  function onChange(pagination, filters, sorter, extra) {
    console.log('params', pagination, filters, sorter, extra);
    const filterData ={
      sortField: sorter.field,
      sortOrder: sorter.order,
      pagination,
      ...filters,
    }
  }
  function onChangeDate(date, dateString) {
    console.log(date, dateString);
  }
  const searchInventory=(e)=>{
    const value = e.target.value;
    setFilterInventory({'searchKey':value});
    if(value){      
      setIsFilter(true);
    }else{      
      setIsFilter(false);
    }
  }
  const onSearch = value => {
    setFilterInventory({'searchKey':value});
    if(value){      
      setIsFilter(true);
    }else{      
      setIsFilter(false);
    }
   
  };  
  function handleChangeSelect(value,selectType) {
    if(selectType == 'city'){
      getAllCityHubsAPI(value);
      setFilterInventory({...filterInventory,  city_id: value})
      
    }else if(selectType == 'vehicle_filter'){  
      setFilterInventory({...filterInventory,  vehicle_id: value})
      
    }else if(selectType == 'business_supplier'){  
      setFilterInventory({...filterInventory,  business_supplier_id: value})
      
    }else{
      setFilterInventory({...filterInventory,  hub_id: value})
    } 
  }
  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    },
    getCheckboxProps: (record) => ({       
      // Column configuration not to be checked
      name: record.name,
    }),
  };
  
  return (
    <>
      <Loader loading={loader} />
      <h2 className="mb-4 text-danger fw-bold fs-3">Vehicle Details</h2>
      <div className="col-12 d-flex">
        {/* <div className="col-2">
          <Search placeholder="input search loading default" allowClear  onSearch={onSearch} onPressEnter={(e)=>searchInventory(e)}   />
        </div> */}
        <div className="col-6 d-flex">
        <div className="col-4 ms-2">
            <Select defaultValue="" name="vehicle_filter" className="w-100" id="vehicle_filter"  onChange={(e)=>handleChangeSelect(e,'vehicle_filter')}>
              <Option value="">Select Vehicle</Option>
              {
                allVehiclesDataFilter.map(function(item, i){
                    return <Option value={item._id} >{item.vehicle_name} ({item.vehicle_model})</Option>
                })
              }
            </Select>
          </div>
          <div className="col-4 ms-2">
            <Select defaultValue="" name="business_supplier_filter" className="w-100" id="business_supplier_filter"  onChange={(e)=>handleChangeSelect(e,'business_supplier')}>
              <Option value="">Select Business Supplier</Option>
              {
                businessSupplierDataFilter.map(function(item, i){
                    return <Option value={item._id} >{item.supplier_name}</Option>
                })
              }
            </Select>
          </div>
          <div className="col-4 ms-2">
            <Select defaultValue="" name="city_filter" className="w-100" id="city_filter" onChange={(e)=>handleChangeSelect(e,'city')}>
              <Option value="">Select City</Option>
              {
                cityDataFilter.map(function(item, i){
                    return <Option value={item._id} >{item.name}</Option>
                })
              }
            </Select>
          </div>
          <div className="col-4 ms-2">
            <Select defaultValue="" style={{ width: 120 }} className="w-100" name="hub_filter" id="hub_filter"  onChange={(e)=>handleChangeSelect(e,'hub')}>
              <Option value="">Select Hub</Option>
              {
                hubDataFilter.map(function(item, i){
                    return <Option value={item._id} >{item.title}</Option>
                })
              }
            </Select>
          </div>
        </div>
        <div className="col-6">
          
        </div>
      </div>     
      <Table rowSelection={{
          type: 'checkbox',
          ...rowSelection,
        }}  loading={isLoading} onChange={onChange} columns={columns} dataSource={dataSource} />
      {/* view modal details start */}
      <Modal
        title="Vehicle Details"
        destroyOnClose="true"
        centered
        visible={isModalVisibleDetails}
        onOk={() => setIsModalVisibleDetails(false)}
        okType="danger"
        onCancel={() => setIsModalVisibleDetails(false)}
        cancelText="Close"
        width={1000}
        footer={<button type="button" onClick={() => setIsModalVisibleDetails(false)} class="ant-btn ant-btn-default"><span>Close</span></button>}
      >
        <div>
          <Descriptions
            bordered
            title=""
            size="middle"
          // extra={<Button type="primary">Edit</Button>}
          >
            <Descriptions.Item label="Business Supplier">{inventoryDetails ? inventoryDetails.vehicle_id.business_supplier : ""}</Descriptions.Item>
            <Descriptions.Item label="City">{inventoryDetails ? inventoryDetails.vehicle_id.city : ""}</Descriptions.Item>
            <Descriptions.Item label="Hub">{inventoryDetails ? inventoryDetails.vehicle_id.hub : ""}</Descriptions.Item>
            <Descriptions.Item label="Vehicle Name">{inventoryDetails ? inventoryDetails.vehicle_id.vehicle_name : ""}</Descriptions.Item>
            <Descriptions.Item label="Vehicle Model">{inventoryDetails ? inventoryDetails.vehicle_id.vehicle_model : ""}</Descriptions.Item>
            <Descriptions.Item label="Vehicle Color">{inventoryDetails ? inventoryDetails.vehicle_id.vehicle_color : ""}</Descriptions.Item>
            <Descriptions.Item label="Vehicle Registration Number">{inventoryDetails ? inventoryDetails.vehicle_id.vehicle_regNo : ""}</Descriptions.Item>
            <Descriptions.Item label="Manufature Date">{inventoryDetails ? inventoryDetails.vehicle_id.manufacture_date : ""}</Descriptions.Item>
            <Descriptions.Item label="Date Of Sale">{inventoryDetails ? inventoryDetails.vehicle_id.date_of_sale : ""}</Descriptions.Item>
            <Descriptions.Item label="Date Of Use">{inventoryDetails ? inventoryDetails.vehicle_id.date_of_use : ""}</Descriptions.Item>
            <Descriptions.Item label="Chassis Number">{inventoryDetails ? inventoryDetails.vehicle_id.chassis_no : ""}</Descriptions.Item>
            <Descriptions.Item label="Motor/Engine No">{inventoryDetails ? inventoryDetails.vehicle_id.moter_engine_no : ""}</Descriptions.Item>
            <Descriptions.Item label="Battery No.">{inventoryDetails ? inventoryDetails.vehicle_id.battery_no : ""}</Descriptions.Item>
            <Descriptions.Item label="2nd Battery No.">{inventoryDetails ? inventoryDetails.vehicle_id.battery_no_2 : ""}</Descriptions.Item>
            <Descriptions.Item label="Controller No">{inventoryDetails ? inventoryDetails.vehicle_id.controller_number : ""}</Descriptions.Item>
            <Descriptions.Item label="1st Charger No">{inventoryDetails ? inventoryDetails.vehicle_id.charger_no : ""}</Descriptions.Item>
            <Descriptions.Item label="2nd Charger No">{inventoryDetails ? inventoryDetails.vehicle_id.charger_no_2 : ""}</Descriptions.Item>
            <Descriptions.Item label="Key No">{inventoryDetails ? inventoryDetails.vehicle_id.key_no : ""}</Descriptions.Item>
            <Descriptions.Item label="Scooter Invoice">{inventoryDetails ? inventoryDetails.vehicle_id.scooter_invoice : ""}</Descriptions.Item>
            <Descriptions.Item label="Scooter Reg Date">{inventoryDetails ? inventoryDetails.vehicle_id.scooter_reg_date : ""}</Descriptions.Item>
            <Descriptions.Item label="Scooter Reg. upto">{inventoryDetails ? inventoryDetails.vehicle_id.scooter_reg_upto : ""}</Descriptions.Item>
            <Descriptions.Item label="Insurance Company">{inventoryDetails ? inventoryDetails.vehicle_id.insurance_company : ""}</Descriptions.Item>
            <Descriptions.Item label="Policy No Insurance">{inventoryDetails ? inventoryDetails.vehicle_id.policy_number_insurance : ""}</Descriptions.Item>
            <Descriptions.Item label="Date Policy Exp">{inventoryDetails ? inventoryDetails.vehicle_id.date_policy_expiry : ""}</Descriptions.Item>
            <Descriptions.Item label="Permit no">{inventoryDetails ? inventoryDetails.vehicle_id.exp_date : ""}</Descriptions.Item>
            <Descriptions.Item label="Exp Date">{inventoryDetails ? inventoryDetails.vehicle_id.permit_no : ""}</Descriptions.Item>
            <Descriptions.Item label="SGST">{inventoryDetails ? inventoryDetails.vehicle_id.sgsts : ""}</Descriptions.Item>
            <Descriptions.Item label="CGST">{inventoryDetails ? inventoryDetails.vehicle_id.cgst : ""}</Descriptions.Item>
            <Descriptions.Item label="IOT Device">{inventoryDetails ? inventoryDetails.vehicle_id.iot_device : ""}</Descriptions.Item>
            <Descriptions.Item label="Model">{inventoryDetails ? inventoryDetails.vehicle_id.model : ""}</Descriptions.Item>

          </Descriptions>

        </div>

      </Modal>
      {/* view modal details end */}
      {/* view modal details start */}
      <Modal
        title="Vehicle Timeline"
        destroyOnClose="true"
        centered
        visible={isModalVisibleTimeline}
        onOk={() => setIsModalVisibleTimeline(false)}
        okType="danger"
        onCancel={() => setIsModalVisibleTimeline(false)}
        cancelText="Close"
        width={1000}
        footer={<button type="button" onClick={() => setIsModalVisibleTimeline(false)} class="ant-btn ant-btn-default"><span>Close</span></button>}
      >
        <div>
          <h4 className="mb-4 text-secondary">Vehicle Name : Honda</h4>
          <button type="button" className="btn btn-danger float-end mb-3 "  >Download CSV</button>
          <Table rowSelection={{ type: 'checkbox' }} onChange={onChange} columns={columnsVehicle} dataSource={dataVehicleTimeline} />
        </div>

      </Modal>
      {/* view modal details end */}
    </>
  );
}
const mapStateToProps = (state, ownProps = {}) => {
  console.log(state) // state
  /*console.log(ownProps) // {}*/
  const { user } = state.auth;
  const { inventoryData } = state.inventory;
  return {
    user,
    inventoryData
  };
}

export default connect(mapStateToProps)(Vehicle_detailsComp);

