import React, { useState,useEffect } from "react";
import Link from "next/link";
import { connect,useDispatch, useSelector } from "react-redux";
import { useRouter } from 'next/router'
import { logout } from "../../Redux/Action/auth";
import {CONFIRM_ALERT} from "../../provider/confirmAlertProvider";
import { Logout } from "../../Redux/Action/auth";
import Loader from "../../Components/loader";
import { HANDLE_SUCCESS, HANDLE_ERROR, HTTP_SERVICE_CALL } from "../../provider/ApiProvider";
/* ant design */
import 'antd/dist/antd.css';
import { Tabs,Avatar,Button, Checkbox, Form, Input } from 'antd';
import {EditOutlined,DeleteOutlined,AntDesignOutlined} from '@ant-design/icons'

import { checkOldPassword,change_password} from "../../Redux/Action/users";

function ProfileComp(props) {
    const [loader, setLoader] = useState(false);  
    const router = useRouter(); 
    const dispatch = useDispatch();
    const { user: currentUser,user_cities,user_hubs,user_role } = props;  
    const { isLoggedIn } = useSelector(state => state.auth);
    // console.log('currentUser',currentUser);
    const [form] = Form.useForm();

    const [userProfile, setUserProfile] = useState({
      "first_name":"",
      "last_name":"",
      "email":"",
      "phone_number":"",
      "city_id":"",
      "hub_id":"",
      "cities":"",
      "hubs":"",
      "address":"",
      "_id":"",      
    });  

    const { TabPane } = Tabs;

    useEffect(()=>{  
        if (!isLoggedIn) {
        router.push('/login');
        }
        setUserProfile({...userProfile,first_name:(currentUser)?currentUser.first_name:"",last_name:(currentUser)?currentUser.last_name:"",email:(currentUser )?currentUser.email:"",phone_number:(currentUser)?currentUser.phone_number:"",address:(currentUser)?currentUser.address:"",_id:(currentUser)?currentUser._id:"",city_id:(currentUser)?currentUser.city_id:"",hub_id:(currentUser)?currentUser.hub_id:"",cities:user_cities,hubs:user_hubs});
        
    },[]);
    console.log('userProfile',userProfile)

    const onChangeTabs = (key) => {
      console.log(key);
    };

    const onFinish = (values) => {
      console.log('Success:', values);
      var ApiPath = change_password(values);
      try {
          setLoader(true);      
          dispatch(ApiPath)
          .then((res) => {
            
              setLoader(false);      
              if(res.status === true){
                  form.resetFields();
                  HANDLE_SUCCESS(res.message);                  
              }else{
                  HANDLE_ERROR(res.message);
              }
          })
          .catch((error) => {
              setLoader(false);
                HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
          });
      } catch (error) {
          setLoader(false);
          HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      }

    };
  
    const onFinishFailed = (errorInfo) => {
      console.log('Failed:', errorInfo);
    };

  return (
    <>
      <Loader loading={loader} />
      {/* <h2 className="mb-4 text-secondary">Profile</h2> */}
      <div className="p-4 shadow rounded-6 bg-white">
        <div>
        <Tabs defaultActiveKey="profile" onChange={onChangeTabs}>
          <TabPane tab="Profile" key="profile">
            <div className="row">
              <div className="col-lg-4">
                <div className="card mb-4">
                  <div className="card-body text-center">                  
                    <img src="img/User-Icon.svg" alt={"avatar"}
                      className="rounded-circle img-fluid" style={{"width": "125px",'border':"1px solid #ddd"}} />
                    <h5 className="my-3">{userProfile.first_name} {userProfile.last_name}</h5>
                    {/* <p className="text-muted mb-1">Full Stack Developer</p>
                    <p className="text-muted mb-4">Bay Area, San Francisco, CA</p> */}
                    {/* <div className="d-flex justify-content-center mb-2">
                      <button type="button" className="btn btn-primary">Follow</button>
                      <button type="button" className="btn btn-outline-primary ms-1">Message</button>
                    </div> */}
                  </div>
                </div>
              </div>
              <div className="col-lg-8">
                <div className="card mb-4">
                  <div className="card-body">
                    <div className="row">
                      <div className="col-sm-3">
                        <p className="mb-0">Full Name</p>
                      </div>
                      <div className="col-sm-9">
                        <p className="text-muted mb-0">{userProfile.first_name} {userProfile.last_name}</p>
                      </div>
                    </div>
                    <hr />
                    <div className="row">
                      <div className="col-sm-3">
                        <p className="mb-0">Email</p>
                      </div>
                      <div className="col-sm-9">
                        <p className="text-muted mb-0">{userProfile.email}</p>
                      </div>
                    </div>
                    <hr />
                    <div className="row">
                      <div className="col-sm-3">
                        <p className="mb-0">Phone</p>
                      </div>
                      <div className="col-sm-9">
                        <p className="text-muted mb-0">{userProfile.phone_number}</p>
                      </div>
                    </div>
                    <hr />                    
                    <div className="row">
                      <div className="col-sm-3">
                        <p className="mb-0">Address</p>
                      </div>
                      <div className="col-sm-9">
                        <p className="text-muted mb-0">{userProfile.address}</p>
                      </div>
                    </div>
                    <hr />                    
                    <div className="row">
                      <div className="col-sm-3">
                        <p className="mb-0">User Role</p>
                      </div>
                      <div className="col-sm-9">
                        <p className="text-muted mb-0">{user_role== "FAE_ADMIN"?"FAE ADMIN":user_role=="FAE_MAIN_ADMIN"?"FAE MAIN ADMIN":user_role=="FAE_CITY_ADMIN"?"FAE CITY ADMIN":user_role=="FAE_HUB_ADMIN"?"FAE HUB ADMIN":user_role=="FAE_HUB_MANAGER"?"FAE HUB MANAGER":user_role=="FAE_STAFF"?"FAE STAFF":""}</p>
                      </div>
                    </div>
                    {user_role != "FAE_MAIN_ADMIN" && user_role != "FAE_ADMIN"?
                      <>
                      <hr />
                      <div className="row">
                        <div className="col-sm-3">
                          <p className="mb-0">Cities</p>
                        </div>
                        <div className="col-sm-9">
                          <p className="text-muted mb-0">{userProfile.cities}</p>
                        </div>
                      </div>
                      <hr />
                      <div className="row">
                        <div className="col-sm-3">
                          <p className="mb-0">Hubs</p>
                        </div>
                        <div className="col-sm-9">
                          <p className="text-muted mb-0">{userProfile.hubs}</p>
                        </div>
                      </div>
                      </>
                    :''}
                  </div>
                </div>
              </div>
            </div>
          </TabPane>
          <TabPane tab="Change Password" key="changepassword">
            <div className="row">
              <div className="col-lg-12">
                <Form
                  form={form}
                  name="changepasswordForm"
                    
                  initialValues={{
                    oldpassword: null,
                    newpassword: null,
                    confirmnewpassword: null,
                    _id: userProfile._id,
                  }}
                  onFinish={onFinish}
                  onFinishFailed={onFinishFailed}
                  autoComplete="off"
                  layout="vertical"
                >
                  <Form.Item
                    hasFeedback
                    allowClear
                    label="Old Password"
                    name="oldpassword"  
                    rules={[
                      {
                        required: true,
                        message: "Please input your old password!"
                      },
                    ]}
                  >
                    <Input.Password />
                  </Form.Item>      

                  <Form.Item
                    hasFeedback
                    allowClear
                    label="New Password"
                    name="newpassword"                    
                    rules={[
                      {
                        required: true,
                        message: 'Please input your new password!',
                      },
                    ]}
                  >
                    <Input.Password />
                  </Form.Item>  
                  <Form.Item
                    hasFeedback
                    allowClear
                    label="Confirm New Password"
                    name="confirmnewpassword"
                    dependencies={['newpassword']}
                    rules={[
                      {
                        required: true,
                        message: 'Please confirm your password!',
                      },({ getFieldValue }) => ({
                        validator(_, value) {
                          if (!value || getFieldValue('newpassword') === value) {
                            return Promise.resolve();
                          }
            
                          return Promise.reject(new Error('The two passwords that you entered do not match!'));
                        },
                      }),
                    ]}
                  >
                    <Input.Password />
                  </Form.Item> 
                  <Form.Item
                    name="_id"  
                    hidden={true}
                  >
                    <Input value={userProfile._id}  />
                  </Form.Item> 
                  <Form.Item
                  >
                    <Button type="primary" htmlType="submit"  >
                      Submit
                    </Button>
                  </Form.Item>
                </Form>
              </div>
            </div>
            
          </TabPane>
        </Tabs>
        </div>
      </div>
    </>
  );
}
const mapStateToProps = (state, ownProps = {}) => {  
  const {  isLoggedIn,user,user_city_id,user_role,user_hub_id ,user_cities, user_hubs} = state.auth;
  return {
    user,
    isLoggedIn,
    user_city_id,
    user_hub_id,
    user_role,
    user_cities,
    user_hubs
  };
}
  
export default connect(mapStateToProps)(ProfileComp);
