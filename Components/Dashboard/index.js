import React, { useState, useEffect, useCallback,useRef } from "react";
import Link from "next/link";
import { connect, useDispatch, useSelector } from "react-redux";
import { useRouter } from 'next/router';
import { HANDLE_SUCCESS, HANDLE_ERROR } from "../../provider/ApiProvider";
import { changeDateFormat } from "../../provider/helper";
import Loader from "../../Components/loader";
import { getAllDashboardData} from "../../Redux/Action/dashboard";
import { createHub,getAllHubs,allCityHubs,deleteHub,updateHub} from "../../Redux/Action/hubs";
import { getAllCities} from "../../Redux/Action/cities"
/* ant design */
import 'antd/dist/antd.css';
import { Table,Tabs,Row, Col, Tag, Space, Modal, Form, Input, Radio, Upload, message, Button, Descriptions, DatePicker,Select } from 'antd';
import { EditOutlined, DeleteOutlined, InboxOutlined, EyeOutlined, SearchOutlined } from '@ant-design/icons'
// formik import 
import * as Yup from "yup";
import { VALIDATION_SCHEMA, FORMIK } from "../../provider/FormikProvider";
import moment from 'moment';
/* chart code start */
import BarChart from "./BarChart";
import LineChart from "./LineChart";
import PieChart from "./PieChart";
import { UserData } from "./Data";
/* chart code end */

function Dashboard(props) {
  const dateFormat = "DD-MM-YYYY";
  const today = moment(new Date().getDate(),dateFormat)
 
  const router = useRouter();
  const [loader, setLoader] = useState(false);
  const dispatch = useDispatch();
  const { isLoggedIn} = useSelector(state => state.auth);

  const [NoOfSupplier, setNoOfSupplier] = useState(0);
  const [NoOfClients, setNoOfClients] = useState(0);
  const [NoOfVehicleSupplied, setNoOfVehicleSupplied] = useState(0);
  const [NoOfVehicleAssigned, setNoOfVehicleAssigned] = useState(0);
  const [NoOfReferals, setNoOfReferals] = useState(0);
  const [NoOfVehicleBooked, setNoOfVehicleBooked] = useState(0);
  const [NoOfVehicleBookedAmount, setNoOfVehicleBookedAmount] = useState(0);

  const [cityDataFilter, setCityDataFilter] = useState([]);
  const [hubDataFilter, setHubDataFilter] = useState([]);  
  const [filterArrayDash, setFilterArray] =  useState({'searchKey':'','vehicle_id':'','city_id':props.user_city_id?props.user_city_id[0]:'','hub_id':(props.user_role == 'FAE_HUB_ADMIN' || props.user_role == 'FAE_HUB_MANAGER') && props.user_hub_id?props.user_hub_id[0]:'','start_date':changeDateFormat(new Date(), 'YYYY-MM-DD'),'end_date':'' ,'filter_type':''});

  // const NoOfSupplier_ref = useRef(NoOfSupplier);
  // const NoOfClients_ref = useRef(NoOfClients);
  // const NoOfVehicleSupplied_ref = useRef(NoOfVehicleSupplied);
  // const NoOfVehicleAssigned_ref = useRef(NoOfVehicleAssigned);
  // const NoOfReferals_ref = useRef(NoOfReferals);
  // const NoOfVehicleBooked_ref = useRef(NoOfVehicleBooked);
  // const NoOfVehicleBookedAmount_ref = useRef(NoOfVehicleBookedAmount);
  

  const { Search } = Input;
  const { Option } = Select;
  const { TabPane } = Tabs;
  

  const [userData, setUserData] = useState({
    labels: UserData.map((data) => data.year),
    datasets: [
      {
        label: "Users Gained",
        data: UserData.map((data) => data.userGain),
        backgroundColor: [
          "#ed2024",
          "#ECF0F1",
          "#50AF95",
          "#F3BA2F",
          "#2A71D0",
        ],
        borderColor: "black",
        borderWidth: 2,
      },
    ],
  });
  const [user_role, setuser_role] = useState(props.user_role);  
  const [user_city_id, setuser_city_id] = useState(props.user_hub_id);
  const [user_hub_id, setuser_hub_id] = useState(props.user_city_id);
  
  useEffect(() => {
    if (!isLoggedIn) {
      router.push('/login');
    } else {
           
      setuser_role(props.user_role);
      setuser_hub_id(props.user_hub_id)
      setuser_city_id(props.user_city_id); 
      
      getAllCityAPI();
      if(props.user_city_id){
        getAllCityHubsAPI(props.user_city_id[0]);
      } 
      
      // if(props.user_role == 'FAE_MAIN_ADMIN' || props.user_role == 'FAE_ADMIN'){
      //   const getCity = cityDataFilter.filter((i)=> [props.user_city_id].includes(i));
      //   if(getCity.length >0){
      //     setuser_city_id(getCity);         
      //     setFilterArray({...filterArrayDash, city_id: getCity[0] ,hub_id: "",start_date:changeDateFormat(new Date(), 'YYYY-MM-DD'),end_date:""})
      //     getAllCityHubsAPI(getCity[0]);
      //   } 
      // }else{
        
      //   if(props.user_role == 'FAE_HUB_ADMIN' || props.user_role == 'FAE_HUB_MANAGER'){          
      //     setFilterArray({...filterArrayDash,city_id:props.user_city_id?props.user_city_id[0]:'', hub_id: props.user_hub_id[0],start_date:changeDateFormat(new Date(), 'YYYY-MM-DD'),end_date:""})
      //   }else{
      //     setFilterArray({...filterArrayDash, city_id: props.user_city_id?props.user_city_id[0]:'' ,hub_id: "",start_date:changeDateFormat(new Date(), 'YYYY-MM-DD'),end_date:""})
          
      //   }
       
      // }

      
      // getAllDashData();     
      
    }
  }, [])
  
  useEffect(() => {
    getAllDashData();
  }, [filterArrayDash])
  
  useEffect(() => {
    if(props.user_role == 'FAE_MAIN_ADMIN' || props.user_role == 'FAE_ADMIN'){
      const getCity = cityDataFilter.filter((i)=> [props.user_city_id].includes(i));
      if(getCity.length >0){        
        // setFilterArray({...filterArrayDash, city_id: getCity[0] ,hub_id: "",start_date:changeDateFormat(new Date(), 'YYYY-MM-DD'),end_date:""})
      //     getAllCityHubsAPI(getCity[0]);
      }else{
        setFilterArray({...filterArrayDash, city_id: "" ,hub_id: ""})
        setHubDataFilter([]);
      }
    }  
  },[])
 
  const getAllDashData = () => {
    try {
      setLoader(true);
      dispatch(getAllDashboardData(filterArrayDash))
        .then((res) => {
          setLoader(false);
          if (res.status == true) {
            // HANDLE_SUCCESS(res.message); 
            setNoOfSupplier(res.NoOfSupplier);
            setNoOfClients(res.setNoOfClients);
            setNoOfVehicleSupplied(res.NoOfVehicleSupplied);
            setNoOfVehicleAssigned(res.NoOfVehicleAssigned);
            setNoOfReferals(res.NoOfReferals);
            setNoOfVehicleBooked(res.NoOfVehicleBooked);
            setNoOfVehicleBookedAmount(res.NoOfVehicleBookedAmount);
            setCityDataFilter(res.citiesList);            
              
          } else {
            // HANDLE_ERROR(res.message);
          }
        })
        .catch((error) => {
          setLoader(false);
          // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
        });
    } catch (error) {
      setLoader(false);
      // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }

  function handleChangeSelect(value,selectType) {
    if(selectType == 'city'){
      setHubDataFilter([]);
      if(value){
        getAllCityHubsAPI(value);
      }  
      setFilterArray({...filterArrayDash, city_id: value ,hub_id: ''})
      
    }else if(selectType == 'hub'){
      setFilterArray({...filterArrayDash,  hub_id: value})

    }else if(selectType == 'start_date'){ 
      if(value != null){
        setFilterArray({...filterArrayDash,  start_date:changeDateFormat(value._d,"YYYY-MM-DD")})
      }else{
        setFilterArray({...filterArrayDash,  start_date:""})
      }

    }else if(selectType == 'end_date'){ 
      if(value != null){
        setFilterArray({...filterArrayDash,  end_date: changeDateFormat(value._d,"YYYY-MM-DD")})
      }else{
        setFilterArray({...filterArrayDash,  end_date:""})
      }

    }
  }
  
  const getAllCityAPI=()=>{
    try {
      setLoader(true);      
      dispatch(getAllCities())
      .then((res) => {
        console.log('res.data)',res.data)
        setLoader(false);
        setCityDataFilter(res.data);
      })
      .catch((error) => {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });
    } catch (error) {
      setLoader(false);
      // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  const getAllCityHubsAPI=(city_id)=>{
    try {
      setLoader(true);      
      dispatch(allCityHubs(city_id))
      .then((res) => {
        console.log('res.data)',res.data)
        setLoader(false);
        setHubDataFilter(res.data);
      })
      .catch((error) => {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });
    } catch (error) {
      setLoader(false);
      // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }

  return (
    <>
      <Loader loading={loader} />
      <Row className="mb-3">
        <Col span={6}>
          
        </Col>
        <Col span={6}>
         
        </Col>
        <Col span={6} >
          <Row>            
            <Col span={12}>
              {user_role != 'FAE_HUB_ADMIN' && user_role != 'FAE_HUB_MANAGER'?
              <Select value={filterArrayDash.city_id} name="city_filter" className="w-100" id="city_filter" onChange={(e)=>handleChangeSelect(e,'city')}>
                {(user_role == 'FAE_MAIN_ADMIN' || user_role == 'FAE_ADMIN'?
                <Option value="">Select City</Option>:'')}
               
                {
                  // (user.user_role != 'FAE_CITY_ADMIN')?"":'')                
                  cityDataFilter.map(function(item, i){
                    if(user_role == 'FAE_CITY_ADMIN' && user_city_id.includes(item._id)){
                      return <Option value={item._id} >{item.name}</Option>
                    }else if(user_role != 'FAE_CITY_ADMIN'){
                      return <Option value={item._id} >{item.name}</Option>
                    }                     
                  })
                }
              </Select>
              :''}
            </Col>            
            <Col span={12}>
              <Select value={filterArrayDash.hub_id} style={{ width: 120 }} className="w-100" name="hub_filter" id="hub_filter"  onChange={(e)=>handleChangeSelect(e,'hub')}>
                {
                (user_role == 'FAE_MAIN_ADMIN' || user_role == 'FAE_ADMIN' || user_role == 'FAE_CITY_ADMIN')?( <Option value="">Select Hub</Option>):""}

                {
                  
                  hubDataFilter.map(function(item, i){
                    
                    if(user_role == 'FAE_MAIN_ADMIN' || user_role == 'FAE_ADMIN'){
                      return <Option value={item._id} >{item.title}</Option>
                    }else{  
                      if((user_role == 'FAE_HUB_ADMIN' || user_role == 'FAE_HUB_MANAGER')){
                        if(user_hub_id && user_hub_id.includes(item._id) ){                  
                          return <Option value={item._id} >{item.title}</Option>  
                        }
                      }else{
                        return <Option value={item._id} >{item.title}</Option>
                      } 
                    }                     
                  })
                }
              </Select>
            </Col>
          </Row> 
          
         
        </Col>
        <Col span={6}>
          <Row>
            <Col span={12}>
              <DatePicker   format='DD-MM-YYYY'  placeholder="Start Date"  className="float-end  me-2" name="start_date_filter" id="start_date_filter" onClear={(e)=>handleChangeSelect(e,'start_date')} onChange={(e)=>handleChangeSelect(e,'start_date')} defaultValue={moment(new Date(), 'DD-MM-YYYY')} />
            </Col>
            <Col span={12}>
              <DatePicker  format='DD-MM-YYYY' placeholder="End  Date"   className="float-end"  name="end_date_filter" id="end_date_filter" onClear={(e)=>handleChangeSelect(e,'end_date')}  onChange={(e)=>handleChangeSelect(e,'end_date')} defaultValue={""} />
            </Col>
          </Row>         
        </Col>
      </Row>
      <div className="row">
        <div className="col-md-6">
          <div className="row">
            <Link href="/business-supplier"  >
            <div className="col-xl-6 col-md-6 mb-4 cursor-pointer">
              <div className="card shadow h-100 p-3 rounded-6">
                <div className="card-body py-4">
                  <div className="row no-gutters align-items-center">
                    <div className="col mr-2">
                      <div className="text-xs fw-bold text-danger text-uppercase mb-1">
                        Business suppliers
                      </div>
                      <div className="h5 mb-0 fw-bold text-gray-800">{NoOfSupplier}</div>
                    </div>
                    <div className="col-auto">
                      <i className="fas fa-user fa-2x text-danger" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            </Link>
            <Link href="/inventory"  >
            <div className="col-xl-6 col-md-6 mb-4 cursor-pointer">
              <div className="card shadow h-100 p-3 rounded-6">
                <div className="card-body py-4">
                  <div className="row no-gutters align-items-center">
                    <div className="col mr-2">
                      <div className="text-xs fw-bold text-danger text-uppercase mb-1">
                        Vehicle supplied
                      </div>
                      <div className="h5 mb-0 fw-bold text-gray-800">{NoOfVehicleSupplied}</div>
                    </div>
                    <div className="col-auto">
                      <i className="fas fa-paper-plane fa-2x text-danger" />
                    </div>
                  </div>
                </div>
              </div>
            </div>            
            </Link>
            <Link href="/business-client" >
            <div className="col-xl-6 col-md-6 mb-4 cursor-pointer">
              <div className="card border-left-info shadow h-100 p-3 rounded-6">
                <div className="card-body py-4">
                  <div className="row no-gutters align-items-center">
                    <div className="col mr-2">
                      <div className="text-xs fw-bold text-danger text-uppercase mb-1">
                        Business clients
                      </div>
                      <div className="row no-gutters align-items-center">
                        <div className="col-auto">
                          <div className="h5 mb-0 mr-3 fw-bold text-gray-800">
                            {NoOfClients}
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-auto">
                      <i className="fas fa-users fa-2x text-danger" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            </Link>
            <Link href="/inventory"  >
            <div className="col-xl-6 col-md-6 mb-4 cursor-pointer">
              <div className="card shadow h-100 p-3 rounded-6">
                <div className="card-body py-4">
                  <div className="row no-gutters align-items-center">
                    <div className="col mr-2">
                      <div className="text-xs fw-bold text-danger text-uppercase mb-1">
                        Vehicle assigned
                      </div>
                      <div className="h5 mb-0 fw-bold">{NoOfVehicleAssigned}</div>
                    </div>
                    <div className="col-auto">
                      <i className="fas fa-motorcycle fa-2x text-danger" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            </Link>
            {((props.user_role != 'FAE_HUB_ADMIN' && props.user_role != 'FAE_HUB_MANAGER' && props.user_role != 'FAE_STAFF')?
            <Link href="/referals" >
            <div className="col-xl-6 col-md-6 mb-4 cursor-pointer">
              <div className="card shadow h-100 p-3 rounded-6">
                <div className="card-body py-4">
                  <div className="row no-gutters align-items-center">
                    <div className="col mr-2">
                      <div className="text-xs fw-bold text-danger text-uppercase mb-1">
                        referrals
                      </div>
                      <div className="h5 mb-0 fw-bold">{NoOfReferals}</div>
                    </div>
                    <div className="col-auto">
                      <i className="fas fa-share-alt fa-2x text-danger" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            </Link>:'')}
          </div>
        </div>
        <div className="col-md-6">
          <div className="row">
            <Link href="/bookings" >
            <div className="col-xl-6 col-md-6 mb-4 cursor-pointer">
              <div className="card shadow h-100 p-3 rounded-6">
                <div className="card-body py-4">
                  <div className="row no-gutters align-items-center">
                    <div className="col mr-2">
                      <div className="text-xs fw-bold text-danger text-uppercase mb-1">
                        Vehicles booked
                      </div>
                      <div className="h5 mb-0 fw-bold text-gray-800">{NoOfVehicleBooked}</div>
                    </div>
                    <div className="col-auto">
                      <i className="fas fa-user fa-2x text-danger" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            </Link>
            <Link href="/bookings" >
            <div className="col-xl-6 col-md-6 mb-4 cursor-pointer">
              <div className="card shadow h-100 p-3 rounded-6">
                <div className="card-body py-4">
                  <div className="row no-gutters align-items-center">
                    <div className="col mr-2">
                      <div className="text-xs fw-bold text-danger text-uppercase mb-1">
                        Revenue collection
                      </div>
                      <div className="h5 mb-0 fw-bold text-gray-800"><i className="fa-solid fa-indian-rupee-sign  text-danger"></i> {NoOfVehicleBookedAmount}</div>
                    </div>
                    <div className="col-auto">
                      <i className="fas fa-user fa-2x text-danger" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            </Link>
                                 
          </div>
        </div>
        <div className="col-xl-4 col-lg-4">
          <div className="rounded-6 bg-white p-4 shadow">
            <LineChart chartData={userData} />
          </div>
        </div>
        <div className="col-xl-4 col-lg-4">
          <div className="rounded-6 bg-white p-4 shadow">
            <PieChart chartData={userData} className="w-md-50" />
          </div>
        </div>
        <div className="col-xl-4 col-lg-4">
          <div className="rounded-6 bg-white p-4 shadow">
            <BarChart chartData={userData} />
          </div>
        </div>
      </div>
    </>
  );
}
const mapStateToProps = (state, ownProps = {}) => {  
  const { isLoggedIn,user,user_city_id,user_role ,user_hub_id } = state.auth;
  const { inventoryData } = state.inventory;
  return {
    user,
    isLoggedIn,
    user_city_id,
    user_hub_id,
    user_role,
    inventoryData
  };
}
export default connect(mapStateToProps)(Dashboard);