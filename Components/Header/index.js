import React, { useState,useEffect } from "react";
import Image from "next/image";
import Fablogo from "../../public/img/fae-logo.svg";
import Search from "../../public/img/search.svg";
import Link from 'next/link'
import {
  Dropdown,
  Col,
  Navbar,
  Nav,
  Container,
  NavDropdown,
} from "react-bootstrap";
import {CONFIRM_ALERT} from "../../provider/confirmAlertProvider";
import ProfilePic from "../../public/img/User-Icon.svg";
import ActiveLink from "./Activelink";
import { useRouter } from 'next/router'
import Loader from "../../Components/loader";
import { HANDLE_SUCCESS, HANDLE_ERROR, HTTP_SERVICE_CALL } from "../../provider/ApiProvider";
import { Logout } from "../../Redux/Action/auth";
import { connect,useDispatch, useSelector } from "react-redux";

function Header(props) {
 
  const [loader, setLoader] = useState(false);  
  const [user_role, setuser_role] = useState('');  
  const [user_city_id, setuser_city_id] = useState('');  
  const router = useRouter(); 
  const dispatch = useDispatch();
  const { user: currentUser } = props;  
  const { isLoggedIn} = useSelector(state => state.auth);
  
  useEffect(() => {
    setuser_role(props.user_role);
    setuser_city_id(props.user_city_id);
  }, [])
  const logoutApp=()=>{
    const title="Are you sure?";
    const message="You want to logout?";
    CONFIRM_ALERT(title,message,logoutSubmit);     
  }
  const logoutSubmit=()=>{    
    // console.log('logoutSubmit');
    try {
      setLoader(true);      
      dispatch(Logout());       
      setTimeout(() => {
        HANDLE_SUCCESS("You have successfully logged out!"); 
        setLoader(false);
        router.push('/login');
      }, 1000); 
     
    } catch (error) {
      // console.log('error',error);
      setLoader(false);
      HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    } 

  }
  const changeRoutes=(pathname)=>{
    router.push(pathname);     
  }
  
  return (
    <>
      <Loader loading={loader} />
      <Navbar bg="white" expand="lg" className="p-0">
        <Navbar.Brand href="#home" className="p-2 ms-4 pe-4 fab-logo col-2">
          <Image src={Fablogo} alt={"faebikes"} />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <ActiveLink activeClassName="topMenuActive" href="/">
              <a className="nav-link px-2 link-fae mx-3 fw-bold fs-6">Dashboard</a>
            </ActiveLink>
            <ActiveLink activeClassName="topMenuActive" href="/inventory">
              <a className="nav-link px-2 link-fae mx-3 fw-bold fs-6">Inventory</a>
            </ActiveLink>
           
            <ActiveLink activeClassName="topMenuActive" href="/kyc-request">
              <a className="nav-link px-2 link-fae mx-3 fw-bold fs-6" style={(user_role =='FAE_MAIN_ADMIN' || user_role =='FAE_ADMIN'  || user_role == 'FAE_CITY_ADMIN'  || user_role == 'FAE_HUB_ADMIN' )?{'display':''}:{'display':'none'}}>KYC Request</a>
            </ActiveLink>
             
            <ActiveLink activeClassName="topMenuActive" href="/bookings">
              <a className="nav-link px-2 link-fae mx-3 fw-bold fs-6">Bookings</a>
            </ActiveLink>
            <ActiveLink activeClassName="topMenuActive" href="/assistance">
              <a className="nav-link px-2 link-fae mx-3 fw-bold fs-6">Assistance</a>
            </ActiveLink>
          </Nav>
          <div className="col-md-2">
            {/* <form className="card card-sm border-0">
              <div className="card-body row no-gutters align-items-center mt-2">
                <div className="col-auto">
                  <Image src={Search} alt="faebikes search" />
                </div>

                <div className="col">
                  <input
                    className="form-control form-control-lg form-control-borderless p-0"
                    type="search"
                    placeholder="Search here..."
                  />
                </div>
              </div>
            </form> */}
          </div>
          <div className="col-md-1">
            <Dropdown>
              <Dropdown.Toggle
                id="dropdown-button-dark-example1"
                variant="transparent"
                className="m-2 p-2 faeProfile-dropdown"
              >
                <Image
                  width="55"
                  height="55"
                  className="rounded-circle me-2 p-2"
                  src={ProfilePic}
                />
              </Dropdown.Toggle>

              <Dropdown.Menu className="userDropdown rounded-6 shadow" >
                <Dropdown.Item onClick={()=>changeRoutes('/profile')}>Profile</Dropdown.Item>
                {/*  <Dropdown.Item href="#/action-2">Settings</Dropdown.Item> */}
                <Dropdown.Item onClick={()=>logoutApp()} >
                    Logout
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </div>
        </Navbar.Collapse>
      </Navbar>
      {/* <nav className="sb-topnav navbar navbar-expand navbar-white bg-white shadow position-relative">
        <Navbar.Brand href="#home">
          <a className="navbar-brand ps-2 ms-4 pe-4 fab-logo" href="">
            <Image src={Fablogo} alt="faebikes" />
          </a>
        </Navbar.Brand>

        <div className="row w-75 ms-5">
          <Col md={9} className="mt-3 pt-1">
            <ul className="nav col-12 pt-1">
              <li>
                <ActiveLink activeClassName="active" href="/">
                  <a className="nav-link px-2 link-fae mx-3">Dashboard</a>
                </ActiveLink>
              </li>
              <li>
                <ActiveLink activeClassName="active" href="/KYCRequest">
                  <a className="nav-link px-2 link-fae mx-3">KYC Request</a>
                </ActiveLink>
              </li>
              <li>
                <ActiveLink activeClassName="active" href="/Bookings">
                  <a className="nav-link px-2 link-fae mx-3">Bookings</a>
                </ActiveLink>
              </li>
              <li>
                <ActiveLink activeClassName="active" href="/Assistance">
                  <a className="nav-link px-2 link-fae mx-3">Assistance</a>
                </ActiveLink>
              </li>
            </ul>
          </Col>
          <div className="col-md-2 mt-3">
            <form className="card card-sm border-0">
              <div className="card-body row no-gutters align-items-center mt-2">
                <div className="col-auto">
                  <Image src={Search} alt="faebikes search" />
                </div>
               
                <div className="col">
                  <input
                    className="form-control form-control-lg form-control-borderless p-0"
                    type="search"
                    placeholder="Search here..."
                  />
                </div>
              </div>
            </form>
          </div>
          <div className="col-md-1 pt-4">
            <Dropdown>
              <Dropdown.Toggle
                id="dropdown-button-dark-example1"
                variant="transparent"
                className="m-2 p-2 w-100 faeProfile-dropdown"
              >
                <Image
                  width="55"
                  height="55"
                  class="rounded-circle me-2 p-2"
                  src={ProfilePic}
                />
              </Dropdown.Toggle>

              <Dropdown.Menu>
                <Dropdown.Item href="/profile">Profile</Dropdown.Item>
                <Dropdown.Item href="#/action-2">Settings</Dropdown.Item>
                <Dropdown.Item href="#/action-3">Logout</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </div>
         
        </div>
      </nav> */}
    </>
  );
}


const mapStateToProps = (state, ownProps = {}) => {
  // console.log('state',state) // state
  // console.log('ownProps',ownProps) // {}
  const { isLoggedIn,user,user_city_id,user_role } = state.auth;
 
  return {
    user,
    isLoggedIn,
    user_city_id,
    user_role
  };
}

export default connect(mapStateToProps)(Header);