import React from "react";
import Image from "next/image";
import Fablogo from "../../public/img/fae-logo.svg";
import Search from "../../public/img/search.svg";

export default function Header() {
  return (
    <>
      <nav className="sb-topnav navbar navbar-expand navbar-white bg-white shadow position-relative">
        <a className="navbar-brand ps-2 ms-4 pe-4 fab-logo" href="">
          <Image src={Fablogo} alt="faebikes" />
        </a>
        <div className="row w-75 ms-5">
          <div className="col-12 col-md-10 col-lg-8">
            <form className="card card-sm border-0">
              <div className="card-body row no-gutters align-items-center">
                <div className="col-auto">
                  <Image src={Search} alt="faebikes search" />
                </div>
                {/*end of col*/}
                <div className="col">
                  <input
                    className="form-control form-control-lg form-control-borderless p-0"
                    type="search"
                    placeholder="Search here..."
                  />
                </div>
              </div>
            </form>
          </div>
          {/*end of col*/}
        </div>
      </nav>
    </>
  );
}
