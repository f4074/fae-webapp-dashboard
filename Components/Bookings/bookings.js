import React , { useState, useEffect, useCallback } from "react";
import axios from "axios";
import { connect,useDispatch, useSelector } from "react-redux";
import { useRouter } from 'next/router'
import { HANDLE_SUCCESS, HANDLE_ERROR, HTTP_SERVICE_CALL } from "../../provider/ApiProvider";
import  {SET_DATA_LOCALSTORAGE,REMEMBER_PASSWORD,GET_DATA_LOCALSTORAGE} from "../../provider/LocalStorageProvider"
import { BASE_URL, POST, IS_LOADING,ADMIN_SIGN_IN,BASE_URL_CORE,IMAGE_URL_DASH,FILE_UPLOAD } from "../../provider/EndPoints";
import { changeDateFormat } from "../../provider/helper";
import Loader from "../../Components/loader";
import Spinner from "../../Components/Spinner/Spinner";
import { create,getAll,getAllWithFilter,deleteData,updateData ,getDetails,bookingCheckVerifCode,checkAvailability} from "../../Redux/Action/bookings";
import { createHub,getAllHubs,allCityHubs,deleteHub,updateHub} from "../../Redux/Action/hubs";
import { getAllCities} from "../../Redux/Action/cities"

import {getAllInventoryFilter} from "../../Redux/Action/inventory"
import {resendOtp} from "../../Redux/Action/users"
import 'antd/dist/antd.css';
import { Table, Row, Col, Divider,Tag, Space, Modal, Form, Input, Radio, Upload, message, Button, Descriptions, DatePicker,Select,AutoComplete } from 'antd';
import ImgCrop from 'antd-img-crop';
import {EditOutlined,DeleteOutlined,PlusOutlined,SearchOutlined,PlusCircleOutlined,PlusSquareFilled,MinusSquareFilled} from '@ant-design/icons'
// formik import 
import * as Yup from "yup";
import {VALIDATION_SCHEMA,FORMIK} from "../../provider/FormikProvider";
import moment from 'moment';
/* function onChange(pagination, filters, sorter, extra) {
  // console.log('params', pagination, filters, sorter, extra);
}
 */

const BookingsComp=(props)=> { 
  const router = useRouter();  
  const [loader, setLoader] = useState(false);  
  const dispatch = useDispatch();
  const { isLoggedIn } = useSelector(state => state.auth);

  const [user_role, setuser_role] = useState(props.user_role);  
  const [user_city_id, setuser_city_id] = useState(props.user_hub_id);
  const [user_hub_id, setuser_hub_id] = useState(props.user_city_id);

  const { clientData } = useSelector(state => state.business_client);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isModalVisibleDetails, setIsModalVisibleDetails] = useState(false);
  const [isModalVisibleOtp, setIsModalVisibleOtp] = useState(false);
  const [isEditBooking, setIsEditBooking] = useState(false);
  
  const [isEditing, setIsEditing] = useState(false);
  const [editingClient, setEditingClient] = useState(null);
  const [dataSource,setDataSource]= useState([]);
  const [bookingDetails,setBookingDetails]= useState([]);
  const [cityDataFilter, setCityDataFilter] = useState([]);
  const [hubDataFilter, setHubDataFilter] = useState([]);
  const [userData, setUserData] = useState([]);
  const [vehicleData, setVehicleData] = useState([]);
  const [vehicleNameData, setVehicleNameData] = useState([]);
  const [sparesInventoryData, setSparesInventoryData] = useState([]);
  const [iotDevicesInventoryData, setIOtDevicesInventoryData] = useState([]);
  const [businessClientData, setBusinessClientData] = useState([]);
  const [businessSupplierData, setBusinessSupplierData] = useState([]);
  const [filterArray, setFilter] = useState({'searchKey':'','vehicle_regNo':'','start_date':changeDateFormat(new Date(), 'YYYY-MM-DD'),'end_date':"",'city_id':props.user_city_id?props.user_city_id[0]:'','hub_id':(props.user_role == 'FAE_HUB_ADMIN' || props.user_role == 'FAE_HUB_MANAGER') && props.user_hub_id?props.user_hub_id[0]:''});
  const [modalTitle,setModalTitle]= useState('Add');
  const [editId, setEditId] = useState('');
  const [hubDefaultValue, setHubDefaultValue] = useState('');
  const [previewVisible, setPreviewVisible] = useState(false);
  const [previewTitle, setPreviewTitle] = useState(null);
  const [previewImage, setPreviewImage] = useState(null);
  const [fileList, setFileList] = useState([]);  
  const [progress, setProgress] = useState(0);
  const [userInfo, setUserInfo] = useState('');  
  /* autocomplete usestates */
  const [sparesValues, setSparesValues] = useState({spares1:'',spares2:''});
  const [options, setOptions] = useState([]);
  const [optionsSpares, setOptionsSpares] = useState([]);
  const [optionsIotDevices, setOptionsIotDevices] = useState([]);

  const [sparesFields, setSparesFields] = useState([""])
  const [availabilityErrors, setAvailabilityErrors] = useState({"spares":'',"battery_1":'',"battery_2":'',"charger_1":'',"charger_2":'',"iot_devices":''})
  const [imageType, setImageType] = useState(["image/png","image/apng","image/avif","image/gif","image/jpeg","image/svg+xml","image/webp"]);  

  const { Search,TextArea } = Input;
  const { Option } = Select;
  const { RangePicker } = DatePicker;


  useEffect(()=>{
    if (!isLoggedIn) {
        router.push('/login');
    }else{
      // setuser_role(props.user_role);
      // setuser_hub_id(props.user_hub_id)
      // setuser_city_id(props.user_city_id);
      
      // if(props.user_role == 'FAE_MAIN_ADMIN' || props.user_role == 'FAE_ADMIN'){
      //   const getCity = cityDataFilter.filter((i)=> [props.user_city_id].includes(i));
      //   if(getCity.length >0){
      //     setuser_city_id(getCity);         
      //     setFilter({...filterArray, city_id: getCity[0] ,hub_id: ""})
      //   } 
      // }else{
         
      //   if(props.user_role == 'FAE_HUB_ADMIN' || props.user_role == 'FAE_HUB_MANAGER'){          
      //     setFilter({...filterArray, hub_id: props.user_hub_id[0]})
      //   }else{
      //     setFilter({...filterArray, city_id: props.user_city_id?props.user_city_id[0]:'',hub_id: ""})
         
      //   }
      // }

      // if(props.user_city_id){        
      //   getAllCityHubsAPI(props.user_city_id[0]);
      // }
      
      // getAllBookings();

      setuser_role(props.user_role);
      setuser_hub_id(props.user_hub_id)
      setuser_city_id(props.user_city_id); 
      
      getAllCityAPI();
      if(props.user_city_id){
        getAllCityHubsAPI(props.user_city_id[0]);
      } 

      
      if(props.user_role == 'FAE_MAIN_ADMIN' || props.user_role == 'FAE_ADMIN'){
        const getCity = cityDataFilter.filter((i)=> [props.user_city_id].includes(i));
        if(getCity.length >0){        
          setFilter({...filterArray, city_id: getCity[0] ,hub_id: ""})
        }else{
          setFilter({...filterArray, city_id: "" ,hub_id: ""})
        }
      } 
    }        
  },[])

  /* useeffect for fillter booking */
  useEffect(() => {
    getAllBookings();
  }, [filterArray])

  const getAllCityAPI=()=>{
    try {
      setLoader(true);      
      dispatch(getAllCities())
      .then((res) => {
        console.log('res.data)',res.data)
        setLoader(false);
        setCityDataFilter(res.data);
      })
      .catch((error) => {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });
    } catch (error) {
      setLoader(false);
      // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }

  /* formik for assign vehicle */
  var initialValues = {
    _id:"",
    booking_id:"",
    user_id:"",
    business_client_id: "",
    business_supplier_id: "",
    from_date:"",
    to_date:"",
    battery_health: "",
    battery_1 :"",
    battery_2 :"",  
    spares: "",
    spares1: "",
    spares2: "",
    charger_1: "",
    charger_2: "",
    iot_devices: "",
    images: "",
    imageArr:[],
    city_id:user_city_id?user_city_id[0]:"",
    hub_id:"",
    inventory_id:"",
    inventory_id_old:"",
    vehicle_name:"",
    comment:"",
    status_inventory:4,
    status:false,
    created:new Date().toISOString(),
    updated:new Date().toISOString()
  };
  const validationSchema = Yup.object() 
  .shape({
    user_id: Yup.string().default(null).nullable()
    .required('Customer Name is required'),
    // business_client_id: Yup.string().default(null).nullable()
    // .required('Entity name is required'),
    business_supplier_id:Yup.string().default(null).nullable()
    .required('Supplier name is required'),
    from_date: Yup.string().default(null).nullable()
    .required('From Date is required'),
    to_date: Yup.string().default(null).nullable()
    .required('To Date is required'),
    battery_1: Yup.string().default(null).nullable()
    .required('Battery 1 is required'), 
    // battery_2: Yup.string().default(null).nullable()
    // .required('Battery 2 is required'), 
    charger_1: Yup.string().default(null).nullable()
    .required('Charger 1 is required'), 
    // charger_2: Yup.string().default(null).nullable()
    // .required('Charger 2 is required'), 
    iot_devices: Yup.string().default(null).nullable()
    .required('Iot Devices is required'),
    // spares2: Yup.string().default(null).nullable()
    // .required('Spares is required'),
    
    city_id: Yup.string().default(null).nullable()
    .required('City is required'),
    hub_id: Yup.string().default(null).nullable()
    .required('Hub is required'),
    vehicle_name: Yup.string().default(null).nullable()
    .required('Vehicle name is required'),
    inventory_id: Yup.string().default(null).nullable()
    .required('Vehicle is required'),
    
  });

  

  const submitForm =(data)=>{

    if(availabilityErrors.spares || availabilityErrors.iot_devices  || availabilityErrors.battery_1  || availabilityErrors.battery_2  || availabilityErrors.charger_1  || availabilityErrors.charger_2 ){
      return false;
    }
    // console.log('updateBookingApiCall data',data); 
    const parseData = JSON.parse(data)
    let formData = new FormData();
    for (const [key, value] of Object.entries(parseData)) {
      if(key != 'images'){
        formData.append(key, value);
      }
    }
    // add one or more of your files in FormData
    // again, the original file is located at the `originFileObj` key
   
    if(fileList.length>0){
      fileList.map((val,i)=>{
        console.log('val',val);
        if(val.uploadType == "old"){
          formData.append("old_images[]", val.name);
        }else{
          formData.append("images", val.originFileObj);          
        }  
      }) 
    } 
    if(isEditBooking){
      updateBookingApiCall(formData,parseData.booking_id)
    }else{
      createBookingApiCall(formData)
    }
    
  }
  const formik = FORMIK(initialValues,validationSchema,submitForm); 
 
  formik.handleChange = async (e) => {
   
    formik.setFieldValue(e.target.name,e.target.value);     
    setAvailabilityErrors({"spares":'',"battery_1":'',"battery_2":'',"charger_1":'',"charger_2":'',"iot_devices":''});

    if((e.target.name == "spares" || e.target.name == 'iot_devices'  || e.target.name == 'battery_1'  || e.target.name == 'battery_2'   || e.target.name == 'charger_1'  || e.target.name == 'charger_2') && e.target.value.length >0){
      
      if((e.target.value).length>0){
       
        if(e.target.name == 'battery_1' && formik.values.battery_2 == e.target.value){
          setAvailabilityErrors({...availabilityErrors,"battery_1":"Battery Already Added."});

        }else if(e.target.name == 'battery_2' && formik.values.battery_1 == e.target.value){
          setAvailabilityErrors({...availabilityErrors,"battery_2":"Battery Already Added."});

        }else if(e.target.name == 'charger_1' && formik.values.charger_2 == e.target.value){
          setAvailabilityErrors({...availabilityErrors,"charger_1":"Charger Already Added."});

        }else if(e.target.name == 'charger_2' && formik.values.charger_1 == e.target.value){
          setAvailabilityErrors({...availabilityErrors,"charger_2":"Charger Already Added."});

        }else{
          const data = await checkAvailabilityData(e.target.name, e.target.value);
        }
        
      }else{
       
        // setAvailabilityErrors({...availabilityErrors,'spares':""});
      }     
      
    }else{

    }
   
};


let addFormFields = () => {
  setSparesFields([...sparesFields, ""])
}

let removeFormFields = (i) => {
  let newFormValues = [...sparesFields];
  newFormValues.splice(i, 1);
  setSparesFields(newFormValues)
}

  /* formik for otp */
  var initialValuesOtp = {
    booked_by:props.user?props.user._id:'',
    user_id:"",
    booking_id:'',
    old_inventory_id:"",
    otp: "",
    updated:new Date().toISOString()
  };
  const validationSchemaOtp = Yup.object() 
  .shape({
    otp: Yup.string().default(null).nullable()
    .required('Please Enter Verification Code')
    .matches(/^[0-9]+$/, "Must be only digits")
    .min(6, 'Must be exactly 6 digits')
    .max(6, 'Must be exactly 6 digits'),
  });
  const submitFormOtp =(data)=>{
    verifyOtpBooking(data);    
  }
  const formikOtp = FORMIK(initialValuesOtp,validationSchemaOtp,submitFormOtp);
  // console.log('formikOtp',formikOtp.values)
  /* formik for otp end */

  const checkAvailabilityData =async (name,value,supplier_id="",city_id="",hub_id="")=>{
    const data = {
      type:name,
      value:value,
      booking_id:formik.values.booking_id,
      business_supplier_id:supplier_id?supplier_id:formik.values.business_supplier_id,
      city_id:city_id?city_id:formik.values.city_id,
      hub_id:hub_id?hub_id:formik.values.hub_id,
    }
    
    try {
      setLoader(true);      
      dispatch(checkAvailability(data))
      .then((res) => {
        setLoader(false); 
        if(res.status == true){
            if(res.type == 'spares'){
              if(res.data == 'available'){
                setAvailabilityErrors({...availabilityErrors,'spares':""});

              }else{
               
                setAvailabilityErrors({...availabilityErrors,'spares':res.message});

              }
              // formik.setFieldError('spares',res.message) ;
              
            }else if(res.type == 'iot_devices'){
              // formik.setFieldError('iot_devices',res.message) ;
              if(res.data == 'available'){
                setAvailabilityErrors({...availabilityErrors,'iot_devices':""});

              }else{
                setAvailabilityErrors({...availabilityErrors,'iot_devices':res.message});

              }
            }else if(res.type == 'charger_1'){
              // formik.setFieldError('charger_1',res.message) ;
              if(res.data == 'available'){
                setAvailabilityErrors({...availabilityErrors,'charger_1':""});

              }else{
                setAvailabilityErrors({...availabilityErrors,'charger_1':res.message});

              }
            }
            else if(res.type == 'charger_2'){
              // formik.setFieldError('charger_2',res.message) ;
              if(res.data == 'available'){
                setAvailabilityErrors({...availabilityErrors,'charger_2':""});

              }else{
                setAvailabilityErrors({...availabilityErrors,'charger_2':res.message});

              }
            }
            else if(res.type == 'battery_1'){
              // formik.setFieldError('battery_1',res.message) ;
              if(res.data == 'available'){
                setAvailabilityErrors({...availabilityErrors,'battery_1':""});

              }else{
                setAvailabilityErrors({...availabilityErrors,'battery_1':res.message});

              }

            }
            else if(res.type == 'battery_2'){
              // formik.setFieldError('battery_2',res.message) ;
              if(res.data == 'available'){
                setAvailabilityErrors({...availabilityErrors,'battery_2':""});

              }else{
                setAvailabilityErrors({...availabilityErrors,'battery_2':res.message});

              }
            }
        }else{
          HANDLE_ERROR(res.message);
        }          
      })
      .catch((error) => {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });     
    } catch (error) {
      setLoader(false);
      HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  /* create booking api call */
  const createBookingApiCall = async (data) => {
    // console.log('data',data)
    try {
      setLoader(true);      
      dispatch(create(data))
      .then((res) => {
        setLoader(false); 
        if(res.status == true){
          formikOtp.setFieldValue('user_id',res.data.user_id)
          formikOtp.setFieldValue('booking_id',res.data._id)
          formikOtp.setFieldValue('old_inventory_id',res.data.oldInventoryId);
          formikOtp.setFieldValue('otp','')
          setIsModalVisible(false);
          setIsModalVisibleOtp(true);
          HANDLE_SUCCESS(res.message); 
          formik.resetForm();
          setFileList([]);
          setUserInfo('');
         // router.push('/bookings');

          getAllBookings();
          
        }else{
          HANDLE_ERROR(res.message);
        }          
      })
      .catch((error) => {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });     
    } catch (error) {
      setLoader(false);
      HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  };
  // update booking
  const updateBookingApiCall= async (data,id) => {
    try {
      setLoader(true);      
      dispatch(updateData(data,id))
      .then((res) => {
        setLoader(false); 
        if(res.status == true){  
          formikOtp.setFieldValue('user_id',res.data.user_id)
          formikOtp.setFieldValue('booking_id',res.data._id)
          formikOtp.setFieldValue('old_inventory_id',res.data.oldInventoryId);
          formikOtp.setFieldValue('otp','')
          setIsModalVisible(false);
          setIsModalVisibleOtp(true);
          HANDLE_SUCCESS(res.message); 

          // setIsModalVisible(false);
          // HANDLE_SUCCESS(res.message); 
          formik.resetForm();
          setFileList([]);
          setUserInfo('');

           getAllBookings();

          // setTimeout(()=> {
          //   router.push('/bookings');
          // }, 5000);
          
          
        }else{
          HANDLE_ERROR(res.message);
        }          
      })
      .catch((error) => {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });     
    } catch (error) {
      setLoader(false);
      HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  /* verification of otp for create booking */
  const verifyOtpBooking = async (data) => {
    try {
      setLoader(true);      
      dispatch(bookingCheckVerifCode(data))
      .then((res) => {
        setLoader(false);  
        if(res.status == true){
          // setDataSource(res.bookingsData);
          getAllBookings();
          setIsModalVisible(false);
          setIsModalVisibleOtp(false);
          HANDLE_SUCCESS(res.message); 
          formikOtp.resetForm();
        }else{
          HANDLE_ERROR(res.message);
        }          
      })
      .catch((error) => {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });     
    } catch (error) {
      setLoader(false);
      HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  };
  const ResendOtp = async (user_id) => {
    try {
      setLoader(true);      
      dispatch(resendOtp({user_id:user_id}))
      .then((res) => {
        setLoader(false);  
        if(res.status == true){
          HANDLE_SUCCESS(res.message);
        }else{
          HANDLE_ERROR(res.message);
        }          
      })
      .catch((error) => {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });     
    } catch (error) {
      setLoader(false);
      HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }

  /* formik for assign vehicle close */

  // columns for booking list
  const columns = [ 
       {
        key:"21",
        title: 'Status',
        dataIndex: 'inventory_id', 
        render:(inventory_id)=>{
          // console.log('inventory_id',inventory_id)
          if(inventory_id.status== 4){              
            return <>
            <p className="assigned">Assigned</p>
            </>
          } else {
            return <>
              <p className="booked">Reserved</p>
            </>   
          } 
        },     
      },
      {
          key:"1",
          title: 'Booking ID',
          dataIndex: '_id', 
      },    
      {
          key:"2",
          title: 'Contact Person',
          dataIndex: 'user_id', 
          render:(user_id)=>{
              return user_id.name;    
          },     
      },
      {
          key:"3",
          title: 'Phone Number',
          dataIndex: 'user_id',
          render:(user_id)=>{
              return user_id.phone;    
          }, 
      },
      {
          key:"4",
          title: 'Email',
          dataIndex: 'user_id',
          render:(user_id)=>{
              return user_id.email;    
          },   
      },
      // {
      //     key:"5",
      //     title: 'Address',
      //     dataIndex: 'user_id',
      //     render:(user_id)=>{
      //         return user_id.address1;    
      //     },   
      // },  
      
      {
        key:"9",
        title: 'Booking From Date',
        // dataIndex: 'created',
        render:(record)=>{
            return changeDateFormat(record.from_date,"dddd, MMMM D, YYYY") ; 
               
        },
      },
      {
        key:"15",
        title: 'Booking To Date',
        render:(record)=>{
            return changeDateFormat(record.to_date,"dddd, MMMM D, YYYY") ;    
        },
      },
      {
        key:"8",
        title: 'Created Date&Time',
        dataIndex: 'created',
        render:(createdDate)=>{
            return changeDateFormat(createdDate) ; 
               
        },
      },
      {
        key:"10",
        title: 'Actions',
        dataIndex: '_id',
        render:(booking_id)=>{

            return <>
            <button type="button"  className="ant-btn ant-btn-danger" onClick={()=>OnOpenModel('booking_details',booking_id)} ><span>View More</span></button>
            </>
        }
      },
      // {(user_role == 'FAE_MAIN_ADMIN' || user_role == 'FAE_ADMIN')?
      (user_role == 'FAE_MAIN_ADMIN' || user_role == 'FAE_ADMIN' || user_role == 'FAE_CITY_ADMIN'  || user_role == 'FAE_HUB_ADMIN' )?
      {
        key:"9",
        title: '',
        render:(record)=>{
            // 
            return <>
            <button type="button"  className="ant-btn ant-btn-danger float-end" onClick={()=>OnOpenModel('booking_assign',record._id,record)} ><span>Assign</span></button>
            </>
        }
      }:{}
      
  ];
  
  
  /* open model popup for assign vehicle */
  /**
   * 
   * @param {type of model} type 
  */
   async function OnOpenModel(type,booking_id=0,record=null){
    setAvailabilityErrors({"spares":'',"battery_1":'',"battery_2":'',"charger_1":'',"charger_2":'',"iot_devices":''});

    if(type == 'booking_details'){
      const getData =  dataSource.filter((val,index)=>{
        return val._id==booking_id;
      })
      
      setBookingDetails(getData[0]);
      setIsModalVisibleDetails(true)

    }else if(type == 'booking_assign'){
      
      setFileList([]);
      const getData =  dataSource.filter((val,index)=>{
        return val._id==booking_id;
      })
      if(getData){
        
        var EditValues = {booking_id:getData[0]._id,_id:getData[0]._id,user_id:getData[0].user_id._id, business_client_id:"",business_supplier_id: getData[0].inventory_id.supplier_id,from_date:moment(getData[0].from_date),to_date:moment(getData[0].to_date), battery_health: getData[0].battery_health,battery_1 :getData[0].battery_1, battery_2 :getData[0].battery_2,  spares: getData[0].spares, spares1:getData[0].spares1,spares2:getData[0].spares2,charger_1:getData[0].charger_1,charger_2:getData[0].charger_2, iot_devices:getData[0].iot_devices, images:getData[0].images?getData[0].images:'',imageArr:getData[0].imageArr?getData[0].imageArr:'',city_id: getData[0].inventory_id.city_id,hub_id:getData[0].inventory_id.hub_id,inventory_id:getData[0].inventory_id._id,inventory_id_old:getData[0].inventory_id._id,comment:getData[0].comment,status:false, updated:new Date().toISOString(),status_inventory:4,
        };
        formik.setValues(EditValues)
        if(getData.length >0 && getData[0].imageArr && getData[0].imageArr.length >0){
          // setFileList(getData[0].imageArr)
          let getOldFiles  =[];         
         
          const getImageArr = getData[0].imageArr.map(async(image,i) => {
            // console.log('image',image)
            return {
              uid: -i,
              name: image.filename,
              status: 'done',
              url: imageType.includes(image.mimetype)?IMAGE_URL_DASH+"/"+image.path:"",
              filetype: image.mimetype,
              uploadType:"old"
            }
           })
         
           const resolved = await Promise.all(getImageArr)

           setFileList(resolved);

        }
       
        // formik.setFieldValue('user_id',getData[0].user_id._id)
        // formik.setFieldValue('booking_id',getData[0]._id)
        

        handleChangeSelect(getData[0].user_id._id,'user_id','assign_vehicle')
        handleChangeSelect(getData[0].inventory_id.city_id,'city_id','assign_vehicle')
        handleChangeSelect(getData[0].inventory_id.hub_id,'hub_id','assign_vehicle')
        handleChangeSelect(getData[0].vehicle_id.vehicle_name,'vehicle_name',"assign_vehicle")        
        handleChangeSelect(getData[0].inventory_id._id,'inventory_id','assign_vehicle')

        setVehicleNameData([])
        getAllVehiclesAPI(getData[0].inventory_id.city_id,getData[0].inventory_id.hub_id);
        setVehicleData([]);          
        getAllVehiclesAPI(getData[0].inventory_id.city_id,getData[0].inventory_id.hub_id,getData[0].vehicle_id.vehicle_name,"vehicle_name");

      }
      setIsEditBooking(true)
      setIsModalVisible(true)

      
    }else{
      // console.log('test')
      setUserInfo('');
      setFileList([]);
      setHubDataFilter([]);
      // setVehicleData([]);
      setVehicleNameData([])
      // getAllVehiclesAPI("","")
      if(props.user_city_id){        
        getAllCityHubsAPI(props.user_city_id[0]);
        getAllVehiclesAPI( props.user_city_id[0],props.user_hub_id[0]);
      } 
      formik.setValues(initialValues)
      setIsEditBooking(false)
      setIsModalVisible(true)  
         
    }
    
  } 
  // console.log('fileList',fileList)
  
  /* function for getting all booking data */
  const getAllBookings=()=>{
    try {
      setLoader(true);      
      dispatch(getAllWithFilter(filterArray))
      .then((res) => {
        setLoader(false);
        if(res.status == true){

          // HANDLE_SUCCESS(res.message);         
            setDataSource(res.data.data);
            setCityDataFilter(res.data.cityList);
            setUserData(res.data.userList);
            // setVehicleData(res.data.vehiclesList);
            setSparesInventoryData(res.data.sparesInventoryData);
            setIOtDevicesInventoryData(res.data.iotdevicesInventoryData);
            setBusinessClientData(res.data.business_clientsList);
            setBusinessSupplierData(res.data.business_SupplierList);

        }else{
          // HANDLE_ERROR(res.message);
        }  
      })
      .catch((error) => {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });     
    } catch (error) {
      setLoader(false);
      // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }

  /* Get Single booking details */
  /**
   * 
   * @param {get all details of booking using booking id} booking_id 
  */
   const getBookingDetails=(booking_id)=>{
    try {
      setLoader(true);      
      dispatch(getDetails(booking_id))
      .then((res) => {
        setLoader(false);
        if(res.status == true){
          // HANDLE_SUCCESS(res.message);
        }else{
          // HANDLE_ERROR(res.message);
        }  
      })
      .catch((error) => {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });     
    } catch (error) {
      setLoader(false);
      // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  /* search booking data on enter button */
  const searchOnEnter=(e,filterName)=>{
    const value = e.target.value;
    if(value.length >0){
      if(filterName == 'vehicle_regNo'){
        setFilter({...filterArray,  vehicle_regNo: value})
      }else{
        setFilter({...filterArray,  searchKey: value})
      }
      
    }else{
      if(filterName == 'vehicle_regNo'){
        setFilter({...filterArray,  vehicle_regNo: ''})
      }else{
        setFilter({...filterArray,  searchKey: ''})
      }      
    }
  }
  /* search booking data on click search button */
  const onSearch = (value,filterName) => {    
    if(value.length >0){
      if(filterName == 'vehicle_regNo'){
        setFilter({...filterArray,  vehicle_regNo: value})
      }else{
        setFilter({...filterArray,  searchKey: value})
      }
      
    }else{
      if(filterName == 'vehicle_regNo'){
        setFilter({...filterArray,  vehicle_regNo: ''})
      }else{
        setFilter({...filterArray,  searchKey: ''})
      }      
    }
  };
  
  /* function for onchange select data  */
  async function handleChangeSelect(value,selectType,searchType=null) {
    
    if(searchType =='assign_vehicle'){
      formik.setFieldValue(selectType,value);
      if(selectType == 'city_id'){ 
        formik.setFieldValue('hub_id',"");
        formik.setFieldValue('inventory_id',"");
        formik.setFieldValue('vehicle_name',"");
        setHubDefaultValue('') 
        setHubDataFilter([]); 
        setVehicleData([]);
        setVehicleNameData([]);    
        if(value){
          getAllCityHubsAPI(value);
          getAllVehiclesAPI(value,'')
        } 
        // await checkAvailabilityOnChange(formik.values.business_supplier_id,value,formik.values.hub_id);      
        
      }else if(selectType == 'hub_id'){ 
        setHubDefaultValue(value)  
        formik.setFieldValue('inventory_id',"");
        formik.setFieldValue('vehicle_name',"");
        setVehicleData([]);        
        setVehicleNameData([]);        
        if(value){
          getAllVehiclesAPI(formik.values.city_id,value)
        } 
        // await checkAvailabilityOnChange(formik.values.business_supplier_id,formik.values.city_id,value);      
        

      }else if(selectType == 'business_supplier_id'){
        // await checkAvailabilityOnChange(value,formik.values.city_id,formik.values.hub_id);     

        
      }
      else if(selectType == 'vehicle_name'){ 
            
        formik.setFieldValue('inventory_id',"");   
        // setVehicleNameData([]);     
        setVehicleData([]);    
        if(value){
          getAllVehiclesAPI(formik.values.city_id,formik.values.hub_id,value,'vehicle_name')
        } 

      }else if(selectType == 'user_id'){ 
        if(userData){
          let getIndex = userData.findIndex(val=>val._id==value);
         
          if(getIndex>=0){
            setUserInfo(userData[getIndex]);
          }else{
            setUserInfo('')
          }
        }
      } 

    }else{
      if(selectType == 'city'){ 
        setFilter({...filterArray,  city_id: value, hub_id: ''})
        setHubDefaultValue('') 
        setHubDataFilter([]); 
        if(value){
          getAllCityHubsAPI(value);
        }       
        
      }else if(selectType == 'start_date'){ 
        if(value != null){
          setFilter({...filterArray,  start_date:changeDateFormat(value._d, 'YYYY-MM-DD')})
          // changeDateFormat(value._d,"YYYY-MM-DD")
        }else{
          setFilter({...filterArray,  start_date:""})
        }
  
      }else if(selectType == 'end_date'){ 
        if(value != null){
          setFilter({...filterArray,  end_date: changeDateFormat(value._d, 'YYYY-MM-DD')})
        }else{
          setFilter({...filterArray,  end_date:""})
        }
  
      }else if(selectType == 'hub'){  
        setHubDefaultValue(value) 
        setFilter({...filterArray,  hub_id: value})
      } 
      /* else if(selectType == 'hub'){ 
          setFilter({...filterArray,  hub_id: value})
        } 
      } */

    }
  }

  const checkAvailabilityOnChange=async (supplier_id,city_id,hub_id)=>{
    if(formik.values.spares !='' && formik.values.spares != undefined){
      const checkSparesData = await checkAvailabilityData('spares',formik.values.spares,supplier_id,city_id,hub_id);
    }

    if(formik.values.iot_devices !='' && formik.values.iot_devices != undefined){
      const dataiot_devices = await checkAvailabilityData('iot_devices',formik.values.iot_devices,supplier_id,city_id,hub_id);
    }
   
    if(formik.values.charger_1 !='' && formik.values.charger_1 != undefined){
      const datacharger_1 = await checkAvailabilityData('charger_1',formik.values.charger_1,supplier_id,city_id,hub_id);
    }

    if(formik.values.charger_2 !='' && formik.values.charger_2 != undefined){
      const datacharger_2 = await checkAvailabilityData('charger_2',formik.values.charger_2,supplier_id,city_id,hub_id);
    }

    if(formik.values.battery_1 !='' && formik.values.battery_1 != undefined){
      const databattery_1 = await checkAvailabilityData('battery_1',formik.values.battery_1,supplier_id,city_id,hub_id);
    }
    if(formik.values.battery_2 !='' && formik.values.battery_2 != undefined){
      const databattery_2 = await checkAvailabilityData('battery_2',formik.values.battery_2,supplier_id,city_id,hub_id);
    }
  }
  /* get all hubs by city_id */
  const getAllCityHubsAPI=(city_id)=>{
    try {
      setLoader(true);      
      dispatch(allCityHubs(city_id))
      .then((res) => {
        setLoader(false);
        setHubDataFilter(res.data);
      })
      .catch((error) => {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });
    } catch (error) {
      setLoader(false);
      // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  /* get all vehicle by city_id & hub_id */
  const getAllVehiclesAPI=(city_id,hub_id,vehicle_name=null,type=null)=>{
    // console.log('vehicle_name',vehicle_name)
    // console.log('type',type)
    if(type == "vehicle_name"){
      var ApiAction = getAllInventoryFilter({'city_id':city_id,'hub_id':hub_id,'vehicle_name':vehicle_name,'filter_type':'vehicle_booking'});      
    }else{
      var ApiAction = getAllInventoryFilter({'city_id':city_id,'hub_id':hub_id,'filter_type':'vehicle_booking','vehicle_name':""});
    }

    try {
      setLoader(true);      
      dispatch(ApiAction)
      .then((res) => {
        setLoader(false);
        setVehicleNameData(res.data.vahicleNameInventoryData);        
        if(type == "vehicle_name"){
          setVehicleData(res.data.vahicleInventoryData);
        }
      })
      .catch((error) => {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });
    } catch (error) {
      setLoader(false);
      // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  
  // table row selection function
  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      // console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    },
    getCheckboxProps: (record) => ({      
      // Column configuration not to be checked
      name: record.name,
    }),
  };

  /* file upload start */
  const onChange = (event) => {
 
  };
  const onChangeUpload = ({ fileList: newFileList }) => {
    setFileList(newFileList);
  };

  const uploadImage = async options => {    
    const { onSuccess, onError, file, onProgress } = options;
    const fmData = new FormData();
    const config = {
      headers: { "content-type": "multipart/form-data" },
      onUploadProgress: event => {
        const percent = Math.floor((event.loaded / event.total) * 100);
        setProgress(percent);
        if (percent === 100) {
          setTimeout(() => setProgress(0), 1000);
        }
        onProgress({ percent: (event.loaded / event.total) * 100 });
      }
    };
    fmData.append("image", file);
    try {
      const res = await axios.post(
        BASE_URL_CORE+FILE_UPLOAD,
        fmData,
        config
      );

      onSuccess("Ok");
      console.log("server res: ", res);
    } catch (err) {
      console.log("Eroor: ", err);
      const error = new Error("Some error");
      onError({ err });
    }
  };
  useEffect(() => {
      formik.setFieldValue('images',fileList);
  }, [fileList])

  /* preview uploaded file */
  const onPreview = async file => {
    let src = file.url;
    if (!src) {
      src = await new Promise(resolve => {
        const reader = new FileReader();
        reader.readAsDataURL(file.originFileObj);
        reader.onload = () => resolve(reader.result);
      });
    }
    const image = new Image();
    image.src = src;
    setPreviewVisible(true);
    setPreviewTitle(file.name || file.url.substring(file.url.lastIndexOf('/') + 1));
    setPreviewImage(src);

    // const imgWindow = window.open(src);
    // imgWindow.document.write(image.outerHTML);
  };

  /* Autocomplete code start */

  const onSearchAutoComplete =async(searchText,type) => {   
    if(type == 'spares1'){
      let filtered =  sparesInventoryData.filter(element => {
        let part_number = element.part_number;
        if(part_number){
          return part_number.toLowerCase().indexOf(searchText) > -1;
        }else{
          return part_number;
        }
        
      });
  
      let filteredset = filtered.map(element => {
        let part_number = element.part_number;
        return { label: part_number, value: part_number, id: element._id,type: type };
      });
      
      setOptionsSpares(
        !searchText ? [] : filteredset,
      );
    }else if(type == 'iot_devices'){
      let filtered =  iotDevicesInventoryData.filter(element => {
        let model_of_device = element.iot_devicesDetails.model_of_device;
        let imei = element.iot_devicesDetails.imei;
        let make = element.iot_devicesDetails.make;
        let device_serial_number = element.iot_devicesDetails.device_serial_number;
        if(model_of_device){
          return model_of_device.toLowerCase().indexOf(searchText) > -1;
        }else{
          return model_of_device;
        }
        
      });
  
      let filteredset = filtered.map(element => {
        let model_of_device = element.iot_devicesDetails.model_of_device;
        return { label: model_of_device, value: model_of_device, id: element._id,type: type };
      });
      
      setOptionsIotDevices(
        !searchText ? [] : filteredset,
      );
    }
    
  };

  const onSelectAutoComplete = (value,data) => {  
    setSparesValues({spares1:'',spares2:''}); 
           
    // if(data && data.type == 'spares1'){      
    //   setSparesValues({...sparesValues,spares1:value});
    //   formik.setFieldValue('spares1',data.id);
    // }
    // if(data && data.type == 'spares2'){      
    //   setSparesValues({...sparesValues,spares1:value});
    //   formik.setFieldValue('spares2',data.id);
    // }
    // if(data && data.type == 'iot_devices'){      
    //   setSparesValues({...sparesValues,spares1:value});
    //   formik.setFieldValue('iot_devices',data.id);
    // }
  };
  const clearSpares=(event)=>{
    // console.log('onSclearSpareselect', event);
  }

  const onChangeAutoComplete = (value,type) => {
    formik.setFieldValue(type,value);
  };
 
  /* Autocomplete code end */


  const disabledDate = (current) => {
    // Can not select days before today and today    
    return current && current < moment().subtract(1, 'day');
  };
  


            
  
  return (
    <>
      <Loader loading={loader} />
      <Row>
        <Col span={12}> <h2 className="mb-4 text-danger fw-bold fs-3">Bookings List</h2></Col>
        <Col span={12}> 
          {(user_role == 'FAE_MAIN_ADMIN' || user_role == 'FAE_ADMIN' || user_role == 'FAE_CITY_ADMIN'  || user_role == 'FAE_HUB_ADMIN')?
          <button type="button"  className="ant-btn ant-btn-danger float-end" onClick={()=>OnOpenModel('booking')} ><span>Assign</span></button>
          :''}
        </Col>
      </Row>     
     
      <div className="p-4 shadow rounded-6 bg-white">
      <div className="col-12 d-flex mb-3">
        <div className="col-3">
          <Search placeholder="Search by Phone Number or email" allowClear id="searchByPhone" onSearch={(searchText)=>onSearch(searchText,'searchKey')} onPressEnter={(e)=>searchOnEnter(e,'searchKey')}   />
        </div>
        <div className="col-3">
          <Search placeholder="Search Vehicle by registration number"  id="searchByVahicleRegistrationNo" allowClear  onSearch={(searchText)=>onSearch(searchText,'vehicle_regNo')} onPressEnter={(e)=>searchOnEnter(e,'vehicle_regNo')}   />
        </div>
        <div className="col-3 d-flex">
          {user_role != 'FAE_HUB_ADMIN' && user_role != 'FAE_HUB_MANAGER'?
          <div className="col-6 ms-2">            
            <Select  value={filterArray.city_id} name="city_filter" className="w-100" id="city_filter" onChange={(e)=>handleChangeSelect(e,'city')}>
              {(user_role == 'FAE_MAIN_ADMIN' || user_role == 'FAE_ADMIN'?
                <Option value="">Select City</Option>:'')}
              
              {
                cityDataFilter.map(function(item, i){
                
                  if(user_role == 'FAE_CITY_ADMIN' && user_city_id.includes(item._id)){
                    return <Option key={item._id} value={item._id} >{item.name}</Option>
                  }else if(user_role != 'FAE_CITY_ADMIN'){
                    return <Option key={item._id} value={item._id} >{item.name}</Option>
                  }                      
                })
                
              }
            </Select>
          </div>
          :''}
          <div className="col-6">
            <Select value={filterArray.hub_id} style={{ width: 120 }} className="w-100" name="hub_filter" id="hub_filter"  onChange={(e)=>handleChangeSelect(e,'hub')}>
              {
                (user_role == 'FAE_MAIN_ADMIN' || user_role == 'FAE_ADMIN' || user_role == 'FAE_CITY_ADMIN')?( <Option value="">Select Hub</Option>):""}
              {
                hubDataFilter.map(function(item, i){
                  if(user_role == 'FAE_MAIN_ADMIN' || user_role == 'FAE_ADMIN'){
                    return <Option key={item._id} value={item._id} >{item.title}</Option>
                  }else{  
                    if((user_role == 'FAE_HUB_ADMIN' || user_role == 'FAE_HUB_MANAGER')){
                      if(user_hub_id && user_hub_id.includes(item._id) ){                  
                        return <Option key={item._id} value={item._id} >{item.title}</Option>  
                      }
                    }else{
                      return <Option key={item._id} value={item._id} >{item.title}</Option>
                    } 
                  }                    
                })
              }
            </Select>
          </div>
        </div>
        <div className="col-3">          
          <DatePicker   format='DD-MM-YYYY'  placeholder="Booking Start Date"  defaultValue={moment(new Date(), 'YYYY-MM-DD')}   className="ms-2" name="start_date_filter" id="start_date_filter" onClear={(e)=>handleChangeSelect(e,'start_date')} onChange={(e)=>handleChangeSelect(e,'start_date')} />
          <DatePicker  format='DD-MM-YYYY' placeholder="Booking End Date"  defaultValue={""}  className=""  name="end_date_filter" id="end_date_filter" onClear={(e)=>handleChangeSelect(e,'end_date')}  onChange={(e)=>handleChangeSelect(e,'end_date')} />
          {/* showTime format='DD-MM-YYYY HH:mm:ss' */}

        </div>
      </div>   
        {/* <button type="button" className="btn btn-danger float-end mb-3 " onClick={()=>onAddClient()} >Add New</button> */}
        <div>
          <Table  scroll={{x: 1000}} onChange={onChange} columns={columns} dataSource={dataSource}  />
          {/* rowSelection={{
          type: 'checkbox',
          ...rowSelection,
        }} */}
        </div>
        {/* ReAssign Model start */}
        <Modal
        title="Assign Vehicle"
        destroyOnClose={true}
        maskClosable={false}
        centered
        visible={isModalVisible}
        onOk={() => {
          formik.submitForm(); 
          // setIsModalVisibleOtp(true)
          // setIsModalVisible(false)
        }}
        okType="danger"
        onCancel={() =>{
          setIsModalVisible(false)
          setIsEditing(false)
        }}
        cancelText="Close"
        okText={isEditBooking?"Request OTP":"Send OTP"}
        width={1000}
      >
      <form onSubmit={formik.handleSubmit} className=""  encType="multipart/form-data">
        <Row>
        <Col span={24}  className="mb-2">
            <label htmlFor="floatingInput" className="pb-2"  style={{'fontWeight':'600'}}>Customer Name</label>
            <Col span={24} >
              <Select   name="user_id" id="user_id" className="w-100" onChange={(e)=>handleChangeSelect(e,'user_id','assign_vehicle')}  value={userData.length>0?formik.values.user_id:''}  onBlur={formik.handleBlur} disabled={isEditBooking} >                   
                <Option value="">Select Customer</Option>
                {
                  userData.map(function(item, i){
                    return <Option key={item._id} value={item._id} >{item.name}</Option>
                  })
                }
              </Select>
              {formik.errors.user_id && formik.touched.user_id && (
                <div className="input-feedback invailid_feedback">{formik.errors.user_id}</div>
              )}  
            </Col>
        </Col>
        <Col span={24}  className="mb-3 mt-3">
          <Descriptions title="User Info" layout="vertical" labelStyle={{'fontWeight':'600',"paddingBottom":"5px"}}>
            <Descriptions.Item label="Name">{userInfo?userInfo.name:''}</Descriptions.Item>
            <Descriptions.Item label="Email">{userInfo?userInfo.email:''}</Descriptions.Item>
            <Descriptions.Item label="Mobile">{userInfo?userInfo.phone:''}</Descriptions.Item>
          </Descriptions>
        </Col>
        <Col span={24} className="mb-2" >
          <Col span={24}  className="mb-2">
              <label htmlFor="floatingInput" className="pb-2"  style={{'fontWeight':'600'}}>Business Supplier</label>
              <Col span={24} >
                <Select  name="business_supplier_id" id="business_supplier_id" className="w-100" onChange={(e)=>handleChangeSelect(e,'business_supplier_id','assign_vehicle')} value={formik.values.business_supplier_id}  onBlur={formik.handleBlur} >  

                  <Option value="" >Select Business Supplier</Option>
                  {
                    businessSupplierData.map(function(item, i){
                      return <Option value={item._id} >{item.supplier_name}</Option>
                    })
                  }
                </Select>
                {formik.errors.business_supplier_id && formik.touched.business_supplier_id && (
                  <div className="input-feedback invailid_feedback">{formik.errors.business_supplier_id}</div>
                )} 
              </Col>
          </Col>
        </Col>
        <Col span={24} className="mb-2" >
          <Row>
            <Col span={6}  >
              <label htmlFor="floatingInput" className="pb-2"  style={{'fontWeight':'600'}}>For City</label>
              <Col span={24} >
                <Select  name="city_id" className="w-100" onChange={(e)=>handleChangeSelect(e,'city_id','assign_vehicle')}   value={formik.values.city_id}  onBlur={formik.handleBlur} >                   
                  {(user_role == 'FAE_MAIN_ADMIN' || user_role == 'FAE_ADMIN'?
                  <Option value="">Select City</Option>:'')}
                    {
                      cityDataFilter.map(function(item, i){
                        if(user_role == 'FAE_CITY_ADMIN' && user_city_id.includes(item._id)){
                          return <Option value={item._id} >{item.name}</Option>
                        }else if(user_role != 'FAE_CITY_ADMIN'){
                          return <Option value={item._id} >{item.name}</Option>
                        }
                      })
                    }
                </Select>
                {formik.errors.city_id && formik.touched.city_id && (
                  <div className="input-feedback invailid_feedback">{formik.errors.city_id}</div>
                )}
              </Col>
            </Col>
            <Col span={6} className="mb-3" >
              <label htmlFor="floatingInput" className="pb-2"  style={{'fontWeight':'600'}}>For Hub</label>
              <Col span={24} >
                <Select  name="hub_id" className="w-100" value={formik.values.hub_id}   onBlur={formik.handleBlur} onChange={(e)=>handleChangeSelect(e,'hub_id','assign_vehicle')} >                   
                  <Option value="">Select Hub</Option>
                  {
                    hubDataFilter.map(function(item, i){
                        return <Option key={item._id} value={item._id} >{item.title}</Option>
                    })
                  }
                </Select>
                {formik.errors.hub_id && formik.touched.hub_id && (
                  <div className="input-feedback invailid_feedback">{formik.errors.hub_id}</div>
                )}
              </Col> 
            </Col>
            <Col span={6} className="mb-3" >
              <label htmlFor="floatingInput" className="pb-2"  style={{'fontWeight':'600'}}>Vehicle Name</label>
              <Col span={24} >
                <Select value={formik.values.vehicle_name} name="vehicle_name" id="vehicle_name" className="w-100" onChange={(e)=>handleChangeSelect(e,'vehicle_name','assign_vehicle')}   onBlur={formik.handleBlur} >                   
                  <Option value="">Select Vehicle</Option>
                  {
                    vehicleNameData.map(function(item, i){                      
                      return <Option key={item._id} value={item.vehicle_name } >{item.vehicle_name} </Option>                        
                    })
                  }
                </Select>
                {formik.errors.vehicle_name && formik.touched.vehicle_name && (
                  <div className="input-feedback invailid_feedback">{formik.errors.vehicle_name}</div>
                )}
              </Col> 
            </Col>
            <Col span={6} className="mb-3" >
              <label htmlFor="floatingInput" className="pb-2"  style={{'fontWeight':'600'}}>Vehicle</label>
              <Col span={24} >
                <Select  name="inventory_id" id="inventory_id" className="w-100" onChange={(e)=>handleChangeSelect(e,'inventory_id','assign_vehicle')} value={formik.values.inventory_id}  onBlur={formik.handleBlur} >                   
                  <Option value="">Select Vehicle</Option>
                  {
                    vehicleData.map(function(item, i){
                        if(item.vehicleDetails.book_status == 0 || formik.values.inventory_id_old == item._id){
                          return <Option key={item._id} value={item._id} >{item.vehicleDetails.vehicle_name} - {item.vehicleDetails.vehicle_regNo} </Option>
                        }
                    })
                  }
                </Select>
                {formik.errors.inventory_id && formik.touched.inventory_id && (
                  <div className="input-feedback invailid_feedback">{formik.errors.inventory_id}</div>
                )}
              </Col> 
            </Col>
          </Row>
        </Col>
        <Col span={24} className="mb-2" >
          <Row>                
            <Col span={12}  className="mb-2">
                <label htmlFor="floatingInput" className="pb-2"  style={{'fontWeight':'600'}}>From Date</label>
                <DatePicker  format='DD-MM-YYYY' disabledDate={disabledDate} placeholder="From Date"   className="w-100"  name="from_date" id="from_date" onClear={(e)=>handleChangeSelect(e,'from_date','assign_vehicle')}  onChange={(e)=>handleChangeSelect(e,'from_date','assign_vehicle')} value={formik.values.from_date}  onBlur={formik.handleBlur} />
                {formik.errors.from_date && formik.touched.from_date && (
                  <div className="input-feedback invailid_feedback">{formik.errors.from_date}</div>
                )} 
              </Col>
            <Col span={12}  className="mb-2">
              <label htmlFor="floatingInput" className="pb-2"  style={{'fontWeight':'600'}}>To Date</label>
              <DatePicker   format='DD-MM-YYYY'  placeholder="To Date" disabledDate={disabledDate}  className="w-100" name="to_date" id="to_date" onClear={(e)=>handleChangeSelect(e,'to_date','assign_vehicle')} onChange={(e)=>handleChangeSelect(e,'to_date','assign_vehicle')} value={formik.values.to_date}  onBlur={formik.handleBlur} />
              {formik.errors.to_date && formik.touched.to_date && (
                <div className="input-feedback invailid_feedback">{formik.errors.to_date}</div>
              )} 
              {/* showTime format='DD-MM-YYYY HH:mm:ss' */}
            </Col>
          </Row>
        </Col>
        <Col span={24} className="mb-2" >
          <Row>                
            <Col span={12}  className="mb-2" >
              <label htmlFor="floatingInput" className="pb-2"  style={{'fontWeight':'600'}}>Spares</label>
              <Col span={24} className="" >
              <Input
                  type="text"                  
                  id="spares"
                  name="spares"
                  placeholder="Spares"
                  value={formik.values.spares}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur} 
                  className={
                  formik.errors.spares && formik.touched.spares
                      ? "text-input error form-control rounded-4 "
                      : "text-input form-control rounded-4 "
                  }
                />
              {/* {sparesFields.map((element, index) => (
                <>
                <Row  className="mt-2">
                    <Col span={21} className="" >
                      <Input
                          key={index}
                          type="text"                  
                          id="spares"
                          name="spares"
                          placeholder="Spares"
                          value={formik.values.spares}
                          onChange={formik.handleChange}
                          onBlur={formik.handleBlur} 
                          className={
                          formik.errors.spares && formik.touched.spares
                              ? "text-input error form-control rounded-4 "
                              : "text-input form-control rounded-4 "
                          }
                        />
                    </Col>
                    <Col span={3} className="" >
                      {
                        index ? 
                          <MinusSquareFilled className={"float-end  me-2"} style={{ fontSize: '32px', color: '#ff4d4f' }}  onClick={() => removeFormFields(index)} />
                        : <PlusSquareFilled className={"float-end  me-2"} style={{ fontSize: '32px', color: '#ff4d4f' }}  onClick={() => addFormFields()} />
                      }
                    </Col>
                </Row>                
              </>  
              ))} */}

                {availabilityErrors.spares && (
                    <div className="input-feedback invailid_feedback">{availabilityErrors.spares}</div>
                )}

                {/* {formik.errors.spares && formik.touched.spares && (
                    <div className="input-feedback invailid_feedback">{formik.errors.spares}</div>
                )} */}
                {/* <AutoComplete
                  options={optionsSpares}
                  style={{
                    width: '100%',
                  }}
                  allowClear={true}
                  onSelect={onSelectAutoComplete}
                  onSearch={(searchText)=>onSearchAutoComplete(searchText,'spares1')}
                  onChange={(value)=>onChangeAutoComplete(value,"spares")}
                  placeholder="Search Spares..."
                  name="spares1"
                  id="spares1"
                  // value={sparesValues.spares1}
                  value={formik.values.spares}
                  // onChange={formik.handleChange}
                  onBlur={formik.handleBlur} 
                  onClear={(e)=>{clearSpares(e)}}
                /> */}
                
              </Col>
            </Col>  
            <Col span={12}  className="mb-2">
              <label htmlFor="floatingInput" className="pb-2"  style={{'fontWeight':'600'}}>Iot Devices</label>
              <Input
                    type="text"                  
                    id="iot_devices"
                    name="iot_devices"
                    placeholder="Iot Devices"
                    value={formik.values.iot_devices}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur} 
                    className={
                    formik.errors.iot_devices && formik.touched.iot_devices
                        ? "text-input error form-control rounded-4 "
                        : "text-input form-control rounded-4 "
                    }
                />
                {/* <AutoComplete
                  options={optionsIotDevices}
                  style={{
                    width: '100%',
                  }}
                  allowClear={true}
                  onSelect={onSelectAutoComplete}
                  onSearch={(searchText)=>onSearchAutoComplete(searchText,'iot_devices')}
                  onChange={(value)=>onChangeAutoComplete(value,"iot_devices")}
                  placeholder="Search IOT devices..."
                  name="iot_devices"
                  id="iot_devices"
                  value={formik.values.iot_devices}
                  // onChange={formik.handleChange}
                  onBlur={formik.handleBlur} 
                  onClear={(e)=>{clearSpares(e)}}
                /> */}
                {availabilityErrors.iot_devices && (
                    <div className="input-feedback invailid_feedback">{availabilityErrors.iot_devices}</div>
                )}
                {/* {formik.errors.iot_devices && formik.touched.iot_devices && (
                    <div className="input-feedback invailid_feedback">{formik.errors.iot_devices}</div>
                )}    */}
            </Col> 
          </Row>
        </Col>
        <Col span={24} className="mb-2" >
          <Row>                
            <Col span={12}  className="mb-2">
              <label htmlFor="floatingInput" className="pb-2"  style={{'fontWeight':'600'}}>Battery 1</label>
              <Input
                    type="text"                  
                    id="battery_1"
                    name="battery_1"
                    placeholder="Name"
                    value={formik.values.battery_1}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur} 
                    className={
                    formik.errors.battery_1 && formik.touched.battery_1
                        ? "text-input error form-control rounded-4 "
                        : "text-input form-control rounded-4 "
                    }
                />
                {availabilityErrors.battery_1 && (
                    <div className="input-feedback invailid_feedback">{availabilityErrors.battery_1}</div>
                )}
                {/* {formik.errors.battery_1 && formik.touched.battery_1 && (
                    <div className="input-feedback invailid_feedback">{formik.errors.battery_1}</div>
                )}    */}
            </Col>
            <Col span={12}  className="mb-2">
              <label htmlFor="floatingInput" className="pb-2"  style={{'fontWeight':'600'}}>Battery 2</label>
              <Input
                    type="text"                  
                    id="battery_2"
                    name="battery_2"
                    placeholder="Name"
                    value={formik.values.battery_2}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur} 
                    className={
                    formik.errors.battery_2 && formik.touched.battery_2
                        ? "text-input error form-control rounded-4 "
                        : "text-input form-control rounded-4 "
                    }
                />
                {availabilityErrors.battery_2 && (
                    <div className="input-feedback invailid_feedback">{availabilityErrors.battery_2}</div>
                )}
                {/* {formik.errors.battery_2 && formik.touched.battery_2 && (
                    <div className="input-feedback invailid_feedback">{formik.errors.battery_2}</div>
                )}    */}
            </Col>
          </Row>
        </Col>
        <Col span={24} className="mb-2" >
          <Row>  
            <Col span={12}  className="mb-2">
              <label htmlFor="floatingInput" className="pb-2"  style={{'fontWeight':'600'}}>Charger 1</label>
              <Input
                    type="text"                  
                    id="charger_1"
                    name="charger_1"
                    placeholder="Name"
                    value={formik.values.charger_1}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur} 
                    className={
                    formik.errors.charger_1 && formik.touched.charger_1
                        ? "text-input error form-control rounded-4 "
                        : "text-input form-control rounded-4 "
                    }
                />
                 {availabilityErrors.charger_1 && (
                    <div className="input-feedback invailid_feedback">{availabilityErrors.charger_1}</div>
                )}
                {/* {formik.errors.charger_1 && formik.touched.charger_1 && (
                    <div className="input-feedback invailid_feedback">{formik.errors.charger_1}</div>
                )}    */}
            </Col>
            <Col span={12}  className="mb-2">
              <label htmlFor="floatingInput" className="pb-2"  style={{'fontWeight':'600'}}>Charger 2</label>
              <Input
                    type="text"                  
                    id="charger_2"
                    name="charger_2"
                    placeholder="Name"
                    value={formik.values.charger_2}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur} 
                    className={
                    formik.errors.charger_2 && formik.touched.charger_2
                        ? "text-input error form-control rounded-4 "
                        : "text-input form-control rounded-4 "
                    }
                />
                {availabilityErrors.charger_2 && (
                    <div className="input-feedback invailid_feedback">{availabilityErrors.charger_2}</div>
                )}
                {/* {formik.errors.charger_2 && formik.touched.charger_2 && (
                    <div className="input-feedback invailid_feedback">{formik.errors.charger_2}</div>
                )}    */}
            </Col> 
          </Row>
        </Col>
        <Col span={24} className="mb-3">
          <label htmlFor="floatingInput"  className="pb-2"  style={{'fontWeight':'600'}}>Comment</label>
          <TextArea rows={4} placeholder="" name="comment" id="comment"  value={formik.values.comment} onChange={formik.handleChange}  onBlur={formik.handleBlur} />
        </Col>
        <Col span={24} className="mb-3">
          <label htmlFor="floatingInput" className="pb-2"  style={{'fontWeight':'600'}}>Upload Photos/ Video</label>
          {/* <ImgCrop rotate> */}
              <Upload
                customRequest={uploadImage}
                multiple={true}
                listType="picture-card"
                fileList={fileList}
                onChange={onChangeUpload}
                onPreview={onPreview}
                accept="image/*,video/*"
              >
                {fileList.length <15 && '+ Upload'}
                
              </Upload>
            {/* </ImgCrop> */}
            <Modal
              visible={previewVisible}
              title={previewTitle}
              footer={null}
              onCancel={()=>{
                setPreviewVisible(false)
              }}
            >
              <img alt="example" style={{ width: '100%' }} src={previewImage} />
            </Modal>
          </Col>          
        </Row>
      </form>
      </Modal>

        {/* ReAssign Model end */}

        {/* send otp poup start */}       
        <Modal title="Verification Code" visible={isModalVisibleOtp} 
        onOk={() => {
          formikOtp.submitForm()
        }}
        okType="danger"
        destroyOnClose={true}
        maskClosable={false}
        onCancel={() =>{
          setIsModalVisibleOtp(false)
        }}
        cancelText="Close"
        okText={"Save"} >
          <form onSubmit={formikOtp.handleSubmit} className=""  enctype="multipart/form-data">
            <Input.Group size="large">
              <Row gutter={8}>
                <Col span={24}>
                  <Input  name="otp" id="otp" value={formikOtp.values.otp} onChange={formikOtp.handleChange} onBlur={formikOtp.handleBlur} className="w-100 mb-2" maxLength={6} />
                  {formikOtp.errors.otp && formikOtp.touched.otp && (
                    <div className="input-feedback invailid_feedback pt-2">{formikOtp.errors.otp}</div>
                  )}
                </Col>
                <Col span={24}>
                  <p className="form-check-label">
                    Don't receive the code?
                    <a className="text-danger ms-2" onClick={()=>ResendOtp(formikOtp.values.user_id)}>Resend</a>
                  </p>
                </Col>
              </Row>
            </Input.Group>
          </form>
        </Modal>
        {/* send otp poup end */}
        {/* bokking details popup start */}
        <Modal title="Booking Details" visible={isModalVisibleDetails} 
        onOk={() => {
          setIsModalVisibleDetails(false)
        }}
        okType="danger"
        destroyOnClose={true}
        maskClosable={false}
        onCancel={() =>{
          setIsModalVisibleDetails(false)
        }}
        cancelText="Close" >
          <Descriptions title="" column={{ xxl:2, xl:2, lg:2, md: 2, sm: 2, xs: 1 }} labelStyle={{fontWeight:"600"}}  >
            <Descriptions.Item label="Booking ID">{bookingDetails?bookingDetails._id:''}</Descriptions.Item>            
            <Descriptions.Item label="Transaction ID">{bookingDetails&&bookingDetails.bookingstransactions?bookingDetails.bookingstransactions[0].txn_id:''}</Descriptions.Item>
            <Descriptions.Item label="Payment Mode">{bookingDetails&&bookingDetails.bookingstransactions?bookingDetails.bookingstransactions[0].payment_mode:''}</Descriptions.Item>
            <Descriptions.Item label="Booking Amount">{bookingDetails?bookingDetails.amount:''}</Descriptions.Item>
            <Descriptions.Item label="Discount Amount ">{bookingDetails?bookingDetails.discount_amount:''}</Descriptions.Item>
            <Descriptions.Item label="Tax Amount">{bookingDetails?bookingDetails.tax_amount:''}</Descriptions.Item>
            <Descriptions.Item label="Total Amount">{bookingDetails?bookingDetails.total_amount:''}</Descriptions.Item>            
          </Descriptions>
        </Modal>
        {/* bokking details popup end */}
        
      </div>
    </>
  );
}

const mapStateToProps = (state, ownProps = {}) => {
  const { isLoggedIn,user,user_city_id,user_role ,user_hub_id } = state.auth;
  const { inventoryData } = state.inventory;
  return {
    user,
    isLoggedIn,
    user_city_id,
    user_hub_id,
    user_role,
    inventoryData
  };
}

export default connect(mapStateToProps)(BookingsComp);
