import React, { useState,useEffect } from "react";
import Link from "next/link";
import { connect,useDispatch, useSelector } from "react-redux";
import { useRouter } from 'next/router'
import { logout } from "../../Redux/Action/auth";
import {CONFIRM_ALERT} from "../../provider/confirmAlertProvider";
import { Logout } from "../../Redux/Action/auth";
import Loader from "../../Components/loader";
import { HANDLE_SUCCESS, HANDLE_ERROR, HTTP_SERVICE_CALL } from "../../provider/ApiProvider";
import ActiveLink from "../Header/Activelink";

function Sidebar(props) {  
  const [loader, setLoader] = useState(false);  
  const router = useRouter(); 
  const dispatch = useDispatch();
  const { user: currentUser } = props;  
  const { isLoggedIn } = useSelector(state => state.auth);  
  const [user_role, setuser_role] = useState('');  
  const [user_city_id, setuser_city_id] = useState('');

  useEffect(()=>{  
    if (!isLoggedIn) {
      router.push('/login');
    }else{
      setuser_role(props.user_role);
      setuser_city_id(props.user_city_id);
    }
  },[]);

  
  const logoutApp=()=>{
    const title="Are you sure?";
    const message="You want to logout?";
    CONFIRM_ALERT(title,message,logoutSubmit); 
    
  }
  const logoutSubmit=()=>{    
    // console.log('logoutSubmit');
    try {
      setLoader(true);      
      dispatch(Logout());       
      setTimeout(() => {
        HANDLE_SUCCESS("You have successfully logged out!"); 
        setLoader(false);
        router.push('/login');
      }, 1000); 
     
    } catch (error) {
      // console.log('error',error);
      setLoader(false);
      HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    } 

  }

  return (
    <>
      <Loader loading={loader} />
      <div
        id="layoutSidenav_nav"
        className="layoutSidenav_nav position-relative"
      >
        <nav
          className="sb-sidenav accordion sb-sidenav-white shadow-theme bg-white shadow-sm"
          id="sidenavAccordion"
        >
          <div className="team px-4 pt-4">
            <div className="member border-bottom">            
              <img
                src="img/User-Icon.svg"
                className="rounded-circle shadow"
                alt
              />
              
              <h4 className="text-center text-theme text-capitalize ">{currentUser?currentUser.data?currentUser.data.first_name:currentUser.first_name:""} {currentUser?currentUser.data?currentUser.data.last_name:currentUser.last_name:""}</h4>
              <p className="text-center">{/* CTO */}</p>
              {/* <div className="social">
                <a>
                  <i className="bi bi-twitter" />
                </a>
                <a>
                  <i className="bi bi-facebook" />
                </a>
                <a>
                  <i className="bi bi-instagram" />
                </a>
              </div> */}
            </div>
          </div>

          <div className="sb-sidenav-menu">
            <div className="nav">
              <ActiveLink activeClassName="activesidebar"  href="/business-supplier"  className="cursor-pointer">
                  <div className="fae nav-link mx-4 my-2 fw-bold rounded-1 cursor-pointer">
                    <div className="sb-nav-link-icon">
                      {/* <i className="fas fa-tachometer-alt" /> */}
                    </div>
                    Business Supplier
                  </div>
                </ActiveLink>
                <ActiveLink activeClassName="activesidebar"  href="/business-client"  className="cursor-pointer" >
                  <div className="fae nav-link mx-4 my-2 fw-bold rounded-1 cursor-pointer ">
                    <div className="sb-nav-link-icon">
                      {/* <i className="fas fa-tachometer-alt" /> */}
                    </div>
                    Business Client
                  </div>
                </ActiveLink>
                <ActiveLink activeClassName="activesidebar"  href="/fae-operated-city"  className="cursor-pointer" >
                  <div className="fae nav-link mx-4 my-2 fw-bold rounded-1 cursor-pointer ">
                    <div className="sb-nav-link-icon">
                      {/* <i className="fas fa-tachometer-alt" /> */}
                    </div>
                    Fae operated cities
                  </div>
                </ActiveLink>
                {/* <ActiveLink  activeClassName="activesidebar" href="/inventory"  className="cursor-pointer" >
                  <div className="fae nav-link mx-4 my-2 fw-bold rounded-1 cursor-pointer ">
                    <div className="sb-nav-link-icon">
                    </div>
                    Inventory
                  </div>
                </ActiveLink> */}
              <ActiveLink activeClassName="activesidebar"  href="/referals"  className="cursor-pointer">
                <div className="fae nav-link mx-4 my-2 fw-bold rounded-1 cursor-pointer">
                  <div className="sb-nav-link-icon">
                  </div>
                  Referrals
                </div>
              </ActiveLink>
              <ActiveLink  activeClassName="activesidebar" href="/users"  className="cursor-pointer">
                <div className="fae nav-link mx-4 my-2 fw-bold rounded-1 cursor-pointer">
                  <div className="sb-nav-link-icon">
                  </div>
                  Users
                </div>
              </ActiveLink>
              <ActiveLink activeClassName="activesidebar"  href="/dashboard-users"  className="cursor-pointer">
                  <div className="fae nav-link mx-4 my-2 fw-bold rounded-1 cursor-pointer"  style={(user_role != null && (user_role=='FAE_ADMIN' || user_role=='FAE_MAIN_ADMIN'))?{'display':''}:{'display':'none'}}>
                    <div className="sb-nav-link-icon">
                      {/* <i className="fas fa-tachometer-alt" /> */}
                    </div>
                    Dashboard Users
                  </div>
                </ActiveLink> 
                           
              {/* <Link href="/hub"  className="cursor-pointer">
                <div className="fae nav-link mx-4 my-2 fw-bold rounded-1 cursor-pointer">
                  <div className="sb-nav-link-icon">
                    <i className="fas fa-tachometer-alt" />
                  </div>
                  Hub
                </div>
              </Link> */}              
              <div onClick={()=>logoutApp()}  className="cursor-pointer" >
                <div className="fae nav-link mx-4 my-2 fw-bold rounded-1 cursor-pointer ">
                  <div className="sb-nav-link-icon">
                    {/* <i className="fas fa-tachometer-alt" /> */}
                  </div>
                  Logout
                </div>
              </div>
            </div>
          </div>
          <div className="sb-sidenav-footer">
            <div className="small">Logged in as:</div>
            FAEBIKES
          </div>
        </nav>
      </div>
    </>
  );
}



const mapStateToProps = (state, ownProps = {}) => { 
  const { isLoggedIn,user,user_city_id,user_role } = state.auth;
  return {
    user,
    isLoggedIn,
    user_city_id,
    user_role
  };
}

export default connect(mapStateToProps)(Sidebar);