import React , { useState, useEffect, useCallback } from "react";
import { connect,useDispatch, useSelector } from "react-redux";
import { useRouter } from 'next/router';
import { HANDLE_SUCCESS, HANDLE_ERROR } from "../../provider/ApiProvider";
import Loader from "../loader";
import { createInventory,getAllInventory,getDetailsInventory,deleteInventory,updateInventory} from "../../Redux/Action/inventory";
import { createHub,getAllHubs,allCityHubs,deleteHub,updateHub} from "../../Redux/Action/hubs";
import { BASE_URL, POST, IS_LOADING,ADMIN_SIGN_IN,BASE_URL_CORE,IMAGE_URL_DASH,FILE_UPLOAD } from "../../provider/EndPoints";

/* ant design */
import 'antd/dist/antd.css';
import { Table, Tag, Space,Modal, Form, Input, Radio , Upload, message,Button ,Descriptions,DatePicker} from 'antd';
import {EditOutlined,DeleteOutlined,InboxOutlined,EyeOutlined} from '@ant-design/icons'
// formik import 
import * as Yup from "yup";
import {VALIDATION_SCHEMA,FORMIK} from "../../provider/FormikProvider";

import { CSVLink, CSVDownload } from "react-csv";
import { vehicleCsvHeaders,vehicleCsvData } from "./vehicleCsvData";
import { sparesCsvHeaders,sparesCsvData } from "./sparesCsvData";
import { iotDevicesCsvHeaders,iotDevicesCsvData } from "./iotDevicesCsvData";

const UploadCSVComp=(props)=> {
  // console.log('props',props) 
  const router = useRouter();  
  const [loader, setLoader] = useState(false);  
  const dispatch = useDispatch();
  const { isLoggedIn } = useSelector(state => state.auth);    
  const [form] = Form.useForm();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [modalTitle,setModalTitle]= useState('Add');
  const [modelWidth,setModelWidth]= useState(500);
  const [cityData,setCityData]= useState([]);
  const [businessSupplierData,setBusinessSupplierData]= useState([]);
  const [hubData,setHubData]= useState([]);
  const [csvFile, setCsvFile] = useState();
  const [csvArray, setCsvArray] = useState([]);
  const [inventoryDetails, setInventoryDetails] = useState('');
  const [downloadText, setDownloadText] = useState('Download Vehicle CSV');
  const [downloadType, setDownloadType] = useState('vehicle');
  const [downloadFileName, setDownloadFileName] = useState('inventoryVehicleSample.csv');
  const [csvHeaders, setCsvHeaders] = useState('');
  const [csvData, setCsvData] = useState([]);
  const [progress, setProgress] = useState(0);
  

  useEffect(() => {
    setCsvHeaders(vehicleCsvHeaders);
    setCsvData(vehicleCsvData);    
  }, [])

  
  useEffect(() => {  
    if(props.inventoryData.citiesList){
      setCityData(props.inventoryData.citiesList);
    } 
    if(props.inventoryData.businessSupplierList){
      setBusinessSupplierData(props.inventoryData.businessSupplierList);
    } 
    
  }, [props.inventoryData])
  /* formik code start city */
  var initialValues = {
    city_id:null,
    hub_id: null,
    business_supplier_id: null,
    type: 'vehicle',     
    csv_file_data: null,     
    createType: 'csv',     
    created:new Date(),
    updated:new Date()
  };
  
  const validationSchema = Yup.object() 
  .shape({
    business_supplier_id: Yup.string().default(null).nullable()
    .required('Business Supplier is required'),
    city_id: Yup.string().default(null).nullable()
    .required('City is required'),
    hub_id: Yup.string().default(null).nullable()
    .required('Hub is required'),
    type: Yup.string().default(null).nullable()
    .required('Type is required'),
    csv_file_data: Yup.array().default(null).nullable()
    .required('Please Upload Csv File')
  });   

  const submitFormCsv =(data)=>{
    console.log('data submitForm' ,data);  
    callAPI(data);   
  }
  const formikCsv = FORMIK(initialValues,validationSchema,submitFormCsv);

  
  useEffect(() => {
    formikCsv.setFieldValue("type",props.tabActiveType); 
    setCSvFiles(props.tabActiveType);
  }, [props.tabActiveType])

  useEffect(() => {
    formikCsv.setFieldValue("business_supplier_id",props.filterInventory.business_supplier_id); 
    formikCsv.setFieldValue("city_id",props.filterInventory.city_id); 
    formikCsv.setFieldValue("hub_id",props.filterInventory.hub_id); 
 
    getAllCityHubsAPI(props.filterInventory.city_id);
 
 }, [props.filterInventory])

 formikCsv.handleChange = (e) => {
    if(e.target.name == 'city_id'){      
      const city_id_selected = e.target.value;
      formikCsv.setFieldValue("city_id",city_id_selected); 
      formikCsv.setFieldValue("hub_id",''); 
      getAllCityHubsAPI(city_id_selected);
    }else if(e.target.name == 'type'){ 
      formikCsv.setFieldValue(e.target.name,e.target.value);
      // setDownloadType(e.target.value);
      setCSvFiles(e.target.value);     
      // if(e.target.value == 'spares'){
      //   setDownloadText("Download Spares CSV")
      //   setDownloadFileName('inventorySparesSample.csv');
      //   setCsvHeaders(sparesCsvHeaders);
      //   setCsvData(sparesCsvData);
      // }else if(e.target.value == 'iot_devices'){
      //   setDownloadText("Download IOT Devices CSV")
      //   setDownloadFileName('inventoryIotDevicesSample.csv');
      //   setCsvHeaders(iotDevicesCsvHeaders);
      //   setCsvData(iotDevicesCsvData);
      // }else{
      //   setDownloadText("Download Vehicle CSV")
      //   setDownloadFileName('inventoryVehicleSample.csv');
      //   setCsvHeaders(vehicleCsvHeaders);
      //   setCsvData(vehicleCsvData);
      // }
       // 
    }else{

      formikCsv.setFieldValue(e.target.name,e.target.value); 
    }
   
  };
  
  const setCSvFiles=(value)=>{
    setDownloadType(value);
    if(value == 'spares'){
      setDownloadText("Download Spares CSV")
      setDownloadFileName('inventorySparesSample.csv');
      setCsvHeaders(sparesCsvHeaders);
      setCsvData(sparesCsvData);
    }else if(value == 'iot_devices'){
      setDownloadText("Download IOT Devices CSV")
      setDownloadFileName('inventoryIotDevicesSample.csv');
      setCsvHeaders(iotDevicesCsvHeaders);
      setCsvData(iotDevicesCsvData);
    }else{
      setDownloadText("Download Vehicle CSV")
      setDownloadFileName('inventoryVehicleSample.csv');
      setCsvHeaders(vehicleCsvHeaders);
      setCsvData(vehicleCsvData);
    }
  }
  const getAllCityHubsAPI=(city_id)=>{
    try {
      setLoader(true);      
      dispatch(allCityHubs(city_id))
      .then((res) => {
        console.log('res.data)',res.data)
        setLoader(false);
        setHubData(res.data);
      })
      .catch((error) => {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });
    } catch (error) {
      setLoader(false);
      // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  /* formik code  */
  /* CSV file upload code start */
  
  const { Dragger } = Upload;
  const uploadProps = {
    accept:".csv,.xlsx,.xls",
    name: 'file',
    multiple: false,
    // customRequest:{uploadImage},
    action:BASE_URL_CORE+FILE_UPLOAD,
    onChange(info) {
        console.log('info',info);
      const { status } = info.file;
      if (status !== 'uploading') {
        console.log(info.file, info.fileList);
      }
      if (status === 'done') {  
        // console.log('info.file',info.file);
        setCsvFile(info.file.originFileObj);
        const file = csvFile;
        const reader = new FileReader();
        reader.onload = function(e) {
            const text = e.target.result;
            console.log('text',text);
            processCSV(text)
        }
        reader.readAsText(info.file.originFileObj);
        // setUploadedFile(info.file)        
        message.success(`${info.file.name} file uploaded successfully.`);
      } else if (status === 'error') {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
    onDrop(e) {
      console.log('Dropped files', e.dataTransfer.files);
    },
    onRemove(e) {
        console.log('Dropped files onRemove');
    },
    progress: {
        strokeColor: {
            '0%': '#108ee9',
            '100%': '#87d068',
        },
        strokeWidth: 3,
        format: percent => `${parseFloat(percent.toFixed(2))}%`,
    },
  };

  const uploadImage = async options => {    
    const { onSuccess, onError, file, onProgress } = options;
    const fmData = new FormData();
    const config = {
      headers: { "content-type": "multipart/form-data" },
      onUploadProgress: event => {
        const percent = Math.floor((event.loaded / event.total) * 100);
        setProgress(percent);
        if (percent === 100) {
          setTimeout(() => setProgress(0), 1000);
        }
        onProgress({ percent: (event.loaded / event.total) * 100 });
      }
    };
    fmData.append("image", file);
    try {
      const res = await axios.post(
        BASE_URL_CORE+FILE_UPLOAD,
        fmData,
        config
      );

      onSuccess("Ok");
      console.log("server res: ", res);
    } catch (err) {
      console.log("Eroor: ", err);
      const error = new Error("Some error");
      onError({ err });
    }
  };
  const processCSV = (str, delim=',') => {
    console.log('str',str);
    const headers = str.slice(0,str.indexOf('\n')).split(delim);
    const rows = str.slice(str.indexOf('\n')+1).split('\n');
    // console.log("str.indexOf('\n')",str.slice(441));
    console.log('headers',headers);
    console.log('rows',rows);

    const newArray = rows.map( row => {
        console.log('row',row);
        const values = row.split(delim);
        console.log('values',values);
        const eachObject = headers.reduce((obj, header, i) => {
          console.log('obj',obj)
          console.log('header',header)
          console.log('i',i)
          obj[header] = values[i];
          return obj;
        }, {})
        // console.log('eachObject',eachObject);
        return eachObject;
              
    })
    console.log('newArray',newArray);
    setCsvArray(newArray)
    formikCsv.setFieldValue("csv_file_data",newArray); 
}
  /* CSV file upload code end */
  const callAPI = async (data) => {
    console.log('data',data)
    try {
      setLoader(true);      
      dispatch(createInventory(data))
      .then((res) => {
        console.log('res',res);
        setLoader(false); 
        if(res.status == true){
          // const vahicleInventoryData = res.data.vahicleInventoryData;
          // const vahicleInventoryData = res.data.vahicleInventoryData;
          // const vahicleInventoryData = res.data.vahicleInventoryData;
          setIsModalVisible(false);
          HANDLE_SUCCESS(res.message);
          // formikCsv.setValues(initialValues);  
          // form.resetFields()
          formikCsv.setFieldValue("csv_file_data",null); 
        }else{
          HANDLE_ERROR(res.message);
        }          
      })
      .catch((error) => {
        setLoader(false);
        HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });     
    } catch (error) {
      setLoader(false);
      HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }

  };
  const downloadCSVSample=()=>{
     if(downloadType == 'spares'){

     }else if(downloadType == 'iot_devices'){

    }else{

    }
  }
  
  /* formik code end */
  const onAdd=(type)=>{
    if(!(props.filterInventory  && props.filterInventory.business_supplier_id  && props.filterInventory.city_id && props.filterInventory.hub_id)){
        HANDLE_ERROR("Please Select Business supplier, City and Hub");
        return false;
    }
    // getAllInventoryData();
    setModalTitle("Add New Inventory (CSV)");
    setModelWidth(600)
    setIsModalVisible(true);  
  }

  const getAllInventoryData=()=>{
    try {
      setLoader(true);      
      dispatch(getAllInventory())
      .then((res) => {
        setLoader(false);
        setCityData(res.data.citiesList);
        setBusinessSupplierData(res.data.businessSupplierList);
        // setHubData(res.data.allHubs);
      })
      .catch((error) => {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });
    } catch (error) {
      setLoader(false);
      // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  return (
    <>     

      <Loader loading={loader} />      
      {/* <button type="button" className="btn btn-danger float-end mb-3 " onClick={()=>onAdd('csv')} >Upload CSV</button> */}
      <Button type="danger" className="float-end mb-3" onClick={()=>onAdd('csv')} >Upload CSV</Button>

      
        
      <Modal title={modalTitle}  
      visible={isModalVisible}
      destroyOnClose="true"
      centered="true"
      okText="Save"
      okType="danger"
      width={modelWidth}
      onOk={()=>{
        // console.log('forminkscv')
        formikCsv.submitForm();      
      }} 
      onCancel={()=>{
        setIsModalVisible(false)
        formikCsv.setValues(initialValues);  
        // form.resetFields()
      }} >
         {/* csv form start */}
          <form onSubmit={formikCsv.handleSubmit} className=""  >
            <div className="rounded-6 bg-white "> 
              {/* <div className="form-floating mb-3 col-12 me-3">
                <select className={
                    formikCsv.errors.business_supplier_id && formikCsv.touched.business_supplier_id
                        ? "form-select error"
                        : "form-select"
                    }  id="business_supplier_id" name="business_supplier_id" aria-label="Business Supplier" value={formikCsv.values.business_supplier_id}
                    onChange={formikCsv.handleChange}
                    onBlur={formikCsv.handleBlur} >
                    <option value="" selected  >Select</option>
                    {
                    businessSupplierData.map(function(item, i){
                        return <option value={item._id} >{item.supplier_name}</option>
                    })
                    }
                </select>
                <label for="floatingSelect">Choose Supplier</label>
                {formikCsv.errors.business_supplier_id && formikCsv.touched.business_supplier_id && (
                <div className="input-feedback invailid_feedback">{formikCsv.errors.business_supplier_id}</div>
                )}
              </div>       */}
              {/* <div className="d-flex">
                  <div className="form-floating mb-3 col-6 me-3">
                      <select className={
                          formikCsv.errors.city_id && formikCsv.touched.city_id
                              ? "form-select error"
                              : "form-select"
                          }  id="city_id" name="city_id" aria-label="City" value={formikCsv.values.city_id}
                          onChange={formikCsv.handleChange}
                          onBlur={formikCsv.handleBlur} disabled >
                          {
                          cityData.map(function(item, i){
                              return <option value={item._id} selected={formikCsv.values.city_id==item._id?true:false} >{item.name}</option>
                          })
                          }
                      </select>
                      <label for="floatingSelect">City</label>
                      {formikCsv.errors.city_id && formikCsv.touched.city_id && (
                      <div className="input-feedback invailid_feedback">{formikCsv.errors.city_id}</div>
                      )}
                  </div>  
                  <div className="form-floating mb-3 col-6">
                      <select className={
                          formikCsv.errors.hub_id && formikCsv.touched.hub_id
                              ? "form-select error"
                              : "form-select"
                          }  id="hub_id" name="hub_id" aria-label="Hub" value={formikCsv.values.hub_id}
                          onChange={formikCsv.handleChange}
                          onBlur={formikCsv.handleBlur} disabled={props.filterInventory.hub_id!=""?true:false}>
                          <option value="" selected  >Select</option>
                          {
                          hubData.map(function(item, i){
                              return <option value={item._id} selected={formikCsv.values.city_id==item._id?true:false} >{item.title}</option>
                          })
                          }
                      </select>
                      <label for="floatingSelect">Hub</label>
                      {formikCsv.errors.hub_id && formikCsv.touched.hub_id && (
                      <div className="input-feedback invailid_feedback">{formikCsv.errors.hub_id}</div>
                      )}
                  </div>  
              </div> */}
              <div className="form-floating mb-3 my-5">           
                  <div className="form-check form-check-inline">
                      <input className={
                          formikCsv.errors.type && formikCsv.touched.type
                          ? "error form-check-input"
                          : "form-check-input"
                      } value="vehicle"
                      checked={formikCsv.values.type =='vehicle'}
                      onChange={formikCsv.handleChange}
                      onBlur={formikCsv.handleBlur} type="radio" name="type" id="type1"  />
                      <label className="form-check-label" htmlFor="inlineRadio1">Vehicle</label>
                  </div>
                  <div className="form-check form-check-inline" >
                      <input className={
                          formikCsv.errors.type && formikCsv.touched.type
                          ? "error form-check-input"
                          : "form-check-input"
                      } type="radio" name="type" id="type2" value="spares"
                      checked={formikCsv.values.type == 'spares'}
                      onChange={formikCsv.handleChange}
                      onBlur={formikCsv.handleBlur} />
                      <label className="form-check-label" htmlFor="inlineRadio2">Spares</label>
                  </div> 
                  <div className="form-check form-check-inline" >
                      <input className={
                          formikCsv.errors.type && formikCsv.touched.type
                          ? "error form-check-input"
                          : "form-check-input"
                      } type="radio" name="type" id="type3" value="iot_devices"
                      checked={formikCsv.values.type == 'iot_devices'}
                      onChange={formikCsv.handleChange}
                      onBlur={formikCsv.handleBlur} />
                      <label className="form-check-label" htmlFor="inlineRadio2">IOT devices</label>
                  </div> 

                  {formikCsv.errors.type && formikCsv.touched.type && (
                  <div className="input-feedback invailid_feedback">{formikCsv.errors.type}</div>
                  )}
                 
                  <Button type="danger" className="float-end" >{/*  onClick={()=>downloadCSVSample()} */}
                    
                    <CSVLink data={csvData} headers={csvHeaders} filename={downloadFileName} >{downloadText}</CSVLink>
                    {/* <CSVDownload data={csvData} target="_blank" /> */}
                  </Button>
              </div>  

              <div className="col-12 mt-5 mb-5">
                <Dragger {...uploadProps}  >
                    <p className="ant-upload-drag-icon">
                    <InboxOutlined />
                    </p>
                    <p className="ant-upload-text">Click or drag file to this area to upload</p>
                    <p className="ant-upload-hint">
                    Support for a single or bulk upload. Strictly prohibit from uploading company data or other
                    band files
                    </p>
                </Dragger>
                    
                {formikCsv.errors.csv_file_data && formikCsv.touched.csv_file_data && (
                  <div className="input-feedback invailid_feedback">{formikCsv.errors.csv_file_data}</div>
                )}
              </div>     
             
            </div>    
          </form> 
          {/* csv form end */}

      </Modal>
      {/* add modal end */}
     
    </>
  );
}
const removeDoubleQuotes = (stringValue) => {
  const result = stringValue.replaceAll("\"", "");
  // const result = stringValue.substring(1, str.length - 1);
  return result;
}

const mapStateToProps = (state, ownProps = {}) => {
  console.log(state) // state
   /*console.log(ownProps) // {}*/
  const { user } = state.auth;
  const { inventoryData } = state.inventory;
  return {
    user,
    inventoryData
  };
}

export default connect(mapStateToProps)(UploadCSVComp);

