import React , { useState, useEffect, useCallback } from "react";
import { connect,useDispatch, useSelector } from "react-redux";
import { useRouter } from 'next/router';
import { HANDLE_SUCCESS, HANDLE_ERROR, } from "../../provider/ApiProvider";
import Loader from "../loader";
import { createInventory,getAllInventory,getDetailsInventory,deleteInventory,updateInventory,isEditModalInventory} from "../../Redux/Action/inventory";
import { createHub,getAllHubs,allCityHubs,deleteHub,updateHub} from "../../Redux/Action/hubs";
/* ant design */
import 'antd/dist/antd.css';
import { Table, Tag, Space,Modal, Form, Input, Radio , Upload, message,Button ,Descriptions,DatePicker} from 'antd';
import {EditOutlined,DeleteOutlined,InboxOutlined,EyeOutlined} from '@ant-design/icons'
// formik import 
import * as Yup from "yup";
import {VALIDATION_SCHEMA,FORMIK} from "../../provider/FormikProvider";

const AddManualInventoryComp=(props)=> {
//   console.log('AddManualInventoryComp props',props) 
  const router = useRouter();  
  const [loader, setLoader] = useState(false);  
  const dispatch = useDispatch();
  const { isLoggedIn } = useSelector(state => state.auth);   
  const [form] = Form.useForm();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [modalTitle,setModalTitle]= useState('Add');
  const [modelWidth,setModelWidth]= useState(500);
  const [businessSupplierData,setBusinessSupplierData]= useState([]);
  const [cityData,setCityData]= useState([]);
  const [hubData,setHubData]= useState([]);
  const [inventoryType,setInventoryType]= useState('vehicle');
  
  /* formik code start */   
  var initialValuesManual = {business_supplier_id: null,city_id:null,hub_id: null,type: 'vehicle',  business_supplier: null, vehicle_name: null,vehicle_model: null,vehicle_color: null, vehicle_regNo: null,  manufacture_date: null,date_of_sale: null,date_of_use: null,chassis_no: null,moter_engine_no: null,battery_no: null, battery_no_2: null,controller_number: null, charger_no: null, charger_no_2: null, key_no: null, scooter_invoice: null,scooter_reg_date: null, scooter_reg_upto: null,insurance_company: null, policy_number_insurance: null,date_policy_expiry: null, exp_date: null,  permit_no: null, sgst: null,cgst: null, iot_device: null, model: null, quantity: 0,amount: 0, advance_amount: 0,book_status:0,createType: 'manual', created:new Date(),updated:new Date()
  };
  const validationSchemaManual = Yup.object() 
  .shape({
    business_supplier_id: Yup.string().default(null).nullable()
    .required('Business Supplier is required'),
    city_id: Yup.string().default(null).nullable()
    .required('City is required'),
    hub_id: Yup.string().default(null).nullable()
    .required('Hub is required'),
    type: Yup.string().default(null).nullable()
    .required('Type is required'),
  });   
/* validation for spares */
var initialValuesManualSpares = {business_supplier_id: null,city_id:null,hub_id: null,type: 'spares',  part_type: "spares",  part_number: null, supplier_description: null,image_url: null,quantity: null, uom: null,  hsn: null,gst: null,dealer_sale_price: null,dlp: null,sale_rate: null,mrp: null, model: null,createType: 'manual',assigned_status:0, created:new Date(),updated:new Date()
};

/* validation for iot devices */
var initialValuesManualIotDevices = {business_supplier_id: null,city_id:null,hub_id: null,type: 'iot_devices',  device_serial_number: null, make: null,model_of_device: null,imei: null, provided_by: null,  consignmentTrackingNumber_CourierDocket: null,CustomerOrderNumber: null,DispatchDetailsRemarks: null,WarrantyStartdate: null,Warrantyterms: null,WarrantyEnddate: null, VF_MSISDN: null, VF_ICCID: null, VF_IMSI: null, circle: null, history_remark: null, device_status: null, sim_status: null,createType: 'manual',assigned_status:0, created:new Date(),updated:new Date()
};

const submitFormManual =(data)=>{
    // console.log('data submitForm' ,data);  
    callAPI(data);   
}
const formikManual = FORMIK(initialValuesManual,validationSchemaManual,submitFormManual);

useEffect(() => {
//    console.log('props.vehicleEditData',props.vehicleEditData)     
   updateModalData(props.vehicleEditData)   
}, [props.vehicleEditData])

const updateModalData=(vehicleEditData)=>{
    dispatch(isEditModalInventory(vehicleEditData));
}



useEffect(() => {
    console.log('props.EditRecord',props.EditRecord)
    setIsEditing(props.EditRecord.isEditing);
    if(props.vehicleEditData.data){
        if(props.EditRecord.isEditing == true){
            if(props.vehicleEditData.data.type == 'vehicle'){
                var setValues = {_id: props.vehicleEditData.data._id,vehicle_id: props.vehicleEditData.data.vehicle_id,
                    business_supplier_id: props.vehicleEditData.data.supplier_id,city_id: props.vehicleEditData.data.city_id,hub_id:  props.vehicleEditData.data.hub_id,type: 'vehicle',  business_supplier:  props.vehicleEditData.data.vehicleDetails.business_supplier, vehicle_name: props.vehicleEditData.data.vehicleDetails.vehicle_name,vehicle_model:  props.vehicleEditData.data.vehicleDetails.vehicle_model,vehicle_color:  props.vehicleEditData.data.vehicleDetails.vehicle_color, vehicle_regNo:  props.vehicleEditData.data.vehicleDetails.vehicle_regNo,  manufacture_date:  props.vehicleEditData.data.vehicleDetails.manufacture_date,date_of_sale:  props.vehicleEditData.data.vehicleDetails.date_of_sale,date_of_use: props.vehicleEditData.data.vehicleDetails.date_of_use,chassis_no: props.vehicleEditData.data.vehicleDetails.chassis_no,moter_engine_no: props.vehicleEditData.data.vehicleDetails.moter_engine_no ,battery_no: props.vehicleEditData.data.vehicleDetails.battery_no , battery_no_2: props.vehicleEditData.data.vehicleDetails.battery_no_2 ,controller_number:  props.vehicleEditData.data.vehicleDetails.controller_number, charger_no:  props.vehicleEditData.data.vehicleDetails.charger_no, charger_no_2:  props.vehicleEditData.data.vehicleDetails.charger_no_2, key_no:  props.vehicleEditData.data.vehicleDetails.key_no, scooter_invoice:  props.vehicleEditData.data.vehicleDetails.scooter_invoice,scooter_reg_date: props.vehicleEditData.data.vehicleDetails.scooter_reg_date, scooter_reg_upto:  props.vehicleEditData.data.vehicleDetails.scooter_reg_upto,insurance_company:  props.vehicleEditData.data.vehicleDetails.insurance_company, policy_number_insurance:  props.vehicleEditData.data.vehicleDetails.policy_number_insurance,date_policy_expiry: props.vehicleEditData.data.vehicleDetails.date_policy_expiry, exp_date: props.vehicleEditData.data.vehicleDetails.exp_date,  permit_no: props.vehicleEditData.data.vehicleDetails.permit_no, sgst: props.vehicleEditData.data.vehicleDetails.sgst,cgst:  props.vehicleEditData.data.vehicleDetails.cgst, iot_device: props.vehicleEditData.data.vehicleDetails.iot_device, model: props.vehicleEditData.data.vehicleDetails.model, quantity:  props.vehicleEditData.data.vehicleDetails.quantity,amount: props.vehicleEditData.data.vehicleDetails.amount, advance_amount: props.vehicleEditData.data.vehicleDetails.advance_amount,createType: 'manual',book_status:props.vehicleEditData.data.vehicleDetails.book_status,  created:new Date(),updated:new Date()
                };
                
            }else if(props.vehicleEditData.data.type == 'spares'){
                var setValues = {_id: props.vehicleEditData.data._id,spares_id: props.vehicleEditData.data.spares_id,
                    business_supplier_id: props.vehicleEditData.data.supplier_id,city_id: props.vehicleEditData.data.city_id,hub_id:  props.vehicleEditData.data.hub_id,type: 'spares',part_type:props.vehicleEditData.data.sparesDetails.part_type,  part_number: props.vehicleEditData.data.sparesDetails.part_number, supplier_description: props.vehicleEditData.data.sparesDetails.supplier_description,image_url: props.vehicleEditData.data.sparesDetails.image_url,quantity: props.vehicleEditData.data.sparesDetails.quantity, uom: props.vehicleEditData.data.sparesDetails.uom,  hsn: props.vehicleEditData.data.sparesDetails.hsn,gst:  props.vehicleEditData.data.sparesDetails.gst,dealer_sale_price: props.vehicleEditData.data.sparesDetails.dealer_sale_price,dlp: props.vehicleEditData.data.sparesDetails.dlp,sale_rate:  props.vehicleEditData.data.sparesDetails.sale_rate,mrp: props.vehicleEditData.data.sparesDetails.mrp, model: props.vehicleEditData.data.sparesDetails.model,createType: 'manual',assigned_status:props.vehicleEditData.data.sparesDetails.assigned_status,  created:new Date(),updated:new Date()
                };
                
            }else if(props.vehicleEditData.data.type == 'iot_devices'){
                var setValues = {_id: props.vehicleEditData.data._id,iot_device_id: props.vehicleEditData.data.iot_device_id,
                    business_supplier_id: props.vehicleEditData.data.supplier_id,city_id: props.vehicleEditData.data.city_id,hub_id:  props.vehicleEditData.data.hub_id,type: 'iot_devices',device_serial_number: props.vehicleEditData.data.iot_devicesDetails.device_serial_number, make: props.vehicleEditData.data.iot_devicesDetails.make,model_of_device: props.vehicleEditData.data.iot_devicesDetails.model_of_device,imei: props.vehicleEditData.data.iot_devicesDetails.imei, provided_by: props.vehicleEditData.data.iot_devicesDetails.provided_by,  consignmentTrackingNumber_CourierDocket: props.vehicleEditData.data.iot_devicesDetails.consignmentTrackingNumber_CourierDocket,CustomerOrderNumber: props.vehicleEditData.data.iot_devicesDetails.CustomerOrderNumber,DispatchDetailsRemarks: props.vehicleEditData.data.iot_devicesDetails.DispatchDetailsRemarks,WarrantyStartdate: props.vehicleEditData.data.iot_devicesDetails.WarrantyStartdate,Warrantyterms: props.vehicleEditData.data.iot_devicesDetails.Warrantyterms,WarrantyEnddate: props.vehicleEditData.data.iot_devicesDetails.WarrantyEnddate, VF_MSISDN: props.vehicleEditData.data.iot_devicesDetails.VF_MSISDN, VF_ICCID: props.vehicleEditData.data.iot_devicesDetails.VF_ICCID, VF_IMSI: props.vehicleEditData.data.iot_devicesDetails.VF_IMSI, circle: props.vehicleEditData.data.iot_devicesDetails.circle, history_remark: props.vehicleEditData.data.iot_devicesDetails.history_remark, device_status: props.vehicleEditData.data.iot_devicesDetails.device_status, sim_status: props.vehicleEditData.data.iot_devicesDetails.sim_status,createType: 'manual',assigned_status:props.vehicleEditData.data.iot_devicesDetails.assigned_status,  created:new Date(),updated:new Date()
                };
                
            }else{
                var setValues =initialValuesManual;
            }
            formikManual.setValues(setValues);
            setInventoryType(props.vehicleEditData.data.type);        
            onAdd('editManual');
        }else{        
            formikManual.setValues(initialValuesManual)
        }
    
    }
      
},[props.EditRecord])


useEffect(() => {
    formikManual.setFieldValue("type",props.tabActiveType); 
    setInventoryType(props.tabActiveType); 
},[props.tabActiveType])


useEffect(() => {
    // console.log('props.filterInventory',props.filterInventory)
    formikManual.setFieldValue("business_supplier_id",props.filterInventory.business_supplier_id); 
    formikManual.setFieldValue("city_id",props.filterInventory.city_id); 
    formikManual.setFieldValue("hub_id",props.filterInventory.hub_id); 
 
    getAllCityHubsAPI(props.filterInventory.city_id);
 
 }, [props.filterInventory])


formikManual.handleChange = (e) => {
    // console.log('formikManual e',e);
    if(e.target.name == 'city_id'){      
        const city_id_selected = e.target.value;
        formikManual.setFieldValue("city_id",city_id_selected); 
        formikManual.setFieldValue("hub_id",""); 
        getAllCityHubsAPI(city_id_selected);

    }else if(e.target.name == 'type'){ 
        if(e.target.value == 'spares'){
            formikManual.setValues(initialValuesManualSpares);            

        }else if(e.target.value == 'iot_devices'){
            formikManual.setValues(initialValuesManualIotDevices) ;

        }else{
            formikManual.setValues(initialValuesManual);
        }

        setInventoryType(e.target.value);
        formikManual.setFieldValue(e.target.name,e.target.value);
        formikManual.setFieldValue("business_supplier_id",props.filterInventory.business_supplier_id); 
        formikManual.setFieldValue("city_id",props.filterInventory.city_id); 
        formikManual.setFieldValue("hub_id",props.filterInventory.hub_id); 

    }else{
        formikManual.setFieldValue(e.target.name,e.target.value); 
    }
};



const getAllCityHubsAPI=(city_id)=>{
    try {
        setLoader(true);      
        dispatch(allCityHubs(city_id))
        .then((res) => {
        setLoader(false);
        setHubData(res.data);
        })
        .catch((error) => {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
        });
    } catch (error) {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
}
  /* formik code  */
const callAPI = async (data) => {
    try {
        setLoader(true);  
        if(props.EditRecord && props.EditRecord.isEditing == true){
            var setApiFunction = updateInventory(data,props.EditRecord.data._id);
        }else{
            var setApiFunction = createInventory(data);            
        }    
        dispatch(setApiFunction)
        .then((res) => {
        setLoader(false); 
        if(res.status == true){
            updateModalData({'isEditing':false,'type':"", 'data':null})
            // const allInventoryData = res.data.allInventoryData;
            setIsModalVisible(false);
            HANDLE_SUCCESS(res.message); 
            
            // formikManual.resetForm();
        }else{
            HANDLE_ERROR(res.message);
        }          
        })
        .catch((error) => {
        setLoader(false);
        HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
        });     
    } catch (error) {
        setLoader(false);
        HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }

};
  
  
/* formik code end */
const onAdd=(type)=>{     
    if(type == 'editManual'){

        setIsEditing(true);
        // getAllInventoryData();   
        setModalTitle("Edit Inventory");
        setModelWidth(1000)
        setIsModalVisible(true); 

        
    }else{
        // setInventoryType('vehicle')
        setIsEditing(false);
        
        if(!(props.filterInventory  && props.filterInventory.business_supplier_id  && props.filterInventory.city_id && props.filterInventory.hub_id)){
            HANDLE_ERROR("Please Select Business supplier, City and Hub");
            return false;
        }
        // getAllInventoryData();   
        setModalTitle("Add New Inventory (Manual)");
        setModelWidth(1000)
        setIsModalVisible(true); 

        if(inventoryType == 'spares'){
            formikManual.setValues(initialValuesManualSpares);

        }else if(inventoryType == 'iot_devices'){
            formikManual.setValues(initialValuesManualIotDevices);

        }else{
            formikManual.setValues(initialValuesManual);
        }

        formikManual.setFieldValue("business_supplier_id",props.filterInventory.business_supplier_id); 
        formikManual.setFieldValue("city_id",props.filterInventory.city_id); 
        formikManual.setFieldValue("hub_id",props.filterInventory.hub_id);

        
    }
    
}
 
const getAllInventoryData=()=>{
    try {
      setLoader(true);      
      dispatch(getAllInventory())
      .then((res) => {
        setLoader(false);
        setCityData(res.data.citiesList);
        setBusinessSupplierData(res.data.businessSupplierList);
        // setHubData(res.data.allHubs);
      })
      .catch((error) => {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });
    } catch (error) {
      setLoader(false);
      // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
}
  
function onChange(pagination, filters, sorter, extra) {
    // console.log('params', pagination, filters, sorter, extra);
}
  
  return (
    <>
        <Loader loading={loader} />  
        {/* <button type="button" className="btn btn-danger float-end mb-3 me-3 " onClick={()=>onAdd('manual')} >Add Manual</button> */}

        <Button type="danger" className="float-end mb-3 me-3" onClick={()=>onAdd('manual')} >Add Manual</Button>
        {/* add modal start */}  
        <Modal title={modalTitle}  
        visible={isModalVisible}
        destroyOnClose="true"
        centered="true"
        okText="Save"
        okType="danger"
        width={modelWidth}
        onOk={()=>{
            formikManual.submitForm()                  
        }} 
        onCancel={()=>{
            setIsModalVisible(false)
            form.resetFields();
        // Modal.destroyAll();
        }} >
            
            {/* manual add form start */}
            <form onSubmit={formikManual.handleSubmit} className="" >
                <div className="p-4 rounded-6 bg-white ">                      
                    <div className="form-floating mb-3 mt-2 col-12">           
                        <div className="form-check form-check-inline">
                            <input className={
                                formikManual.errors.type && formikManual.touched.type
                                ? "error form-check-input"
                                : "form-check-input"
                            } value="vehicle"
                            checked={formikManual.values.type =='vehicle'}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur} type="radio" name="type" id="type1" disabled={isEditing?true:false}  />
                            <label className="form-check-label" htmlFor="type1">Vehicle</label>
                        </div>
                        <div className="form-check form-check-inline" >
                            <input className={
                                formikManual.errors.type && formikManual.touched.type
                                ? "error form-check-input"
                                : "form-check-input"
                            } type="radio" name="type" id="type2" value="spares"
                            checked={formikManual.values.type == 'spares'}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur} disabled={isEditing?true:false} />
                            <label className="form-check-label" htmlFor="type2">Spares</label>
                        </div> 
                        <div className="form-check form-check-inline" >
                            <input className={
                                formikManual.errors.type && formikManual.touched.type
                                ? "error form-check-input"
                                : "form-check-input"
                            } type="radio" name="type" id="type3" value="iot_devices"
                            checked={formikManual.values.type == 'iot_devices'}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}  disabled={isEditing?true:false} />
                            <label className="form-check-label" htmlFor="type3">IOT devices</label>
                        </div> 

                        {formikManual.errors.type && formikManual.touched.type && (
                        <div className="input-feedback invailid_feedback">{formikManual.errors.type}</div>
                        )}
                    </div> 
                    <div  className={inventoryType=='vehicle'?'row manualAddForm':'row manualAddForm d-none'}>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            id="vehicle_name"
                            name="vehicle_name"
                            placeholder="Vehicle Name"
                            value={formikManual.values.vehicle_name}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.vehicle_name && formikManual.touched.vehicle_name
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Vehicle Name </label>
                        {formikManual.errors.vehicle_name && formikManual.touched.vehicle_name && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.vehicle_name}</div>
                        )}
                        </div>   
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="vehicle_model"
                            id="vehicle_model"
                            placeholder="Vehicle Model"
                            value={formikManual.values.vehicle_model}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.vehicle_model && formikManual.touched.vehicle_model
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Vehicle Model </label>
                        {formikManual.errors.vehicle_model && formikManual.touched.vehicle_model && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.vehicle_model}</div>
                        )}
                        </div> 
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="vehicle_color"
                            id="vehicle_color"
                            placeholder="Vehicle Color"
                            value={formikManual.values.vehicle_color}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.vehicle_color && formikManual.touched.vehicle_color
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Vehicle Color </label>
                        {formikManual.errors.vehicle_color && formikManual.touched.vehicle_color && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.vehicle_color}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="vehicle_regNo"
                            id="vehicle_regNo"
                            placeholder="Vehicle Color"
                            value={formikManual.values.vehicle_regNo}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.vehicle_regNo && formikManual.touched.vehicle_regNo
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                            disabled={isEditing &&  formikManual.values.book_status==1?true:false}
                        />
                        <label htmlFor="floatingInput">Vehicle RegNo </label>
                        {formikManual.errors.vehicle_regNo && formikManual.touched.vehicle_regNo && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.vehicle_regNo}</div>
                        )}
                        </div> 
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="date"                  
                            name="manufacture_date"
                            id="manufacture_date"
                            placeholder="Manufacture Date"
                            value={formikManual.values.manufacture_date}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.manufacture_date && formikManual.touched.manufacture_date
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Manufacture Date </label>
                        {formikManual.errors.manufacture_date && formikManual.touched.manufacture_date && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.manufacture_date}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">                  
                        <input
                            type="date"                  
                            name="date_of_sale"
                            id="date_of_sale"
                            placeholder="Date Of Sale"
                            value={formikManual.values.date_of_sale}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.date_of_sale && formikManual.touched.date_of_sale
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Date Of Sale </label>
                        {formikManual.errors.date_of_sale && formikManual.touched.date_of_sale && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.date_of_sale}</div>
                        )}
                        </div> 
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="date"                  
                            name="date_of_use"
                            placeholder="Date Of Use"
                            value={formikManual.values.date_of_use}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.date_of_use && formikManual.touched.date_of_use
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Date Of Use </label>
                        {formikManual.errors.date_of_use && formikManual.touched.date_of_use && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.date_of_use}</div>
                        )}
                        </div> 
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="chassis_no"
                            placeholder="Chassis no"
                            value={formikManual.values.chassis_no}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.chassis_no && formikManual.touched.chassis_no
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Chassis no </label>
                        {formikManual.errors.chassis_no && formikManual.touched.chassis_no && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.chassis_no}</div>
                        )}
                        </div> 
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="moter_engine_no"
                            placeholder="Moter/engine Number"
                            value={formikManual.values.moter_engine_no}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.moter_engine_no && formikManual.touched.moter_engine_no
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Moter/engine Number </label>
                        {formikManual.errors.moter_engine_no && formikManual.touched.moter_engine_no && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.moter_engine_no}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="battery_no"
                            placeholder="Battery Number"
                            value={formikManual.values.battery_no}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.battery_no && formikManual.touched.battery_no
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Battery Number </label>
                        {formikManual.errors.battery_no && formikManual.touched.battery_no && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.battery_no}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="battery_no_2"
                            placeholder="2nd Battery Number"
                            value={formikManual.values.battery_no_2}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.battery_no_2 && formikManual.touched.battery_no_2
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">2nd Battery Number </label>
                        {formikManual.errors.battery_no_2 && formikManual.touched.battery_no_2 && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.battery_no_2}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="controller_number"
                            placeholder="Controller Number"
                            value={formikManual.values.controller_number}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.controller_number && formikManual.touched.controller_number
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Controller Number </label>
                        {formikManual.errors.controller_number && formikManual.touched.controller_number && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.controller_number}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="charger_no"
                            placeholder="Charger Number"
                            value={formikManual.values.charger_no}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.charger_no && formikManual.touched.charger_no
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Charger Number </label>
                        {formikManual.errors.charger_no && formikManual.touched.charger_no && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.charger_no}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="charger_no_2"
                            placeholder="2nd Charger Number"
                            value={formikManual.values.charger_no_2}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.charger_no_2 && formikManual.touched.charger_no_2
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">2nd Charger Number </label>
                        {formikManual.errors.charger_no_2 && formikManual.touched.charger_no_2 && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.charger_no_2}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="key_no"
                            placeholder="Key Number"
                            value={formikManual.values.key_no}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.key_no && formikManual.touched.key_no
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Key Number </label>
                        {formikManual.errors.key_no && formikManual.touched.key_no && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.key_no}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="scooter_invoice"
                            placeholder="Scooter Invoice"
                            value={formikManual.values.scooter_invoice}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.scooter_invoice && formikManual.touched.scooter_invoice
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Scooter Invoice </label>
                        {formikManual.errors.scooter_invoice && formikManual.touched.scooter_invoice && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.scooter_invoice}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="date"                  
                            name="scooter_reg_date"
                            placeholder="Scooter Reg Date"
                            value={formikManual.values.scooter_reg_date}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.scooter_reg_date && formikManual.touched.scooter_reg_date
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Scooter Reg Date </label>
                        {formikManual.errors.scooter_reg_date && formikManual.touched.scooter_reg_date && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.scooter_reg_date}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="scooter_reg_upto"
                            placeholder="Scooter Reg Upto"
                            value={formikManual.values.scooter_reg_upto}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.scooter_reg_upto && formikManual.touched.scooter_reg_upto
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Scooter Reg Upto </label>
                        {formikManual.errors.scooter_reg_upto && formikManual.touched.scooter_reg_upto && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.scooter_reg_upto}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="insurance_company"
                            placeholder="Insurance Company"
                            value={formikManual.values.insurance_company}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.insurance_company && formikManual.touched.insurance_company
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Insurance Company </label>
                        {formikManual.errors.insurance_company && formikManual.touched.insurance_company && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.insurance_company}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="policy_number_insurance"
                            placeholder="Policy Insurance Number"
                            value={formikManual.values.policy_number_insurance}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.policy_number_insurance && formikManual.touched.policy_number_insurance
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Policy Insurance Number </label>
                        {formikManual.errors.policy_number_insurance && formikManual.touched.policy_number_insurance && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.policy_number_insurance}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="date"                  
                            name="date_policy_expiry"
                            placeholder="Policy Expiry Date"
                            value={formikManual.values.date_policy_expiry}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.date_policy_expiry && formikManual.touched.date_policy_expiry
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Policy Expiry Date </label>
                        {formikManual.errors.date_policy_expiry && formikManual.touched.date_policy_expiry && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.date_policy_expiry}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="date"                  
                            name="exp_date"
                            placeholder="Expiry Date"
                            value={formikManual.values.exp_date}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.exp_date && formikManual.touched.exp_date
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Expiry Date </label>
                        {formikManual.errors.exp_date && formikManual.touched.exp_date && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.exp_date}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="permit_no"
                            placeholder="Permit Number"
                            value={formikManual.values.permit_no}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.permit_no && formikManual.touched.permit_no
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Permit Number</label>
                        {formikManual.errors.permit_no && formikManual.touched.permit_no && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.permit_no}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="sgst"
                            placeholder="SGST"
                            value={formikManual.values.sgst}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.sgst && formikManual.touched.sgst
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">SGST</label>
                        {formikManual.errors.sgst && formikManual.touched.sgst && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.sgst}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="cgst"
                            placeholder="CGST"
                            value={formikManual.values.cgst}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.cgst && formikManual.touched.cgst
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">CGST</label>
                        {formikManual.errors.cgst && formikManual.touched.cgst && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.cgst}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="iot_device"
                            placeholder="IOT Device"
                            value={formikManual.values.iot_device}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.iot_device && formikManual.touched.iot_device
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">IOT Device</label>
                        {formikManual.errors.iot_device && formikManual.touched.iot_device && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.iot_device}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="model"
                            placeholder="Model"
                            value={formikManual.values.model}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.model && formikManual.touched.model
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Model</label>
                        {formikManual.errors.model && formikManual.touched.model && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.model}</div>
                        )}
                        </div>
                        {/* <div className="form-floating mb-3  col-4">
                            <input
                                type="number"                  
                                name="quantity"
                                placeholder="Quantity"
                                value={formikManual.values.quantity}
                                onChange={formikManual.handleChange}
                                onBlur={formikManual.handleBlur}
                                className={
                                formikManual.errors.quantity && formikManual.touched.quantity
                                    ? "text-input error form-control rounded-4 border-dark shadow"
                                    : "text-input form-control rounded-4 border-dark shadow"
                                }
                            />
                            <label htmlFor="floatingInput">Quantity</label>
                            {formikManual.errors.quantity && formikManual.touched.quantity && (
                                <div className="input-feedback invailid_feedback">{formikManual.errors.quantity}</div>
                            )}
                        </div> */}
                        <div className="form-floating mb-3  col-4">
                            <input
                                type="number"                  
                                name="amount"
                                placeholder="Amount"
                                value={formikManual.values.amount}
                                onChange={formikManual.handleChange}
                                onBlur={formikManual.handleBlur}
                                className={
                                formikManual.errors.amount && formikManual.touched.amount
                                    ? "text-input error form-control rounded-4 border-dark shadow"
                                    : "text-input form-control rounded-4 border-dark shadow"
                                }
                            />
                            <label htmlFor="floatingInput">Amount</label>
                            {formikManual.errors.amount && formikManual.touched.amount && (
                                <div className="input-feedback invailid_feedback">{formikManual.errors.amount}</div>
                            )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="number"                  
                            name="advance_amount"
                            placeholder="Advance Amount"
                            value={formikManual.values.advance_amount}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.advance_amount && formikManual.touched.advance_amount
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Advance Amount</label>
                        {formikManual.errors.advance_amount && formikManual.touched.advance_amount && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.advance_amount}</div>
                        )}
                        </div>
                    </div>
                    <div  className={inventoryType=='spares'?'row manualAddForm':'row manualAddForm d-none'}>
                    <div className="form-floating mb-3  col-12">
                        {/* <input
                            type="text"                  
                            name="part_number"
                            placeholder="Part Number"
                            value={formikManual.values.part_number}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.part_number && formikManual.touched.part_number
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        /> */}
                        <select value={formikManual.values.part_type} name="part_type" onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur} className={
                            formikManual.errors.part_type && formikManual.touched.part_type
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            } disabled={isEditing&& formikManual.values.assigned_status==1?true:false}>
                            <option value={"spares"}>Spares</option>
                            <option value={"charger"}>Charger</option>
                            <option value={"battery"}>Battery</option>
                        </select>
                        <label htmlFor="floatingInput">Part Type </label>
                        {formikManual.errors.part_type && formikManual.touched.part_type && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.part_type}</div>
                        )}
                        </div>   
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="part_number"
                            placeholder="Part Number"
                            value={formikManual.values.part_number}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.part_number && formikManual.touched.part_number
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                            disabled={isEditing&& formikManual.values.assigned_status==1?true:false}
                        />
                        <label htmlFor="floatingInput">Part Number </label>
                        {formikManual.errors.part_number && formikManual.touched.part_number && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.part_number}</div>
                        )}
                        </div>   
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="supplier_description"
                            placeholder="Supplier Description"
                            value={formikManual.values.supplier_description}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.supplier_description && formikManual.touched.supplier_description
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Supplier Description </label>
                        {formikManual.errors.supplier_description && formikManual.touched.supplier_description && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.supplier_description}</div>
                        )}
                        </div> 
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="image_url"
                            placeholder="Image Url"
                            value={formikManual.values.image_url}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.image_url && formikManual.touched.image_url
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Image Url </label>
                        {formikManual.errors.image_url && formikManual.touched.image_url && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.image_url}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="quantity"
                            placeholder="Quantity"
                            value={formikManual.values.quantity}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.quantity && formikManual.touched.quantity
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                            // disabled={isEditing&& formikManual.values.assigned_status==1?true:false}
                        />
                        <label htmlFor="floatingInput">Quantity </label>
                        {formikManual.errors.quantity && formikManual.touched.quantity && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.quantity}</div>
                        )}
                        </div> 
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="uom"
                            placeholder="UOM"
                            value={formikManual.values.uom}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.uom && formikManual.touched.uom
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">UOM </label>
                        {formikManual.errors.uom && formikManual.touched.uom && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.uom}</div>
                        )}
                        </div> 
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="hsn"
                            placeholder="HSN"
                            value={formikManual.values.hsn}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.hsn && formikManual.touched.hsn
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">HSN </label>
                        {formikManual.errors.hsn && formikManual.touched.hsn && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.hsn}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="gst"
                            placeholder="GST"
                            value={formikManual.values.gst}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.gst && formikManual.touched.gst
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">GST </label>
                        {formikManual.errors.gst && formikManual.touched.gst && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.gst}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="dealer_sale_price"
                            placeholder="Dealer Sale Price"
                            value={formikManual.values.dealer_sale_price}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.dealer_sale_price && formikManual.touched.dealer_sale_price
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Dealer Sale Price </label>
                        {formikManual.errors.dealer_sale_price && formikManual.touched.dealer_sale_price && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.dealer_sale_price}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="dlp"
                            placeholder="DLP"
                            value={formikManual.values.dlp}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.dlp && formikManual.touched.dlp
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">DLP </label>
                        {formikManual.errors.dlp && formikManual.touched.dlp && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.dlp}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="sale_rate"
                            placeholder="Sale Rate"
                            value={formikManual.values.sale_rate}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.sale_rate && formikManual.touched.sale_rate
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Sale Rate </label>
                        {formikManual.errors.sale_rate && formikManual.touched.sale_rate && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.sale_rate}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="mrp"
                            placeholder="MRP"
                            value={formikManual.values.mrp}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.mrp && formikManual.touched.mrp
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">MRP </label>
                        {formikManual.errors.mrp && formikManual.touched.mrp && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.mrp}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="model"
                            placeholder="Model"
                            value={formikManual.values.model}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.model && formikManual.touched.model
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Model</label>
                        {formikManual.errors.model && formikManual.touched.model && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.model}</div>
                        )}
                        </div>                       
                    </div>
                    <div  className={inventoryType=='iot_devices'?'row manualAddForm':'row manualAddForm d-none'}>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="device_serial_number"
                            placeholder="Device Serial Number"
                            value={formikManual.values.device_serial_number}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.device_serial_number && formikManual.touched.device_serial_number
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                            disabled={isEditing&& formikManual.values.assigned_status==1?true:false}
                        />
                        <label htmlFor="floatingInput">Device Serial Number </label>
                        {formikManual.errors.device_serial_number && formikManual.touched.device_serial_number && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.device_serial_number}</div>
                        )}
                        </div>   
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="make"
                            placeholder="Make"
                            value={formikManual.values.make}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.make && formikManual.touched.make
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Make </label>
                        {formikManual.errors.make && formikManual.touched.make && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.make}</div>
                        )}
                        </div> 
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="model_of_device"
                            placeholder="Model Of Device"
                            value={formikManual.values.model_of_device}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.model_of_device && formikManual.touched.model_of_device
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Model Of Device </label>
                        {formikManual.errors.model_of_device && formikManual.touched.model_of_device && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.model_of_device}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="imei"
                            placeholder="IMEI"
                            value={formikManual.values.imei}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.imei && formikManual.touched.imei
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">IMEI </label>
                        {formikManual.errors.imei && formikManual.touched.imei && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.imei}</div>
                        )}
                        </div> 
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="provided_by"
                            placeholder="Provided By"
                            value={formikManual.values.provided_by}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.provided_by && formikManual.touched.provided_by
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Provided By </label>
                        {formikManual.errors.provided_by && formikManual.touched.provided_by && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.provided_by}</div>
                        )}
                        </div> 
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="consignmentTrackingNumber_CourierDocket"
                            placeholder="Consignment Tracking Number/ Courier Docket"
                            value={formikManual.values.consignmentTrackingNumber_CourierDocket}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.consignmentTrackingNumber_CourierDocket && formikManual.touched.consignmentTrackingNumber_CourierDocket
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Consignment Tracking Number/ Courier Docket </label>
                        {formikManual.errors.consignmentTrackingNumber_CourierDocket && formikManual.touched.consignmentTrackingNumber_CourierDocket && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.consignmentTrackingNumber_CourierDocket}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="CustomerOrderNumber"
                            placeholder="Customer Order Number"
                            value={formikManual.values.CustomerOrderNumber}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.CustomerOrderNumber && formikManual.touched.CustomerOrderNumber
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Customer Order Number </label>
                        {formikManual.errors.CustomerOrderNumber && formikManual.touched.CustomerOrderNumber && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.CustomerOrderNumber}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="DispatchDetailsRemarks"
                            placeholder="Dispatch Details Remarks"
                            value={formikManual.values.DispatchDetailsRemarks}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.DispatchDetailsRemarks && formikManual.touched.DispatchDetailsRemarks
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Dispatch Details Remarks </label>
                        {formikManual.errors.DispatchDetailsRemarks && formikManual.touched.DispatchDetailsRemarks && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.DispatchDetailsRemarks}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="date"                  
                            name="WarrantyStartdate"
                            placeholder="Warranty Startdate"
                            value={formikManual.values.WarrantyStartdate}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.WarrantyStartdate && formikManual.touched.WarrantyStartdate
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Warranty Startdate </label>
                        {formikManual.errors.WarrantyStartdate && formikManual.touched.WarrantyStartdate && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.WarrantyStartdate}</div>
                        )}
                        </div>                        
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="date"                  
                            name="WarrantyEnddate"
                            placeholder="Warranty Enddate"
                            value={formikManual.values.WarrantyEnddate}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.WarrantyEnddate && formikManual.touched.WarrantyEnddate
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Warranty Enddate </label>
                        {formikManual.errors.WarrantyEnddate && formikManual.touched.WarrantyEnddate && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.WarrantyEnddate}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="Warrantyterms"
                            placeholder="Warranty terms "
                            value={formikManual.values.Warrantyterms}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.Warrantyterms && formikManual.touched.Warrantyterms
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Warranty terms </label>
                        {formikManual.errors.Warrantyterms && formikManual.touched.Warrantyterms && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.Warrantyterms}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="VF_MSISDN"
                            placeholder="VF MSISDN"
                            value={formikManual.values.VF_MSISDN}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.VF_MSISDN && formikManual.touched.VF_MSISDN
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">VF MSISDN </label>
                        {formikManual.errors.VF_MSISDN && formikManual.touched.VF_MSISDN && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.VF_MSISDN}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="VF_ICCID"
                            placeholder="VF ICCID"
                            value={formikManual.values.VF_ICCID}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.VF_ICCID && formikManual.touched.VF_ICCID
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">VF ICCID </label>
                        {formikManual.errors.VF_ICCID && formikManual.touched.VF_ICCID && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.VF_ICCID}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="VF_IMSI"
                            placeholder="VF IMSI"
                            value={formikManual.values.VF_IMSI}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.VF_IMSI && formikManual.touched.VF_IMSI
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">VF IMSI </label>
                        {formikManual.errors.VF_IMSI && formikManual.touched.VF_IMSI && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.VF_IMSI}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="circle"
                            placeholder="Circle"
                            value={formikManual.values.circle}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.circle && formikManual.touched.circle
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Circle </label>
                        {formikManual.errors.circle && formikManual.touched.circle && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.circle}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="history_remark"
                            placeholder="History Remark"
                            value={formikManual.values.history_remark}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.history_remark && formikManual.touched.history_remark
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">History Remark</label>
                        {formikManual.errors.history_remark && formikManual.touched.history_remark && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.history_remark}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="device_status"
                            placeholder="Device Status"
                            value={formikManual.values.device_status}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.device_status && formikManual.touched.device_status
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Device Status</label>
                        {formikManual.errors.device_status && formikManual.touched.device_status && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.device_status}</div>
                        )}
                        </div>
                        <div className="form-floating mb-3  col-4">
                        <input
                            type="text"                  
                            name="sim_status"
                            placeholder="Sim Status"
                            value={formikManual.values.sim_status}
                            onChange={formikManual.handleChange}
                            onBlur={formikManual.handleBlur}
                            className={
                            formikManual.errors.sim_status && formikManual.touched.sim_status
                                ? "text-input error form-control rounded-4 border-dark shadow"
                                : "text-input form-control rounded-4 border-dark shadow"
                            }
                        />
                        <label htmlFor="floatingInput">Sim Status</label>
                        {formikManual.errors.sim_status && formikManual.touched.sim_status && (
                            <div className="input-feedback invailid_feedback">{formikManual.errors.sim_status}</div>
                        )}
                        </div>
                    </div>
                </div>    
            {/* manual add form end */}
            </form>
        </Modal>
        {/* add modal end */}     
    </>
  );
}
const mapStateToProps = (state, ownProps = {}) => {
//   console.log(state) // state
   /*console.log(ownProps) // {}*/  
  const { user } = state.auth;
  const { inventoryData,EditRecord } = state.inventory;
  return {
    user,
    inventoryData,
    EditRecord
  };
}

export default connect(mapStateToProps)(AddManualInventoryComp);

