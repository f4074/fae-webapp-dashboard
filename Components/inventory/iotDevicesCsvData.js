
export const iotDevicesCsvHeaders = [
    { label: "Business Supplier", key: "business_supplier" },
    { label: "City", key: "city" },
    { label: "Hub", key: "hub" },
    { label: "Device Serial No", key: "device_serial_number" },
    { label: "Make", key: "make" },
    { label: "Model of device/ Description", key: "model_of_device" },
    { label: "IMEI", key: "imei" },
    { label: "Provided By", key: "provided_by" },
    { label: "Consignment Tracking Number/ Courier Docket**", key: "consignmentTrackingNumber_CourierDocket" },
    { label: "Customer Order Number", key: "CustomerOrderNumber" },
    { label: "Dispatch details Remarks (If any)", key: "DispatchDetailsRemarks" },
    { label: "Warranty Start date", key: "WarrantyStartdate" },
    { label: "Warranty terms (months) can be 0*", key: "Warrantyterms" },
    { label: "Warranty End date", key: "WarrantyEnddate" },
    { label: "VF MSISDN**", key: "VF_MSISDN" },
    { label: "VF ICCID**", key: "VF_ICCID" },
    { label: "VF IMSI**", key: "VF_IMSI" },
    { label: "Circle**", key: "circle" },
    { label: "History Remarks***", key: "history_remark" },
    { label: "Device status***", key: "device_status" },
    { label: "SIM Status***", key: "sim_status" },
]

export const iotDevicesCsvData = [
    { business_supplier: "HONDA", city: "Patrapur", hub: "Hub1" , device_serial_number: "Iotd1", make: "Concox" , model_of_device: "Wetrack 2", imei: "355172101157228" , provided_by: "VITSL", consignmentTrackingNumber_CourierDocket: "21015200284563/21015200284562/21015200284561/21015200284560/21015200284559" , CustomerOrderNumber: "3002839844/3002839845", DispatchDetailsRemarks: "already delivered" , WarrantyStartdate: "44373", Warrantyterms: "12" , WarrantyEnddate: "44372", VF_MSISDN: "5755080335818" , VF_ICCID: "8991860044618021413", VF_IMSI: "404864461802141", circle: "Karnataka", history_remark: "", device_status: "Active", sim_status: "Active"},
    
    { business_supplier: "HONDA", city: "Patrapur", hub: "Hub1" , device_serial_number: "Iotd2", make: "Concox" , model_of_device: "Wetrack 2", imei: "355172101157228" , provided_by: "VITSL", consignmentTrackingNumber_CourierDocket: "21015200284563/21015200284562/21015200284561/21015200284560/21015200284559" , CustomerOrderNumber: "3002839844/3002839845", DispatchDetailsRemarks: "already delivered" , WarrantyStartdate: "44373", Warrantyterms: "12" , WarrantyEnddate: "44372", VF_MSISDN: "5755080335818" , VF_ICCID: "8991860044618021413", VF_IMSI: "404864461802141", circle: "Karnataka", history_remark: "", device_status: "Active", sim_status: "Active"},  
    
    { business_supplier: "HONDA", city: "Patrapur", hub: "Hub1" , device_serial_number: "Iotd3", make: "Concox" , model_of_device: "Wetrack 2", imei: "355172101157228" , provided_by: "VITSL", consignmentTrackingNumber_CourierDocket: "21015200284563/21015200284562/21015200284561/21015200284560/21015200284559" , CustomerOrderNumber: "3002839844/3002839845", DispatchDetailsRemarks: "already delivered" , WarrantyStartdate: "44373", Warrantyterms: "12" , WarrantyEnddate: "44372", VF_MSISDN: "5755080335818" , VF_ICCID: "8991860044618021413", VF_IMSI: "404864461802141", circle: "Karnataka", history_remark: "", device_status: "Active", sim_status: "Active"},  
    { business_supplier: "HONDA", city: "Patrapur", hub: "Hub1" , device_serial_number: "Iotd4", make: "Concox" , model_of_device: "Wetrack 2", imei: "355172101157228" , provided_by: "VITSL", consignmentTrackingNumber_CourierDocket: "21015200284563/21015200284562/21015200284561/21015200284560/21015200284559" , CustomerOrderNumber: "3002839844/3002839845", DispatchDetailsRemarks: "already delivered" , WarrantyStartdate: "44373", Warrantyterms: "12" , WarrantyEnddate: "44372", VF_MSISDN: "5755080335818" , VF_ICCID: "8991860044618021413", VF_IMSI: "404864461802141", circle: "Karnataka", history_remark: "", device_status: "Active", sim_status: "Active"},  
    { business_supplier: "HONDA", city: "Patrapur", hub: "Hub1" , device_serial_number: "Iotd5", make: "Concox" , model_of_device: "Wetrack 2", imei: "355172101157228" , provided_by: "VITSL", consignmentTrackingNumber_CourierDocket: "21015200284563/21015200284562/21015200284561/21015200284560/21015200284559" , CustomerOrderNumber: "3002839844/3002839845", DispatchDetailsRemarks: "already delivered" , WarrantyStartdate: "44373", Warrantyterms: "12" , WarrantyEnddate: "44372", VF_MSISDN: "5755080335818" , VF_ICCID: "8991860044618021413", VF_IMSI: "404864461802141", circle: "Karnataka", history_remark: "", device_status: "Active", sim_status: "Active"},  
];