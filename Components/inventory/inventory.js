import React, { useState, useEffect, useCallback } from "react";
import { connect, useDispatch, useSelector } from "react-redux";
import { useRouter } from 'next/router';
import { HANDLE_SUCCESS, HANDLE_ERROR } from "../../provider/ApiProvider";
import { changeDateFormat } from "../../provider/helper";
import Loader from "../../Components/loader";
import { createInventory, getAllInventory,getAllInventoryFilter, getDetailsInventory, deleteInventory, updateInventory ,deleteMultiple,getVehicleTimeLineInventory} from "../../Redux/Action/inventory";
import { createHub,getAllHubs,allCityHubs,deleteHub,updateHub} from "../../Redux/Action/hubs";
import { getAllCities} from "../../Redux/Action/cities"

/* ant design */
import 'antd/dist/antd.css';
import { Table,Tabs,Row, Col, Tag, Space, Modal, Form, Input, Radio, Upload, message, Button, Descriptions, DatePicker,Select } from 'antd';
import { EditOutlined, DeleteOutlined, InboxOutlined, EyeOutlined, SearchOutlined } from '@ant-design/icons'
// formik import 
import * as Yup from "yup";
import { VALIDATION_SCHEMA, FORMIK } from "../../provider/FormikProvider";
import UploadCSVComp from "./uploadCsv";
import AddManualInventoryComp from "./AddManualInventory";
import TransferVehicleComp from "./transferVehicle";
import TransferSparesComp from "./transferSpares";

import { CSVLink, CSVDownload } from "react-csv";

const invenotyComp = (props) => {
  const router = useRouter();
  const [loader, setLoader] = useState(false);
  const dispatch = useDispatch();
  const { isLoggedIn} = useSelector(state => state.auth);
  const { supplierData } = useSelector(state => state.business_supplier);

  const [form] = Form.useForm();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isEditModalVisible, setIsEditModalVisible] = useState(false);
  const [isModalVisibleDetails, setIsModalVisibleDetails] = useState(false);
  const [isModalVisibleTimeline, setIsModalVisibleTimeline] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [editingData, setEditingData] = useState(null);
  const [dataSource, setDataSource] = useState([]);
  const [dataSourceVehicle, setDataSourceVehicle] = useState([]);
  const [dataSourceSpares, setDataSourceSpares] = useState([]);
  const [dataSourceIotDevices, setDataSourceIotDevices] = useState([]);
  const [allVehiclesDataFilter, setAllVehiclesDataFilter] = useState([]);
  const [businessSupplierDataFilter, setBusinessSupplierDataFilter] = useState([]);
  const [cityDataFilter, setCityDataFilter] = useState([]);
  const [hubDataFilter, setHubDataFilter] = useState([]);
  const [dataVehicleTimeline, setDataVehicleTimeline] = useState([]);
  const [timeline_vehicle_name, set_timeline_vehicle_name] = useState([]);
  const [inventoryDetails, setInventoryDetails] = useState('');
  const [detailsTitle, setDetailsTitle] = useState('');
  const [filterInventory, setFilterInventory] = useState({'searchKey':'','vehicle_id':'','business_supplier_id':'','city_id':props.user_city_id?props.user_city_id[0]:'','hub_id':(props.user_role == 'FAE_HUB_ADMIN' || props.user_role == 'FAE_HUB_MANAGER') && props.user_hub_id?props.user_hub_id[0]:'','city_name':'','hub_name':'' ,'filter_type':'','part_type_filter':''});
  const [vehicleEditData, setVehicleEditData] = useState({'isEditing':false,'type':"", 'data':null});
  const [isFilter, setIsFilter] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [isDeletedVehicle, setIsDeletedVehicle] = useState(false);
  const [isDeletedSpares, setIsDeletedSpares] = useState(false);
  const [isDeletedIotDevices, setIsDeletedIotDevices] = useState(false);
  /* usestate for download csv timeline */
  const [downloadFileName, setDownloadFileName] = useState('vehicleTimeline.csv');
  const [csvHeaders, setCsvHeaders] = useState('');
  const [csvData, setCsvData] = useState([]);

  const [deleteIds, setDeleteIds] = useState({
    "vehicle":[],
    "spares":[],
    'iot_devices':[]
  });

  const [deleteAllIds, setDeleteAllIds] = useState({
    "vehicle":[],
    "spares":[],
    'iot_devices':[]
  });
  const [tabActiveType, setTabActiveType] = useState('vehicle');
  const [selectedRowsData, setSelectedRowsData] = useState([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [selectedRows, setSelectedRows] = useState([]);
  const { Search } = Input;
  const { Option } = Select;
  const { TabPane } = Tabs;

  const vehicleTimelineHeaders = [
    { label: "Date", key: "created" },
    { label: "Title", key: "title" },
    { label: "Description", key: "description" },
    { label: "Status", key: "status" },
  ]
  const [user_role, setuser_role] = useState(props.user_role);  
  const [user_city_id, setuser_city_id] = useState(props.user_hub_id);
  const [user_hub_id, setuser_hub_id] = useState(props.user_city_id);

  useEffect(() => {
    if (!isLoggedIn) {
      router.push('/login');
    } else {
      
      // setuser_role(props.user_role);
      // setuser_hub_id(props.user_hub_id)
      // setuser_city_id(props.user_city_id);
      // if(props.user_role == 'FAE_MAIN_ADMIN' || props.user_role == 'FAE_ADMIN'){
      //   const getCity = cityDataFilter.filter((i)=> [props.user_city_id].includes(i));
      //   if(getCity.length >0){
      //     setuser_city_id(getCity);         
      //     setFilterInventory({...filterInventory, city_id: getCity[0] ,hub_id: ""})
      //   } 
      // }else{
         
      //   if(props.user_role == 'FAE_HUB_ADMIN' || props.user_role == 'FAE_HUB_MANAGER'){          
      //     setFilterInventory({...filterInventory,city_id: props.user_city_id[0], hub_id: props.user_hub_id[0]})
      //   }else{
      //     setFilterInventory({...filterInventory, city_id: props.user_city_id[0] ,hub_id: ""})
         
      //   }
      // }


      // if(props.user_city_id){        
      //   getAllCityHubsAPI(props.user_city_id[0]);
      // }

      setuser_role(props.user_role);
      setuser_hub_id(props.user_hub_id)
      setuser_city_id(props.user_city_id); 

      
      
      getAllCityAPI();
      if(props.user_city_id){
        getAllCityHubsAPI(props.user_city_id[0]);
      } 

      if(props.user_role == 'FAE_MAIN_ADMIN' || props.user_role == 'FAE_ADMIN'){
        const getCity = cityDataFilter.filter((i)=> [props.user_city_id].includes(i));
        if(getCity.length >0){        
          setFilterInventory({...filterInventory, city_id: getCity[0] ,hub_id: ""})
        }else{
          setFilterInventory({...filterInventory, city_id: "" ,hub_id: ""})
        }
      } 
      
      setCsvHeaders(vehicleTimelineHeaders);
      // getAllInventoryData();
    }
  }, [])
  

  useEffect(() => {
    if(props.inventoryData != null){
      setDataSourceVehicle(props.inventoryData.vahicleInventoryData);
      setDataSourceSpares(props.inventoryData.sparesInventoryData);
      setDataSourceIotDevices(props.inventoryData.iotDevicesInventoryData);
      setSelectedRowKeys([])
      setSelectedRows([])
      setSelectedRowsData([]);
      setDeleteIds({
        "vehicle":[],
        "spares":[],
        'iot_devices':[]
      })
    }
  }, [props.inventoryData])
  

  useEffect(() => {
    if(filterInventory.vehicle_id.length > 0 || filterInventory.business_supplier_id.length > 0 || filterInventory.city_id.length > 0  ||filterInventory.hub_id.length > 0 ){          
      setIsFilter(true);
    }else{      
      setIsFilter(false);
    }     
    
  },[isFilter])


  useEffect(() => {    
    // setSelectedRowsData([]);
    getAllInventoryData(); 
  }, [filterInventory])

  
  useEffect(() => {    
    // setSelectedRowsData([]);    
    if(props.EditRecord.isEditing == false){
      getAllInventoryData(); 
    }
   
    // 
  }, [props.EditRecord])

  /* useEffect(() => {  
    
  }, [isFilter]) */

  // useEffect(() => {  
  //   if(props && props.inventoryData)    
  //   setDataSource(props.inventoryData.inventoryData); 
  // }, [dataSource])

  useEffect(() => {  
    if(props && props.inventoryData){
      setDataSource(props.inventoryData.vahicleInventoryData);
    }  
  }, [dataSourceVehicle])

  useEffect(() => {  
    if(props && props.inventoryData){
      setDataSource(props.inventoryData.sparesInventoryData); 
    } 
  }, [dataSourceSpares])

  useEffect(() => {  
    if(props && props.inventoryData){
      setDataSource(props.inventoryData.iotDevicesInventoryData); 
    } 
  }, [dataSourceIotDevices])
  
  const getAllCityAPI=()=>{
    try {
      setLoader(true);      
      dispatch(getAllCities())
      .then((res) => {
        console.log('res.data)',res.data)
        setLoader(false);
        setCityDataFilter(res.data);
      })
      .catch((error) => {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });
    } catch (error) {
      setLoader(false);
      // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  const onDelete = (record, type=null) => {
    if(type == 'all'){
      var title =  'Are you sure, you want to empty data?';
    }else{
      if(selectedRowsData.length <=0){
        HANDLE_ERROR("Please Select atleast one record!");
      }  
      var title =  'Are you sure, you want to delete this record?';
    }
    // showModal();
    Modal.confirm({
      title: title,
      okText: "Yes",
      okType: "danger",
      onOk: () => {
        deleteCallApi(record);
      }
    })
    
  }
  const deleteCallApi = (record) => {
    try {
      setLoader(true);      
      dispatch(deleteMultiple(record))
      .then((res) => {
        setLoader(false); 
        if(res.status == true){          
          HANDLE_SUCCESS(res.message);

          // empty selected row variable
          setSelectedRowKeys([])
          setSelectedRows([])
          setSelectedRowsData([]);
          setIsDeletedVehicle(false)
          setIsDeletedSpares(false)
          setIsDeletedIotDevices(false)
          setDeleteIds({
            "vehicle":[],
            "spares":[],
            'iot_devices':[]
          })
          getAllInventoryData();
          
          // if(tabActiveType =='vahicles'){
          //   setDataSourceVehicle(pre=>{
          //     return pre.filter((client)=>record.includes(client._id) == false);
          //   })
          // }
          // if(tabActiveType =='spares'){
            
          //   setDataSourceSpares(pre=>{          
          //     return pre.filter((client)=>record.includes(client._id) == false);              
          //   })
          // }
          // if(tabActiveType =='iot_devices'){
          //   setDataSourceIotDevices(pre=>{
          //     return pre.filter((client)=>record.includes(client._id) == false);
          //   })
          // }
          
           
        }else{
          HANDLE_ERROR(res.message);
        }  
      })
      .catch((error) => {
        setLoader(false);
        HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });     
    } catch (error) {
      setLoader(false);
      HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }

  const getAllInventoryData = () => {    
    /* if(isFilter){
      var apiAction =  getAllInventoryFilter(filterInventory)
    }else{
      var apiAction =  getAllInventory()
    } */
    var apiAction =  getAllInventoryFilter(filterInventory);

    // 
    try {
      setLoader(true);
      dispatch(apiAction)
        .then((res) => {
          setLoader(false);
          setCityDataFilter(res.data.citiesList);
          setBusinessSupplierDataFilter(res.data.businessSupplierList);
          setAllVehiclesDataFilter(res.data.allVehicles);
          // setHubData(res.data.allHubs);
          
          if (res.status == true) {
            // HANDLE_SUCCESS(res.message);         
            // setDataSource(res.data.inventoryData); 
            setAllInventoryData(res.data.vahicleInventoryData,res.data.sparesInventoryData,res.data.iotDevicesInventoryData)
          } else {
            // HANDLE_ERROR(res.message);
          }
        })
        .catch((error) => {
          setLoader(false);
          // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
        });
    } catch (error) {
      setLoader(false);
      // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }

 const setAllInventoryData=(vahicleInventoryData,sparesInventoryData,iotDevicesInventoryData)=>{
    setDataSourceVehicle(vahicleInventoryData);
    setDataSourceSpares(sparesInventoryData);
    setDataSourceIotDevices(iotDevicesInventoryData);
    
    const vahicleInventoryDataSet=vahicleInventoryData
    var ids_v = vahicleInventoryDataSet.map(function(i) {
      return i._id;
    });

    const sparesInventoryDataSet= sparesInventoryData
    var ids_s = sparesInventoryDataSet.map(function(i) {
      return i._id;
    });

    const iotDevicesInventoryDataSet= iotDevicesInventoryData
    var ids_Iot_d = iotDevicesInventoryDataSet.map(function(i) {
      return i._id;
    });
    setDeleteAllIds({...deleteAllIds,
      "vehicle":ids_v,"spares":ids_s,"iot_devices":ids_Iot_d
    })
  }
  const getAllCityHubsAPI=(city_id)=>{
    try {
      setLoader(true);      
      dispatch(allCityHubs(city_id))
      .then((res) => {
        // console.log('res.data)',res.data)
        setLoader(false);
        setHubDataFilter(res.data);
      })
      .catch((error) => {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });
    } catch (error) {
      setLoader(false);
      // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  /* get vehicle details  */
  const getVehicleDetails = (data) => {
    try {
      setLoader(true);
      dispatch(getDetailsInventory(data._id))
        .then((res) => {
          console.log('res.data)', res.data)
          setLoader(false);
          if (res.status == true) {
            // HANDLE_SUCCESS(res.message);         
            setInventoryDetails(res.data);
            if(res.data && res.data.type =='spares'){
              setDetailsTitle("Spares Details")
            }else if(res.data && res.data.type =='iot_devices'){
              setDetailsTitle("Iot Devices Details")
            }else{
              setDetailsTitle("Vehicle Details")
            }
              
          } else {
            // HANDLE_ERROR(res.message);
          }
        })
        .catch((error) => {
          setLoader(false);
          // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
        });
    } catch (error) {
      setLoader(false);
      // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  /* column for vehicle list */
  const columnsVehicle = [
    {

      key: "1",
      title: 'Vehicle Name',
      // dataIndex: 'vehicle_name',
      render: (data) => {
        return data.vehicleDetails.vehicle_name;
      },
    },
    {
      key: "2",
      title: 'Registration Number',
      // dataIndex: 'vehicle_regNo',
      render: (data) => {
        return data.vehicleDetails.vehicle_regNo;
      },
    },
    {
      key: "3",
      title: 'Created On',
      dataIndex: 'created',
      render: (created) => {
        return changeDateFormat(created);
      },
    },
    {
      key: "4",
      title: 'Inventory Details',
      render: (record) => {
        return <>
          <button type="button" className="btn btn-outline-secondary btn-sm" onClick={() => {
            getVehicleDetails(record)
            setIsModalVisibleDetails(true)

          }} >
            <i className="fa fa-eye me-2"></i>
            View Details
          </button>

        </>
      }
    },
    {
      key: "5",
      title: 'Vehicle Timeline',
      render: (record) => {
        return <>
          <button className="btn btn-outline-secondary btn-sm" size="small" onClick={() => {
            getVehicleTimelineList(record);
            setIsModalVisibleTimeline(true)

          }} >
            <i className="fa fa-eye me-2"></i>
            View Timeline
          </button>

        </>
      }
    },
    {
      key: "6",
      title: 'Status',
      dataIndex: 'status',
      render: (status) => {
        if (status == 2) {
          return (<Tag color="red">Not Available</Tag>);
        } else if (status == 3) {
          return (<Tag color="orange">Booked</Tag>);
        }else if (status == 4) {
          return  (<Tag color="green">Assigned</Tag>);
        } else {
          return (<Tag color="blue">Available</Tag>);
        }
      }
    },
    // {
    //   key: "7",
    //   title: 'Assigned To',
    //   dataIndex: 'assignto',
    //   render: (data) => {
    //     return "None";
    //   },
    // },
    (user_role == 'FAE_MAIN_ADMIN'  || user_role == 'FAE_ADMIN'  || user_role == 'FAE_CITY_ADMIN'  || user_role == 'FAE_HUB_ADMIN')?
    {
      key: "8",
      title: 'Action',
      render: (record) => {
        return <>
          <button className="btn btn-outline-secondary btn-sm" size="small" onClick={() => {
            onEdit('vehicle',record);
          }} ><i className="fa fa-edit me-2"></i> Edit</button>
        </>
      },
    }:{}
  ];
  
  /* column for spares */
  const columnsSpares = [
    {

      key: "1",
      title: 'Part Number',
      // dataIndex: 'part_number',
      render: (data) => {
        return data.sparesDetails.part_number;
      },
    },

     {
      key: "101",
      title: 'Part Type',
      // dataIndex: 'part_type',
      render: (data) => {
        return data.sparesDetails.part_type;
      },
    },

    {
      key: "2",
      title: 'Supplier Description',
      // dataIndex: 'supplier_description',
      render: (data) => {
        return data.sparesDetails.supplier_description;
      },
    }, 
    {
      key: "5",
      title: 'Quantity',
      render: (record) => {
        return <>
        { record.sparesDetails.quantity}
        </>
      }
    },   
    {
      key: "4",
      title: 'Inventory Details',
      render: (record) => {
        return <>
          <button type="button" className="btn btn-outline-secondary btn-sm" onClick={() => {
            getVehicleDetails(record)
            setIsModalVisibleDetails(true)

          }} >
            <i className="fa fa-eye me-2"></i>
            View Details
          </button>
        </>
      }
    },
    
    {
      key: "3",
      title: 'Created On',
      dataIndex: 'created',
      render: (created) => {
        return changeDateFormat(created);
      },
    },
    {
      key: "6",
      title: 'Status',
      // dataIndex: 'status',
      render: (record) => {
        if (record.sparesDetails.assigned_status == 1) {
          return (<Tag color="red">Not Available</Tag>);
        }else {
          return (<Tag color="blue">Available</Tag>);
        }
      }
    },
    
    (user_role == 'FAE_MAIN_ADMIN'  || user_role == 'FAE_ADMIN'  || user_role == 'FAE_CITY_ADMIN'  || user_role == 'FAE_HUB_ADMIN')?
    {
      key: "8",
      title: 'Action',
      render: (record) => {
        return <>
          <button className="btn btn-outline-secondary btn-sm" size="small" onClick={() => {
            onEdit('spares',record);
          }} >
            <i className="fa fa-edit me-2"></i>
            Edit
          </button>
        </>
      },
    }:{}
  ];
  /* column for iot devices */
  const columnsIotDevices = [
    {

      key: "1",
      title: 'Device Serial Number',
      // dataIndex: 'device_serial_number',
      render: (data) => {
        return data.iot_devicesDetails.device_serial_number;
      },
    },
    {
      key: "2",
      title: 'Make',
      // dataIndex: 'make',
      render: (data) => {
        return data.iot_devicesDetails.make;
      },
    }, 
    {
      key: "5",
      title: 'Model Of Device',
      // dataIndex: 'model_of_device',
      render: (data) => {
        return data.iot_devicesDetails.model_of_device;
      },      
    },      
    {
      key: "6",
      title: 'Customer Order Number',
      // dataIndex: 'CustomerOrderNumber',
      render: (data) => {
        return data.iot_devicesDetails.CustomerOrderNumber;
      },             
    },  
    {
      key: "4",
      title: 'Inventory Details',
      render: (record) => {
        return <>
          <button type="button" className="btn btn-outline-secondary btn-sm" onClick={() => {
            getVehicleDetails(record)
            setIsModalVisibleDetails(true)

          }} >
            <i className="fa fa-eye me-2"></i>
            View Details
          </button>
        </>
      }
    },    
    {
      key: "3",
      title: 'Created On',
      dataIndex: 'created',
      render: (created) => {
        return changeDateFormat(created);
      },
    },
    {
      key: "6",
      title: 'Status',
      // dataIndex: 'status',
      render: (record) => {
        // console.log('record',record)
        if (record.iot_devicesDetails.assigned_status == 1) {
          return (<Tag color="red">Not Available</Tag>);
        }else {
          return (<Tag color="blue">Available</Tag>);
        }
      }
    },
  
    (user_role == 'FAE_MAIN_ADMIN'  || user_role == 'FAE_ADMIN'  || user_role == 'FAE_CITY_ADMIN'  || user_role == 'FAE_HUB_ADMIN')?
    {
      key: "8",
      title: 'Action',
      render: (record) => {
        return <>
          <button className="btn btn-outline-secondary btn-sm" size="small" onClick={() => {
            onEdit('iot_devices',record);
          }} >
            <i className="fa fa-edit me-2"></i>
            Edit
          </button>
        </>
      },
    }:{}
  ];
  // column fot vehicle timeline
  const columnsTimeline= [
    {

      key: "1",
      title: 'Date',
      render: (record) => {
        return changeDateFormat(record.created);
      },
      
    },
    {
      key: "2",
      title: 'Title',
      render: (record) => {
        return record.title;
      },
    },
    {
      key: "3",
      title: 'Description',
      render: (record) => {
        return record.description;
      },
    },  
    // {
    //   key: "3",
    //   title: 'From City',
    //   render: (record) => {        
    //     return record.from_city_id.name;
    //   },    
    // }, 
    // {
    //   key: "6",
    //   title: 'From Hub',
    //   render: (record) => {        
    //     return record.from_hub_id.title;
    //   },    
    // },  
    // {
    //   key: "3",
    //   title: 'To City',
    //   render: (record) => {        
    //     return record.to_city_id.name;
    //   },    
    // }, 
    // {
    //   key: "6",
    //   title: 'To Hub',
    //   render: (record) => {        
    //     return record.to_hub_id.title;
    //   },    
    // },
    {
      key: "4",
      title: 'Status',
      render: (record) => {        
        if (record.status == 2) {
          return " Not Available";
        } else if (record.status == 3) {
          return "Booked";
        }else if (record.status == 4) {
          return "Assigned";
        } else {
          return "Available";
        }
      },    
    },      
  ];
  const getVehicleTimelineList = async(data) => {
    try {
      setLoader(true);
      dispatch(getVehicleTimeLineInventory(data._id))
        .then(async(res) => {          
          setLoader(false);
          if (res.status == true) {
            // HANDLE_SUCCESS(res.message);         
            setDataVehicleTimeline(res.data);
            if(res.data){
              let setCsvArr=[];
              const getData = res.data.map(async(val,i)=>{ 
                setCsvArr.push({ created:changeDateFormat(val.created), title: val.title,description:val.description, status:val.status == 2?"Not Available":val.status == 3?"Booked":val.status == 4?"Assigned":"Available"})               
                // setCsvData([...csvData,{ created:changeDateFormat(val.created), title: val.title,description:val.description, status:val.status == 2?"Not Available":val.status == 3?"Booked":val.status == 4?"Assigned":"Available"}])
              })
              const getData1 = await Promise.all(getData);
              setCsvData(setCsvArr);
              set_timeline_vehicle_name(res.data[0].vehicle_id.vehicle_name)
            }
              
          } else {
            // HANDLE_ERROR(res.message);
          }
        })
        .catch((error) => {
          setLoader(false);
          // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
        });
    } catch (error) {
      setLoader(false);
      // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  
  function onChange(pagination, filters, sorter, extra) {
    console.log('params', pagination, filters, sorter, extra);
    const filterData ={
      sortField: sorter.field,
      sortOrder: sorter.order,
      pagination,
      ...filters,
    }
  }
  function onChangeDate(date, dateString) {
    console.log(date, dateString);
  }

  const onSearch = value => {
    setFilterInventory({...filterInventory, searchKey: value,filter_type:tabActiveType})
    if(value){      
      setIsFilter(true);
    }else{      
      setIsFilter(false);
    }
   
  };  
  const searchOnEnter=(e)=>{
    const value = e.target.value;
    // setFilterInventory({'searchKey':value});
    setFilterInventory({...filterInventory, searchKey: value,filter_type:tabActiveType})
    if(value.length >0){
      setIsFilter(true);
    }else{
      setIsFilter(false);
    }

  }
  
  const onEdit=(editType,record)=>{
    // console.log('editType',editType)
    // if(!(filterInventory  && filterInventory.business_supplier_id  && filterInventory.city_id && filterInventory.hub_id)){
    //   HANDLE_ERROR("Please Select Business supplier, City and Hub");
    //   return false;
    // }
    setIsEditModalVisible(true)
    setVehicleEditData({...vehicleEditData, isEditing:true,type:editType,data:record});
  }  
  function handleChangeSelect(value,selectType) {
    if(selectType == 'city'){
      setHubDataFilter([]);
      if(value){
        getAllCityHubsAPI(value);
      }
      
      const searchCityName = cityDataFilter.filter((val,i)=>{
        return val._id == value;
      })
      let cityNameGet = "";
      if(searchCityName.length >0){
        cityNameGet = searchCityName[0].name;
      }      
      setFilterInventory({...filterInventory, city_id: value,  city_name:cityNameGet ,hub_id: '',hub_name:'',filter_type:tabActiveType})
      
    }else if(selectType == 'hub'){

      const searchHubName = hubDataFilter.filter((val,i)=>{
        return val._id == value;
      })
      let hubNameGet = "";
      if(searchHubName.length >0){
        hubNameGet = searchHubName[0].title;
      }
      
      setFilterInventory({...filterInventory,  hub_id: value,hub_name:hubNameGet,filter_type:tabActiveType})

    }else if(selectType == 'vehicle_filter'){  
      setFilterInventory({...filterInventory,  vehicle_id: value,filter_type:tabActiveType})
      
    }else if(selectType == 'business_supplier'){  
      setFilterInventory({...filterInventory,  business_supplier_id: value,filter_type:tabActiveType})
      
    }else if(selectType == 'part_type_filter'){  
      setFilterInventory({...filterInventory,  part_type_filter: value,filter_type:tabActiveType})
    }
  }
  const onSelectChange = (newSelectedRowKeys,newSelectedRows) => {
    console.log('selectedRowKeys changed: ', newSelectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
    setSelectedRows(newSelectedRows)
    setSelectedRowsData(newSelectedRows);
    if(newSelectedRows.length >0){
      var ids = newSelectedRows.map(function(i) {
        return i._id;
      });
      if(tabActiveType == 'vehicle'){
        setIsDeletedVehicle(true)
        setDeleteIds({...deleteIds,
          "vehicle":ids
        })
      }
      if(tabActiveType == 'spares'){
        setIsDeletedSpares(true)
        setDeleteIds({...deleteIds,
          "spares":ids
        })
      }

      if(tabActiveType == 'iot_devices'){
        setIsDeletedIotDevices(true)
        setDeleteIds({...deleteIds,
          "iot_devices":ids
        })
      }
     
    }else{

      setIsDeletedVehicle(false)
      setIsDeletedSpares(false)
      setIsDeletedIotDevices(false)
      setDeleteIds({
        "vehicle":[],
        "spares":[],
        'iot_devices':[]
      })
    }
  };
  const rowSelection = {
    selectedRowKeys,selectedRows, 
    onChange: onSelectChange,
    getCheckboxProps: (record) => {
      if (tabActiveType == "vehicle" && record.type == "vehicle" && (record.status == 3 || record.status ==4)) {
        return {
          disabled: true 
        };
      }else if (tabActiveType == "spares"  && record.type=="spares" && (record.sparesDetails.assigned_status == 1 || record.total_booking_count >0)) {
        return {
          disabled: true 
        };
      }
      else if (tabActiveType == "iot_devices"  && record.type=="iot_devices" && (record.iot_devicesDetails.assigned_status == 1)) {
        return {
          disabled: true 
        };
      }  
    }
  };
  // const rowSelection = {
  //   onChange: (selectedRowKeys, selectedRows) => {
  //     console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
  //     setSelectedRows(selectedRows)
  //     setSelectedRowKeys(selectedRowKeys);
  //     setSelectedRowsData(selectedRows);

  //     if(selectedRows.length >0){
  //       var ids = selectedRows.map(function(i) {
  //         return i._id;
  //       });
  //       if(tabActiveType == 'vehicle'){
  //         setIsDeletedVehicle(true)
  //         setDeleteIds({...deleteIds,
  //           "vehicle":ids
  //         })
  //       }
  //       if(tabActiveType == 'spares'){
  //         setIsDeletedSpares(true)
  //         setDeleteIds({...deleteIds,
  //           "spares":ids
  //         })
  //       }

  //       if(tabActiveType == 'iot_devices'){
  //         setIsDeletedIotDevices(true)
  //         setDeleteIds({...deleteIds,
  //           "iot_devices":ids
  //         })
  //       }
       
  //     }else{

  //       setIsDeletedVehicle(false)
  //       setIsDeletedSpares(false)
  //       setIsDeletedIotDevices(false)
  //       setDeleteIds({
  //         "vehicle":[],
  //         "spares":[],
  //         'iot_devices':[]
  //       })
  //     }
      
  //   },
  //   getCheckboxProps: (record) => {
  //     // console.log('record',record)
  //     return {
  //       name: record.name,
  //       key: record.key
  //     };
  //   },   
    
  // };
   /* code for tabs */
  function callback(key) {
    // console.log(key);
    
    if(key == '2'){
      setTabActiveType('spares')
      setFilterInventory({...filterInventory, filter_type: 'spares'})
    }else  if(key == '3'){
      setTabActiveType('iot_devices')
      setFilterInventory({...filterInventory, filter_type: 'iot_devices'})
    }else  if(key == '1'){
      setTabActiveType('vehicle')
      setFilterInventory({...filterInventory, filter_type: 'vehicle'})
    }else{
      setTabActiveType('vehicle')
      setFilterInventory({...filterInventory, filter_type: 'vehicle'})
    }
  }
  return (
    <>
      <Loader loading={loader} />
      <h3 className="mb-4 text-danger fw-bold fs-3">Inventories</h3>
      <Row className="mb-3">
        <Col span={6}>
          <Select value={filterInventory.business_supplier_id} style={{ width: 120 }} className="w-100" name="business_supplier_filter" id="business_supplier_filter"  onChange={(e)=>handleChangeSelect(e,'business_supplier')}>
            <Option value="">Select Business Supplier</Option>
            {
              businessSupplierDataFilter.map(function(item, i){
                  return <Option  key={item._id}  value={item._id} >{item.supplier_name}</Option>
              })
            }
          </Select>
        </Col>
        {/* {user_role != 'FAE_HUB_ADMIN' && user_role != 'FAE_HUB_MANAGER'? */}
        <Col span={6}>
          <Select value={filterInventory.city_id} name="city_filter" className="w-100" id="city_filter" onChange={(e)=>handleChangeSelect(e,'city')}>
          {(props.user_role == 'FAE_MAIN_ADMIN' || props.user_role == 'FAE_ADMIN')?
            <Option value="">Select City</Option>:""}
            {
              cityDataFilter.map(function(item, i){
                if((user_role == 'FAE_CITY_ADMIN' || user_role == 'FAE_HUB_ADMIN' || user_role == 'FAE_HUB_MANAGER') && item._id == user_city_id[0]){
                  return <Option  key={item._id}  value={item._id} >{item.name}</Option>
                }else if(user_role == 'FAE_MAIN_ADMIN'  || user_role == 'FAE_ADMIN'  || user_role == 'FAE_STAFF'){
                  return <Option   key={item._id} value={item._id} >{item.name}</Option>
                } 
              })
            }
          </Select>
        </Col>
        {/* :''} */}
        <Col span={6}>
          <Select value={filterInventory.hub_id} style={{ width: 120 }} className="w-100" name="hub_filter" id="hub_filter"  onChange={(e)=>handleChangeSelect(e,'hub')}>
            {
                (user_role == 'FAE_MAIN_ADMIN' || user_role == 'FAE_ADMIN' || user_role == 'FAE_CITY_ADMIN')?( <Option value="">Select Hub</Option>):""}
            {             

              hubDataFilter.map(function(item, i){

                if(user_role == 'FAE_MAIN_ADMIN' || user_role == 'FAE_ADMIN'){
                  return <Option value={item._id} >{item.title}</Option>
                }else{  
                  if((user_role == 'FAE_HUB_ADMIN' || user_role == 'FAE_HUB_MANAGER')){
                    if(user_hub_id && user_hub_id.includes(item._id) ){                  
                      return <Option value={item._id} >{item.title}</Option>  
                    }
                  }else{
                    return <Option value={item._id} >{item.title}</Option>
                  } 
                }
                
              })
            }
          </Select>
        </Col>       
        <Col span={6}>
          {(user_role == 'FAE_MAIN_ADMIN'  || user_role == 'FAE_ADMIN'  || user_role == 'FAE_CITY_ADMIN'  || user_role == 'FAE_HUB_ADMIN')?
          <UploadCSVComp filterInventory={filterInventory} tabActiveType={tabActiveType}/>
          :''}
          {(user_role == 'FAE_MAIN_ADMIN'  || user_role == 'FAE_ADMIN'  || user_role == 'FAE_CITY_ADMIN'  || user_role == 'FAE_HUB_ADMIN')?
            <AddManualInventoryComp filterInventory={filterInventory} vehicleEditData={vehicleEditData}  tabActiveType={tabActiveType}  />
          :''}
        </Col>
      </Row> 
      <Tabs defaultActiveKey="1" size="large" onChange={callback}  type="card" >
        <TabPane tab="Vehicle" key="1">
          <Row>
            <Col span={8}>   
              <Search placeholder="Search by Registration Number" allowClear  onSearch={onSearch} onPressEnter={(e)=>searchOnEnter(e)}   />          
            </Col>
            <Col span={8}>   
                    
            </Col>
            <Col span={8}>   
              {(user_role == 'FAE_MAIN_ADMIN'  || user_role == 'FAE_ADMIN'  || user_role == 'FAE_CITY_ADMIN'  || user_role == 'FAE_HUB_ADMIN' )?          
                <TransferVehicleComp filterInventory={filterInventory} cityDataFilter={cityDataFilter} tabActiveType={tabActiveType} selectedRowsData={selectedRowsData} />
              :''}
              
              {/* {deleteAllIds.vehicle.length>0 && (user_role == 'FAE_MAIN_ADMIN' || user_role == 'FAE_ADMIN' )?
              <Button type="danger" className="float-end mb-3 me-3" onClick={()=>onDelete(deleteAllIds.vehicle,'all')} >Empty Vehicle Data</Button>
              :''} */}

              <Button type="danger" className={isDeletedVehicle?"mb-3 me-3 float-end":"float-end d-none mb-3 me-3"} onClick={()=>onDelete(deleteIds.vehicle)} >Delete Record</Button>
              
            </Col>
          </Row> 
          <Table  scroll={{x: 1000}} rowSelection={(user_role == 'FAE_MAIN_ADMIN' || user_role == 'FAE_ADMIN'  || user_role == 'FAE_CITY_ADMIN'  || user_role == 'FAE_HUB_ADMIN' )?{
              type: 'checkbox',
              ...rowSelection,
            }:''}  loading={isLoading} onChange={onChange} columns={columnsVehicle} dataSource={dataSourceVehicle} />
          {/* view modal details start */}
          <Modal
            title={detailsTitle}
            destroyOnClose="true"
            centered
            visible={isModalVisibleDetails}
            onOk={() => setIsModalVisibleDetails(false)}
            okType="danger"
            onCancel={() => setIsModalVisibleDetails(false)}
            cancelText="Close"
            width={1000}
            footer={<button type="button" onClick={() => setIsModalVisibleDetails(false)} class="ant-btn ant-btn-default"><span>Close</span></button>}
          >
            <div>
              {/* show vehicle details */}
              <Descriptions
                bordered
                title=""
                size="middle"
                style={{'display':(inventoryDetails && inventoryDetails.type == "vehicle"?'':'none')}}
              // extra={<Button type="primary">Edit</Button>}
              >
                <Descriptions.Item label="Business Supplier">{inventoryDetails && inventoryDetails.supplier_id ? inventoryDetails.supplier_id.supplier_name : ""}</Descriptions.Item>
                <Descriptions.Item label="City">{inventoryDetails && inventoryDetails.city_id  ? inventoryDetails.city_id.name : ""}</Descriptions.Item>
                <Descriptions.Item label="Hub">{inventoryDetails && inventoryDetails.hub_id  ? inventoryDetails.hub_id.title : ""}</Descriptions.Item>
                <Descriptions.Item label="Vehicle Name">{inventoryDetails && inventoryDetails.vehicle_id  ? inventoryDetails.vehicle_id.vehicle_name : ""}</Descriptions.Item>
                <Descriptions.Item label="Vehicle Model">{inventoryDetails && inventoryDetails.vehicle_id  ? inventoryDetails.vehicle_id.vehicle_model : ""}</Descriptions.Item>
                <Descriptions.Item label="Vehicle Color">{inventoryDetails && inventoryDetails.vehicle_id ? inventoryDetails.vehicle_id.vehicle_color : ""}</Descriptions.Item>
                <Descriptions.Item label="Vehicle Registration Number">{inventoryDetails && inventoryDetails.vehicle_id  ? inventoryDetails.vehicle_id.vehicle_regNo : ""}</Descriptions.Item>
                <Descriptions.Item label="Manufature Date">{inventoryDetails && inventoryDetails.vehicle_id  ? inventoryDetails.vehicle_id.manufacture_date : ""}</Descriptions.Item>
                <Descriptions.Item label="Date Of Sale">{inventoryDetails && inventoryDetails.vehicle_id  ? inventoryDetails.vehicle_id.date_of_sale : ""}</Descriptions.Item>
                <Descriptions.Item label="Date Of Use">{inventoryDetails && inventoryDetails.vehicle_id  ? inventoryDetails.vehicle_id.date_of_use : ""}</Descriptions.Item>
                <Descriptions.Item label="Chassis Number">{inventoryDetails && inventoryDetails.vehicle_id  ? inventoryDetails.vehicle_id.chassis_no : ""}</Descriptions.Item>
                <Descriptions.Item label="Motor/Engine No">{inventoryDetails && inventoryDetails.vehicle_id  ? inventoryDetails.vehicle_id.moter_engine_no : ""}</Descriptions.Item>
                <Descriptions.Item label="Battery No.">{inventoryDetails && inventoryDetails.vehicle_id  ? inventoryDetails.vehicle_id.battery_no : ""}</Descriptions.Item>
                <Descriptions.Item label="2nd Battery No.">{inventoryDetails && inventoryDetails.vehicle_id  ? inventoryDetails.vehicle_id.battery_no_2 : ""}</Descriptions.Item>
                <Descriptions.Item label="Controller No">{inventoryDetails && inventoryDetails.vehicle_id  ? inventoryDetails.vehicle_id.controller_number : ""}</Descriptions.Item>
                <Descriptions.Item label="1st Charger No">{inventoryDetails && inventoryDetails.vehicle_id  ? inventoryDetails.vehicle_id.charger_no : ""}</Descriptions.Item>
                <Descriptions.Item label="2nd Charger No">{inventoryDetails && inventoryDetails.vehicle_id  ? inventoryDetails.vehicle_id.charger_no_2 : ""}</Descriptions.Item>
                <Descriptions.Item label="Key No">{inventoryDetails  && inventoryDetails.vehicle_id ? inventoryDetails.vehicle_id.key_no : ""}</Descriptions.Item>
                <Descriptions.Item label="Scooter Invoice">{inventoryDetails   && inventoryDetails.vehicle_id ? inventoryDetails.vehicle_id.scooter_invoice : ""}</Descriptions.Item>
                <Descriptions.Item label="Scooter Reg Date">{inventoryDetails  && inventoryDetails.vehicle_id  ? inventoryDetails.vehicle_id.scooter_reg_date : ""}</Descriptions.Item>
                <Descriptions.Item label="Scooter Reg. upto">{inventoryDetails  && inventoryDetails.vehicle_id ? inventoryDetails.vehicle_id.scooter_reg_upto : ""}</Descriptions.Item>
                <Descriptions.Item label="Insurance Company">{inventoryDetails  && inventoryDetails.vehicle_id ? inventoryDetails.vehicle_id.insurance_company : ""}</Descriptions.Item>
                <Descriptions.Item label="Policy No Insurance">{inventoryDetails  && inventoryDetails.vehicle_id ? inventoryDetails.vehicle_id.policy_number_insurance : ""}</Descriptions.Item>
                <Descriptions.Item label="Date Policy Exp">{inventoryDetails  && inventoryDetails.vehicle_id ? inventoryDetails.vehicle_id.date_policy_expiry : ""}</Descriptions.Item>
                <Descriptions.Item label="Permit no">{inventoryDetails  && inventoryDetails.vehicle_id ? inventoryDetails.vehicle_id.exp_date : ""}</Descriptions.Item>
                <Descriptions.Item label="Exp Date">{inventoryDetails  && inventoryDetails.vehicle_id ? inventoryDetails.vehicle_id.permit_no : ""}</Descriptions.Item>
                <Descriptions.Item label="SGST">{inventoryDetails  && inventoryDetails.vehicle_id ? inventoryDetails.vehicle_id.sgsts : ""}</Descriptions.Item>
                <Descriptions.Item label="CGST">{inventoryDetails  && inventoryDetails.vehicle_id ? inventoryDetails.vehicle_id.cgst : ""}</Descriptions.Item>
                <Descriptions.Item label="IOT Device">{inventoryDetails  && inventoryDetails.vehicle_id ? inventoryDetails.vehicle_id.iot_device : ""}</Descriptions.Item>
                <Descriptions.Item label="Model">{inventoryDetails  && inventoryDetails.vehicle_id ? inventoryDetails.vehicle_id.model : ""}</Descriptions.Item>
                <Descriptions.Item label="Amount">{inventoryDetails  && inventoryDetails.vehicle_id ? inventoryDetails.vehicle_id.amount : ""}</Descriptions.Item>
                <Descriptions.Item label="Advance Amount">{inventoryDetails  && inventoryDetails.vehicle_id ? inventoryDetails.vehicle_id.advance_amount : ""}</Descriptions.Item>

              </Descriptions>

               {/* show spares details */}
               <Descriptions
                bordered
                title=""
                size="middle"
                style={{'display':(inventoryDetails && inventoryDetails.type == 'spares'?'':'none')}}              
              >
               <Descriptions.Item label="Business Supplier">{inventoryDetails && inventoryDetails.supplier_id ? inventoryDetails.supplier_id.supplier_name : ""}</Descriptions.Item>               
                <Descriptions.Item label="City">{inventoryDetails && inventoryDetails.city_id  ? inventoryDetails.city_id.name : ""}</Descriptions.Item>
                <Descriptions.Item label="Hub">{inventoryDetails && inventoryDetails.hub_id  ? inventoryDetails.hub_id.title : ""}</Descriptions.Item>
                <Descriptions.Item label="Part Number">{inventoryDetails &&  inventoryDetails.spares_id  ? inventoryDetails.spares_id.part_number : ""}</Descriptions.Item>
                <Descriptions.Item label="Supplier Description">{inventoryDetails &&  inventoryDetails.spares_id  ? inventoryDetails.spares_id.supplier_description : ""}</Descriptions.Item>
                <Descriptions.Item label="Quantity">{inventoryDetails &&  inventoryDetails.spares_id  ? inventoryDetails.spares_id.quantity : ""}</Descriptions.Item>
                <Descriptions.Item label="UOM">{inventoryDetails &&  inventoryDetails.spares_id  ? inventoryDetails.spares_id.uom : ""}</Descriptions.Item>
                <Descriptions.Item label="HSN">{inventoryDetails &&  inventoryDetails.spares_id  ? inventoryDetails.spares_id.hsn : ""}</Descriptions.Item>
                <Descriptions.Item label="GST">{inventoryDetails  &&  inventoryDetails.spares_id ? inventoryDetails.spares_id.gst : ""}</Descriptions.Item>
                <Descriptions.Item label="Dealer Sale Price">{inventoryDetails  &&  inventoryDetails.spares_id ? inventoryDetails.spares_id.dealer_sale_price : ""}</Descriptions.Item>                
                <Descriptions.Item label="DLP">{inventoryDetails  &&  inventoryDetails.spares_id ? inventoryDetails.spares_id.dlp : ""}</Descriptions.Item>                
                <Descriptions.Item label="Sale Rate">{inventoryDetails  &&  inventoryDetails.spares_id ? inventoryDetails.spares_id.sale_rate : ""}</Descriptions.Item>                
                <Descriptions.Item label="MRP">{inventoryDetails  &&  inventoryDetails.spares_id ? inventoryDetails.spares_id.mrp : ""}</Descriptions.Item>                
                <Descriptions.Item label="Model">{inventoryDetails  &&  inventoryDetails.spares_id ? inventoryDetails.spares_id.model : ""}</Descriptions.Item>
                <Descriptions.Item label="Part Type">{inventoryDetails  &&  inventoryDetails.spares_id ? inventoryDetails.spares_id.part_type : ""}</Descriptions.Item>
                               
              </Descriptions>

               {/* show iot_devices details */}
               <Descriptions
                bordered
                title=""
                size="middle"
                style={{'display':(inventoryDetails && inventoryDetails.type == 'iot_devices'?'':'none')}}             
              >
                <Descriptions.Item label="Business Supplier">{inventoryDetails && inventoryDetails.supplier_id ? inventoryDetails.supplier_id.supplier_name : ""}</Descriptions.Item>
                <Descriptions.Item label="City">{inventoryDetails && inventoryDetails.city_id  ? inventoryDetails.city_id.name : ""}</Descriptions.Item>
                <Descriptions.Item label="Hub">{inventoryDetails && inventoryDetails.hub_id  ? inventoryDetails.hub_id.title : ""}</Descriptions.Item>
                <Descriptions.Item label="Device Serial Number">{inventoryDetails  &&  inventoryDetails.iot_device_id ? inventoryDetails.iot_device_id.device_serial_number : ""}</Descriptions.Item>
                <Descriptions.Item label="Make">{inventoryDetails  &&  inventoryDetails.iot_device_id ? inventoryDetails.iot_device_id.make : ""}</Descriptions.Item>
                <Descriptions.Item label="Model Of Device">{inventoryDetails  &&  inventoryDetails.iot_device_id ? inventoryDetails.iot_device_id.model_of_device : ""}</Descriptions.Item>
                <Descriptions.Item label="IMEI">{inventoryDetails  &&  inventoryDetails.iot_device_id ? inventoryDetails.iot_device_id.imei : ""}</Descriptions.Item>
                <Descriptions.Item label="Provided By">{inventoryDetails   &&  inventoryDetails.iot_device_id? inventoryDetails.iot_device_id.provided_by : ""}</Descriptions.Item>
                <Descriptions.Item label="Consignment Tracking Number/Courier Docket">{inventoryDetails  &&  inventoryDetails.iot_device_id ? inventoryDetails.iot_device_id.consignmentTrackingNumber_CourierDocket : ""}</Descriptions.Item>
                <Descriptions.Item label="Customer Order Number">{inventoryDetails  &&  inventoryDetails.iot_device_id ? inventoryDetails.iot_device_id.CustomerOrderNumber : ""}</Descriptions.Item>

              </Descriptions>

            </div>

          </Modal>
          {/* view modal details end */}
          {/* view modal details start */}
          <Modal
            title="Vehicle Timeline"
            destroyOnClose="true"
            centered
            visible={isModalVisibleTimeline}
            onOk={() => setIsModalVisibleTimeline(false)}
            okType="danger"
            onCancel={() => setIsModalVisibleTimeline(false)}
            cancelText="Close"
            width={1000}
            footer={<button type="button" onClick={() => setIsModalVisibleTimeline(false)} class="ant-btn ant-btn-default"><span>Close</span></button>}
          >
            <Row>
              <Col span={24}>
                <Row>
                  <Col span={12}>
                    {/* <h4 className="mb-4 text-secondary">Vehicle Name : {timeline_vehicle_name}</h4> */}
                  </Col>
                  <Col span={12}>
                    <button type="button" className="btn btn-danger float-end mb-3 downloadcsv"  >
                      <CSVLink data={csvData} headers={csvHeaders} filename={downloadFileName} >Download CSV</CSVLink>
                     
                    </button>
                  </Col>
                  </Row>
              </Col>
              
              <Col span={24}>
                {<Table  scroll={{x: 1000}} columns={columnsTimeline} dataSource={dataVehicleTimeline} />}
              </Col>
              
            </Row>

          </Modal>
          {/* view modal details end */}
        </TabPane>
        <TabPane tab="Spares" key="2">
          <Row>
            <Col span={8}>   
              <Search placeholder="Search by Part Number" allowClear  onSearch={onSearch} onPressEnter={(e)=>searchOnEnter(e)}   />          
            </Col>
            <Col span={8}>   
              <Select  name="part_type_filter" className="w-100" id="part_type_filter" onChange={(e)=>handleChangeSelect(e,'part_type_filter')} defaultValue={filterInventory.part_type_filter}>
                <Option value="" >All</Option>
                <Option value="spares">Spares</Option>
                <Option value="battery">Battery</Option>
                <Option value="charger">Charger</Option>              
              </Select>  
            </Col>
            <Col span={8}>
              {(user_role == 'FAE_MAIN_ADMIN'  || user_role == 'FAE_ADMIN')? 
                <TransferSparesComp filterInventory={filterInventory} tabActiveType={tabActiveType} selectedRowsData={selectedRowsData} />
              :''}
              {/* {deleteAllIds.spares.length>0 && (user_role == 'FAE_MAIN_ADMIN' || user_role == 'FAE_ADMIN' )?
                <Button type="danger" className="float-end mb-3 me-3" onClick={()=>onDelete(deleteAllIds.spares,'all')} >Empty Spares Date</Button>
                :''} */}
               <Button type="danger" className={isDeletedSpares?"float-end mb-3 me-3":"float-end d-none mb-3 me-3"} onClick={()=>onDelete(deleteIds.spares)} >Delete Record</Button>
              
            </Col>
          </Row>          
          <Table  scroll={{x: 1000}} rowSelection={(user_role == 'FAE_MAIN_ADMIN' || user_role == 'FAE_ADMIN'  || user_role == 'FAE_CITY_ADMIN'  || user_role == 'FAE_HUB_ADMIN')?{
              type: 'checkbox',
              ...rowSelection,
            }:''}  loading={isLoading} onChange={onChange} columns={columnsSpares} dataSource={dataSourceSpares} />
        </TabPane> 
        <TabPane tab="IOT Devices" key="3">
            <Row className="mb-3"> 
              <Col span={8}>   
                <Search placeholder="Search by Device Serial Number" allowClear  onSearch={onSearch} onPressEnter={(e)=>searchOnEnter(e)}   />          
              </Col>
              <Col span={16}> 
                
                {/* {deleteAllIds.iot_devices.length>0?
                <Button type="danger" className="float-end mb-3 me-3" onClick={()=>onDelete(deleteAllIds.iot_devices,'all')} >Empty IOT Devices Data</Button>
                :''}    */}

                <Button type="danger" className={isDeletedIotDevices?"float-end mb-3 me-3":"float-end d-none mb-3 me-3"} onClick={()=>onDelete(deleteIds.iot_devices)} >Delete Record</Button>
      
              </Col>
             
              
            </Row> 
          <Table   scroll={{x: 1000}} rowSelection={(user_role == 'FAE_MAIN_ADMIN' || user_role == 'FAE_ADMIN'  || user_role == 'FAE_CITY_ADMIN'  || user_role == 'FAE_HUB_ADMIN')?{
              type: 'checkbox',
              ...rowSelection,
            }:''}  loading={isLoading} onChange={onChange} columns={columnsIotDevices} dataSource={dataSourceIotDevices} />
        </TabPane>       
      </Tabs>
         
    </>
  );
}
const mapStateToProps = (state, ownProps = {}) => {
  // console.log('state',state) // state
  /*console.log(ownProps) // {}*/
  const {  isLoggedIn,user,user_city_id,user_role,user_hub_id } = state.auth;
  const { inventoryData,inventoryDetails,EditRecord } = state.inventory;
  return {
    user,
    isLoggedIn,
    user_city_id,
    user_hub_id,
    user_role,
    inventoryData,
    inventoryDetails,
    EditRecord
  };
}

export default connect(mapStateToProps)(invenotyComp);

