export const vehicleCsvHeaders = [
    { label: "Business Supplier", key: "business_supplier" },
    { label: "City", key: "city" },
    { label: "Hub", key: "hub" },
    { label: "Vehicle Name", key: "vehicle_name" },
    { label: "Vehicle Model", key: "vehicle_model" },
    { label: "Vehicle color", key: "vehicle_color" },
    { label: "Vehcile RegNo", key: "vehicle_regNo" },
    { label: "Manufacture Date", key: "manufacture_date" },
    { label: "Date of Sale", key: "date_of_sale" },
    { label: "Date of use", key: "date_of_use" },
    { label: "Chassis No", key: "chassis_no" },
    { label: "Motor/Engine No", key: "moter_engine_no" },
    { label: "Battery No.", key: "battery_no" },
    { label: "2nd Battery No.", key: "battery_no_2s" },
    { label: "Controller No", key: "controller_number" },
    { label: "1st Charger No", key: "charger_no" },
    { label: "2nd Charger No", key: "charger_no_2" },
    { label: "Key No", key: "key_no" },
    { label: "Scooter Invoice", key: "scooter_invoice" },
    { label: "Scooter Reg Date", key: "scooter_reg_date" },
    { label: "Scooter Reg. upto", key: "scooter_reg_upto" },
    { label: "Insurance Company", key: "insurance_company" },
    { label: "Policy No Insurance", key: "policy_number_insurance" },
    { label: "Date Policy Exp", key: "date_policy_expiry" },
    { label: "Permit no", key: "permit_no" },
    { label: "Exp Date", key: "exp_date" },
    { label: "SGST", key: "sgst" },
    { label: "CGST", key: "cgst" },
    { label: "IOT Device", key: "iot_device" },
    { label: "Model", key: "model" },
    { label: "Amount", key: "amount" },
    { label: "Advance Amount", key: "advance_amount" },
    { label: "Image 1", key: "image1" },
    { label: "Image 2", key: "image2" },
    { label: "Image 3", key: "image3" },
]
export const vehicleCsvData = [
    { "business_supplier": "HONDA", "city": "Patrapur", "hub": "Hub1", "vehicle_name": "Honda Navi", "vehicle_model": "Navi" , "vehicle_color": "Red", "vehicle_regNo": "OD 51 AA 0001" , "manufacture_date": "2016-12-01", "date_of_sale": "2017-12-01" , "date_of_use": "2018-02-01", "chassis_no": "ME4JF653JH7009756" , "moter_engine_no": "JF65E71009813", "battery_no": "HE 17 I W3 1708 AJ 2221" , "battery_no_2": "HE 17 I W3 1708 AJ 2222", "controller_number": "HE7217050041" , "charger_no": "SSXH1812341942", "charger_no_2": "SSXH1812341943" , "key_no": "LD2512501", "scooter_invoice": "KA 51 AC 6347 " , "scooter_reg_date": "11th Jan 2021", "scooter_reg_upto": "10th Jan 2023" , "insurance_company": "IFFCO-TOKIO GENERAL INSURANCE CO.LTD", "policy_number_insurance": "1-19AK2RHN/MB228094" , "date_policy_expiry": "1st Nov 2020", "permit_no": "KA/03/CC/MCYCLE/2019/1078" , "exp_date": "18th Nov 2024", "sgst": "" , "cgst": "", "iot_device": "85901" , "model": ""  , "amount": 100, "advance_amount": 100, "image1": "https://fae.tiprodev.xyz/img/cargo.png", "image2": "https://fae.tiprodev.xyz/img/cargo.png", "image3": "https://fae.tiprodev.xyz/img/storm+.png"}, 

    { "business_supplier": "HONDA", "city": "Patrapur", "hub": "Hub1", "vehicle_name": "Honda Cliq", "vehicle_model": "Cliq" , "vehicle_color": "Red", "vehicle_regNo": "OD 51 AB 0002" , "manufacture_date": "2016-12-02", "date_of_sale": "2017-12-01" , "date_of_use": "2018-02-01", "chassis_no": "ME4JF762GH7000749" , "moter_engine_no": "JF76E70007524", "battery_no": "HE 17 I W3 1708 AJ 2221" , "battery_no_2": "HE 17 I W3 1708 AJ 2222", "controller_number": "HE7217050041" , "charger_no": "SSXH1812341942", "charger_no_2": "SSXH1812341943" , "key_no": "LD2512501", "scooter_invoice": "KA 51 AC 6347 " , "scooter_reg_date": "11th Jan 2021", "scooter_reg_upto": "10th Jan 2023" , "insurance_company": "IFFCO-TOKIO GENERAL INSURANCE CO.LTD", "policy_number_insurance": "1-19AK2RHN/MB228094" , "date_policy_expiry": "1st Nov 2020", "permit_no": "KA/03/CC/MCYCLE/2019/1078" , "exp_date": "18th Nov 2024", "sgst": "" , "cgst": "", "iot_device": "85901" , "model": "" , "amount": 100, "advance_amount": 100, "image1": "https://fae.tiprodev.xyz/img/cargo.png", "image2": "https://fae.tiprodev.xyz/img/cargo.png", "image3": "https://fae.tiprodev.xyz/img/storm+.png"} ,
    
    { "business_supplier": "HONDA", "city": "Patrapur", "hub": "Hub1", "vehicle_name": "Honda Dev", "vehicle_model": "Dev" , "vehicle_color": "Yellow", "vehicle_regNo": "OD 51 AB 0003 " , "manufacture_date": "2016-12-02", "date_of_sale": "2017-12-01" , "date_of_use": "2018-02-01", "chassis_no": "ME4JF762GH7000749" , "moter_engine_no": "JF76E70007524", "battery_no": "HE 17 I W3 1708 AJ 2221" , "battery_no_2": "HE 17 I W3 1708 AJ 2222", "controller_number": "HE7217050041" , "charger_no": "SSXH1812341942", "charger_no_2": "SSXH1812341943" , "key_no": "LD2512501", "scooter_invoice": "KA 51 AC 6347 " , "scooter_reg_date": "11th Jan 2021", "scooter_reg_upto": "10th Jan 2023" , "insurance_company": "IFFCO-TOKIO GENERAL INSURANCE CO.LTD", "policy_number_insurance": "1-19AK2RHN/MB228094" , "date_policy_expiry": "1st Nov 2020", "permit_no": "KA/03/CC/MCYCLE/2019/1078" , "exp_date": "18th Nov 2024", "sgst": "" , "cgst": "", "iot_device": "85901" , "model": "" , "amount": 100, "advance_amount": 100, "image1": "https://fae.tiprodev.xyz/img/cargo.png", "image2": "https://fae.tiprodev.xyz/img/cargo.png", "image3": "https://fae.tiprodev.xyz/img/storm+.png"},

    { "business_supplier": "HONDA", "city": "Patrapur", "hub": "Hub1", "vehicle_name": "Honda Dev", "vehicle_model": "Dev" , "vehicle_color": "Red", "vehicle_regNo": "OD 51 AB 0004" , "manufacture_date": "2016-12-02", "date_of_sale": "2017-12-01" , "date_of_use": "2018-02-01", "chassis_no": "ME4JF762GH7000749" , "moter_engine_no": "JF76E70007524", "battery_no": "HE 17 I W3 1708 AJ 2221" , "battery_no_2": "HE 17 I W3 1708 AJ 2222", "controller_number": "HE7217050041" , "charger_no": "SSXH1812341942", "charger_no_2": "SSXH1812341943" , "key_no": "LD2512501", "scooter_invoice": "KA 51 AC 6347 " , "scooter_reg_date": "11th Jan 2021", "scooter_reg_upto": "10th Jan 2023" , "insurance_company": "IFFCO-TOKIO GENERAL INSURANCE CO.LTD", "policy_number_insurance": "1-19AK2RHN/MB228094" , "date_policy_expiry": "1st Nov 2020", "permit_no": "KA/03/CC/MCYCLE/2019/1078" , "exp_date": "18th Nov 2024", "sgst": "" , "cgst": "", "iot_device": "85901" , "model": "" , "amount": 100, "advance_amount":100, "image1": "https://fae.tiprodev.xyz/img/cargo.png", "image2": "https://fae.tiprodev.xyz/img/cargo.png", "image3": "https://fae.tiprodev.xyz/img/storm+.png"},

    
    { "business_supplier": "HONDA", "city": "Patrapur", "hub": "Hub1", "vehicle_name": "Honda Dora", "vehicle_model": "Dora" , "vehicle_color": "Red", "vehicle_regNo": "OD 51 AB 0005" , "manufacture_date": "2016-12-02", "date_of_sale": "2017-12-01" , "date_of_use": "2018-02-01", "chassis_no": "ME4JF762GH7000749" , "moter_engine_no": "JF76E70007524", "battery_no": "HE 17 I W3 1708 AJ 2221" , "battery_no_2": "HE 17 I W3 1708 AJ 2222", "controller_number": "HE7217050041" , "charger_no": "SSXH1812341942", "charger_no_2": "SSXH1812341943" , "key_no": "LD2512501", "scooter_invoice": "KA 51 AC 6347 " , "scooter_reg_date": "11th Jan 2021", "scooter_reg_upto": "10th Jan 2023" , "insurance_company": "IFFCO-TOKIO GENERAL INSURANCE CO.LTD", "policy_number_insurance": "1-19AK2RHN/MB228094" , "date_policy_expiry": "1st Nov 2020", "permit_no": "KA/03/CC/MCYCLE/2019/1078" , "exp_date": "18th Nov 2024", "sgst": "" , "cgst": "", "iot_device": "85901" , "model": "" , "amount": 100, "advance_amount": 100, "image1": "https://fae.tiprodev.xyz/img/cargo.png", "image2": "https://fae.tiprodev.xyz/img/cargo.png", "image3": "https://fae.tiprodev.xyz/img/storm+.png"},

    { "business_supplier": "HONDA", "city": "Patrapur", "hub": "Hub1", "vehicle_name": "Honda Cliq", "vehicle_model": "Cliq" , "vehicle_color": "Red", "vehicle_regNo": "OD 51 AB 0006" , "manufacture_date": "2016-12-02", "date_of_sale": "2017-12-01" , "date_of_use": "2018-02-01", "chassis_no": "ME4JF762GH7000749" , "moter_engine_no": "JF76E70007524", "battery_no": "HE 17 I W3 1708 AJ 2221" , "battery_no_2": "HE 17 I W3 1708 AJ 2222", "controller_number": "HE7217050041" , "charger_no": "SSXH1812341942", "charger_no_2": "SSXH1812341943" , "key_no": "LD2512501", "scooter_invoice": "KA 51 AC 6347 " , "scooter_reg_date": "11th Jan 2021", "scooter_reg_upto": "10th Jan 2023" , "insurance_company": "IFFCO-TOKIO GENERAL INSURANCE CO.LTD", "policy_number_insurance": "1-19AK2RHN/MB228094" , "date_policy_expiry": "1st Nov 2020", "permit_no": "KA/03/CC/MCYCLE/2019/1078" , "exp_date": "18th Nov 2024", "sgst": "" , "cgst": "", "iot_device": "85901" , "model": "" , "amount": 100, "advance_amount": 100, "image1": "https://fae.tiprodev.xyz/img/cargo.png", "image2": "https://fae.tiprodev.xyz/img/cargo.png", "image3": "https://fae.tiprodev.xyz/img/storm+.png"},

    { "business_supplier": "HONDA", "city": "Patrapur", "hub": "Hub1", "vehicle_name": "Honda Cliq", "vehicle_model": "Cliq" , "vehicle_color": "Red", "vehicle_regNo": "OD 51 AB 0007" , "manufacture_date": "2016-12-02", "date_of_sale": "2017-12-01" , "date_of_use": "2018-02-01", "chassis_no": "ME4JF762GH7000749" , "moter_engine_no": "JF76E70007524", "battery_no": "HE 17 I W3 1708 AJ 2221" , "battery_no_2": "HE 17 I W3 1708 AJ 2222", "controller_number": "HE7217050041" , "charger_no": "SSXH1812341942", "charger_no_2": "SSXH1812341943" , "key_no": "LD2512501", "scooter_invoice": "KA 51 AC 6347 " , "scooter_reg_date": "11th Jan 2021", "scooter_reg_upto": "10th Jan 2023" , "insurance_company": "IFFCO-TOKIO GENERAL INSURANCE CO.LTD", "policy_number_insurance": "1-19AK2RHN/MB228094" , "date_policy_expiry": "1st Nov 2020", "permit_no": "KA/03/CC/MCYCLE/2019/1078" , "exp_date": "18th Nov 2024", "sgst": "" , "cgst": "", "iot_device": "85901" , "model": "" , "amount": 100, "advance_amount": 100, "image1": "https://fae.tiprodev.xyz/img/cargo.png", "image2": "https://fae.tiprodev.xyz/img/cargo.png", "image3": "https://fae.tiprodev.xyz/img/storm+.png"}
]