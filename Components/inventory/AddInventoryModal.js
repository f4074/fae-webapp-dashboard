import React , { useState, useEffect, useCallback } from "react";
import { connect,useDispatch, useSelector } from "react-redux";
import { useRouter } from 'next/router';
import { HANDLE_SUCCESS, HANDLE_ERROR, } from "../../provider/ApiProvider";
import Loader from "../loader";
import { createInventory,getAllInventory,getDetailsInventory,deleteInventory,updateInventory} from "../../Redux/Action/inventory";
import { createHub,getAllHubs,allCityHubs,deleteHub,updateHub} from "../../Redux/Action/hubs";
/* ant design */
import 'antd/dist/antd.css';
import { Table, Tag, Space,Modal, Form, Input, Radio , Upload, message,Button ,Descriptions,DatePicker} from 'antd';
import {EditOutlined,DeleteOutlined,InboxOutlined,EyeOutlined} from '@ant-design/icons'
// formik import 
import * as Yup from "yup";
import {VALIDATION_SCHEMA,FORMIK} from "../../provider/FormikProvider";

const AddInventoryModalComp=(props)=> {
    
}

const mapStateToProps = (state, ownProps = {}) => {
    console.log(state) // state
     /*console.log(ownProps) // {}*/  
    const { user } = state.auth;
    const { inventoryData } = state.inventory;
    return {
      user,
      inventoryData
    };
  }
  
export default connect(mapStateToProps)(AddInventoryModalComp);