import React , { useState, useEffect, useCallback } from "react";
import { connect,useDispatch, useSelector } from "react-redux";
import { useRouter } from 'next/router';
import { HANDLE_SUCCESS, HANDLE_ERROR, } from "../../provider/ApiProvider";
import Loader from "../loader";
import { createInventory,getAllInventory,getAllInventoryFilter,getDetailsInventory,deleteInventory,updateInventory,transferInventory} from "../../Redux/Action/inventory";
import { createHub,getAllHubs,allCityHubs,deleteHub,updateHub} from "../../Redux/Action/hubs";
/* ant design */
import 'antd/dist/antd.css';
import { Table, Tag, Space,Modal, Form, Input, Radio , Upload, message,Button ,Descriptions,DatePicker} from 'antd';
import {EditOutlined,DeleteOutlined,InboxOutlined,EyeOutlined} from '@ant-design/icons'
// formik import 
import * as Yup from "yup";
import {VALIDATION_SCHEMA,FORMIK} from "../../provider/FormikProvider";

const TransferSparesComp=(props)=> { 
  const router = useRouter();  
  const [loader, setLoader] = useState(false);  
  const dispatch = useDispatch();
  const { isLoggedIn } = useSelector(state => state.auth);   
  const [form] = Form.useForm();
  const [isModalVisibleTransferVehicle, setIsModalVisibleTransferVehicle] = useState(false);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [modalTitle,setModalTitle]= useState('Add');
  const [modelWidth,setModelWidth]= useState(500);
  const [cityData,setCityData]= useState([]);
  const [allVehiclesData,setAllVehiclesData]= useState([]);
  const [hubData,setHubData]= useState([]);
  const [vehicleDetails,setVehicleDetails]= useState(null);
  
  /* formik code start */   
  var initialValues = {
    selectedRowsData:props.selectedRowsData,
    from_city_id:props.filterInventory?props.filterInventory.city_id:'',
    from_hub_id: props.filterInventory?props.filterInventory.hub_id:"",
    from_city_name:props.filterInventory?props.filterInventory.city_name:'',
    from_hub_name: props.filterInventory?props.filterInventory.hub_name:"",
    to_city_id:"",
    to_hub_id: "",
    type: 'spares',
    // transferQuantity:0 ,
    created:new Date(),
    updated:new Date()
  };
  const validationSchema = Yup.object() 
  .shape({
    to_city_id: Yup.string().default(null).nullable()
    .required('City is required'),
    to_hub_id: Yup.string().default(null).nullable()
    .required('Hub is required'),
    // transferQuantity: Yup.number().default(null).nullable()
    // .required('Quantity is required'),
  });    

  const submitForm =(data)=>{ 
    callAPI(data);   
  }
  const formik = FORMIK(initialValues,validationSchema,submitForm);
  formik.handleChange = (e) => {
    if(e.target.name == 'to_city_id'){      
      const city_id_selected = e.target.value;
      formik.setFieldValue("to_city_id",city_id_selected); 
      formik.setFieldValue("to_hub_id",""); 
      getAllCityHubsAPI(city_id_selected);

    }else if(e.target.name == 'to_hub_id'){      
      const hub_id_selected = e.target.value;
      formik.setFieldValue("to_hub_id",hub_id_selected);

    }else if(e.target.name == 'vehicle_id'){       
      const vehicle_id_selected = e.target.value;
      formik.setFieldValue("vehicle_id",vehicle_id_selected); 
      showVehicleDetails(vehicle_id_selected);
    }
    /* else if(e.target.name == 'transferQuantity'){ 
      formik.setFieldValue(e.target.name,e.target.value); 
      const qty =e.target.value;
      console.log('vehicleDetails',vehicleDetails);
      // checkQuantity(qty,)

    } */else{
      formik.setFieldValue(e.target.name,e.target.value); 
    }
  };
  const showVehicleDetails=(vehicle_id_selected)=>{
    const getIndex = allVehiclesData.findIndex(x=>x._id==vehicle_id_selected);
    setVehicleDetails(allVehiclesData[getIndex]);    
  }
  const getAllCityHubsAPI=(city_id)=>{
    try {
      setLoader(true);      
      dispatch(allCityHubs(city_id))
      .then((res) => {
        setLoader(false);
        setHubData(res.data);
      })
      .catch((error) => {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });
    } catch (error) {
      setLoader(false);
      // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  /* formik code  */
const callAPI = async (data) => {
  const dataGet = JSON.parse(data);
  try {
      setLoader(true);      
      dispatch(transferInventory(data))
      .then((res) => {
        setLoader(false); 
        if(res.status == true){
            setIsModalVisible(false);
            HANDLE_SUCCESS(res.message); 
            formik.resetForm();
            getAllInventoryData();
        }else{
            HANDLE_ERROR(res.message);
        }          
    })
    .catch((error) => {
      setLoader(false);
      HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    });     
  } catch (error) {
    setLoader(false);
    HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
  }
};
  
  
  /* formik code end */
  const onAdd=(type)=>{ 
    console.log('props.filterInventory',props.filterInventory)
    formik.setFieldValue('selectedRowsData',props.selectedRowsData?props.selectedRowsData:''); 
    formik.setFieldValue('from_city_id',props.filterInventory?props.filterInventory.city_id:''); 
    formik.setFieldValue('from_hub_id',props.filterInventory?props.filterInventory.hub_id:''); 
    formik.setFieldValue('from_city_name',props.filterInventory?props.filterInventory.city_name:''); 
    formik.setFieldValue('from_hub_name',props.filterInventory?props.filterInventory.hub_name:''); 
    // formik.setFieldValue('quantity',props.selectedRowsData?props.selectedRowsData.sparesDetails.quantity:''); 

    if(!(props.filterInventory && props.filterInventory.city_id && props.filterInventory.hub_id)){
      HANDLE_ERROR("Please Select City and Hub");
      return false;
    }
    if((props.selectedRowsData).length <=0){
      HANDLE_ERROR("Please select atleast one record.");
      return false;
    }

    // getAllInventoryData();   
    setModalTitle("Transfer Spares");
    setModelWidth(1000)
    setIsModalVisible(true); 
  }
 
const getAllInventoryData=()=>{
    try {
      setLoader(true);      
      dispatch(getAllInventoryFilter(props.filterInventory))
      .then((res) => {
        setLoader(false);
        setCityData(res.data.citiesList);
        // setAllVehiclesData(res.data.allVehicles);
        // setHubData(res.data.allHubs);
      })
      .catch((error) => {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });
    } catch (error) {
      setLoader(false);
      // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
}
  
function onChange(pagination, filters, sorter, extra) {
    console.log('params', pagination, filters, sorter, extra);
}
  
  return (
    <>
      <Loader loading={loader} />
        <Button type="danger" className="float-end mb-3 me-3" onClick={()=>onAdd('transfer_spares')} >Transfer Spares</Button>
      {/* transfer vehicle model start */}
      <Modal
        title={modalTitle}
        destroyOnClose="true"
        centered
        visible={isModalVisible}
        onOk={()=>{
            formik.submitForm()                  
        }} 
        okType="danger"
        okText="Save"
        onCancel={() => {
                setIsModalVisible(false)     
                formik.setFieldValue("city_id",""); 
                formik.setFieldValue("hub_id","");             
            }
        }
        cancelText="Close"
        width={1000}
        // footer={<button type="button" onClick={() => setIsModalVisibleTransferVehicle(false)} class="ant-btn ant-btn-default"><span>Close</span></button>}
      >
        <div className="col-12 row">
          <form onSubmit={formik.handleSubmit} className=""  >
          <div className="col-12">            
                <div className="pt-4 pb-2 pe-4 ps-4 rounded-6 bg-white"> 
                  <div className="d-flex">
                      <div className="mb-3 col-6 me-3">
                          <div><h5>From City</h5></div>
                          <p>{formik.values.from_city_name}</p>
                      </div>  
                      <div className=" mb-3 col-6">
                          <div><h5>To City</h5></div>
                          <select className="form-select" id="to_city_id" name="to_city_id" aria-label="City" value={formik.values.to_city_id}
                          onChange={formik.handleChange}
                          onBlur={formik.handleBlur} >
                              <option value="" selected  >Choose City</option>
                              {
                              cityData.map(function(item, i){
                                  return <option value={item._id} >{item.name}</option>
                              })
                              }
                          </select> 
                          {formik.errors.to_city_id && formik.touched.to_city_id && (
                              <div className="input-feedback invailid_feedback">{formik.errors.to_city_id}</div>
                          )}
                      </div>  
                  </div>
                  <div className="d-flex ">
                      <div className="mb-3 col-6 me-3">
                          <h5>From Hub</h5>
                          <p>{formik.values.from_hub_name}</p>
                      </div>  
                      <div className="mb-3 col-6">
                          <div><h5>To Hub</h5></div>
                          <select className="form-select"  id="to_hub_id" name="to_hub_id" aria-label="City" value={formik.values.to_hub_id}
                              onChange={formik.handleChange}
                              onBlur={formik.handleBlur} >
                              <option value="" selected  >Choose Hub</option>
                              {
                              hubData.map(function(item, i){
                                  return <option value={item._id} >{item.title}</option>
                              })
                              }
                          </select>  
                          {formik.errors.to_hub_id && formik.touched.to_hub_id && (
                              <div className="input-feedback invailid_feedback">{formik.errors.to_hub_id}</div>
                          )}                          
                      </div>  
                  </div>
              </div> 
              {/* <div className="mb-3 col-12">
                <h6 className="mb-3">Quantity</h6>
                <Input
                    type="text"                  
                    id="transferQuantity"
                    name="transferQuantity"
                    placeholder="Quantity"
                    value={formik.values.transferQuantity}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    className={
                    formik.errors.transferQuantity && formik.touched.transferQuantity
                        ? "error"
                        : ""
                    }
                />  
                {formik.errors.transferQuantity && formik.touched.transferQuantity && (
                    <div className="input-feedback invailid_feedback">{formik.errors.transferQuantity}</div>
                )}              
              </div> */}
              
          </div>
          </form> 
        </div>
      </Modal>
      {/* transfer vehicle model end */}    
    </>
  );
}
const mapStateToProps = (state, ownProps = {}) => {
  const { user } = state.auth;
  const { inventoryData } = state.inventory;
  return {
    user,
    inventoryData
  };
}

export default connect(mapStateToProps)(TransferSparesComp);

