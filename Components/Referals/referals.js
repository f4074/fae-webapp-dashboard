import React, { useState, useEffect, useCallback } from "react";
import Image from 'next/image'
import { connect, useDispatch, useSelector } from "react-redux";
import { useRouter } from 'next/router';
import { HANDLE_SUCCESS, HANDLE_ERROR } from "../../provider/ApiProvider";
import { IMAGE_URL,NO_IMAGE_ERROR } from "../../provider/EndPoints";
import { changeDateFormat } from "../../provider/helper";
import Loader from "../../Components/loader";
import { createReferal,getAllReferals,deleteReferal,deleteMultipleReferals,updateReferal} from "../../Redux/Action/referals";
import { createHub,getAllHubs,allCityHubs,deleteHub,updateHub} from "../../Redux/Action/hubs";
/* ant design */
import 'antd/dist/antd.css';
import { Table,Tabs, Tag, Space, Modal, Form, Input, Radio, Upload, message, Button, Descriptions, DatePicker,Select,Switch,Row,Col } from 'antd';
import { EditOutlined, DeleteOutlined, InboxOutlined, EyeOutlined, SearchOutlined } from '@ant-design/icons'
// formik import 
import * as Yup from "yup";
import { VALIDATION_SCHEMA, FORMIK } from "../../provider/FormikProvider";
import FormList from "antd/lib/form/FormList";
import moment from 'moment';

const ReferalsComp = (props) => {
  const router = useRouter();
  const [loader, setLoader] = useState(false);
  const dispatch = useDispatch();
  const { isLoggedIn } = useSelector(state => state.auth);
  const [form] = Form.useForm();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [modalTitle,setModalTitle]= useState('Add New User');
  const [isModalVisibleDetails, setIsModalVisibleDetails] = useState(false);
  const [dataSource, setDataSource] = useState([]);
  const [dataSourceReferal, setDataSourceReferal] = useState([]);
  const [userKycDeatils, setUserKycDeatils] = useState([]);
  const [cityDataFilter, setCityDataFilter] = useState([]);
  const [hubDataFilter, setHubDataFilter] = useState([]);
  const [filterArray, setFilter] = useState({'city_id':props.user_city_id?props.user_city_id[0]:'','hub_id':(props.user_role == 'FAE_HUB_ADMIN' || props.user_role == 'FAE_HUB_MANAGER') && props.user_hub_id?props.user_hub_id[0]:''});
  const [searchKey, setSearchKey] = useState(null);
  const [isFilter, setIsFilter] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [editId, setEditId] = useState('');
  const [activeTab, setActiveTab] = useState(1);

  const [user_role, setuser_role] = useState(props.user_role);  
  const [user_city_id, setuser_city_id] = useState(props.user_hub_id);
  const [user_hub_id, setuser_hub_id] = useState(props.user_city_id);


  const { Search } = Input;
  const { Option } = Select;
  const { TabPane } = Tabs;
  var setCityStatus =0;
  useEffect(() => {
    if (!isLoggedIn) {
      router.push('/login');
    } else {
      setuser_role(props.user_role);
      setuser_hub_id(props.user_hub_id)
      setuser_city_id(props.user_city_id);
    
      if(props.user_role == 'FAE_HUB_ADMIN' || props.user_role == 'FAE_HUB_MANAGER'){          
        setFilter({...filterArray, hub_id: props.user_hub_id[0]})
      }else{
       
        // setFilter({...filterArray, city_id: props.user_city_id[0] ,hub_id: ""})
      }

      if(props.user_city_id){        
        getAllCityHubsAPI(props.user_city_id[0]);
      } 
      // getAllReferalsData();
    }
  }, [])

  useEffect(() => {
    if(cityDataFilter.length >0){
      if(props.user_role == 'FAE_MAIN_ADMIN' || props.user_role == 'FAE_ADMIN'){
        const getCity = cityDataFilter.filter((i)=> props.user_city_id.includes(i._id));
        
        if(getCity.length >0){        
          setFilter({...filterArray, city_id: props.user_city_id[0]})
        }else{
          setFilter({...filterArray, city_id: "" ,hub_id: ""})
        }
      }
    }  
  }, [cityDataFilter.length >0 &&  setCityStatus == 0])

  useEffect(() => { 
    getAllReferalsData(); 
  }, [filterArray])

  useEffect(() => {
    getAllReferalsData();
  }, [activeTab])
  
  /* formik code start */   
  var initialValues = {
    city_id:user_city_id?user_city_id[0]:'',
    campaign_name:'',
    campaign_description:'',
    discount_amount:'',
    start_date:'',
    end_date:'',
    active_status:true,
    created:new Date(),
    updated:new Date()
  };
  const validationSchema = Yup.object() 
  .shape({
    city_id: Yup.string().default(null).nullable()
    .required('Please select City'),
    campaign_name: Yup.string().default(null).nullable()
    .required('Campaign name is required'),  
    campaign_description: Yup.string().default(null).nullable()
    .required('Campaign Description is required'),
    discount_amount: Yup.number().default(null).nullable()
    .required('Discount Amount is required'),
    start_date: Yup.string().default(null).nullable()
    .required('Please select Start Date'),
    end_date: Yup.string().default(null).nullable()
    .required('Please select End Date'),
  }); 
  const submitForm =(data)=>{  
      callAPI(data);   
  }
  const formik = FORMIK(initialValues,validationSchema,submitForm);

  formik.handleChange = (e,type=null) => {
    if(type == 'end_date'){
      formik.setFieldValue('end_date',e._d);   
    }else if(type == 'start_date'){
      formik.setFieldValue('start_date',e._d);   
    }else{
      formik.setFieldValue(e.target.name,e.target.value);   
      if(e.target.name == 'city_id'){
        setHubDataFilter([]);          
        // setFilter({...filterArray,  city_id: e.target.value,hub_id:''})
        getAllCityHubsAPI(e.target.value);      
      } 
    } 
  };
  /* formik code end */  
  /* call Api for create referals and update referals data */ 
  const callAPI= (data)=> {
    if(isEditing){
        const dataParse = JSON.parse(data)
        const editId =dataParse._id;
        var ApiPath = updateReferal(data,editId);
    }else{
        var ApiPath = createReferal(data)
    }
    try {
        setLoader(true);      
        dispatch(ApiPath)
        .then((res) => {
            setLoader(false);      
            if(res.status === true){
                HANDLE_SUCCESS(res.message);
                getAllReferalsData();
                // setDataSource(res.campaignDataAll); 
                // setDataSourceReferal(res.referalsDataAll)                  
                setIsModalVisible(false)           
                formik.resetFields();  
                formik.setValues(initialValues); 
            }else{
                HANDLE_ERROR(res.message);
                // setIsModalVisibleDetails(true)
            }
        })
        .catch((error) => {
            setLoader(false);
            // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
        });
    } catch (error) {
        setLoader(false);
        HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  /* open create popup onclick create new  referals button */ 
  const onAdd=()=>{
    setIsEditing(false);
    setModalTitle("Create New Referrals");
    setIsModalVisible(true);  
    formik.setValues(initialValues);   
  }

/* open edit popup onclick edit  referals button */ 
  const  onEdit=(record)=>{
    console.log('record',record);
    /* if(record){
      getAllCityHubsAPI(record.city_id)
    } */
    console.log('record',record);
    setIsEditing(true);
    setIsModalVisible(true);
    setModalTitle("Edit");
    formik.setValues(
    {
      _id:record._id,
      city_id:record.city_id._id,
      campaign_name:record.campaign_name,
      campaign_description:record.campaign_description,
      discount_amount:record.discount_amount,
      start_date:record.start_date,
      end_date:record.end_date,
      active_status:record.active_status,
      updated:new Date()
    });


  }
  /* show confirm dialog onclick delete button */ 
  const onDelete = (record) => {
    // showModal();
    Modal.confirm({
      title: 'Are you sure, you want to delete this record?',
      okText: "Yes",
      okType: "danger",
      onOk: () => {
        deleteCallApi(record);
      }
    })
  }
  /* call api for delete record */ 
  const deleteCallApi = (record) => {
    try {
      setLoader(true);      
      dispatch(deleteReferal(record._id))
      .then((res) => {
        setLoader(false); 
        if(res.status == true){          
          HANDLE_SUCCESS(res.message); 
          // setDataSource(res.data);         
          setDataSource(pre=>{
            return pre.filter((client)=>client._id!=record._id);
          })  
        }else{
          HANDLE_ERROR(res.message);
        }  
      })
      .catch((error) => {
        setLoader(false);
        HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });     
    } catch (error) {
      setLoader(false);
      HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  /* get all referals data list */ 
  const getAllReferalsData=()=>{
    try {
        setLoader(true);      
        dispatch(getAllReferals(filterArray))
        .then((res) => {
          setLoader(false);
          if(setCityStatus ==0){
            setCityDataFilter(res.citiesData)
            setCityStatus =1; 
          }
         
          setDataSource(res.data);
          setDataSourceReferal(res.referalsDataAll);
        })
        .catch((error) => {
          setLoader(false);
          // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
        });
      } catch (error) {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      }
  }
  /* get all hub list by selecting city */ 
  const getAllCityHubsAPI=(city_id)=>{
    try {
      setLoader(true);      
      dispatch(allCityHubs(city_id))
      .then((res) => {
        setLoader(false);
        setHubDataFilter(res.data);
      })
      .catch((error) => {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });
    } catch (error) {
      setLoader(false);
      // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  /* change status true/false */ 
  function onChangeActive(checked,data) {
    setIsEditing(true);
    const getStatus = {
        _id:data._id,
        active_status:checked,
        updated:new Date()
    }
    var ApiPath = updateReferal(getStatus,data._id);
    try {
        setLoader(true);      
        dispatch(ApiPath)
        .then((res) => {
            setLoader(false);      
            if(res.status === true){
              HANDLE_SUCCESS(res.message);          
              setDataSource(res.campaignDataAll); 
            }else{
              HANDLE_ERROR(res.message);
            }
        })
        .catch((error) => {
            setLoader(false);
            // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
        });
    }catch (error) {
        setLoader(false);
        HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  /* funtion for disable date */ 
  function disabledDate(current) {
    // Can not select days before today
    return current && current < moment().startOf('day');
  }
  /* columns For referals */ 
  const columnsReferal = [
    {
      key: "1",
      title: 'Name',
      render: (record) => {
        if(record.user_id){
           return record.user_id.name
        }       
      },
    },
    {
      key: "2",
      title: 'Referral Used By',
      render: (record) => {
        if(record.reffered_user_id){
          return record.reffered_user_id.name
        }
      },
    },
    {
      key: "4",
      title: 'Referral Amount',
      render: (record) => {
        return record.referral_amount
      },
    },
    {
      key: "4",
      title: 'Referral Type',
      render: (record) => {
        return record.referral_type
      },
    },
    {
        key: "3",
        title: 'Created By',
        render: (record) => {
          return changeDateFormat(record.created)
        },
    },    
    {
      key: "5",
      title: 'Referral Code',
      // dataIndex: 'active_status',
      render: (record) => {
        return <>
          {record.user_id?record.user_id.referral_code:''}         
        </>;
      }
    },
  ]
  /* columns  For campaign */ 
  const columns = [
    {
      key: "1",
      title: 'Campaign Name',
      dataIndex: 'campaign_name',
    },{
      key: "2",
      title: 'Discount Amount',
      dataIndex: 'discount_amount',
    },{
      key: "5",
      title: 'Campaign Details',
      dataIndex: 'campaign_description',
    },
    {
        key: "3",
        title: 'Start Date',
        dataIndex: 'start_date',
        render: (start_date) => {
          return changeDateFormat(start_date)
        },
    },
    {
      key: "4",
      title: 'End Date',
      dataIndex: 'end_date',
      render: (end_date) => {
        return changeDateFormat(end_date)
      },
    },
    {
      key: "5",
      title: 'Inactive/Active',
      // dataIndex: 'active_status',
      render: (record) => {
        return <> 
        <Switch checked={record.active_status} name="active_status"  onChange={(value)=>{onChangeActive(value,record)}} className="" />
        </>;
      }
    },   
    {
      key: "7",
      title: 'Action',
      // dataIndex: '_id',
      render: (record) => {
        return <>          
           <i className="fa fa-edit me-2"  onClick={() => {
               onEdit(record)    
          }}  style={{'color': 'blue','cursor':'pointer'}}></i>

          <i className="fa fa-trash me-2"  onClick={() => {
               onDelete(record)    
          }}  style={{'color': 'red','cursor':'pointer'}}></i>
        </>
      }
    },
   
  ];
  /* table onchange */
  function onChange(pagination, filters, sorter, extra) {
    // console.log('params', pagination, filters, sorter, extra);
    const filterData ={
      sortField: sorter.field,
      sortOrder: sorter.order,
      pagination,
      ...filters,
    }
  }
  /* function for city and hub filter */ 
  function handleChangeSelect(value,selectType) {
    if(selectType == 'city'){
      getAllCityHubsAPI(value);
      setFilter({...filterArray,  city_id: value})
      
    }else{
      // setFilter({...filterArray,  hub_id: value})
    }
  }
  /* search data on enter  */ 
  const searchOnEnter=(e)=>{
    const value = e.target.value;
    if(value.length >0){
      setSearchKey(value);
    }else{
      setSearchKey('null');
    }
  }
  /* search data onclick search button  */ 
  const onSearch = value => {
    if(value.length >0){
      setSearchKey(value);
    }else{
      setSearchKey('null');
    }    
  };


  function tabChange(key) {
    // console.log(key);
    setActiveTab(key);
  }
  
  return (
    <>
      <Loader loading={loader} />
      {/* <h2 className="mb-4 text-danger fw-bold fs-3">Referals</h2> */}

      <Tabs defaultActiveKey="1" onChange={tabChange}>
        <TabPane tab="User Referrals" key="1">
        <Row className="mb-2 mt-3">             
              <Col span={5} className="mb-3">
                {user_role != 'FAE_HUB_ADMIN' && user_role != 'FAE_HUB_MANAGER'?
                <Select  value={filterArray.city_id} name="city_filter" className="w-100" id="city_filter" onChange={(e)=>handleChangeSelect(e,'city')}>
                  {(user_role == 'FAE_MAIN_ADMIN' || user_role == 'FAE_ADMIN'?
                  <Option value="">Select City</Option>:'')
                  }
                
                {
                  cityDataFilter.map(function(item, i){
                    
                    if(user_role == 'FAE_CITY_ADMIN' && user_city_id.includes(item._id)){
                      return <Option key={item._id} value={item._id} >{item.name}</Option>
                    }else if(user_role != 'FAE_CITY_ADMIN'){
                      return <Option key={item._id} value={item._id} >{item.name}</Option>
                    }                      
                  })
                  
                }
              </Select>
              :''}
              </Col>
              <Col span={5} className="mb-3">
                {/* <Select value={filterArray.hub_id} style={{ width: 120 }} className="w-100" name="hub_filter" id="hub_filter"  onChange={(e)=>handleChangeSelect(e,'hub')}>
                  <Option value="">Select Hub</Option>
                  {
                    hubDataFilter.map(function(item, i){
                      if(user_hub_id.includes(item._id)){
                        return <Option value={item._id} >{item.title}</Option>
                      }else if(user_role == 'FAE_MAIN_ADMIN' || user_role == 'FAE_ADMIN'){
                        return <Option value={item._id} >{item.title}</Option>
                      }                      
                    })
                  }
                </Select> */}
              </Col>
             
            </Row>
            <Table scroll={{x: 1000}} loading={isLoading} onChange={onChange} columns={columnsReferal} dataSource={dataSourceReferal} />
            {/* rowSelection={{ type: 'checkbox' }} */}
        </TabPane>
        {(user_role != 'FAE_HUB_ADMIN' && user_role != 'FAE_STAFF')?
          <TabPane tab="Campaign" key="2">
            <div className="col-12 d-flex mb-3">
              {/* <div className="col-2">
                <Search placeholder="Search by Name" allowClear  onSearch={onSearch} onPressEnter={(e)=>searchOnEnter(e)}   />
              </div> */}
              <div className="col-6 d-flex">
                <div className="col-4 ms-2">
                {user_role != 'FAE_HUB_ADMIN' && user_role != 'FAE_HUB_MANAGER'?
                  <Select defaultValue={filterArray.city_id} name="city_filter" className="w-100" id="city_filter" onChange={(e)=>handleChangeSelect(e,'city')}>
                    {(user_role == 'FAE_MAIN_ADMIN' || user_role == 'FAE_ADMIN'?
                    <Option value="">Select City</Option>:'')
                    }
                    {
                      cityDataFilter.map(function(item, i){
                        if(user_role == 'FAE_CITY_ADMIN' && user_city_id.includes(item._id)){
                          return <Option key={item._id} value={item._id} >{item.name}</Option>
                        }else if(user_role != 'FAE_CITY_ADMIN'){
                          return <Option key={item._id} value={item._id} >{item.name}</Option>
                        } 
                      })
                    }
                  </Select>
                  :''}
                </div>
                <div className="col-4 ms-2">
                  {/* <Select defaultValue="" style={{ width: 120 }} className="w-100" name="hub_filter" id="hub_filter"  onChange={(e)=>handleChangeSelect(e,'hub')}>
                    <Option value="">Select Hub</Option>
                    {
                      hubDataFilter.map(function(item, i){
                          return <Option key={item._id} value={item._id} >{item.title}</Option>
                      })
                    }
                  </Select> */}
                </div>
              </div>
              <div className="col-6">   
                {(user_role != "FAE_STAFF" && user_role != "FAE_HUB_MANAGER")?           
                <button type="button" className="btn btn-danger float-end mb-3 " onClick={()=>onAdd()} >Create New</button> 
                :''} 
              </div>
            </div>     
            <Table  scroll={{x: 1000}}  loading={isLoading} onChange={onChange} columns={columns} dataSource={dataSource} />
            {/* rowSelection={{ type: 'checkbox' }} */}
            {/* Edit modal start */}
            <Modal
              title={modalTitle}
              destroyOnClose="true"
              centered
              visible={isModalVisible}
              onOk={()=>{
                formik.submitForm()                  
              }} 
              onCancel={()=>{
                setIsModalVisible(false)
                form.resetFields();
              // Modal.destroyAll();
              }}
              okType="danger"
              okText="Save"
              cancelText="Close"
              width={1000}       
            >
              <form onSubmit={formik.handleSubmit} className="" >           
                  <div className="mb-3 col-12 d-flex">
                      <div className="form-floating mb-3  col-12 me-3">
                          <select name="city_id" className={
                                  formik.errors.city_id && formik.touched.city_id
                                      ? "text-input error form-control rounded-4 "
                                      : "text-input form-control rounded-4 "
                                  } value={formik.values.city_id} id="city_id" onBlur={formik.handleBlur} onChange={formik.handleChange}>
                          {/* <option value="">Select City</option> */}
                          
                          {                          
                            cityDataFilter.map(function(item, i){                            
                              if(user_role == 'FAE_CITY_ADMIN' && user_city_id.includes(item._id)){
                                return <option key={item._id} value={item._id} >{item.name}</option>
                                
                              }else if(user_role != 'FAE_CITY_ADMIN'){                              
                                return <option key={item._id} value={item._id} >{item.name}</option>
                              } 
                            })
                          }
                        </select>
                          <label htmlFor="floatingInput">Select City</label>
                          {formik.errors.city_id && formik.touched.city_id && (
                              <div className="input-feedback invailid_feedback">{formik.errors.city_id}</div>
                          )}
                      </div>
                      {/* <div className="form-floating mb-3  col-6">
                          <select  name="hub_id" className={
                                  formik.errors.hub_id && formik.touched.hub_id
                                      ? "text-input error form-control rounded-4 "
                                      : "text-input form-control rounded-4 "
                                  } id="hub_id" value={formik.values.hub_id} onBlur={formik.handleBlur} onChange={formik.handleChange}>
                            <option value="">Select Hub</option>
                            {
                              hubDataFilter.map(function(item, i){
                                  return <option key={item._id} selected={formik.values.hub_id==item._id?true:false} value={item._id} >{item.title}</option>
                              })
                            }
                          </select>
                            <label htmlFor="floatingInput">Select Hub</label>
                            {formik.errors.hub_id && formik.touched.hub_id && (
                                <div className="input-feedback invailid_feedback">{formik.errors.hub_id}</div>
                            )}
                      </div> */}
                  </div>
                  <div className="mb-3 col-12 d-flex">                  
                    <div className="form-floating mb-3  col-6  me-3">
                      <DatePicker disabledDate={disabledDate} value={formik.values.start_date?moment(formik.values.start_date):''} format='DD-MM-YYYY' placeholder="Start Date"  className={
                          formik.errors.start_date && formik.touched.start_date
                              ? "text-input error form-control rounded-4 "
                              : "text-input form-control rounded-4 "
                          }  name="start_date" id="start_date" onClear={(e)=>handleChange(e,'start_date')}   
                          onChange={(e)=>formik.handleChange(e,'start_date')}
                          onBlur={formik.handleBlur} />
                    
                      <label htmlFor="floatingInput">Start Date </label>
                      {formik.errors.end_date && formik.touched.start_date && (
                          <div className="input-feedback invailid_feedback">{formik.errors.start_date}</div>
                      )}
                    </div>
                    <div className="form-floating mb-3  col-6  me-3">
                      <DatePicker disabledDate={disabledDate} value={formik.values.end_date?moment(formik.values.end_date):''} format='DD-MM-YYYY' placeholder="End Date"  className={
                          formik.errors.end_date && formik.touched.end_date
                              ? "text-input error form-control rounded-4 "
                              : "text-input form-control rounded-4 "
                          }  name="end_date" id="end_date" onClear={(e)=>handleChange(e,'end_date')}   
                          onChange={(e)=>formik.handleChange(e,'end_date')}
                          onBlur={formik.handleBlur} />
                    
                      <label htmlFor="floatingInput">End Date </label>
                      {formik.errors.end_date && formik.touched.end_date && (
                          <div className="input-feedback invailid_feedback">{formik.errors.end_date}</div>
                      )}
                    </div>
                  </div>
                  <div className="mb-3 col-12 d-flex">
                      <div className="form-floating mb-3  col-6 me-3">
                          <Input
                              type="text"                  
                              id="campaign_name"
                              name="campaign_name"
                              placeholder="Campaign Name"
                              value={formik.values.campaign_name}
                              onChange={formik.handleChange}
                              onBlur={formik.handleBlur}
                              className={
                              formik.errors.campaign_name && formik.touched.campaign_name
                                  ? "text-input error form-control rounded-4 "
                                  : "text-input form-control rounded-4 "
                              }
                          />
                          <label htmlFor="floatingInput">Campaign Name</label>
                          {formik.errors.campaign_name && formik.touched.campaign_name && (
                              <div className="input-feedback invailid_feedback">{formik.errors.campaign_name}</div>
                          )}
                      </div>
                      <div className="form-floating mb-3  col-6">
                          <Input
                              type="text"                  
                              id="discount_amount"
                              name="discount_amount"
                              placeholder="Discount Amount"
                              value={formik.values.discount_amount}
                              onChange={formik.handleChange}
                              onBlur={formik.handleBlur}
                              className={
                              formik.errors.discount_amount && formik.touched.discount_amount
                                  ? "text-input error form-control rounded-4 "
                                  : "text-input form-control rounded-4 "
                              }
                          />
                          <label htmlFor="floatingInput">Discount Amount </label>
                          {formik.errors.discount_amount && formik.touched.discount_amount && (
                              <div className="input-feedback invailid_feedback">{formik.errors.discount_amount}</div>
                          )}
                      </div>
                      
                  </div>
                  <div className="mb-3 col-12 d-flex">  
                  <div className="form-floating mb-3  col-12 me-3">
                          <Input
                              type="text"                  
                              id="campaign_description"
                              name="campaign_description"
                              placeholder="Campaign Description"
                              value={formik.values.campaign_description}
                              onChange={formik.handleChange}
                              onBlur={formik.handleBlur}
                              className={
                              formik.errors.campaign_description && formik.touched.campaign_description
                                  ? "text-input error form-control rounded-4 "
                                  : "text-input form-control rounded-4 "
                              }
                          />
                          <label htmlFor="floatingInput">Campaign Description</label>
                          {formik.errors.campaign_description && formik.touched.campaign_description && (
                              <div className="input-feedback invailid_feedback">{formik.errors.campaign_description}</div>
                          )}
                      </div>
                  </div>

              </form>
            </Modal>      
            {/* Edit modal end */}
          </TabPane>
        :''}
      </Tabs>      
    </>
  );
}
const mapStateToProps = (state, ownProps = {}) => {  
  const {  isLoggedIn,user,user_city_id,user_role,user_hub_id  } = state.auth;
  const { inventoryData } = state.inventory;
  return {
    user,
    isLoggedIn,
    user_city_id,
    user_hub_id,
    user_role,
    inventoryData
  };
}

export default connect(mapStateToProps)(ReferalsComp);

