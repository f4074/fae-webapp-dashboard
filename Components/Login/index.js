import React, { useState,useEffect } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { connect,useDispatch, useSelector } from "react-redux";
import { useRouter } from 'next/router'
import Image from "next/image";
import Fablogo from "../../public/img/fae-logo.svg";
import Link from "next/link";
import "../../node_modules/bootstrap-icons/font/bootstrap-icons.css"


import { HANDLE_SUCCESS, HANDLE_ERROR, HTTP_SERVICE_CALL } from "../../provider/ApiProvider";
import  {SET_DATA_LOCALSTORAGE,REMEMBER_PASSWORD,GET_DATA_LOCALSTORAGE} from "../../provider/LocalStorageProvider"
import { BASE_URL, POST, IS_LOADING,ADMIN_SIGN_IN } from "../../provider/EndPoints";
import Loader from "../../Components/loader";
import Spinner from "../../Components/Spinner/Spinner";
import { Login } from "../../Redux/Action/auth";

// formik import 
import * as Yup from "yup";
import {VALIDATION_SCHEMA,FORMIK} from "../../provider/FormikProvider";


const LoginComp=(props) => {
  const [passwordType, setPasswordType] = useState("password");
  const [passwordInput, setPasswordInput] = useState("");
  const handlePasswordChange =(evnt)=>{
      setPasswordInput(evnt.target.value);
  }
  const togglePassword =()=>{
    if(passwordType==="password")
    {
      setPasswordType("text")
      return;
    }
    setPasswordType("password")
  }
  // console.log('props',props);
  const router = useRouter();  
  const [loader, setLoader] = useState(false);  
  const dispatch = useDispatch();
  const { isLoggedIn } = useSelector(state => state.auth);

  useEffect(()=>{
    // setLoader(true);
    // 
    if (isLoggedIn) {
      router.push('/dashboard');
    }
      
  },[])

  /* formik code start */
  if (typeof window !== 'undefined') {   
    var initialValues = {
      email: GET_DATA_LOCALSTORAGE("email_rm"),
      mobile: '',
      password:GET_DATA_LOCALSTORAGE("password_rm"),
      rememberPassword:Boolean(GET_DATA_LOCALSTORAGE('remember_pass'))
    };
  }else{
    var initialValues = {
      email:null,
      mobile: null,
      password:null,
      rememberPassword:null
    };
  }


  const validationSchema = Yup.object() 
  .shape({
    password: Yup.string().default(null).nullable()
    .min(6, 'Must be greater then 6 or equal characters')
    .required('Password is required'),
    email: Yup.string().default(null).nullable().email('Invalid email address').required('Email / Mobile is required'),
  });
  
  const submitForm =(data)=>{
      // console.log('data',data);
      // call api here
      // const dataGet = JSON.stringify(data, null, 2);     
      LoginSubmit(data);
      
  }
  const formik = FORMIK(initialValues,validationSchema,submitForm); 
  
  /* formik code end */
  

  const LoginSubmit = async (data) => {
    let dataObj = JSON.parse(data); 
    // console.log('dataObj',dataObj); 
    
    try {
      setLoader(true);      
      dispatch(Login({"email":dataObj.email, "password":dataObj.password}))
      .then((res) => {
        console.log('res ' ,res);       
        if(res.status == true){
          HANDLE_SUCCESS(res.message);
          /* remember code start */
          const dataGet = JSON.parse(data)
          REMEMBER_PASSWORD(dataGet); 
          /* remember code end */  
          /* redirect dashboard page */
          setTimeout(() => {
            router.push('/');
          }, 1000); 
          /* redirect dashboard page end */ 
        }else{
          HANDLE_ERROR(res.message);
        }    
        setLoader(false);    
        
      })
      .catch((error) => {
        // console.log('error ',error);  
        setLoader(false);
        HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });

     
    } catch (error) {
      setLoader(false);
      HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }

  };
  return (
    <>
    <Loader loading={loader} />
      <div
        className="modal-dialog border-0 modal-dialog-centered mt-md-5 w-100"
        role="document"
      >
        <div className="modal-content rounded-6 shadow border-0 pt-5">
          <a
            className="navbar-brand ps-2 ms-4 pe-4 mb-3 fab-logo text-center"
            href=""
          >
            <Image src={Fablogo} alt="faebikes" />
          </a>
          <div className="modal-header px-5 pb-4 border-bottom-0">
            <h2 className="fw-bold mb-0 text-theme fs-3">Sign In As Admin</h2>
          </div>
          <div className="modal-body p-5 pt-0">
            <form onSubmit={formik.handleSubmit} className="">
              <div className="form-floating mb-3">
                <input
                  type="email"                  
                  id="email"
                  placeholder="Email address / Mobile Number"
                  value={formik.values.email}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  className={
                    formik.errors.email && formik.touched.email
                      ? "text-input error form-control rounded-4 border-dark shadow"
                      : "text-input form-control rounded-4 border-dark shadow"
                  }
                />
                <label htmlFor="floatingInput">Email address / Mobile </label>
                
                {formik.errors.email && formik.touched.email && (
                  <div className="input-feedback invailid_feedback">{formik.errors.email}</div>
                )}
              </div>
              <div className="form-floating mb-3">
                <input
                  type={passwordType}
                  id="password"
                  placeholder="Password"
                  value={formik.values.password}
                  onChange={formik.handleChange}
                  onBlur={formik.andleBlur}
                  className={
                    formik.errors.password && formik.touched.password
                      ? "text-input error form-control rounded-4 border-dark shadow"
                      : "text-input form-control rounded-4 border-dark shadow"
                  }
                />
                { passwordType==="password"? <i className="bi bi-eye-slash togglePassword" onClick={togglePassword} ></i> :<i className="bi bi-eye togglePassword" onClick={togglePassword}></i> }
                <label htmlFor="floatingPassword">Password</label>
                {formik.errors.password && formik.touched.password && (
                  <div className="input-feedback invailid_feedback">{formik.errors.password}</div>
                )}
              </div>
              <div className="form-check mb-3">
                <input
                  className="form-check-input"
                  type="checkbox"
                  defaultValue
                  id="rememberPassword"
                  onChange={formik.handleChange}
                  checked={formik.values.rememberPassword}
                />
                <label htmlFor="rememberPassword" className="form-check-label">Remember password</label>
                
              </div>
              <button
                className="w-100 mb-2 btn btn-lg rounded-4 btn-outline-danger fae-login"
                type="submit" >
                  {/* disabled={formik.isSubmitting} */}
                Login
              </button>
             
              <Link href="/forgot-password"  >
                <p className="mt-2 text-center fw-bold fs-5 foe-fp cursor-pointer ">
                  Forget Password?
                </p>
              </Link>
            </form>
          </div>
        </div>
      </div>
    </>
  );
 
}

const mapStateToProps = (state, ownProps = {}) => {
  console.log(state) // state
  console.log(ownProps) // {}
  const { user } = state.auth;
  return {
    user,
  };
}

export default connect(mapStateToProps)(LoginComp);