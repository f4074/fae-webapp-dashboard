import React from "react";
import { Spinner } from 'react-bootstrap';
const Loader = ({ loading }) => {
  if (loading) {
    return (
      <div
        style={{
          position: "fixed",
          left: 0,
          top: 0,
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          width: "100%",
          height: "100%",
          background: "rgba(0, 0, 0, 0.5)",
          zIndex: 9999
        }}
      >   
        <div className="full-loader">
            <div className="container alignmentCenter">
                <div className="loader">
                    <Spinner animation="border" variant="dark" role="status">
                        <span className="sr-only">Loading...</span>
                    </Spinner>
                </div>
            </div>
        </div>
      </div >
    );
  } else {
    return <div style={{ display: "none" }} />;
  }
};

export default Loader;