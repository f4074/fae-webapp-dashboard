import React, { useState,useEffect } from "react";
import { connect,useDispatch, useSelector } from "react-redux";
import Link from "next/link";
import { useRouter } from 'next/router'
import Image from "next/image";
import Fablogo from "../../public/img/fae-logo.svg";

import { HANDLE_SUCCESS, HANDLE_ERROR, HTTP_SERVICE_CALL } from "../../provider/ApiProvider";
import  {SET_DATA_LOCALSTORAGE,REMEMBER_PASSWORD,GET_DATA_LOCALSTORAGE} from "../../provider/LocalStorageProvider"
import { BASE_URL, POST, IS_LOADING,ADMIN_SIGN_IN } from "../../provider/EndPoints";
import Loader from "../../Components/loader";
import Spinner from "../../Components/Spinner/Spinner";
import { ForgotPassword,otp_verify,change_password } from "../../Redux/Action/users";

// formik import 
import * as Yup from "yup";
import {VALIDATION_SCHEMA,FORMIK} from "../../provider/FormikProvider";


const ForgotPassComp=(props) => {
  // console.log('props',props);
  const router = useRouter();  
  const [loader, setLoader] = useState(false);  
  const [isShowOtp, setIsShowOtp] = useState(false);  
  const [isVerifyOtp, setIsVerifyOtp] = useState(false);  
  const dispatch = useDispatch();
  const { isLoggedIn } = useSelector(state => state.auth);

  useEffect(()=>{
    // setLoader(true);
    // 
    if (isLoggedIn) {
      // router.push('/');
    }
      
  },[])

  

  /* formik code start */
  var initialValues = {
    email:null,
    type:'admin',
  };
  const validationSchema = Yup.object() 
  .shape({
    email: Yup.string().default(null).nullable().email('Invalid email address').required('Email is required')
    
  });
  
  const submitForm =(data)=>{    
      ForgotPasswordSubmit(data);
      
  }
  const formik = FORMIK(initialValues,validationSchema,submitForm); 

   /* formik code start */
   var initialValuesOtp = {
    email:null,   
    otp:null,  
    type:'admin', 
  };
  const validationSchemaOtp = Yup.object() 
  .shape({
    email: Yup.string().default(null).nullable().email('Invalid email address').required('Email is required'),
    otp: Yup.number().default(null).nullable().required('OTP is required'),
  });
  
  const submitFormOtp =(data)=>{    
    otpVerify(data);
      
  }
  const formikOtp = FORMIK(initialValuesOtp,validationSchemaOtp,submitFormOtp);
// verify otp
  var initialValuesVerifyOtp = {
    password:null,   
    cpassword:null,   
    id:null,   
    type:'admin',   
  };
  const validationSchemaVerifyOtp = Yup.object() 
  .shape({
    password: Yup.string().default(null).nullable().required('New Password is required'),
    cpassword: Yup.string().default(null).nullable().required('Confirm Password is required').oneOf([Yup.ref("password")], "Passwords do not match"),
  });
  
  const submitFormVerifyOtp =(data)=>{    
    changePasswordCall(data);
      
  }
  const formikVerifyOtp = FORMIK(initialValuesVerifyOtp,validationSchemaVerifyOtp,submitFormVerifyOtp);
  
  
  /* formik code end */
 
  useEffect(()=>{
    // formikOtp.setFieldValue('email',formik.values.email)      
  },[formik.values.email])
  
  // console.log('formik',formik)
  console.log('formikOtp',formikOtp)
  
  const ForgotPasswordSubmit = async (data) => {
    let dataObj = JSON.parse(data); 
    try {
      setLoader(true);      
      dispatch(ForgotPassword({"email":dataObj.email}))
      .then((res) => {
        console.log('res ' ,res);
        console.log('formik ' ,formik);
        setLoader(false);           
        if(res.status == true){
          formikOtp.setFieldValue("email",dataObj.email)
          setIsShowOtp(true);          
          HANDLE_SUCCESS(res.message);         
          formik.resetForm();
        }else{
          HANDLE_ERROR(res.message);
        }    
        
        
      })
      .catch((error) => {
        setLoader(false);
        HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });
      
    } catch (error) {
      setLoader(false);
      HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }

  };
  // verify otp for password 
  const otpVerify = async (data) => {
    let dataObj = JSON.parse(data); 
    console.log('dataObj',dataObj); 
    
    try {
      setLoader(true);      
      dispatch(otp_verify({"email":dataObj.email,"otp":dataObj.otp}))
      .then((res) => {
        console.log('otp_verify res ' ,res);
        setLoader(false);           
        if(res.status == true){
          formikVerifyOtp.setFieldValue('id',res.data._id)          
          setIsShowOtp(false);
          setIsVerifyOtp(true);
          HANDLE_SUCCESS(res.message);         
          formikOtp.resetForm();
          
        }else{
          HANDLE_ERROR(res.message);
        }    
        
        
      })
      .catch((error) => {
        setLoader(false);
        HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });
      
    } catch (error) {
      setLoader(false);
      HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }

  };
  // verify otp for password 
  const changePasswordCall = async (data) => {
    let dataObj = JSON.parse(data); 
    try {
      setLoader(true);      
      dispatch(change_password(data))
      .then((res) => {
        console.log('res ' ,res);
        setLoader(false);           
        if(res.status == true){
          // setIsShowOtp(false);
          // setIsVerifyOtp(false);
          HANDLE_SUCCESS(res.message); 
          formikVerifyOtp.resetForm();        
          /* redirect login page */
          setTimeout(() => {
            router.push('/login');
          }, 500); 
          /* redirect login page end */ 
        }else{
          HANDLE_ERROR(res.message);
        }    
        
        
      })
      .catch((error) => {
        setLoader(false);
        HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });
      
    } catch (error) {
      setLoader(false);
      HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }

  };
  
  return (
    <>
    <Loader loading={loader} />
      <div
        className="modal-dialog border-0 modal-dialog-centered mt-md-5 w-100"
        role="document"
      >
        <div className="modal-content rounded-6 shadow border-0 pt-5">
          <a
            className="navbar-brand ps-2 ms-4 pe-4 mb-3 fab-logo text-center"
            href=""
          >
            <Image src={Fablogo} alt="faebikes" />
          </a>
          <div className="modal-header px-5 pb-4 border-bottom-0">
            <h2 className="fw-bold mb-0 text-theme fs-3">Forgot Password</h2>
          </div>
          <div className={!(isShowOtp || isVerifyOtp) ?"modal-body p-5 pt-0":'fmodal-body p-5 pt-0 d-none'}>
            <form onSubmit={formik.handleSubmit} className="">
              <div className="form-floating mb-3">
                <input
                  type="email"                  
                  id="email"
                  placeholder="Enter Email address "
                  value={formik.values.email}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  className={
                    formik.errors.email && formik.touched.email
                      ? "text-input error form-control rounded-4 border-dark shadow"
                      : "text-input form-control rounded-4 border-dark shadow"
                  }
                />
                <label htmlFor="floatingInput">Email address  </label>
                {formik.errors.email && formik.touched.email && (
                  <div className="input-feedback invailid_feedback">{formik.errors.email}</div>
                )}
              </div>

              <div className={isVerifyOtp?"form-floating mb-3":'form-floating mb-3 d-none'} >
                <input
                  type="text"                  
                  id="otp"
                  name="otp"                  
                  placeholder="Enter OTP "
                  value={formik.values.otp}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  className={
                    formik.errors.otp && formik.touched.otp
                      ? "text-input error form-control rounded-4 border-dark shadow"
                      : "text-input form-control rounded-4 border-dark shadow"
                  }
                />
                <label htmlFor="floatingInput">OTP  </label>
                {formik.errors.otp && formik.touched.otp && (
                  <div className="input-feedback invailid_feedback">{formik.errors.otp}</div>
                )}
              </div>
             
              <button
                className="w-100 mb-2 btn btn-lg rounded-4 btn-outline-danger fae-login"
                type="submit" >
                  {/* disabled={formik.isSubmitting} */}
                Submit
              </button>
             
              <Link href="/login"  >
                <p className="mt-2 text-center fw-bold fs-5 foe-fp cursor-pointer">
                  Back to Login!
                </p>
              </Link>
            </form>
          </div>

          <div className={isShowOtp?"modal-body p-5 pt-0":'fmodal-body p-5 pt-0 d-none'}>
            <form onSubmit={formikOtp.handleSubmit} className="">
              <div className="form-floating mb-3">
                <input
                  readOnly
                  type="email"                  
                  id="email"
                  name="email"
                  placeholder="Enter Email address "
                  value={formikOtp.values.email}
                  onChange={formikOtp.handleChange}
                  onBlur={formikOtp.handleBlur}
                  className={
                    formikOtp.errors.email && formikOtp.touched.email
                      ? "text-input error form-control rounded-4 border-dark shadow"
                      : "text-input form-control rounded-4 border-dark shadow"
                  }
                />
                <label htmlFor="floatingInput">Email address  </label>
                {formikOtp.errors.email && formikOtp.touched.email && (
                  <div className="input-feedback invailid_feedback">{formikOtp.errors.email}</div>
                )}
              </div>

              <div className={"form-floating mb-3"} >
                <input
                  type="text"                  
                  id="otp"
                  name="otp"
                  placeholder="Enter OTP "
                  value={formikOtp.values.otp}
                  onChange={formikOtp.handleChange}
                  onBlur={formikOtp.handleBlur}
                  className={
                    formikOtp.errors.otp && formikOtp.touched.otp
                      ? "text-input error form-control rounded-4 border-dark shadow"
                      : "text-input form-control rounded-4 border-dark shadow"
                  }
                />
                <label htmlFor="floatingInput">OTP  </label>
                {formikOtp.errors.otp && formikOtp.touched.otp && (
                  <div className="input-feedback invailid_feedback">{formikOtp.errors.otp}</div>
                )}
              </div>
             
              <button
                className="w-100 mb-2 btn btn-lg rounded-4 btn-outline-danger fae-otp-verify"
                type="submit" >
                  {/* disabled={formik.isSubmitting} */}
                Submit
              </button>
             
              <Link href="/login"  >
                <p className="mt-2 text-center fw-bold fs-5 foe-fp cursor-pointer">
                  Back to Login!
                </p>
              </Link>
            </form>
          </div>

          <div className={isVerifyOtp?"modal-body p-5 pt-0":'fmodal-body p-5 pt-0 d-none'}>
            <form onSubmit={formikVerifyOtp.handleSubmit} className="">
              <div className="form-floating mb-3">
                <input
                  type="password"                  
                  id="password"
                  name="password"
                  placeholder="Enter New Password "
                  value={formikVerifyOtp.values.password}
                  onChange={formikVerifyOtp.handleChange}
                  onBlur={formikVerifyOtp.handleBlur}
                  className={
                    formikVerifyOtp.errors.password && formikVerifyOtp.touched.password
                      ? "text-input error form-control rounded-4 border-dark shadow"
                      : "text-input form-control rounded-4 border-dark shadow"
                  }
                />
                <label htmlFor="floatingInput">Enter New Password  </label>
                {formikVerifyOtp.errors.password && formikVerifyOtp.touched.password && (
                  <div className="input-feedback invailid_feedback">{formikVerifyOtp.errors.password}</div>
                )}
              </div>
              <div className="form-floating mb-3">
                <input
                  type="password"                  
                  id="cpassword"
                  name="cpassword"
                  placeholder="Enter Confirm Password "
                  value={formikVerifyOtp.values.cpassword}
                  onChange={formikVerifyOtp.handleChange}
                  onBlur={formikVerifyOtp.handleBlur}
                  className={
                    formikVerifyOtp.errors.cpassword && formikVerifyOtp.touched.cpassword
                      ? "text-input error form-control rounded-4 border-dark shadow"
                      : "text-input form-control rounded-4 border-dark shadow"
                  }
                />
                <label htmlFor="floatingInput">Enter Confirm Password  </label>
                {formikVerifyOtp.errors.cpassword && formikVerifyOtp.touched.cpassword && (
                  <div className="input-feedback invailid_feedback">{formikVerifyOtp.errors.cpassword}</div>
                )}
              </div>
              <button
                className="w-100 mb-2 btn btn-lg rounded-4 btn-outline-danger fae-otp-verify"
                type="submit" >
                  {/* disabled={formik.isSubmitting} */}
                Submit
              </button>
             
              <Link href="/login"  >
                <p className="mt-2 text-center fw-bold fs-5 foe-fp cursor-pointer">
                  Back to Login!
                </p>
              </Link>
            </form>
          </div>
        </div>
      </div>
    </>
  );
 
}

const mapStateToProps = (state, ownProps = {}) => {
  console.log(state) // state
//   console.log(ownProps) // {}
  const { user } = state.auth;
  return {
    user,
  };
}

export default connect(mapStateToProps)(ForgotPassComp);