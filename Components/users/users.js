import React, { useState, useEffect, useCallback } from "react";
import Image from 'next/image'
import { connect, useDispatch, useSelector } from "react-redux";
import { useRouter } from 'next/router';
import { HANDLE_SUCCESS, HANDLE_ERROR } from "../../provider/ApiProvider";
import { IMAGE_URL,NO_IMAGE_ERROR } from "../../provider/EndPoints";
import { changeDateFormat } from "../../provider/helper";
import Loader from "../../Components/loader";
import { createUser,getAllUsers,deleteUser,deleteMultipleUsers,updateUser,getKycDetails,getAllKycList} from "../../Redux/Action/users";
import { createHub,getAllHubs,allCityHubs,deleteHub,updateHub} from "../../Redux/Action/hubs";
/* ant design */
import 'antd/dist/antd.css';
import { Table, Tag, Space, Modal, Form, Input, Radio, Upload, message, Button, Descriptions, DatePicker,Select,Switch } from 'antd';
import { EditOutlined, DeleteOutlined, InboxOutlined, EyeOutlined, SearchOutlined } from '@ant-design/icons'
// formik import 
import * as Yup from "yup";
import { VALIDATION_SCHEMA, FORMIK } from "../../provider/FormikProvider";
import FormList from "antd/lib/form/FormList";

const UsersComp = (props) => {
  const router = useRouter();
  const [loader, setLoader] = useState(false);
  const dispatch = useDispatch();
  const { isLoggedIn} = useSelector(state => state.auth);
  const [form] = Form.useForm();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [modalTitle,setModalTitle]= useState('Add New User');
  const [isModalVisibleDetails, setIsModalVisibleDetails] = useState(false);
  const [dataSource, setDataSource] = useState([]);
  const [userKycDeatils, setUserKycDeatils] = useState([]);
  const [cityDataFilter, setCityDataFilter] = useState([]);
  const [hubDataFilter, setHubDataFilter] = useState([]);
  const [filterArray, setFilter] = useState({'city_id':props.user_city_id?props.user_city_id[0]:'','hub_id':(props.user_role == 'FAE_HUB_ADMIN' || props.user_role == 'FAE_HUB_MANAGER') && props.user_hub_id?props.user_hub_id[0]:'','searchKey':''});
  const [searchKey, setSearchKey] = useState(null);
  const [isFilter, setIsFilter] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [editId, setEditId] = useState('');
  const { Search } = Input;
  const { Option } = Select;
  const [user_role, setuser_role] = useState(props.user_role);  
  const [user_city_id, setuser_city_id] = useState(props.user_hub_id);
  const [user_hub_id, setuser_hub_id] = useState(props.user_city_id);
  var setCityStatus =0;
  useEffect(() => {
    if (!isLoggedIn) {
      router.push('/login');
    } else {
      setuser_role(props.user_role);
      setuser_hub_id(props.user_hub_id)
      setuser_city_id(props.user_city_id);
    
      if(props.user_role == 'FAE_HUB_ADMIN' || props.user_role == 'FAE_HUB_MANAGER'){          
        setFilter({...filterArray, hub_id: props.user_hub_id[0]})
      }else{
        // setFilter({...filterArray, city_id: props.user_city_id[0] })
      }

      if(props.user_city_id){        
        getAllCityHubsAPI(props.user_city_id[0]);
      }
      getAllUsersData();     
    }
  }, [])

  useEffect(() => {
    if(cityDataFilter.length >0){
      if(props.user_role == 'FAE_MAIN_ADMIN' || props.user_role == 'FAE_ADMIN'){
        const getCity = cityDataFilter.filter((i)=> props.user_city_id.includes(i._id));
        
        if(getCity.length >0){        
          setFilter({...filterArray, city_id: props.user_city_id[0]})
        }else{
          setFilter({...filterArray, city_id: "" ,hub_id: ""})
        }
      }
    }   
  }, [cityDataFilter.length >0 &&  setCityStatus == 0])

  useEffect(() => {
    getAllUsersData();
  },[filterArray])
  
  /* formik code start */   
  var initialValues = {
    name:'',
    email:'',
    password:'',
    phone:'',
    city_id:'',
    hub_id:'',
    address1:'',
    zipcode:'',
    created:new Date(),
    updated:new Date()
  };
  const validationSchema = Yup.object() 
  .shape({
    name: Yup.string().default(null).nullable()
    .required('Name is required'),   
    email: Yup.string().default(null).nullable()
    .required('Email is required').email('Email is invalid'),
    password: Yup.string().default(null).nullable()
    .required('Password is required'),
    phone: Yup.string().default(null).nullable()
    .required('Phone number is required'),
    city_id: Yup.string().default(null).nullable()
    .required('Please select City'),
    // hub_id: Yup.string().default(null).nullable()
    // .required('Please select Hub'),
    address1: Yup.string().default(null).nullable()
    .required('Address is required'),
    zipcode: Yup.string().default(null).nullable()
    .required('zipcode is required'),
  }); 
  const submitForm =(data)=>{  
      callAPI(data);   
  }
  const formik = FORMIK(initialValues,validationSchema,submitForm);

  formik.handleChange = (e) => {       
      formik.setFieldValue(e.target.name,e.target.value);  
      // if(e.target.name == 'city_id'){
      //   setHubDataFilter([]);          
      //   // setFilter({...filterArray,  city_id: e.target.value,hub_id:''})
      //   getAllCityHubsAPI(e.target.value);      
      // }else if(e.target.name == 'hub_id'){
      //   // setFilter({...filterArray,  hub_id: e.target.value})
      // }else{
        
      // }
  };
  const callAPI= (data)=> {
    if(isEditing){
        const dataParse = JSON.parse(data)
        const editId =dataParse._id;
        var ApiPath = updateUser(data,editId);
    }else{
        var ApiPath = createUser(data)
    }
    try {
        setLoader(true);      
        dispatch(ApiPath)
        .then((res) => {
            setLoader(false);      
            if(res.status === true){
                HANDLE_SUCCESS(res.message);
                // setDataSource(res.data.UsersList);  
                getAllUsersData();                 
                setIsModalVisible(false)           
                formik.resetFields();  
                formik.setValues(initialValues); 
            }else{
                HANDLE_ERROR(res.message);
                // setIsModalVisibleDetails(true)
            }
        })
        .catch((error) => {
            setLoader(false);
            // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
        });
    } catch (error) {
        setLoader(false);
        HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  const  onEdit=(record)=>{
    formik.setValues({
      _id:record._id,
      name:record.name,
      email:record.email,
      password:record.password,
      phone:record.phone,
      city_id:record.city_id._id,
      hub_id:record.hub_id._id,
      address1:record.address1,
      zipcode:record.zipcode,
      created:record.created,
      updated:new Date()
    });
   
    if(record){
      getAllCityHubsAPI(record.city_id._id)
    }
    setIsEditing(true);
    setIsModalVisible(true);
    setModalTitle("Edit User");
   
  }
  const onDelete = (record) => {
    // showModal();
    Modal.confirm({
      title: 'Are you sure, you want to delete this record?',
      okText: "Yes",
      okType: "danger",
      onOk: () => {
        deleteCallApi(record);
      }
    })
  }
  const deleteCallApi = (record) => {
    try {
      setLoader(true);      
      dispatch(deleteUser(record._id))
      .then((res) => {
        setLoader(false); 
        if(res.status == true){          
          HANDLE_SUCCESS(res.message); 
          // setDataSource(res.data.UsersList); 
          getAllUsersData();         
          /* setDataSource(pre=>{
            return pre.filter((client)=>client._id!=record._id);
          }) */  
        }else{
          HANDLE_ERROR(res.message);
        }  
      })
      .catch((error) => {
        setLoader(false);
        HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });     
    } catch (error) {
      setLoader(false);
      HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  const getAllUsersData=()=>{
    try {
        setLoader(true);      
        dispatch(getAllUsers(filterArray))
        .then((res) => {
          setLoader(false);
          if(setCityStatus == 0){
            setCityDataFilter(res.data.citiesList)
            setCityStatus =1;
          }
          
          setDataSource(res.data.UsersList);
        })
        .catch((error) => {
          setLoader(false);
          // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
        });
      } catch (error) {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      }
  }
  const getUsersKycDetails=(user_id)=>{
    try {
        setLoader(true);      
        dispatch(getKycDetails(user_id))
        .then((res) => {
          setLoader(false);
          setUserKycDeatils(res.data);
          if(res.data && res.data.user_id);
          formik.setFieldValue("kyc_status",res.data.kyc_status); 
        })
        .catch((error) => {
          setLoader(false);
          // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
        });
      } catch (error) {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      }
  }
  const getAllCityHubsAPI=(city_id)=>{
    try {
      setLoader(true);      
      dispatch(allCityHubs(city_id))
      .then((res) => {
        setLoader(false);
        setHubDataFilter(res.data);
      })
      .catch((error) => {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });
    } catch (error) {
      setLoader(false);
      // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  function onChangeActive(checked,data) {
    setIsEditing(true);
    const getStatus = {
        _id:data._id,
        active_status:checked,
        updated:new Date()
    }
    var ApiPath = updateUser(getStatus,data._id);
    try {
        setLoader(true);      
        dispatch(ApiPath)
        .then((res) => {
            setLoader(false);      
            if(res.status === true){
              HANDLE_SUCCESS(res.message);          
              setDataSource(res.data.UsersList);                   
              // setIsModalVisible(false)           
              // formik.resetFields();  
              // formik.setValues(initialValues); 
            }else{
              HANDLE_ERROR(res.message);
            }
        })
        .catch((error) => {
            setLoader(false);
            // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
        });
    }catch (error) {
        setLoader(false);
        HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  const columns = [
    {
      key: "1",
      title: 'Name',
      dataIndex: 'name',
    },
    {
        key: "2",
        title: 'Phone Number',
        dataIndex: 'phone',
    },
    {
      key: "3",
      title: 'Email',
      dataIndex: 'email',
      render: (email) => {
        return email;
      },
    },
    {
      key: "4",
      title: 'Address',
      dataIndex: 'address1',
      render: (address) => {
        return address;
      }
    },
    (user_role != 'FAE_STAFF')?
    {
      key: "5",
      title: 'Inactive/Active',
    //   dataIndex: 'status',
      render: (record) => {       
        return <> 
        <Switch checked={record.active_status} onChange={(value)=>{onChangeActive(value,record)}} className="" />
        </>;
      }
    }
    :{},
    (user_role != 'FAE_STAFF')?
    {
      key: "6",
      title: 'Edit User',
      // dataIndex: '_id',
      render: (record) => {
        return <>  
                
          <Button type="danger" className="btn btn-outline-secondary btn-sm" size="small" onClick={() => {
               onEdit(record)    
          }} >
            <i className="fa fa-edit me-2"  style={{'color': 'white'}}></i>
            Edit 
          </Button>

        </>
      }
    }
    :{},
    (user_role != 'FAE_STAFF')?
    {
      key: "7",
      title: 'Delete User',
      // dataIndex: '_id',
      render: (record) => {
        return <>          
          <Button type="danger" className="btn btn-danger btn-sm" size="small" onClick={() => {
            onDelete(record)
          }} >
            <i className="fa fa-trash me-2"  style={{'color': 'white'}}></i>
            Delete 
          </Button>

        </>
      }
    }
    :{}
  ];
  function onChange(pagination, filters, sorter, extra) {
    // console.log('params', pagination, filters, sorter, extra);
    const filterData ={
      sortField: sorter.field,
      sortOrder: sorter.order,
      pagination,
      ...filters,
    }
  }
   
  function handleChangeSelect(value,selectType) {
    if(selectType == 'city_id'){
      formik.setFieldValue("city_id",value);
      setHubDataFilter([]);          
      // setFilter({...filterArray,  city_id: e.target.value,hub_id:''})
      getAllCityHubsAPI(value);      
    }else if(selectType == 'hub_id'){
      formik.setFieldValue("hub_id",value);
      // setFilter({...filterArray,  hub_id: e.target.value})
    }else{
      if(selectType == 'city'){
        getAllCityHubsAPI(value);
        setFilter({...filterArray,  city_id: value})
        
      }else{
        setFilter({...filterArray,  hub_id: value})
      } 
    }
    
  }

  const searchOnEnter=(e)=>{
    const value = e.target.value;
    if(value.length >0){
      setSearchKey(value);
      setFilter({...filterArray,  searchKey: value})
    }else{
      setSearchKey('null');
      setFilter({...filterArray,  searchKey: ''})
    }
  }
  const onSearch = value => {
    if(value.length >0){
      setSearchKey(value);
      setFilter({...filterArray,  searchKey: value})
    }else{
      setSearchKey('null');
      setFilter({...filterArray,  searchKey: ''})
    }
    
  };

  return (
    <>
      <Loader loading={loader} />
      <h2 className="mb-4 text-danger fw-bold fs-3">Users</h2>
      <div className="col-12 d-flex mb-3">
        <div className="col-4">
          <Search placeholder="Search by Name or Mobile or Email" allowClear  onSearch={onSearch} onPressEnter={(e)=>searchOnEnter(e)}   />
        </div>
        <div className="col-6 d-flex">
          <div className="col-4 ms-2">
            <Select value={filterArray.city_id} name="city_filter" placeholder="Filter By City" className="w-100" id="city_filter" onChange={(e)=>handleChangeSelect(e,'city')}>              
              {(user_role == 'FAE_MAIN_ADMIN' || user_role == 'FAE_ADMIN'?
                <Option value="">Filter By City</Option>:'')
              }
              {
                cityDataFilter.map(function(item, i){
                  if((user_role == 'FAE_CITY_ADMIN' || user_role == 'FAE_HUB_ADMIN' || user_role == 'FAE_HUB_MANAGER') && item._id == user_city_id[0]){
                    return <Option key={item._id} value={item._id} >{item.name}</Option>
                  }else if(user_role == 'FAE_MAIN_ADMIN'  || user_role == 'FAE_ADMIN'  || user_role == 'FAE_STAFF'){
                    return <Option key={item._id} value={item._id} >{item.name}</Option>
                  }                 
                })
              }
            </Select>
          </div>
          <div className="col-4 ms-2">
            {/* <Select defaultValue="" style={{ width: 120 }} className="w-100" name="hub_filter" id="hub_filter"  onChange={(e)=>handleChangeSelect(e,'hub')}>
              <Option value="">Select Hub</Option>
              {
                hubDataFilter.map(function(item, i){
                    return <Option value={item._id} >{item.title}</Option>
                })
              }
            </Select> */}
          </div>
        </div>
        <div className="col-6">
         
        </div>
      </div>     
      <Table   scroll={{x: 1000}} loading={isLoading} onChange={onChange} columns={columns} dataSource={dataSource} />
      {/* rowSelection={{ type: 'checkbox' }} */}
      {/* Edit modal start */}
      <Modal
        title={modalTitle}
        destroyOnClose="true"
        centered
        visible={isModalVisible}
        onOk={()=>{
          formik.submitForm()                  
        }} 
        onCancel={()=>{
          setIsModalVisible(false)
          form.resetFields();
        // Modal.destroyAll();
        }}
        okType="danger"
        okText="Save"
        cancelText="Close"
        width={1000}       
      >
        <form onSubmit={formik.handleSubmit} className="" >           
            <div className="mb-3 col-12 d-flex">
                <div className="mb-3  col-12 me-3">
                  <label htmlFor="floatingInput">Select City</label>
                  <Select name="city_id" className={
                          formik.errors.city_id && formik.touched.city_id
                              ? "error rounded-4 w-100"
                              : "rounded-4  w-100"
                          } value={formik.values.city_id} id="city_id" onBlur={formik.handleBlur} onChange={(e)=>handleChangeSelect(e,'city_id')}>
                    {(user_role == 'FAE_MAIN_ADMIN' || user_role == 'FAE_ADMIN'?
                      <Option value="">Select City</Option>:'')
                    }
                    {
                      cityDataFilter.map(function(item, i){                        
                        if((user_role == 'FAE_CITY_ADMIN' || user_role == 'FAE_HUB_ADMIN' || user_role == 'FAE_HUB_MANAGER') && item._id == user_city_id[0]){
                          return <Option key={item._id} value={item._id} >{item.name}</Option>
                        }else if(user_role == 'FAE_MAIN_ADMIN'  || user_role == 'FAE_ADMIN'  || user_role == 'FAE_STAFF'){
                          return <Option key={item._id} value={item._id} >{item.name}</Option>
                        }
                      })
                    }
                  </Select>
                    
                    {formik.errors.city_id && formik.touched.city_id && (
                        <div className="input-feedback invailid_feedback">{formik.errors.city_id}</div>
                    )}
                </div>
                {/* <div className=" mb-3  col-6">
                <label htmlFor="floatingInput">Select Hub</label>
                  <Select  name="hub_id" className={
                            formik.errors.hub_id && formik.touched.hub_id
                                ? "text-input error form-control rounded-4 "
                                : "text-input form-control rounded-4 "
                            } id="hub_id" value={formik.values.hub_id} onBlur={formik.handleBlur} onChange={(e)=>handleChangeSelect(e,'hub_id')}>
                      <Option value="">Select Hub</Option>
                      {
                        hubDataFilter.map(function(item, i){
                            return <Option selected={formik.values.hub_id==item._id?true:false} value={item._id} >{item.title}</Option>
                        })
                      }
                  </Select>
                      
                  {formik.errors.hub_id && formik.touched.hub_id && (
                      <div className="input-feedback invailid_feedback">{formik.errors.hub_id}</div>
                  )}
                </div> */}
            </div>
            <div className="mb-3 col-12 d-flex">
                <div className="form-floating mb-3  col-6 me-3">
                    <Input
                        type="text"                  
                        id="name"
                        name="name"
                        placeholder="Name"
                        value={formik.values.name}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        className={
                        formik.errors.name && formik.touched.name
                            ? "text-input error form-control rounded-4 "
                            : "text-input form-control rounded-4 "
                        }
                    />
                    <label htmlFor="floatingInput">Full Name </label>
                    {formik.errors.name && formik.touched.name && (
                        <div className="input-feedback invailid_feedback">{formik.errors.name}</div>
                    )}
                </div>
                <div className={isEditing?'form-floating mb-3  col-6 me-3':'form-floating mb-3  col-6 me-3'}>
                    <Input
                        type="text"                  
                        id="email"
                        name="email"
                        placeholder="Email"
                        value={formik.values.email}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        className={
                        formik.errors.email && formik.touched.email
                            ? "text-input error form-control rounded-4 "
                            : "text-input form-control rounded-4 "
                        }
                        readOnly={false}
                    />
                    <label htmlFor="floatingInput">Email</label>
                    {formik.errors.email && formik.touched.email && (
                        <div className="input-feedback invailid_feedback">{formik.errors.email}</div>
                    )}
                </div>
            </div>
            <div className="mb-3 col-12 d-flex">                     
                {!isEditing?
                <div className="form-floating mb-3  col-6">
                    <Input
                        type="password"                  
                        id="password"
                        name="password"
                        placeholder="Password"
                        value={formik.values.password}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        className={
                        formik.errors.password && formik.touched.password
                            ? "text-input error form-control rounded-4 "
                            : "text-input form-control rounded-4 "
                        }
                    />
                    <label htmlFor="floatingInput">Password </label>
                    {formik.errors.password && formik.touched.password && (
                        <div className="input-feedback invailid_feedback">{formik.errors.password}</div>
                    )}
                </div>
                :''}
                 <div className="form-floating mb-3  col-6  me-3">
                    <Input
                        type="text"                  
                        id="phone"
                        name="phone"
                        placeholder="Phone Number"
                        value={formik.values.phone}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        className={
                        formik.errors.phone && formik.touched.phone
                            ? "text-input error form-control rounded-4 "
                            : "text-input form-control rounded-4 "
                        }
                        readOnly={true}
                    />
                    <label htmlFor="floatingInput">Phone Number </label>
                    {formik.errors.phone && formik.touched.phone && (
                        <div className="input-feedback invailid_feedback">{formik.errors.phone}</div>
                    )}
                </div>
                <div className="form-floating mb-3  col-6  me-3">
                    <Input
                        type="text"                  
                        id="zipcode"
                        name="zipcode"
                        placeholder="zipcode"
                        value={formik.values.zipcode}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        className={
                        formik.errors.pincode && formik.touched.zipcode
                            ? "text-input error form-control rounded-4 "
                            : "text-input form-control rounded-4 "
                        }
                    />
                    <label htmlFor="floatingInput">zipcode </label>
                    {formik.errors.zipcode && formik.touched.zipcode && (
                        <div className="input-feedback invailid_feedback">{formik.errors.zipcode}</div>
                    )}
                </div>
            </div>
            <div className="mb-3 col-12 d-flex">  
                <div className="form-floating mb-3  col-12">
                    <Input
                        type="text"                  
                        id="address"
                        name="address1"
                        placeholder="Address"
                        value={formik.values.address1}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        className={
                        formik.errors.address1 && formik.touched.address1
                            ? "text-input error form-control rounded-4 "
                            : "text-input form-control rounded-4 "
                        }
                    />
                    <label htmlFor="floatingInput">Address </label>
                    {formik.errors.address1 && formik.touched.address1 && (
                        <div className="input-feedback invailid_feedback">{formik.errors.address1}</div>
                    )}
                </div>
                
            </div>
            <div className="mb-3 col-12 d-flex">  
               
                {/* <div className="form-floating mb-3  col-6">
                    <Input
                        type="text"                  
                        id="address"
                        name="address"
                        placeholder="Address"
                        value={formik.values.address}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        className={
                        formik.errors.address && formik.touched.address
                            ? "text-input error form-control rounded-4 "
                            : "text-input form-control rounded-4 "
                        }
                    />
                    <label htmlFor="floatingInput">Address </label>
                    {formik.errors.address && formik.touched.address && (
                        <div className="input-feedback invailid_feedback">{formik.errors.address}</div>
                    )}
                </div> */}
            </div>

        </form>
      </Modal>
     
      {/* Edit modal end */}
      
    </>
  );
}
const mapStateToProps = (state, ownProps = {}) => {  
  const {  isLoggedIn,user,user_city_id,user_role,user_hub_id } = state.auth;
  const { inventoryData } = state.inventory;
  return {
    user,
    isLoggedIn,
    user_city_id,
    user_hub_id,
    user_role,
    inventoryData
  };
}

export default connect(mapStateToProps)(UsersComp);

