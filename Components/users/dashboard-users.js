import React, { useState, useEffect, useCallback } from "react";
import Image from 'next/image'
import { connect, useDispatch, useSelector } from "react-redux";
import { useRouter } from 'next/router';
import { HANDLE_SUCCESS, HANDLE_ERROR } from "../../provider/ApiProvider";
import { IMAGE_URL,NO_IMAGE_ERROR } from "../../provider/EndPoints";
import { changeDateFormat } from "../../provider/helper";
import Loader from "../../Components/loader";
import { createUser,getAllUsers,deleteUser,deleteMultipleUsers,updateUser,getKycDetails,getAllKycList,getAllDashUsers,createDashUser,updateDashUser,deleteDashUser} from "../../Redux/Action/users";
import { createHub,getAllHubs,allCityHubs,deleteHub,updateHub} from "../../Redux/Action/hubs";
/* ant design */
import 'antd/dist/antd.css';
import { Table, Tag,Row, Col, Space, Modal, Form, Input, Radio, Upload, message, Button, Descriptions, DatePicker,Select,Switch } from 'antd';
import { EditOutlined, DeleteOutlined, InboxOutlined, EyeOutlined, SearchOutlined } from '@ant-design/icons'
// formik import 
import * as Yup from "yup";
import { VALIDATION_SCHEMA, FORMIK } from "../../provider/FormikProvider";
import FormList from "antd/lib/form/FormList";
//import mystyle from "../../public/css/select.css";




const classobj = {
                    antselect: {
                    //lineHeight: 3.5715
              
                  },


                
}

const DashUsersComp = (props) => {
  
  const router = useRouter();
  const [loader, setLoader] = useState(false);
  const dispatch = useDispatch();
  const { isLoggedIn } = useSelector(state => state.auth);
  const [form] = Form.useForm();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [isModalVisibleDetails, setIsModalVisibleDetails] = useState(false);
  const [dataSource, setDataSource] = useState([]);
  const [modalTitle,setModalTitle]= useState('Add New User');
  const [userKycDeatils, setUserKycDeatils] = useState([]);
  const [cityDataFilter, setCityDataFilter] = useState([]);
  const [hubDataFilter, setHubDataFilter] = useState([]);
  const [filterArray, setFilter] = useState({'city_id':'','hub_id':'','user_role':'','status':'','searchKey':''});
  const [selectMode, setSelectMode] = useState({'city':'multiple','hub':'multiple'});
  const [searchKey, setSearchKey] = useState(null);
  const [isFilter, setIsFilter] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [editId, setEditId] = useState('');
  const [userRoleData, setUserRoleData] = useState([
      {'key':'FAE_ADMIN','name':'FAE ADMIN'},
      {'key':'FAE_CITY_ADMIN','name':'FAE CITY ADMIN'},
      {'key':'FAE_HUB_ADMIN','name':'FAE HUB ADMIN'},
      {'key':'FAE_HUB_MANAGER','name':'FAE HUB MANAGER'},
      // {'key':'FAE_STAFF','name':'FAE STAFF'}
  ]);
  const [getcityhubmode, setcityhubmode] = useState(true);
  const { Search } = Input;
  const { Option } = Select;

  useEffect(() => {
    if (!isLoggedIn) {
      router.push('/login');
    } else {      
      getAllUsersData();
     
    }
  }, [])

  useEffect(() => {
    getAllUsersData();
  }, [filterArray])
  
  useEffect(() => {
    
  }, [isEditing])
  
  /* formik code start */   
  var initialValues = {
    first_name:'',
    last_name:'',
    email:'',
    password:'',
    phone_number:'',
    user_role:'',
    city_id:[],
    hub_id:[],
    address:'',
    pincode:'',
    created:new Date(),
    updated:new Date()
  };
  const validationSchema = Yup.object() 
  .shape({
    first_name: Yup.string().default(null).nullable()
    .required('First Name is required'),
    last_name: Yup.string().default(null).nullable()
    .required('Last Name is required'),
    email: Yup.string().default(null).nullable()
    .required('Email is required').email('Email is invalid'),
    password: Yup.string().default(null).nullable()
    .required('Password is required'),
    phone_number: Yup.string().default(null).nullable()
    .required('Phone number is required'),
    user_role: Yup.string().default(null).nullable()
    .required('Please select user role'),
    city_id: Yup.array().required('Please select City'),
    hub_id: Yup.array().required('Please select Hub'),
    address: Yup.string().default(null).nullable()
    .required('Address is required'),
    pincode: Yup.string().default(null).nullable()
    .required('Pincode is required'),
  }); 
  const submitForm =(data)=>{  
      callAPI(data);   
  }
  const formik = FORMIK(initialValues,validationSchema,submitForm);

  formik.handleChange = (e) => {        
      formik.setFieldValue(e.target.name,e.target.value);   
      if(e.target.name == 'city_id'){
        setHubDataFilter([]);          
        setFilter({...filterArray,  city_id: e.target.value,hub_id:''})
        getAllCityHubsAPI(e.target.value);      
      }else if(e.target.name == 'hub_id'){
        setFilter({...filterArray,  hub_id: e.target.value})

      }else if(e.target.name == 'user_role'){
       
        if(e.target.value == "FAE_HUB_ADMIN" || e.target.value == "FAE_HUB_MANAGER"){
          setSelectMode({...selectMode,city:"",hub:""})

        }else  if(e.target.value == "FAE_CITY_ADMIN"){
          setSelectMode({...selectMode,city:"",hub:"multiple"})

        }
          else {
          setSelectMode({...selectMode,city:"multiple",hub:"multiple"})
        }


        if(e.target.value == "FAE_ADMIN"){
          setcityhubmode(false);
        }
        else {
          setcityhubmode(true);
        }



        formik.setFieldValue('hub_id',[])
        formik.setFieldValue('city_id',[])
      }  
  };
  const callAPI= (data)=> {
    if(isEditing){
        const dataParse = JSON.parse(data)
        const editId =dataParse._id;
        var ApiPath = updateDashUser(data,editId);
    }else{
        var ApiPath = createDashUser(data)
    }
    try {
        setLoader(true);      
        dispatch(ApiPath)
        .then((res) => {
            setLoader(false);      
            if(res.status === true){
                HANDLE_SUCCESS(res.message);
                setIsModalVisible(false)  
                // setDataSource(res.data.allAdminData); 
                getAllUsersData();                  
                         
                formik.resetFields();  
                formik.setValues(initialValues); 
            }else{
                HANDLE_ERROR(res.message);
                // setIsModalVisibleDetails(true)
            }
        })
        .catch((error) => {
            setLoader(false);
            // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
        });
    } catch (error) {
        setLoader(false);
        HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  const  onEdit=(record)=>{
    if(record){
      getAllCityHubsAPI(record.city_id)

      if(record.user_role == "FAE_HUB_ADMIN" || record.user_role == "FAE_HUB_MANAGER"){
        setSelectMode({...selectMode,city:"",hub:""})

      }else  if(record.user_role == "FAE_CITY_ADMIN"){
        setSelectMode({...selectMode,city:"",hub:"multiple"})

      }else {
        setSelectMode({...selectMode,city:"multiple",hub:"multiple"})
      }

        if(record.user_role == "FAE_ADMIN"){
          setcityhubmode(false);
        }
        else {
          setcityhubmode(true);
        }




    }
    setIsEditing(true);
    setIsModalVisible(true);
    setModalTitle("Edit User");
    formik.setValues(record);
  }

  const onDelete = (record) => {
    // showModal();
    Modal.confirm({
      title: 'Are you sure, you want to delete this record?',
      okText: "Yes",
      okType: "danger",
      onOk: () => {
        deleteCallApi(record);
      }
    })
  }
  const deleteCallApi = (record) => {
    try {
      setLoader(true);      
      dispatch(deleteDashUser(record._id))
      .then((res) => {
        setLoader(false); 
        if(res.status == true){          
          HANDLE_SUCCESS(res.message);         
          setDataSource(pre=>{
            return pre.filter((client)=>client._id!=record._id);
          })  
        }else{
          HANDLE_ERROR(res.message);
        }  
      })
      .catch((error) => {
        setLoader(false);
        HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });     
    } catch (error) {
      setLoader(false);
      HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  const getAllUsersData=()=>{
    try {
        setLoader(true);      
        dispatch(getAllDashUsers(filterArray))
        .then((res) => {
          setLoader(false);
          setCityDataFilter(res.allCities)
          setDataSource(res.data);
        })
        .catch((error) => {
          setLoader(false);
          // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
        });
      } catch (error) {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      }
  }
  const getAllCityHubsAPI=(city_id,mode='single')=>{
    try {
      setLoader(true);      
      dispatch(allCityHubs(city_id))
      .then((res) => {
        setLoader(false);
        if(mode == "multiple"){
          // const hubDataGet = hubDataFilter;
          if(res.data.length >0){
            const data = res.data;
            data.map(function(val, index){
              setHubDataFilter(hubDataFilter => [...hubDataFilter,val]);
            })
          }
          
        }else{
          setHubDataFilter(res.data);
        }
        
      })
      .catch((error) => {
        setLoader(false);
        // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
      });
    } catch (error) {
      setLoader(false);
      // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  
  function onChangeActive(checked,data) {
    setIsEditing(true);
    const getStatus = {
        _id:data._id,
        status:checked,
        updated:new Date()
    }
    var ApiPath = updateDashUser(getStatus,data._id);
    try {
        setLoader(true);      
        dispatch(ApiPath)
        .then((res) => {
            setLoader(false);      
            if(res.status === true){
              HANDLE_SUCCESS(res.message);          
              setDataSource(res.data.allAdminData);                   
              setIsModalVisible(false)           
              formik.resetFields();  
              formik.setValues(initialValues); 
            }else{
              HANDLE_ERROR(res.message);
            }
        })
        .catch((error) => {
            setLoader(false);
            // HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
        });
    }catch (error) {
        setLoader(false);
        HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }
  }
  const columns = [
    {
      key: "1",
      title: 'Name',
    //   dataIndex: 'name',
      render: (record) => {
        return record.first_name+' '+record.last_name;
      },
    },
    /* {
      key: "2",
      title: 'Contact Person',
      dataIndex: 'contact_persion',
    }, */
    {
        key: "3",
        title: 'Phone Number',
        dataIndex: 'phone_number',
    },
    {
      key: "4",
      title: 'Email',
      dataIndex: 'email',
      render: (email) => {
        return email;
      },
    },
    {
      key: "4",
      title: 'Address',
      dataIndex: 'address',
      render: (address) => {
        return address;
      }
    },
    {
      key: "5",
      title: 'Inactive/Active',
    //   dataIndex: 'status',
      render: (record) => {       
        return <> 
        <Switch checked={record.status} onChange={(value)=>{onChangeActive(value,record)}} className="" />
        </>;
      }
    },
    {
      key: "6",
      title: 'Role',
      render: (record) => {       
        var setUserRole ='';
        if(record.user_role == 'FAE_ADMIN'){
            setUserRole ='FAE ADMIN';
        }else if(record.user_role =='FAE_CITY_ADMIN'){
            setUserRole ='FAE CITY ADMIN';
        }else if(record.user_role =='FAE_HUB_ADMIN'){
            setUserRole ='FAE HUB ADMIN';
        }else if(record.user_role =='FAE_HUB_MANAGER'){
            setUserRole ='FAE HUB MANAGER';
        }else if(record.user_role == 'FAE_STAFF'){
            setUserRole ='FAE STAFF';
        }
        return <> 
            <Tag color="green">{setUserRole}</Tag> 
         
        </>
      }
    },
    {
      key: "7",
      title: 'Edit User',
    //   dataIndex: '_id',
      render: (record) => {
        return <>          
          <Button type="danger" className="btn btn-outline-secondary btn-sm" size="small" onClick={() => {
               onEdit(record)    
          }} >
            <i className="fa fa-edit me-2"  style={{'color': 'white'}}></i>
            Edit 
          </Button>

        </>
      }
    },
    {
      key: "8",
      title: 'Delete User',
      render: (record) => {
        return <>          
          <Button type="danger" className="btn btn-danger btn-sm" size="small" onClick={() => {
            onDelete(record)
          }} >
            <i className="fa fa-trash me-2"  style={{'color': 'white'}}></i>
            Delete 
          </Button>
        </>
      }
    },
  ];
  function onChange(pagination, filters, sorter, extra) {
    // console.log('params', pagination, filters, sorter, extra);
    const filterData ={
      sortField: sorter.field,
      sortOrder: sorter.order,
      pagination,
      ...filters,
    }
  }   
  function handleChangeSelect(value,selectType) { 

    if(selectType == 'city'){
      setHubDataFilter([]);
      setFilter({...filterArray,  city_id:value,hub_id:''})
      getAllCityHubsAPI(value); 

    }else if(selectType == 'hub'){
      setFilter({...filterArray,  hub_id: value})

    }else if(selectType == 'cityAdd'){
      setHubDataFilter([]); 
      if(selectMode.city == 'multiple'){
        if(value.length >0){
          value.map(function(val,index){
            getAllCityHubsAPI(val,'multiple');
          })
        }
        formik.setFieldValue('hub_id',[])
        formik.setFieldValue('city_id',value)
      }else{
        
        getAllCityHubsAPI([value]);
        formik.setFieldValue('city_id',[value])
        formik.setFieldValue('hub_id',[])
      }

    }else if(selectType == 'hubAdd'){
      if(selectMode.hub == 'multiple'){
        formik.setFieldValue('hub_id',value)
      }else{
        
        formik.setFieldValue('hub_id',[value])
      }

    }else if(selectType == 'user_role'){ 
      setFilter({...filterArray,  user_role: value})

    } else if(selectType == 'status'){
      setFilter({...filterArray,  status: value})

    }
  }

  const searchOnEnter=(e)=>{
    const value = e.target.value;
    if(value.length >0){
      setSearchKey(value);
      setFilter({...filterArray,searchKey:value})
    }else{
      setSearchKey('null');
      setFilter({...filterArray,searchKey:''})

    }
  }
  const onSearch = value => {
    if(value.length >0){
      setSearchKey(value);
      setFilter({...filterArray,searchKey:value})

    }else{
      setSearchKey('null');
      setFilter({...filterArray,searchKey:''})

    }
  };
  const onAdd=()=>{
    setIsEditing(false);
    setIsModalVisible(true);    
    setModalTitle("Add New User");
    formik.setValues(initialValues);
  }



  return (
    <>
      <Loader loading={loader} />
      <h2 className="mb-4 text-danger fw-bold fs-3">Dashboard Users</h2>
      <Row className="mb-3">
      <Col span={6}>
        <Search placeholder="Search by Phone Number" allowClear  onSearch={onSearch} onPressEnter={(e)=>searchOnEnter(e)}   />
      </Col>
      <Col span={6}>
        <Select  showSearch placeholder="Filter By Role" optionFilterProp="value" filterOption={(input, option) =>{         
          return option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
        }} name="user_role_filter" className="w-100" id="user_role_filter" onChange={(e)=>handleChangeSelect(e,'user_role')}>          
          <Option value="all">All</Option>
          {
            userRoleData.map(function(item, i){
                return <Option key={item._id} value={item.key} >{item.name}</Option>
            })
          }
        </Select>
      </Col>
      <Col span={6}>
        <Select  placeholder="Filter By Status"  name="status_filter" className="w-100" id="status_filter" onChange={(e)=>handleChangeSelect(e,'status')}>  
          <Option value="all">All</Option>
          <Option value={true}>Active</Option>
          <Option value={false}>Inactive</Option>          
        </Select>
      </Col>
      <Col span={6}>
        
        <Button type="danger" className="float-end mb-3 me-0" onClick={()=>onAdd()} >Add New</Button>
      </Col>
     
    </Row>
      
      <Table  scroll={{x: 1000}} loading={isLoading} onChange={onChange} columns={columns} dataSource={dataSource} />
      {/*  rowSelection={{ type: 'checkbox' }} */}
      {/* view modal details start */}
      <Modal
        title={modalTitle}
        destroyOnClose="true"
        centered
        visible={isModalVisible}
        onOk={()=>{
          formik.submitForm()                  
        }} 
        onCancel={()=>{
          setIsModalVisible(false)
          form.resetFields();
        // Modal.destroyAll();
        }}
        okType="danger"
        okText="Save"
        cancelText="Close"
        width={1000}       
      >
        <form onSubmit={formik.handleSubmit} className="" >
            <div className="form-floating mb-3 col-12 me-3">
                <select status={
                        formik.errors.first_name && formik.touched.first_name
                            ? "error"
                            : ""
                        } value={formik.values.user_role} name="user_role" className={
                            formik.errors.user_role && formik.touched.user_role
                                ? "text-input error form-control rounded-4 "
                                : "text-input form-control rounded-4 "
                            } id="user_role" 
                            onChange={formik.handleChange}
                              onBlur={formik.handleBlur}>
                    
                    <option value='' >Select</option>
                    {
                        userRoleData.map(function(item, i){
                            return <option  key={item._id} value={item.key} >{item.name}</option>
                        })
                    }
                </select>
                <label htmlFor="floatingInput">Select User Role </label>
                {formik.errors.user_role && formik.touched.user_role && (
                    <div className="input-feedback invailid_feedback">{formik.errors.user_role}</div>
                )}
            </div>

            <div className='step3text'  style={!getcityhubmode ? { "display": "none" } : {}}>
            <div className="mb-3 col-12 d-flex">
                <div className="mb-3  col-6 me-3">
                  <label htmlFor="floatingInput" className="pb-2">Select City  </label>
                  <Select mode={selectMode.city} placeholder="select City" name="city_id" className={
                          formik.errors.city_id && formik.touched.city_id
                              ? "text-input error form-control rounded-4"
                              : "text-input  form-control rounded-4"
                          }  id="city_id" onBlur={formik.handleBlur}  value={formik.values.city_id}  onChange={(e)=>handleChangeSelect(e,'cityAdd')} style={classobj.antselect} >
                  
                  {
                    cityDataFilter.map(function(item, i){
                        return <option key={item._id} value={item._id} >{item.name}</option>
                    })
                  }
                </Select>
                  
                  {formik.errors.city_id && formik.touched.city_id && (
                      <div className="input-feedback invailid_feedback">{formik.errors.city_id}</div>
                  )}
                </div>
                <div className=" mb-3  col-6">
                  <label htmlFor="floatingInput"  className="pb-2" >Select Hub</label>
                  <Select  mode={selectMode.hub} placeholder="Select Hub" name="hub_id" className={
                            formik.errors.hub_id && formik.touched.hub_id
                                ? "text-input error form-control rounded-4 "
                                : "text-input form-control rounded-4 "
                            } id="hub_id" value={formik.values.hub_id} onBlur={formik.handleBlur}  onChange={(e)=>handleChangeSelect(e,'hubAdd')}  style={classobj.antselect} >
                      {
                        hubDataFilter.map(function(item, i){
                            return <option key={item._id} selected={formik.values.hub_id==item._id?true:false} value={item._id} >{item.title}</option>
                        })
                      }
                    </Select>
                      
                      {formik.errors.hub_id && formik.touched.hub_id && (
                          <div className="input-feedback invailid_feedback">{formik.errors.hub_id}</div>
                      )}
                </div>
            </div>
            </div>
            <div className="mb-3 col-12 d-flex">
                <div className="form-floating mb-3  col-6 me-3">
                    <Input
                        type="text"                  
                        id="first_name"
                        name="first_name"
                        placeholder="First Name"
                        value={formik.values.first_name}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        className={
                        formik.errors.first_name && formik.touched.first_name
                            ? "text-input error form-control rounded-4 "
                            : "text-input form-control rounded-4 "
                        }
                    />
                    <label htmlFor="floatingInput">First Name </label>
                    {formik.errors.first_name && formik.touched.first_name && (
                        <div className="input-feedback invailid_feedback">{formik.errors.first_name}</div>
                    )}
                </div>
                <div className="form-floating mb-3  col-6">
                    <Input
                        type="text"                  
                        id="last_name"
                        name="last_name"
                        placeholder="Last Name"
                        value={formik.values.last_name}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        className={
                        formik.errors.last_name && formik.touched.last_name
                            ? "text-input error form-control rounded-4 "
                            : "text-input form-control rounded-4 "
                        }
                    />
                    <label htmlFor="floatingInput">Last Name </label>
                    {formik.errors.last_name && formik.touched.last_name && (
                        <div className="input-feedback invailid_feedback">{formik.errors.last_name}</div>
                    )}
                </div>
            </div>
            <div className="mb-3 col-12 d-flex">                
                <div className={isEditing?'form-floating mb-3  col-12 me-3':'form-floating mb-3  col-6 me-3'}>
                    <Input
                        type="text"                  
                        id="email"
                        name="email"
                        placeholder="Email"
                        value={formik.values.email}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        className={
                        formik.errors.email && formik.touched.email
                            ? "text-input error form-control rounded-4 "
                            : "text-input form-control rounded-4 "
                        }
                    />
                    <label htmlFor="floatingInput">Email</label>
                    {formik.errors.email && formik.touched.email && (
                        <div className="input-feedback invailid_feedback">{formik.errors.email}</div>
                    )}
                </div>
                {!isEditing?
                <div className="form-floating mb-3  col-6">
                    <Input
                        type="password"                  
                        id="password"
                        name="password"
                        placeholder="Password"
                        value={formik.values.password}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        className={
                        formik.errors.password && formik.touched.password
                            ? "text-input error form-control rounded-4 "
                            : "text-input form-control rounded-4 "
                        }
                    />
                    <label htmlFor="floatingInput">Password </label>
                    {formik.errors.password && formik.touched.password && (
                        <div className="input-feedback invailid_feedback">{formik.errors.password}</div>
                    )}
                </div>
                :''}
            </div>
            <div className="mb-3 col-12 d-flex">  
                <div className="form-floating mb-3  col-6  me-3">
                    <Input
                        type="text"                  
                        id="phone_number"
                        name="phone_number"
                        placeholder="Phone Number"
                        value={formik.values.phone_number}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        className={
                        formik.errors.phone_number && formik.touched.phone_number
                            ? "text-input error form-control rounded-4 "
                            : "text-input form-control rounded-4 "
                        }
                    />
                    <label htmlFor="floatingInput">Phone Number </label>
                    {formik.errors.phone_number && formik.touched.phone_number && (
                        <div className="input-feedback invailid_feedback">{formik.errors.phone_number}</div>
                    )}
                </div>
                <div className="form-floating mb-3  col-6">
                    <Input
                        type="text"                  
                        id="address"
                        name="address"
                        placeholder="Address"
                        value={formik.values.address}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        className={
                        formik.errors.address && formik.touched.address
                            ? "text-input error form-control rounded-4 "
                            : "text-input form-control rounded-4 "
                        }
                    />
                    <label htmlFor="floatingInput">Address </label>
                    {formik.errors.address && formik.touched.address && (
                        <div className="input-feedback invailid_feedback">{formik.errors.address}</div>
                    )}
                </div>
            </div>
            <div className="mb-3 col-12 d-flex">  
                <div className="form-floating mb-3  col-6  me-3">
                    <Input
                        type="text"                  
                        id="pincode"
                        name="pincode"
                        placeholder="Pincode"
                        value={formik.values.pincode}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        className={
                        formik.errors.pincode && formik.touched.pincode
                            ? "text-input error form-control rounded-4 "
                            : "text-input form-control rounded-4 "
                        }
                    />
                    <label htmlFor="floatingInput">Pincode </label>
                    {formik.errors.pincode && formik.touched.pincode && (
                        <div className="input-feedback invailid_feedback">{formik.errors.pincode}</div>
                    )}
                </div>
                {/* <div className="form-floating mb-3  col-6">
                    <Input
                        type="text"                  
                        id="address"
                        name="address"
                        placeholder="Address"
                        value={formik.values.address}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        className={
                        formik.errors.address && formik.touched.address
                            ? "text-input error form-control rounded-4 "
                            : "text-input form-control rounded-4 "
                        }
                    />
                    <label htmlFor="floatingInput">Address </label>
                    {formik.errors.address && formik.touched.address && (
                        <div className="input-feedback invailid_feedback">{formik.errors.address}</div>
                    )}
                </div> */}
            </div>

        </form>
      </Modal>
      {/* view modal details end */}
      
    </>
  );
}
const mapStateToProps = (state, ownProps = {}) => {  
  const { user } = state.auth;
  const { inventoryData } = state.inventory;
  return {
    user,
    inventoryData
  };
}

export default connect(mapStateToProps)(DashUsersComp);

