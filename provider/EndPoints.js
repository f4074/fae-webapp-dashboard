// Base URL's
export const BASE_URL = process.env.BASE_URL;
export const BASE_URL_CORE =  process.env.BASE_URL_CORE;
export const BASE_URL_AUTH = process.env.BASE_URL_AUTH;
export const IMAGE_URL =  process.env.IMAGE_URL;
export const IMAGE_URL_DASH =  process.env.IMAGE_URL_DASH;
export const IMAGE_URL_DASH_LOCAL =  process.env.IMAGE_URL_DASH_LOCAL;
export const NO_IMAGE_ERROR = "this.src='/img/default-image.png';";
export const AUTH_API =  process.env.AUTH_API;
export const CORE_API =  process.env.CORE_API;

export const NO_IMAGE_ERROR_BASE64 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg==";



//secret keys
export const GOOGLE_CLIENT_SECRET ="";
export const GOOGlE_CLIENT_ID = ""
export const FACEBOOK_APP_ID ="";

// Method constants
export const IS_LOADING = "ISLOADING";
export const POST = "POST";
export const PUT = "PUT";
export const PATCH = "PATCH";
export const GET = "GET";
export const DELETE = "DELETE";
export const IS_DEV = true;

//  Api constants
// user 
export const SIGN_UP = "/user/register";
export const SIGN_IN = "/user/login";
export const SIGN_OUT = "/user/signOut";
export const DASHBOARD = "/user/dashboard";
export const USERS = "/users";
export const USERS_FILTER = "/users_filter";
export const DASH_USERS = "/dash-users";
export const DASH_USERS_FILTER = "/dash-users-filter";
export const DASH_USERS_MULTIPLE_DELETE = "/dash-users-multi-delete";

export const USERS_KYC_DETAILS = "/usersKycDetails";
export const USERS_MULTIPLE_DELETE = "/usersmultiple";
export const USERS_KYC_LIST = "/getAllKycList";
export const USERS_KYC_REQUEST_LIST = "/getAllKycRequest";
export const RESEND_OTP = "/resend_otp";
export const RESEND_OTP_USER = "/resend_otp_user";
export const USERS_FORGOT_PASS = "/fotgot_password";
export const OTP_VERIFY = "/otp_verify";
export const CHANGE_PASSWORD = "/change-password";
export const CHECK_PASSWORD = "/ckeck_password";


// Admin
export const ADMIN_SIGN_UP = "/register";
export const ADMIN_SIGN_IN = "/login";
export const ADMIN_SIGN_OUT = "/signOut";
export const ADMIN_DASHBOARD = "/dashboard";


// business clients
export const BUSINESS_CLIENT = "/business-clients";
export const BUSINESS_CLIENT_FILTER = "/business-clients-filter";


// business suppliers
export const BUSINESS_SUPPLIER = "/business-suppliers";
export const BUSINESS_SUPPLIER_FILTER = "/business-suppliers-filter";

// HUBS
export const FAE_OPRATED_CITIES = "/fae-oprated-city";

// HUBS
export const HUB = "/hub";
export const CITYHUB = "/cityhub";
export const HUB_MULTIPLE_DELETE = "/cityhubmultiple";


// CITIES
export const CITIES = "/cities";

// INVENTORY
export const INVENTORY = "/inventory";
export const INVENTORYFILTER = "/inventory_filter";
export const TRANSFER_VEHICLE_INVENTORY = "/inventory_transfer_vehicle";
export const INVENTORY_MULTIPLE_DELETE = "/inventory_multiple_delete";
export const INVENTORY_TIMELINE = "/inventory_timeline";

// BOOKINGS
export const BOOKINGS = "/bookings";
export const BOOKINGS_FILTER = "/bookings_filter";
export const BOOKINGS_VERIFICATION_CODE = "/checkVerificationCode";
export const CHECK_AVAILABILITY = "/check_availability";
export const FILE_UPLOAD = "/file_upload";


// REFERALS
export const REFERALS = "/referals";
export const REFERALS_FILTER = "/referals_filter";

// REFERALS
export const ASSISTANCE = "/assistance";
export const ASSISTANCE_TYPE = "/assistance_type";
export const ASSISTANCE_FILTER = "/assistance_filter";
export const ASSISTANCE_FARE = "/assistance_fare";
export const PROBLEM_TYPE = "/problem_type";


