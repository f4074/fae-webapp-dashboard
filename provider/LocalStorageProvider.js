

export const SET_DATA_LOCALSTORAGE = (varName,data) => {    
    localStorage.setItem(varName,data);
};
/* 
const data = [{
    "name":"test",
    "email":"test@gmail.com",
}]
const varName = 'userData';
SET_DATA_LOCALSTORAGE(JSON.stringify(data),varName);
*/

export const GET_DATA_LOCALSTORAGE = (varName) => {   
    if (typeof window !== 'undefined') { 
        return localStorage.getItem(varName);
    }else{
        return null;
    }
  
};
/* 
const varName = 'userData';
GET_DATA_LOCALSTORAGE(varName);
*/

export const CLEAR_LOCALSTORAGE = () => {
    localStorage.clear();
};
/* 
CLEAR_LOCALSTORAGE();
*/

export const REMOVE_ITEM_LOCALSTORAGE = (itemName) => {
    localStorage.removeItem(itemName);
};
/* 
const itemName= 'userData';
REMOVE_ITEM_LOCALSTORAGE(itemName);
*/

export const REMEMBER_PASSWORD =(data)=>{
    console.log('setRememberPassword',data);   
    if(data.rememberPassword == true){
      SET_DATA_LOCALSTORAGE("remember_pass",Boolean(data.rememberPassword));
      SET_DATA_LOCALSTORAGE("email_rm",data.email);
      SET_DATA_LOCALSTORAGE("mobile_rm",data.mobile);
      SET_DATA_LOCALSTORAGE("password_rm",data.password);

    }else{
      REMOVE_ITEM_LOCALSTORAGE("remember_pass");
      REMOVE_ITEM_LOCALSTORAGE("email_rm");
      REMOVE_ITEM_LOCALSTORAGE("mobile_rm");
      REMOVE_ITEM_LOCALSTORAGE("password_rm");
    }
}

/* export const SET_REMEMBER_PASSWORD =(email_el,password_el)=>{
    if(GET_DATA_LOCALSTORAGE('remember_pass') == true){

    }else{

    }
   
} */