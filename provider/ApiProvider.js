import axios from "axios";
import helper from "./helper";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
toast.configure();
// import authHeader from "../Redux/Services/auth-header";

export const HTTP_SERVICE_CALL = async (url, type, token, body, params) => {  
  return new Promise(async function (resolve, reject) {
      const userToken = localStorage.getItem("userToken");
      if(userToken){
        const auth = `Bearer ${userToken}`;
        var authHeader = {
          "Accept": "application/json",
          "Content-Type": "application/json;charset=UTF-8",
          // "Authorization": auth,
          "x-access-token":userToken
        }
      }else{
        var authHeader = {
          "Accept": "application/json",
          "Content-Type": "application/json;charset=UTF-8"
        }
      }
      return axios({
        method: type,
        url: url,
        headers: authHeader,
        data: body,
        // params: params
      })
      .then(response => {
        resolve(response)
        
        if (response.status === 200) {
          return resolve(response);
        } else if (response.status === 201) {
          return resolve(response);
        }
        throw Error(response);
      })
      .catch(err => {
        
        if (err && err.response && err.response.status === 401) {         
          // return resolve(err?.response);
        }
        return reject(err?.response);
      });
  });

};

/* 
  const LoginSubmit = async (data) => {
    let dataObj = data;
    
    try {
      setLoader(true);
      let res = await HTTP_SERVICE_CALL(BASE_URL + ADMIN_SIGN_IN, POST, "", dataObj)
      setLoader(false);
      // console.log('res',res);
      if (res && res.status === 200) {
        const resData = res.data;
        if(resData.status == true){
          // const dataGet = JSON.parse(data)
          // console.log('data.rememberPassword',dataGet.rememberPassword);
          HANDLE_SUCCESS(res && res.data.message ? res.data.message : res.statusText);
            // helper.SecureStorageFunc("userToken", "set", res.data?.data?.token);
            // helper.SecureStorageFunc("userData", "set", res.data?.data);          
            router.push('/');
        }else{
          HANDLE_ERROR(res && res.data.message ? res.data.message : res.statusText);
        }      
      } else {
        HANDLE_ERROR();
      }
    } catch (error) {
      setLoader(false);
      HANDLE_ERROR(error && error.data && error.data.message ? error.data.message : error?.statusText);
    }

  };
*/

export const HANDLE_SUCCESS = successRes => {
    if (successRes) {
      toast.success(successRes, {
        autoClose: 2000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: false,
        position: "top-right",
      });
    }
};
/* 
HANDLE_SUCCESS(successMsg);
*/

export const HANDLE_ERROR = errorRes => {
    toast.error(errorRes ? errorRes: "Something went wrong", {
      autoClose: 2000,
      hideProgressBar: true,
      closeOnClick: true,
      pauseOnHover: false,
      position: "top-right",
    });
};
/* 
HANDLE_ERROR(errorMsg);
*/


