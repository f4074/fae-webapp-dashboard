// npm install react-confirm-alert --save

import { confirmAlert } from 'react-confirm-alert'; 
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css

export const CONFIRM_ALERT = (title,message,ConfirmFunction) => {    
    const options = {
        title: title,
        message: message,
        buttons: [
          {
            label: 'Yes',
            onClick: () => {
                console.log("Click Yes");
                ConfirmFunction();
            }
          },
          {
            label: 'No',
            onClick: () => {
                console.log("Click No")
            }
          }
        ],
        // childrenElement: () => <div />,
        // customUI: ({ onClose }) => <div>Custom UI</div>,
        closeOnEscape: true,
        closeOnClickOutside: true,
        keyCodeForClose: [8, 32],
        willUnmount: () => {},
        afterClose: () => {},
        onClickOutside: () => {},
        onKeypressEscape: () => {},
        overlayClassName: "alertConfirm"
      };
      
      return confirmAlert(options);
};
/* 
const title="Are you sure?";
const message="You want to delete this file?";
CONFIRM_ALERT(title,message); 
*/
