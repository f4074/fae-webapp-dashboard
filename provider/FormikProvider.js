/* 
npm install formik --save
npm install yup --save
*/
import { useFormik } from 'formik';
import * as yup from 'yup';

export const VALIDATION_SCHEMA = () => {

    /* return  yup.object({
        email: yup
            .string('Enter your email')
            .email('Enter a valid email')
            .required('Email is required'),
        password: yup
            .string('Enter your password')
        .min(6, 'Password should be of minimum 6 characters length')
        .required('Password is required'),
    }); */

};


export const FORMIK = (initialValues,validationSchema,callbackFunction) => {   

    const formik = useFormik({
        initialValues: initialValues,
        validationSchema: validationSchema,
        onSubmit: (values) => {           
            // console.log('JSON.stringify(values, null, 2)',JSON.stringify(values, null, 2));
            callbackFunction(JSON.stringify(values, null, 2));
        },
    });
    return formik;
};

// export const submitForm =(data)=>{
//     console.log('data',data);
    
// }


/* 
HOW TO USE
import { useFormik } from 'formik';
import * as yup from 'yup';
import {VALIDATION_SCHEMA,FORMIK} from "../provider/formikProvider";

const initialValues = {
    first_name:'',
    last_name:'',
    email: '',
    password: '',
    url:'',
    username:'',
    mobilenumber:''
  };
  const validationSchema = yup.object({
    first_name: yup
    .string('Enter your First Name')
    .required('First Name is required')
    .min(2),
    last_name: yup
    .string('Enter your Last Name')
    .required('Last Name is required')
    .min(2),
    mobilenumber: yup
    .string('Enter your mobile number')
    .required('Mobile number is required')
    .matches(/^\d{10}$/,'Mobile number should be number and 10 characters!'),
    // .min(10,'Mobile number should be 10 characters')
    // .max(10,'Mobile number should be 10 characters'),
    email: yup
        .string('Enter your email')
        .email('Enter a valid email')
        .required('Email is required'),
    password: yup
        .string('Enter your password')
        .required('Password is required')
        .min(6, 'Password should be of minimum 6 characters and at least one uppercase letter, one lowercase letter and one number!')
        .matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/,'Password should be of minimum 6 characters and at least one uppercase letter, one lowercase letter and one number!'),
  });
  const submitForm =(data)=>{
      console.log('data',data);
      // call api here
      
  }
  const formik = FORMIK(initialValues,validationSchema,submitForm);  

*/