import React from "react";
import ForgotPassComp from "../Components/ForgotPassword";

export default function ForgotPassword() {
  return (
    <>
      <div
        className="modal modal-signin position-static d-block py-5 faeSignup d-flex align-items-center"
        tabIndex={-1}
        role="dialog"
        id="modalSignin"
      >
        <ForgotPassComp />
      </div>
    </>
  );
}

ForgotPassword.getLayout = function getLayout(page) {
  return <>{page}</>;
};
