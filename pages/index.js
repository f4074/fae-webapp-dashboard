import React from "react";
// import Counts from ".././Components/Count/Count";
import Dashboard from "../Components/Dashboard";
import LoginComp from "../Components/Login";
import  {SET_DATA_LOCALSTORAGE,GET_DATA_LOCALSTORAGE} from "../provider/LocalStorageProvider"

const user = JSON.parse(GET_DATA_LOCALSTORAGE("userData"));  
const userToken = GET_DATA_LOCALSTORAGE("userToken");

export default function Home() {   
  return (    
    <>
      {/* <Counts /> */}
      <Dashboard />
      {/* <LoginComp /> */}
      {/* {!(user && userToken)?<LoginComp />:  <Dashboard />};  */}
    </>
  );
}
