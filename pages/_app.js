import React from "react";
import NextApp from "next/app";
import "bootstrap/dist/css/bootstrap.min.css";
import "@fortawesome/fontawesome-free/css/all.css";
import "../public/css/styles.css";
import "../public/css/custom.css";
// import withReduxStore from "../lib/with-redux-store";
import store from "../Redux/store";
import { Provider } from "react-redux";
import Layout from "../Components/Layout";

/* class App extends NextApp {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }
    return {
      pageProps,
    };
  }
  
  render() {
    
    const { Component, pageProps, reduxStore } = this.props;
    
    if (Component.getLayout) {
      return Component.getLayout(<Component {...pageProps} />);
    }
    return (
      <Provider store={store}>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      // </Provider>
    );
  }
} */
function App({ Component, pageProps }) {
   
  if (Component.getLayout) {    
    return Component.getLayout(<Provider store={store}><Component {...pageProps} /></Provider>);

  }else{    
    return (
      <Provider store={store}>
        <Layout>
            <Component {...pageProps} />
        </Layout>
      </Provider>
    )
  }
  
}

export default App;
