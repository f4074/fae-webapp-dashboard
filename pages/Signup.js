import React from "react";
import SignupComp from "../Components/SignUp";

export default function Signup() {
  return (
    <>
      <SignupComp />
    </>
  );
}

Signup.getLayout = function getLayout(page) {
  return <>{page}</>;
};
