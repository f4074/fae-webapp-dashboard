import React from "react";
import LoginComp from "../Components/Login";

export default function Login() {
  return (
    <>
      <div
        className="modal modal-signin position-static d-block py-5 faeSignup d-flex align-items-center"
        tabIndex={-1}
        role="dialog"
        id="modalSignin"
      >
        <LoginComp />
      </div>
    </>
  );
}

Login.getLayout = function getLayout(page) {
  return <>{page}</>;
};
