import {
    INVENTORY_SUCCESS,
    INVENTORY_FAIL,
    INVENTORY_TRANSFER_VEHICLE_SUCCESS,
    INVENTORY_TRANSFER_VEHICLE_FAIL,
    INVENTORY_DETAILS_SUCCESS,
    INVENTORY_DETAILS_FAIL,
    INVENTORY_DELETE_SUCCESS,
    INVENTORY_DELETE_FAIL,
    INVENTORY_CREATE_SUCCESS,
    INVENTORY_CREATE_FAIL,
    INVENTORY_UPDATE_SUCCESS,
    INVENTORY_UPDATE_FAIL,
    INVENTORY_TIMELINE_SUCCESS,
    INVENTORY_TIMELINE_FAIL,
    SET_MESSAGE,
  } from "./types";
  
import InventoryService from "../Services/inventory.service";
  
export const createInventory = (dataObj) => (dispatch) => {  
  return InventoryService.create(dataObj).then(
    (response) => {
        // console.log('response',response);
        dispatch({
          type: INVENTORY_CREATE_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: INVENTORY_CREATE_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};

export const getAllInventory = () => (dispatch) => {   
  return InventoryService.getAll().then(
    (response) => {
        console.log('response',response);
        dispatch({
          type: INVENTORY_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: INVENTORY_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};

export const getAllInventoryFilter = (filterInventory) => (dispatch) => {   
  return InventoryService.getAllFilterData(filterInventory).then(
    (response) => {
        console.log('response',response);
        dispatch({
          type: INVENTORY_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: INVENTORY_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};


export const getDetailsInventory = (id) => (dispatch) => {     
  return InventoryService.getDetails(id).then(
    (response) => {
        dispatch({
          type: INVENTORY_DETAILS_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: INVENTORY_DETAILS_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};
export const getVehicleTimeLineInventory= (id) => (dispatch) => {     
  return InventoryService.getVehicleTimeLine(id).then(
    (response) => {
        dispatch({
          type: INVENTORY_TIMELINE_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: INVENTORY_TIMELINE_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};
export const deleteMultiple = (record) => (dispatch) => {
  return InventoryService.deleteMultiple(record).then(
    (response) => {
        dispatch({
          type: INVENTORY_DELETE_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: INVENTORY_DELETE_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};
export const deleteInventory = (id) => (dispatch) => {
  return InventoryService.deleteData(id).then(
    (response) => {
        console.log('response',response);
        dispatch({
          type: INVENTORY_DELETE_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: INVENTORY_DELETE_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};

export const transferInventory = (dataObj) => (dispatch) => {
  // console.log('dataObj service',dataObj);    
  // console.log('dataObj service',vehicle_id);   
  return InventoryService.transferInventory(dataObj).then(
    (response) => {
        console.log('response',response);
        dispatch({
          type: INVENTORY_TRANSFER_VEHICLE_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: INVENTORY_TRANSFER_VEHICLE_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};
export const updateInventory = (dataObj,id) => (dispatch) => {
   
  return InventoryService.updateData(dataObj,id).then(
    (response) => {
        dispatch({
          type: INVENTORY_UPDATE_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: INVENTORY_UPDATE_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};
  

export const isEditModalInventory = (dataObj) => (dispatch) => {  
  console.log('dataObj',dataObj) 
  return Promise.resolve(dispatch({
    type: "IS_MODAL_EDIT_INVENTORY",
    payload: dataObj
  }));
};
  