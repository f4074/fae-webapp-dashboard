import {
    ASSISTANCE_TYPE_DETAILS_SUCCESS,
    ASSISTANCE_TYPE_DETAILS_FAIL,
    ASSISTANCE_TYPE_SUCCESS,
    ASSISTANCE_TYPE_FAIL,
    ASSISTANCE_FARE_DETAILS_SUCCESS,
    ASSISTANCE_FARE_DETAILS_FAIL,
    ASSISTANCE_FARE_SUCCESS,
    ASSISTANCE_FARE_FAIL,
    ASSISTANCE_SUCCESS,
    ASSISTANCE_FAIL,
    PROBLEM_TYPE_SUCCESS,
    PROBLEM_TYPE_FAIL,
    ASSISTANCE_REQUEST_SUCCESS,
    ASSISTANCE_REQUEST_FAIL,
    ASSISTANCE_TYPE_UPDATE_SUCCESS,
    ASSISTANCE_TYPE_UPDATE_FAIL,
    ASSISTANCE_TYPE_DELETE_SUCCESS,
    ASSISTANCE_TYPE_DELETE_FAIL,
    PROBLEM_TYPE_DELETE_SUCCESS,
    PROBLEM_TYPE_DELETE_FAIL,
    ASSISTANCE_FARE_UPDATE_SUCCESS,
    ASSISTANCE_FARE_UPDATE_FAIL,  
    ASSISTANCE_FARE_DELETE_SUCCESS,
    ASSISTANCE_FARE_DELETE_FAIL, 
    SET_MESSAGE,
  } from "./types";
  
import AssistanceService from "../Services/assistance.service";

export const getAllAssistanceFilter = (filterArray) => (dispatch) => {   
  return AssistanceService.getAllAssistanceFilter(filterArray).then(
    (response) => {
        dispatch({
          type: ASSISTANCE_SUCCESS,
          payload: response.data,
        });
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: ASSISTANCE_FAIL,
      });

      return Promise.reject(error);;
    }
  );
};
export const getAllAssistance = () => (dispatch) => {   
  return AssistanceService.getAllAssistance().then(
    (response) => {
        dispatch({
          type: ASSISTANCE_SUCCESS,
          payload: response.data,
        });
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: ASSISTANCE_FAIL,
      });

      return Promise.reject(error);;
    }
  );
};

export const replyAssistanceRequest = (dataObj,id) => (dispatch) => {  
  return AssistanceService.updateData(dataObj,id).then(
    (response) => {
        dispatch({
          type: ASSISTANCE_REQUEST_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: ASSISTANCE_REQUEST_FAIL,
      }); 

      return Promise.reject(error);;
    }
  );
};

/* Assistance type actions start  */
export const createAssistanceType= (dataObj) => (dispatch) => {    
    return AssistanceService.createAssistanceType(dataObj).then(
    (response) => {
        dispatch({
          type: ASSISTANCE_TYPE_SUCCESS,
          payload: response.data,
        });
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: ASSISTANCE_TYPE_FAIL,
      });

      return Promise.reject(error);;
    }
  );
};
export const getAllAssistanceType = () => (dispatch) => {   
  return AssistanceService.getAllAssistanceType().then(
    (response) => {
        dispatch({
          type: ASSISTANCE_TYPE_SUCCESS,
          payload: response.data,
        });
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: ASSISTANCE_TYPE_FAIL,
      });

      return Promise.reject(error);;
    }
  );
};
export const getAssistanceTypeDetails = () => (dispatch) => {   
    return AssistanceService.getDetailsAssistanceType().then(
      (response) => {
          dispatch({
            type: ASSISTANCE_TYPE_DETAILS_SUCCESS,
            payload: response.data,
          });
        return Promise.resolve(response);
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
  
        dispatch({
          type: ASSISTANCE_TYPE_DETAILS_FAIL,
        });
  
        return Promise.reject(error);;
      }
    );
  };

export const updateAssistanceType = (dataObj,id) => (dispatch) => {  
    return AssistanceService.updateAssistanceType(dataObj,id).then(
      (response) => {
          dispatch({
            type: ASSISTANCE_TYPE_UPDATE_SUCCESS,
            payload: response.data,
          });
  
          /*  dispatch({
            type: SET_MESSAGE,
            payload: response.data.message,
          });   */
        return Promise.resolve(response);
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
  
        dispatch({
          type: ASSISTANCE_TYPE_UPDATE_FAIL,
        });
  
        return Promise.reject(error);;
      }
    );
};
    
export const deleteAssistanceType = (id) => (dispatch) => {
  return AssistanceService.deleteAssistanceType(id).then(
    (response) => {
        dispatch({
          type: ASSISTANCE_TYPE_DELETE_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: ASSISTANCE_TYPE_DELETE_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};
export const deleteProblemType = (id) => (dispatch) => {
  return AssistanceService.deleteProblemType(id).then(
    (response) => {
        dispatch({
          type: PROBLEM_TYPE_DELETE_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: PROBLEM_TYPE_DELETE_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};
/* Assistance type actions end  */

/* Assistance fare actions start  */
export const createAssistanceFare= (dataObj) => (dispatch) => {    
  return AssistanceService.createAssistanceFare(dataObj).then(
  (response) => {
      dispatch({
        type: ASSISTANCE_FARE_SUCCESS,
        payload: response.data,
      });
    return Promise.resolve(response);
  },
  (error) => {
    const message =
      (error.response &&
        error.response.data &&
        error.response.data.message) ||
      error.message ||
      error.toString();

    dispatch({
      type: ASSISTANCE_FARE_FAIL,
    });

    return Promise.reject(error);;
  }
);
};
export const getAssistanceFareDetails = () => (dispatch) => {   
  return AssistanceService.getDetailsAssistanceFare().then(
    (response) => {
        dispatch({
          type: ASSISTANCE_FARE_DETAILS_SUCCESS,
          payload: response.data,
        });
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: ASSISTANCE_FARE_DETAILS_FAIL,
      });

      return Promise.reject(error);;
    }
  );
};

export const updateAssistanceFare = (dataObj,id) => (dispatch) => {  
  return AssistanceService.updateAssistanceFare(dataObj,id).then(
    (response) => {
        dispatch({
          type: ASSISTANCE_FARE_UPDATE_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: ASSISTANCE_FARE_UPDATE_FAIL,
      });

      return Promise.reject(error);;
    }
  );
};
  
export const deleteAssistanceFare = (id) => (dispatch) => {
return AssistanceService.deleteAssistanceFare(id).then(
  (response) => {
      dispatch({
        type: ASSISTANCE_FARE_DELETE_SUCCESS,
        payload: response.data,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: response.data.message,
      });   */
    return Promise.resolve(response);
  },
  (error) => {
    const message =
      (error.response &&
        error.response.data &&
        error.response.data.message) ||
      error.message ||
      error.toString();

    dispatch({
      type: ASSISTANCE_FARE_DELETE_FAIL,
    });

    /*  dispatch({
      type: SET_MESSAGE,
      payload: message,
    }); */

    return Promise.reject(error);;
  }
);
};
/* Assistance fare actions end  */

  