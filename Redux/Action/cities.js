import {
    CITY_SUCCESS,
    CITY_FAIL,  
    CITY_DELETE_SUCCESS,
    CITY_DELETE_FAIL,
    SET_MESSAGE,
  } from "./types";
  
import CitiesService from "../Services/cities.service";
  
export const createCity = (dataObj) => (dispatch) => {
    return CitiesService.create(dataObj).then(
    (response) => {
        // console.log('response',response);
        dispatch({
          type: CITY_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: CITY_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);
    }
  );
};

export const getAllCities = () => (dispatch) => {   
  return CitiesService.getAll().then(
    (response) => {
        console.log('response',response);
        dispatch({
          type: CITY_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: CITY_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};

export const deleteCityData = (id) => (dispatch) => {
  console.log('deleteData id',id)
  return CitiesService.deleteData(id).then(
    (response) => {
        console.log('response',response);
        dispatch({
          type: CITY_DELETE_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: CITY_DELETE_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};

export const updateCity = (dataObj,id) => (dispatch) => {
  console.log('dataObj service',dataObj);    
  console.log('dataObj service',id);   
  return CitiesService.updateData(dataObj,id).then(
    (response) => {
        console.log('response',response);
        dispatch({
          type: CITY_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: CITY_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};
  
  
  