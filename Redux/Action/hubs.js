import {
    HUB_SUCCESS,
    HUB_FAIL,
    CITY_HUB_SUCCESS,
    CITY_HUB_FAIL,
    SET_MESSAGE,
  } from "./types";
  
import HubsService from "../Services/hubs.service";
  
export const createHub= (dataObj) => (dispatch) => {    
    return HubsService.create(dataObj).then(
    (response) => {
        // console.log('response',response);
        dispatch({
          type: HUB_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: HUB_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};

export const allCityHubs = (cityid) => (dispatch) => {   
  return HubsService.getAllCityHubs(cityid).then(
    (response) => {
        console.log('response',response);
        dispatch({
          type: CITY_HUB_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: CITY_HUB_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};
export const getAllHubs = () => (dispatch) => {   
  return HubsService.getAll().then(
    (response) => {
        console.log('response',response);
        dispatch({
          type: HUB_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: HUB_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};

export const deleteHub = (id) => (dispatch) => {
  console.log('deleteData id',id)
  return HubsService.deleteData(id).then(
    (response) => {
        console.log('response',response);
        dispatch({
          type: HUB_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: HUB_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};

export const deleteMultipleHub = (ids) => (dispatch) => {
  // console.log('deleteData ids',ids)
  return HubsService.deleteMultipleData(ids).then(
    (response) => {
        console.log('response',response);
        dispatch({
          type: HUB_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: HUB_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};
export const updateHub = (dataObj,id) => (dispatch) => {
  // console.log('dataObj service',dataObj);    
  // console.log('dataObj service',id);   
  return HubsService.updateData(dataObj,id).then(
    (response) => {
        console.log('response',response);
        dispatch({
          type: HUB_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: HUB_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};
  
  
  