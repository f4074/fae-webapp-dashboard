import {
    FAE_OPRATED_CITY_SUCCESS,
    FAE_OPRATED_CITY_FAIL,
    SET_MESSAGE,
  } from "./types";
  
import FaeOperatedCitiesService from "../Services/fae-oprated-cities.service";
  
export const create = (dataObj) => (dispatch) => {  
  return FaeOperatedCitiesService.create(dataObj).then(
    (response) => {
        console.log('response',response);
        dispatch({
          type: FAE_OPRATED_CITY_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: FAE_OPRATED_CITY_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};

export const getAll = () => (dispatch) => {   
  return FaeOperatedCitiesService.getAll().then(
    (response) => {
        console.log('response',response);
        dispatch({
          type: FAE_OPRATED_CITY_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: FAE_OPRATED_CITY_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};

export const deleteData = (id) => (dispatch) => {
  console.log('deleteData id',id)
  return FaeOperatedCitiesService.deleteData(id).then(
    (response) => {
        console.log('response',response);
        dispatch({
          type: FAE_OPRATED_CITY_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: FAE_OPRATED_CITY_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};

export const update = (dataObj,id) => (dispatch) => {
  console.log('dataObj service',dataObj);    
  console.log('dataObj service',id);   
  return FaeOperatedCitiesService.updateData(dataObj,id).then(
    (response) => {
        console.log('response',response);
        dispatch({
          type: FAE_OPRATED_CITY_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: FAE_OPRATED_CITY_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};
  
  
  