import {
    BUSINEESS_SUPPLIER_SUCCESS,
    BUSINEESS_SUPPLIER_FAIL,
    SET_MESSAGE,
  } from "./types";
  
import BusinessSupplierService from "../Services/business_supplier.service";
  
export const create = (dataObj) => (dispatch) => {  
  return BusinessSupplierService.create(dataObj).then(
    (response) => {
        console.log('response',response);
        dispatch({
          type: BUSINEESS_SUPPLIER_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: BUSINEESS_SUPPLIER_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};

export const getAllwithFilter = (filterArray) => (dispatch) => {  
  
  return BusinessSupplierService.getAllwithFilter(filterArray).then(
    (response) => {
      console.log('getAllwithFilter filterArray',response) 
        dispatch({
          type: BUSINEESS_SUPPLIER_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: BUSINEESS_SUPPLIER_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};
export const getAll = () => (dispatch) => {   
  return BusinessSupplierService.getAll().then(
    (response) => {
        console.log('response',response);
        dispatch({
          type: BUSINEESS_SUPPLIER_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: BUSINEESS_SUPPLIER_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};

export const deleteData = (id) => (dispatch) => {
  console.log('deleteData id',id)
  return BusinessSupplierService.deleteData(id).then(
    (response) => {
        console.log('response',response);
        dispatch({
          type: BUSINEESS_SUPPLIER_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: BUSINEESS_SUPPLIER_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};

export const update = (dataObj,id) => (dispatch) => {
  console.log('dataObj service',dataObj);    
  console.log('dataObj service',id);   
  return BusinessSupplierService.updateData(dataObj,id).then(
    (response) => {
        console.log('response',response);
        dispatch({
          type: BUSINEESS_SUPPLIER_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: BUSINEESS_SUPPLIER_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};
  
  
  