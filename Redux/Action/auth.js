import {
    REGISTER_SUCCESS,
    REGISTER_FAIL,
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    LOGOUT,
    SET_MESSAGE,
  } from "./types";
  
  import AuthService from "../Services/auth.service";
  
  export const register = (username, email, password) => (dispatch) => {

    return AuthService.register(username, email, password).then(
      (response) => {
        dispatch({
          type: REGISTER_SUCCESS,
        });
  
        dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });
  
        return Promise.resolve();
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
  
        dispatch({
          type: REGISTER_FAIL,
        });
  
        dispatch({
          type: SET_MESSAGE,
          payload: message,
        });
  
        return Promise.reject(error);
      }
    );
  };
  
  export const Login = (dataObj) => (dispatch) => {  
    
    return AuthService.login(dataObj).then(
      (data) => {
        // console.log('data AuthService',data);
        dispatch({
          type: LOGIN_SUCCESS,
          payload: { user: data },
        });  
        return Promise.resolve(data);
      },
      (error) => {
        console.log('data AuthService error',error);
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
  
        dispatch({
          type: LOGIN_FAIL,
        });
  
        dispatch({
          type: SET_MESSAGE,
          payload: message,
        });
  
        return Promise.reject(error);
      }
    );
  };
  
  export const Logout = () => (dispatch) => {    
    AuthService.logout();  
    dispatch({
      type: LOGOUT,
    });
    // return Promise.resolve(data);
  };
  