import {
    DASHBOARD_SUCCESS,
    DASHBOARD_FAIL,
    SET_MESSAGE,
  } from "./types";
  
import DashboardService from "../Services/dashboard.service";
 
export const getAllDashboardData = (filterArray) => (dispatch) => {   
  return DashboardService.getAll(filterArray).then(
    (response) => {
        console.log('response',response);
        dispatch({
          type: DASHBOARD_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: DASHBOARD_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};

  
  