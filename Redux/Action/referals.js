import {
    REFERALS_DETAILS_SUCCESS,
    REFERALS_DETAILS_FAIL,
    REFERALS_SUCCESS,
    REFERALS_FAIL,
    SET_MESSAGE,
  } from "./types";
  
import ReferalsService from "../Services/referals.service";
/* user actions start  */
export const createReferal= (dataObj) => (dispatch) => {    
    return ReferalsService.create(dataObj).then(
    (response) => {
        dispatch({
          type: REFERALS_SUCCESS,
          payload: response.data,
        });
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: REFERALS_FAIL,
      });

      return Promise.reject(error);;
    }
  );
};
export const getAllReferals = (filterArray) => (dispatch) => {   
  return ReferalsService.getAll(filterArray).then(
    (response) => {
        dispatch({
          type: REFERALS_SUCCESS,
          payload: response.data,
        });
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: REFERALS_FAIL,
      });

      return Promise.reject(error);;
    }
  );
};


export const getReferalsDetails = () => (dispatch) => {   
    return ReferalsService.getDetails().then(
      (response) => {
          dispatch({
            type: REFERALS_DETAILS_SUCCESS,
            payload: response.data,
          });
        return Promise.resolve(response);
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
  
        dispatch({
          type: REFERALS_DETAILS_FAIL,
        });
  
        return Promise.reject(error);;
      }
    );
  };

export const updateReferal = (dataObj,id) => (dispatch) => {  
    return ReferalsService.updateData(dataObj,id).then(
      (response) => {
          dispatch({
            type: REFERALS_SUCCESS,
            payload: response.data,
          });
  
          /*  dispatch({
            type: SET_MESSAGE,
            payload: response.data.message,
          });   */
        return Promise.resolve(response);
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
  
        dispatch({
          type: REFERALS_FAIL,
        });
  
        return Promise.reject(error);;
      }
    );
};
    
export const deleteReferal = (id) => (dispatch) => {
  return ReferalsService.deleteData(id).then(
    (response) => {
        dispatch({
          type: REFERALS_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: REFERALS_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};

export const deleteMultipleReferals = (ids) => (dispatch) => {
  return UsersService.deleteMultipleData(ids).then(
    (response) => {
        dispatch({
          type: REFERALS_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: REFERALS_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};
 
  