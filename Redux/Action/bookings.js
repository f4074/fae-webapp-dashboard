import {
    BOOKINGS_SUCCESS,
    BOOKINGS_FAIL,
    BOOKINGS_DETAILS_SUCCESS,
    BOOKINGS_DETAILS_FAIL,
    BOOKINGS_VERIFICATION_SUCCESS,
    BOOKINGS_VERIFICATION_FAIL,
    VEHICLE_AVAILABILITY_SUCCESS,
    VEHICLE_AVAILABILITY_FAIL,
    SET_MESSAGE,
  } from "./types";
  
import BookingsService from "../Services/bookings.service";
  
export const create = (dataObj) => (dispatch) => {
  return BookingsService.create(dataObj).then(
    (response) => {
        console.log('response',response);
        dispatch({
          type: BOOKINGS_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: BOOKINGS_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};


export const checkAvailability = (dataObj) => (dispatch) => {
  
  return BookingsService.checkvehicleAvailability(dataObj).then(
    (response) => {
        dispatch({
          type:VEHICLE_AVAILABILITY_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: VEHICLE_AVAILABILITY_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};
export const bookingCheckVerifCode = (dataObj) => (dispatch) => {
  return BookingsService.booking_check_verification_code(dataObj).then(
    (response) => {
        dispatch({
          type: BOOKINGS_VERIFICATION_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: BOOKINGS_VERIFICATION_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};

export const update = (dataObj,id) => (dispatch) => {
  return BookingsService.update(dataObj,id).then(
    (response) => {
        console.log('response',response);
        dispatch({
          type: BOOKINGS_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: BOOKINGS_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};

export const getDetails = (id) => (dispatch) => {   
  return BookingsService.getDetails(id).then(
    (response) => {
        console.log('response',response);
        dispatch({
          type: BOOKINGS_DETAILS_SUCCESS,
          payload: response.data,
        });
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: BOOKINGS_DETAILS_FAIL,
      });

      return Promise.reject(error);;
    }
  );
};
export const getAllWithFilter = (filterArray) => (dispatch) => {   
  return BookingsService.getAllWithFilter(filterArray).then(
    (response) => {
        dispatch({
          type: BOOKINGS_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: BOOKINGS_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};
export const getAll = () => (dispatch) => {   
  return BookingsService.getAll().then(
    (response) => {
        console.log('response',response);
        dispatch({
          type: BOOKINGS_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: BOOKINGS_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};

export const deleteData = (id) => (dispatch) => {
  return BookingsService.deleteData(id).then(
    (response) => {
        console.log('response',response);
        dispatch({
          type: BOOKINGS_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: BOOKINGS_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};

export const updateData = (dataObj,id) => (dispatch) => {
  return BookingsService.updateData(dataObj,id).then(
    (response) => {
        dispatch({
          type: BOOKINGS_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: BOOKINGS_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};
  
  
  