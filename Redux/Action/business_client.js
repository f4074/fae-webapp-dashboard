import {
    BUSINEESS_CLIENT_SUCCESS,
    BUSINEESS_CLIENT_FAIL,
    SET_MESSAGE,
  } from "./types";
  
import BusinessClientService from "../Services/business_client.service";
  
export const create = (dataObj) => (dispatch) => {
  console.log('dataObj service',dataObj);    
  return BusinessClientService.create(dataObj).then(
    (response) => {
        console.log('response',response);
        dispatch({
          type: BUSINEESS_CLIENT_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: BUSINEESS_CLIENT_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};
export const update = (dataObj,id) => (dispatch) => {
  console.log('dataObj service',dataObj);  
  return BusinessClientService.update(dataObj,id).then(
    (response) => {
        console.log('response',response);
        dispatch({
          type: BUSINEESS_CLIENT_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: BUSINEESS_CLIENT_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};
export const getAll = () => (dispatch) => {   
  return BusinessClientService.getAll().then(
    (response) => {
        dispatch({
          type: BUSINEESS_CLIENT_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: BUSINEESS_CLIENT_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};
export const getAllwithFilter = (filterArray) => (dispatch) => {   
  return BusinessClientService.getAllwithFilter(filterArray).then(
    (response) => {
        dispatch({
          type: BUSINEESS_CLIENT_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: BUSINEESS_CLIENT_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};

export const deleteClient = (id) => (dispatch) => {
  return BusinessClientService.deleteClient(id).then(
    (response) => {
        console.log('response',response);
        dispatch({
          type: BUSINEESS_CLIENT_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: BUSINEESS_CLIENT_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};

export const updateClient = (dataObj,id) => (dispatch) => {
  console.log('updateClient dataObj',dataObj)
  console.log('updateClient id',id)
  return BusinessClientService.updateClient(dataObj,id).then(
    (response) => {
        console.log('response',response);
        dispatch({
          type: BUSINEESS_CLIENT_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: BUSINEESS_CLIENT_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};
  
  
  