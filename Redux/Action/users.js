import {
    USERS_SUCCESS,
    USERS_FAIL,
    USERS_DETAILS_SUCCESS,
    USERS_DETAILS_FAIL,
    USERS_KYC_LIST_SUCCESS,
    USERS_KYC_LIST_FAIL,
    DASH_USERS_SUCCESS,
    DASH_USERS_FAIL,
    RESEND_OTP_SUCCESS,
    RESEND_OTP_FAIL,
    USERS_FORGOT_PASS_SUCCESS,
    USERS_FORGOT_PASS_FAIL,
    OTP_VERIFY_SUCCESS,
    OTP_VERIFY_FAIL,
    CHANGE_PASSWORD_SUCCESS,
    CHANGE_PASSWORD_FAIL,    
    CHECK_PASSWORD_SUCCESS,
    CHECK_PASSWORD_FAIL,
    SET_MESSAGE,
  } from "./types";
  
import UsersService from "../Services/user.service";
/* user actions start  */
export const createUser= (dataObj) => (dispatch) => {    
    return UsersService.create(dataObj).then(
    (response) => {
        dispatch({
          type: USERS_SUCCESS,
          payload: response.data,
        });
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: USERS_FAIL,
      });

      return Promise.reject(error);;
    }
  );
};
export const ForgotPassword= (dataObj) => (dispatch) => {    
  return UsersService.Forgot_Password(dataObj).then(
  (response) => {
      dispatch({
        type: USERS_FORGOT_PASS_SUCCESS,
        payload: response.data,
      });
    return Promise.resolve(response);
  },
  (error) => {
    const message =
      (error.response &&
        error.response.data &&
        error.response.data.message) ||
      error.message ||
      error.toString();

    dispatch({
      type: USERS_FORGOT_PASS_FAIL,
    });

    return Promise.reject(error);;
  }
);
};

export const getAllUsers = (filterArray) => (dispatch) => {   
  return UsersService.getAllFilter(filterArray).then(
    (response) => {
        dispatch({
          type: USERS_SUCCESS,
          payload: response.data,
        });
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: USERS_FAIL,
      });

      return Promise.reject(error);;
    }
  );
};

export const getAllKycList = (searchKey) => (dispatch) => {   
  return UsersService.getAllKycList(searchKey).then(
    (response) => {        
        dispatch({
          type: USERS_KYC_LIST_SUCCESS,
          payload: response.data,
        });
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: USERS_KYC_LIST_FAIL,
      });

      return Promise.reject(error);;
    }
  );
};
export const getKycDetails = (user_id) => (dispatch) => {   
  return UsersService.getKycDetails(user_id).then(
    (response) => {        
        dispatch({
          type: USERS_DETAILS_SUCCESS,
          payload: response.data,
        });
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: USERS_DETAILS_FAIL,
      });

      return Promise.reject(error);;
    }
  );
};
export const otp_verify = (data) => (dispatch) => {   
  return UsersService.otp_verify(data).then(
    (response) => {        
        dispatch({
          type: OTP_VERIFY_SUCCESS,
          payload: response.data,
        });
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: OTP_VERIFY_FAIL,
      });

      return Promise.reject(error);;
    }
  );
};

export const resendOtp = (data) => (dispatch) => {   
  return UsersService.resendOtp(data).then(
    (response) => {        
        dispatch({
          type: RESEND_OTP_SUCCESS,
          payload: response.data,
        });
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: RESEND_OTP_FAIL,
      });

      return Promise.reject(error);;
    }
  );
};

export const checkOldPassword = (data) => (dispatch) => {   
  return UsersService.checkOldPassword(data).then(
    (response) => {        
        dispatch({
          type: CHECK_PASSWORD_SUCCESS,
          payload: response.data,
        });
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: CHECK_PASSWORD_FAIL,
      });

      return Promise.reject(error);;
    }
  );
};

export const change_password = (data) => (dispatch) => {   
  return UsersService.change_password(data).then(
    (response) => {        
        dispatch({
          type: CHANGE_PASSWORD_SUCCESS,
          payload: response.data,
        });
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: CHANGE_PASSWORD_FAIL,
      });

      return Promise.reject(error);;
    }
  );
};

export const updateUser = (dataObj,id) => (dispatch) => {  
    return UsersService.updateData(dataObj,id).then(
      (response) => {
          dispatch({
            type: USERS_SUCCESS,
            payload: response.data,
          });
  
          /*  dispatch({
            type: SET_MESSAGE,
            payload: response.data.message,
          });   */
        return Promise.resolve(response);
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
  
        dispatch({
          type: USERS_FAIL,
        });
  
        /*  dispatch({
          type: SET_MESSAGE,
          payload: message,
        }); */
  
        return Promise.reject(error);;
      }
    );
};
    
export const deleteUser = (id) => (dispatch) => {
  return UsersService.deleteData(id).then(
    (response) => {
        dispatch({
          type: USERS_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: USERS_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};

export const deleteMultipleUsers = (ids) => (dispatch) => {
  return UsersService.deleteMultipleData(ids).then(
    (response) => {
        dispatch({
          type: USERS_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: USERS_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};
  
/* Admin Actions Start */

export const getAllDashUsers = (filterArray) => (dispatch) => {   
  return UsersService.getAllDashUsers(filterArray).then(
    (response) => {
        dispatch({
          type: DASH_USERS_SUCCESS,
          payload: response.data,
        });
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: DASH_USERS_FAIL,
      });

      return Promise.reject(error);;
    }
  );
};

export const createDashUser= (dataObj) => (dispatch) => {    
  return UsersService.createDashUser(dataObj).then(
  (response) => {
      dispatch({
        type: DASH_USERS_SUCCESS,
        payload: response.data,
      });
    return Promise.resolve(response);
  },
  (error) => {
    const message =
      (error.response &&
        error.response.data &&
        error.response.data.message) ||
      error.message ||
      error.toString();

    dispatch({
      type: DASH_USERS_FAIL,
    });

    return Promise.reject(error);;
  }
);
};

export const updateDashUser = (dataObj,id) => (dispatch) => {  
  return UsersService.updateDashUser(dataObj,id).then(
    (response) => {
        dispatch({
          type: DASH_USERS_SUCCESS,
          payload: response.data,
        });

        /*  dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });   */
      return Promise.resolve(response);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: DASH_USERS_FAIL,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: message,
      }); */

      return Promise.reject(error);;
    }
  );
};
  
export const deleteDashUser = (id) => (dispatch) => {
return UsersService.deleteDashUser(id).then(
  (response) => {
      dispatch({
        type: DASH_USERS_SUCCESS,
        payload: response.data,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: response.data.message,
      });   */
    return Promise.resolve(response);
  },
  (error) => {
    const message =
      (error.response &&
        error.response.data &&
        error.response.data.message) ||
      error.message ||
      error.toString();

    dispatch({
      type: DASH_USERS_FAIL,
    });

    /*  dispatch({
      type: SET_MESSAGE,
      payload: message,
    }); */

    return Promise.reject(error);;
  }
);
};

export const deleteMultipleDashUsers = (ids) => (dispatch) => {
return UsersService.deleteMultipleDashUsers(ids).then(
  (response) => {
      dispatch({
        type: DASH_USERS_SUCCESS,
        payload: response.data,
      });

      /*  dispatch({
        type: SET_MESSAGE,
        payload: response.data.message,
      });   */
    return Promise.resolve(response);
  },
  (error) => {
    const message =
      (error.response &&
        error.response.data &&
        error.response.data.message) ||
      error.message ||
      error.toString();

    dispatch({
      type: DASH_USERS_FAIL,
    });

    /*  dispatch({
      type: SET_MESSAGE,
      payload: message,
    }); */

    return Promise.reject(error);;
  }
);
};
  