import  {SET_DATA_LOCALSTORAGE,GET_DATA_LOCALSTORAGE} from "../../provider/LocalStorageProvider";
import {
    HUB_SUCCESS,
    HUB_FAIL,
    CITY_HUB_SUCCESS,
    SET_MESSAGE,
    CITY_HUB_FAIL,
  } from "../Action/types";
   
  
  const initialState =  { isData: false, hubsData: null,cityhubsData:null }
  export default function (state = initialState, action) {
    const { type, payload } = action;
    switch (type) {
      case HUB_SUCCESS:
        return {
          ...state,
          isData: true,
          hubsData:payload
        };
      case HUB_FAIL:
      return {
        ...state,
        isData: false,
      };      
      case CITY_HUB_SUCCESS:
      return {
        ...state,
        isData: true,
        cityhubsData:payload
      };
      case CITY_HUB_FAIL:
      return {
        ...state,
        isData: false,
        cityhubsData:null
      };
      
      default:
        return state;
    }
  }
  