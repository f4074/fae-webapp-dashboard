import  {SET_DATA_LOCALSTORAGE,GET_DATA_LOCALSTORAGE} from "../../provider/LocalStorageProvider";
import {
    FAE_OPRATED_CITY_SUCCESS,
    FAE_OPRATED_CITY_FAIL,
    SET_MESSAGE,
  } from "../Action/types";
   
  
  const initialState =  { isData: false, faeOpratedCityData: null }
  export default function (state = initialState, action) {
    const { type, payload } = action;
    // console.log('type',type);
    // console.log('payload',payload);
    // console.log('state',state);
    switch (type) {
      case FAE_OPRATED_CITY_SUCCESS:
        return {
          ...state,
          isData: true,
          faeOpratedCityData:payload
        };
      case FAE_OPRATED_CITY_FAIL:
        return {
          ...state,
          isData: false,
        };
      
      default:
        return state;
    }
  }
  