import  {SET_DATA_LOCALSTORAGE,GET_DATA_LOCALSTORAGE} from "../../provider/LocalStorageProvider";
import {
    BOOKINGS_SUCCESS,
    BOOKINGS_FAIL,
    BOOKINGS_DETAILS_SUCCESS,
    BOOKINGS_DETAILS_FAIL,
    BOOKINGS_VERIFICATION_SUCCESS,
    BOOKINGS_VERIFICATION_FAIL,
    VEHICLE_AVAILABILITY_SUCCESS,
    VEHICLE_AVAILABILITY_FAIL,
    SET_MESSAGE,
  } from "../Action/types";
   
  
  const initialState =  { isData: false, bookingsData: null , bookingDetails: null}
  export default function (state = initialState, action) {
    const { type, payload } = action;
   
    switch (type) {
      case BOOKINGS_SUCCESS:
        return {
          ...state,
          isData: true,
          bookingsData:payload
        };
      case BOOKINGS_FAIL:
        return {
          ...state,
          isData: false,
        };
      case BOOKINGS_VERIFICATION_SUCCESS:
        return {
          ...state,
          isData: true,
          bookingDetails:payload
        };
      case BOOKINGS_VERIFICATION_FAIL:
        return {
          ...state,
          isData: false,
        };
        case VEHICLE_AVAILABILITY_SUCCESS:
        return {
          ...state,
          isData: true,
        };
      case VEHICLE_AVAILABILITY_FAIL:
        return {
          ...state,
          isData: false,
        };
      case BOOKINGS_DETAILS_SUCCESS:
        return {
            ...state,
            isData: true,
            bookingDetails:payload
        };
      case BOOKINGS_DETAILS_FAIL:
        return {
            ...state,
            isData: false,
            bookingDetails:null
        };
          
      default:
        return state;
    }
  }
  