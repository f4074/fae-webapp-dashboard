import  {SET_DATA_LOCALSTORAGE,GET_DATA_LOCALSTORAGE} from "../../provider/LocalStorageProvider";
import {
    USERS_DETAILS_SUCCESS,
    USERS_DETAILS_FAIL,
    USERS_SUCCESS,
    USERS_FAIL,
    USERS_KYC_LIST_SUCCESS,
    USERS_KYC_LIST_FAIL,
    DASH_USERS_SUCCESS,
    DASH_USERS_FAIL,
    RESEND_OTP_SUCCESS,
    RESEND_OTP_FAIL,
    SET_MESSAGE,
    USERS_FORGOT_PASS_SUCCESS,
    USERS_FORGOT_PASS_FAIL,
    OTP_VERIFY_SUCCESS,
    OTP_VERIFY_FAIL,
    CHANGE_PASSWORD_SUCCESS,
    CHANGE_PASSWORD_FAIL,
    CHECK_PASSWORD_SUCCESS,
    CHECK_PASSWORD_FAIL,
  } from "../Action/types";
   
  
  const initialState =  { isData: false, usersData: null, usersDetails: null , dashUsersData: null, dashUsersDetails: null, usersKycList: [] }
  export default function (state = initialState, action) {
    const { type, payload } = action;
    switch (type) {
      case USERS_SUCCESS:
        return {
          ...state,
          isData: true,
          usersData:payload
        };
      case USERS_FAIL:
      return {
        ...state,
        isData: false,
        usersData:null
      }; 
      case DASH_USERS_SUCCESS:
        return {
          ...state,
          isData: true,
          usersData:payload
        };
      case DASH_USERS_FAIL:
      return {
        ...state,
        isData: false,
        usersData:null
      }; 
      case USERS_KYC_LIST_SUCCESS:
        return {
          ...state,
          isData: true,
          usersKycList:payload
        };
      case USERS_KYC_LIST_FAIL:
      return {
        ...state,
        isData: false,
        usersKycList:[]
      }; 
      case USERS_DETAILS_SUCCESS:
        return {
          ...state,
          isData: true,
          usersDetails:payload
        };
      case USERS_DETAILS_FAIL:
      return {
        ...state,
        isData: false,
        usersDetails:null
      }; 
      case RESEND_OTP_SUCCESS:
        return {
          ...state,
          isData: true,
        };
      case RESEND_OTP_FAIL:
      return {
        ...state,
        isData: false,
      };
      case USERS_FORGOT_PASS_SUCCESS:
        return {
          ...state,
          isData: true,
        };
      case USERS_FORGOT_PASS_FAIL:
      return {
        ...state,
        isData: false,
      };
      case OTP_VERIFY_SUCCESS:
        return {
          ...state,
          isData: true,
        };
      case OTP_VERIFY_FAIL:
      return {
        ...state,
        isData: false,
      };
      case CHANGE_PASSWORD_SUCCESS:
        return {
          ...state,
          isData: true,
        };
      case CHANGE_PASSWORD_FAIL:
      return {
        ...state,
        isData: false,
      };
      case CHECK_PASSWORD_SUCCESS:
        return {
          ...state,
          isData: true,
        };
      case CHECK_PASSWORD_FAIL:
      return {
        ...state,
        isData: false,
      };
      default:
        return state;
    }
  }
  