import  {SET_DATA_LOCALSTORAGE,GET_DATA_LOCALSTORAGE} from "../../provider/LocalStorageProvider"
import {
    REGISTER_SUCCESS,
    REGISTER_FAIL,
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    LOGOUT,
  } from "../Action/types";
  
  const user = JSON.parse(GET_DATA_LOCALSTORAGE("userData"));  
  const userToken = GET_DATA_LOCALSTORAGE("userToken");  
  
  const initialState = user
    ? { isLoggedIn: true, user:user,userToken:userToken,user_role:user.user_role,user_city_id:user.city_id,user_hub_id:user.hub_id}
    : { isLoggedIn: false, user: null,userToken:null ,user_role:null,user_city_id:'',user_hub_id:'',user_cities:'',user_hubs:'' };
  
  export default function (state = initialState, action) {
    const { type, payload } = action;  
    console.log('payload',payload)  
    switch (type) {
      case REGISTER_SUCCESS:
        return {
          ...state,
          isLoggedIn: false,
          user_role:null
        };
      case REGISTER_FAIL:
        return {
          ...state,
          isLoggedIn: false,
          user_role:null
        };
      case LOGIN_SUCCESS:
        return {
          ...state,
          isLoggedIn: true,
          user: payload && payload.user && payload.user.data ?payload.user.data:null,
          user_city_id: payload && payload.user && payload.user.data?payload.user.data.city_id:null,
          user_hub_id: payload && payload.user && payload.user.data?payload.user.data.hub_id:null,
          userToken:payload && payload.user && payload.user.data?payload.user.token:null,
          user_role:payload && payload.user && payload.user.data?payload.user.data.user_role:null,
          user_cities:payload && payload.user && payload.user.city_data?payload.user.city_data:null,
          user_hubs:payload && payload.user && payload.user.hub_data?payload.user.hub_data:null
        };
      case LOGIN_FAIL:
        return {
          ...state,
          isLoggedIn: false,
          user: null,
          userToken:null,
          user_role:null
        };
      case LOGOUT:
        return {
          ...state,
          isLoggedIn: false,
          user: null,
          userToken:null,
          user_role:null
        };
      default:
        return state;
    }
  }
  