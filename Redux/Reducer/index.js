import { combineReducers } from "redux";
import auth from "./auth";
import message from "./message";
import business_client from "./business_client";
import business_supplier from "./business_supplier";
import fae_oprated_cities from "./fae-oprated-cities";
import cities from "./cities";
import hubs from "./hubs";
import inventory from "./inventory";
import bookings from "./bookings";
import users from "./users";
import assistance from "./assistance";
import referals from "./referals";
import dashboard from "./dashboard";

export default combineReducers({
  auth,
  // message,
  business_client,
  business_supplier,
  fae_oprated_cities,
  cities,
  hubs,
  inventory,
  bookings,
  users,
  assistance,
  referals,
  dashboard
});
