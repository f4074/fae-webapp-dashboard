import  {SET_DATA_LOCALSTORAGE,GET_DATA_LOCALSTORAGE} from "../../provider/LocalStorageProvider";
import {
    REFERALS_DETAILS_SUCCESS,
    REFERALS_DETAILS_FAIL,
    REFERALS_SUCCESS,
    REFERALS_FAIL,
    SET_MESSAGE,
  } from "../Action/types";
   
  
  const initialState =  { isData: false, referalsData: [], referalsDetails: null }
  export default function (state = initialState, action) {
    const { type, payload } = action;
    switch (type) {
      case REFERALS_SUCCESS:
        return {
          ...state,
          isData: true,
          referalsData:payload
        };
      case REFERALS_FAIL:
      return {
        ...state,
        isData: false,
        referalsData:[]
      }; 
      
      case REFERALS_DETAILS_SUCCESS:
        return {
          ...state,
          isData: true,
          referalsDetails:payload
        };
      case REFERALS_DETAILS_FAIL:
      return {
        ...state,
        isData: false,
        referalsDetails:null
      }; 
      default:
        return state;
    }
  }
  