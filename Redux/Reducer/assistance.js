import  {SET_DATA_LOCALSTORAGE,GET_DATA_LOCALSTORAGE} from "../../provider/LocalStorageProvider";
import {
  ASSISTANCE_TYPE_DETAILS_SUCCESS,
  ASSISTANCE_TYPE_DETAILS_FAIL,
  ASSISTANCE_TYPE_SUCCESS,
  ASSISTANCE_TYPE_FAIL,
  ASSISTANCE_FARE_DETAILS_SUCCESS,
  ASSISTANCE_FARE_DETAILS_FAIL,
  ASSISTANCE_FARE_SUCCESS,
  ASSISTANCE_FARE_FAIL,
  ASSISTANCE_SUCCESS,
  ASSISTANCE_FAIL,
  PROBLEM_TYPE_SUCCESS,
  PROBLEM_TYPE_FAIL,
  ASSISTANCE_REQUEST_SUCCESS,
  ASSISTANCE_REQUEST_FAIL,
  ASSISTANCE_TYPE_UPDATE_SUCCESS,
  ASSISTANCE_TYPE_UPDATE_FAIL,
  ASSISTANCE_TYPE_DELETE_SUCCESS,
  ASSISTANCE_TYPE_DELETE_FAIL,
  PROBLEM_TYPE_DELETE_SUCCESS,
  PROBLEM_TYPE_DELETE_FAIL,
  ASSISTANCE_FARE_UPDATE_SUCCESS,
  ASSISTANCE_FARE_UPDATE_FAIL,  
  ASSISTANCE_FARE_DELETE_SUCCESS,
  ASSISTANCE_FARE_DELETE_FAIL, 
    SET_MESSAGE,
  } from "../Action/types";
  
  const initialState =  { isData: false, assistanceData: [], assistanceTypeDetails: null,assistanceType:[], assistanceFareDetails: null,assistanceFare:[] ,problemType:[]}
  export default function (state = initialState, action) {
    const { type, payload } = action;
    switch (type) {
      case ASSISTANCE_SUCCESS:
        return {
          ...state,
          isData: true,
          assistanceData:payload.assistanceRequestData,
          assistanceType:payload.assistance_typeData,
          assistanceFare:payload.assistance_fareData,
          problemType:payload.problem_typeData,
        };
      case ASSISTANCE_FAIL:
      return {
        ...state,
        isData: false,
        assistanceData:[]
      }; 
      case ASSISTANCE_REQUEST_SUCCESS:
        return {
          ...state,
          isData: true,
          assistanceData:payload.assistanceRequestData,
        };
      case ASSISTANCE_REQUEST_FAIL:
      return {
        ...state,
        isData: false,
        // assistanceData:[]
      };       
      case ASSISTANCE_TYPE_DETAILS_SUCCESS:
        return {
          ...state,
          isData: true,
          assistanceTypeDetails:payload
        };
      case ASSISTANCE_TYPE_DETAILS_FAIL:
      return {
        ...state,
        isData: false,
        assistanceTypeDetails:null
      }; 
      case ASSISTANCE_TYPE_SUCCESS:
        return {
          ...state,
          isData: true,
          assistanceType:payload.assistance_typeData
        };
      case ASSISTANCE_TYPE_FAIL:
      return {
        ...state,
        isData: false,
        assistanceType:[]
      };
      case ASSISTANCE_TYPE_UPDATE_SUCCESS:
        return {
          ...state,
          isData: true,
        };
      case ASSISTANCE_TYPE_UPDATE_FAIL:
      return {
        ...state,
        isData: false,
      };
      case ASSISTANCE_TYPE_DELETE_SUCCESS:
        return {
          ...state,
          isData: true,
        };
      case ASSISTANCE_TYPE_DELETE_FAIL:
      return {
        ...state,
        isData: false,
      };
      case PROBLEM_TYPE_DELETE_SUCCESS:
        return {
          ...state,
          isData: true,
        };
      case PROBLEM_TYPE_DELETE_FAIL:
      return {
        ...state,
        isData: false,
      };
      case ASSISTANCE_FARE_UPDATE_SUCCESS:
        return {
          ...state,
          isData: true,
        };
      case ASSISTANCE_FARE_UPDATE_FAIL:
      return {
        ...state,
        isData: false,
      };
      case ASSISTANCE_FARE_DELETE_SUCCESS:
        return {
          ...state,
          isData: true,
        };
      case ASSISTANCE_FARE_DELETE_FAIL:
      return {
        ...state,
        isData: false,
      };
      case PROBLEM_TYPE_SUCCESS:
        return {
          ...state,
          isData: true,
          problemType:payload.problem_typeData
        };
      case PROBLEM_TYPE_FAIL:
      return {
        ...state,
        isData: false,
        problemType:[]
      };
      case ASSISTANCE_FARE_DETAILS_SUCCESS:
        return {
          ...state,
          isData: true,
          assistanceFareDetails:payload
        };
      case ASSISTANCE_FARE_DETAILS_FAIL:
      return {
        ...state,
        isData: false,
        assistanceFareDetails:null
      }; 
      case ASSISTANCE_FARE_SUCCESS:
        return {
          ...state,
          isData: true,
          assistanceFare:payload.assistance_fareData
        };
      case ASSISTANCE_FARE_FAIL:
      return {
        ...state,
        isData: false,
        assistanceFare:[]
      };
      default:
        return state;
    }
  }
  