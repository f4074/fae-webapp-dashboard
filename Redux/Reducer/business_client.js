import  {SET_DATA_LOCALSTORAGE,GET_DATA_LOCALSTORAGE} from "../../provider/LocalStorageProvider";
import {
    BUSINEESS_CLIENT_SUCCESS,
    BUSINEESS_CLIENT_FAIL,
    SET_MESSAGE,
  } from "../Action/types";
   
  
  const initialState =  { isData: false, clientData: null }
  export default function (state = initialState, action) {
    const { type, payload } = action;
    switch (type) {
      case BUSINEESS_CLIENT_SUCCESS:
        return {
          ...state,
          isData: true,
          clientData:payload
        };
      case BUSINEESS_CLIENT_FAIL:
        return {
          ...state,
          isData: false,
        };
      
      default:
        return state;
    }
  }
  