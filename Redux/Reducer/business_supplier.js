import  {SET_DATA_LOCALSTORAGE,GET_DATA_LOCALSTORAGE} from "../../provider/LocalStorageProvider";
import {
    BUSINEESS_SUPPLIER_SUCCESS,
    BUSINEESS_SUPPLIER_FAIL,
    SET_MESSAGE,
  } from "../Action/types";
   
  
  const initialState =  { isData: false, supplierData: null }
  export default function (state = initialState, action) {
    const { type, payload } = action;
    // console.log('type',type);
    // console.log('payload',payload);
    // console.log('state',state);
    switch (type) {
      case BUSINEESS_SUPPLIER_SUCCESS:
        return {
          ...state,
          isData: true,
          supplierData:payload
        };
      case BUSINEESS_SUPPLIER_FAIL:
        return {
          ...state,
          isData: false,
        };
      
      default:
        return state;
    }
  }
  