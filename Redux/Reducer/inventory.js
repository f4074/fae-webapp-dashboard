import  {SET_DATA_LOCALSTORAGE,GET_DATA_LOCALSTORAGE} from "../../provider/LocalStorageProvider";
import {
    INVENTORY_SUCCESS,
    INVENTORY_FAIL,
    INVENTORY_TRANSFER_VEHICLE_SUCCESS,
    INVENTORY_TRANSFER_VEHICLE_FAIL,
    INVENTORY_DETAILS_SUCCESS,
    INVENTORY_DETAILS_FAIL,
    INVENTORY_DELETE_SUCCESS,
    INVENTORY_DELETE_FAIL,
    INVENTORY_CREATE_SUCCESS,
    INVENTORY_CREATE_FAIL,
    INVENTORY_UPDATE_SUCCESS,
    INVENTORY_UPDATE_FAIL,
    INVENTORY_TIMELINE_SUCCESS,
    INVENTORY_TIMELINE_FAIL,
    SET_MESSAGE,
  } from "../Action/types";
   
  
  const initialState =  { isData: false, inventoryData: [], inventoryDetails: null, inventoryTimeline: [] ,EditRecord:{'isEditing':false,'type':"", 'data':null} }
  export default function (state = initialState, action) {
    const { type, payload } = action;
    
    switch (type) {
      case INVENTORY_SUCCESS:
        return {
          ...state,
          isData: true,
          inventoryData:payload
        };
      case INVENTORY_FAIL:
        return {
          ...state,
          isData: false,
        };
      case INVENTORY_DELETE_SUCCESS:
        return {
          ...state,
          isData: true,
          inventoryData:payload
        };
      case INVENTORY_DELETE_FAIL:
        return {
          ...state,
          isData: false,
        };
      case INVENTORY_CREATE_SUCCESS:
        return {
          ...state,
          isData: true,
          inventoryData:payload
        };
      case INVENTORY_CREATE_FAIL:
        return {
          ...state,
          isData: false,
        };
      case INVENTORY_UPDATE_SUCCESS:
        return {
          ...state,
          isData: true,
          inventoryData:payload
        };
      case INVENTORY_UPDATE_FAIL:
        return {
          ...state,
          isData: false,
        };
      case INVENTORY_TRANSFER_VEHICLE_SUCCESS:
        return {
          ...state,
          isData: true,
          inventoryData:payload
        };
      case INVENTORY_TRANSFER_VEHICLE_FAIL:
        return {
          ...state,
          isData: false,
        };
      case INVENTORY_DETAILS_SUCCESS:
        return {
          ...state,
          isData: true,
          inventoryDetails:payload  
        };
      case INVENTORY_DETAILS_FAIL:
        return {
          ...state,
          isData: false,
        };
      case INVENTORY_TIMELINE_SUCCESS:
        return {
          ...state,
          isData: true,
          inventoryTimeline:payload
        };
      case INVENTORY_TIMELINE_FAIL:
        return {
          ...state,
          isData: false,
        };  
      case "IS_MODAL_EDIT_INVENTORY":
        return {
          ...state,
          EditRecord: payload,
        };     
      default:
        return state;
    }
  }
  