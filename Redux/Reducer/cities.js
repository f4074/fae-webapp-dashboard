import  {SET_DATA_LOCALSTORAGE,GET_DATA_LOCALSTORAGE} from "../../provider/LocalStorageProvider";
import {
    CITY_SUCCESS,
    CITY_FAIL,    
    CITY_DELETE_SUCCESS,
    CITY_DELETE_FAIL,
    SET_MESSAGE,
  } from "../Action/types";
   
  
  const initialState =  { isData: false, CityData: null }
  export default function (state = initialState, action) {
    const { type, payload } = action;
    switch (type) {
      case CITY_SUCCESS:
        return {
          ...state,
          isData: true,
          CityData:payload
        };
      case CITY_FAIL:
        return {
          ...state,
          isData: false,
        };
      case CITY_DELETE_SUCCESS:
        return {
          ...state,
          isData: true,
        };
      case CITY_DELETE_FAIL:
        return {
          ...state,
          isData: false,
        };
      
      
      default:
        return state;
    }
  }
  