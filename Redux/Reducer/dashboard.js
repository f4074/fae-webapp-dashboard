import  {SET_DATA_LOCALSTORAGE,GET_DATA_LOCALSTORAGE} from "../../provider/LocalStorageProvider";
import {
    DASHBOARD_SUCCESS,
    DASHBOARD_FAIL,
    SET_MESSAGE,
  } from "../Action/types";
   
  
  const initialState =  { isData: false, dashboardData: null }
  export default function (state = initialState, action) {
    const { type, payload } = action;
    switch (type) {
      case DASHBOARD_SUCCESS:
        return {
          ...state,
          isData: true,
          dashboardData:payload
        };
      case DASHBOARD_FAIL:
        return {
          ...state,
          isData: false,
        };
      
      default:
        return state;
    }
  }
  