import React, { useState,useEffect } from "react";
import axios from "axios";
import { HANDLE_SUCCESS, HANDLE_ERROR, HTTP_SERVICE_CALL } from "../../provider/ApiProvider";
import  {SET_DATA_LOCALSTORAGE,REMEMBER_PASSWORD,GET_DATA_LOCALSTORAGE,REMOVE_ITEM_LOCALSTORAGE} from "../../provider/LocalStorageProvider"
import { BASE_URL,BASE_URL_CORE, POST,GET,PUT,DELETE, IS_LOADING,BUSINESS_CLIENT,BUSINESS_CLIENT_FILTER } from "../../provider/EndPoints";
import { BUSINEESS_CLIENT_FAIL, BUSINEESS_CLIENT_SUCCESS } from "../Action/types";


class BusinessClientService {
    async create(dataObj) { 
      let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + BUSINESS_CLIENT, POST, "", dataObj); 
      return  res.data;  
   
    }
    async updateClient(dataObj,id) { 
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + BUSINESS_CLIENT+'/'+id, PUT, "", dataObj); 
        return res.data;    
    
    }
    async deleteClient(id) {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + BUSINESS_CLIENT+'/'+id, DELETE, "");  
        return res.data;
    
    }
    async getAll() {  
         let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + BUSINESS_CLIENT, GET, ""); 
         return res.data;    
     
     }
     async getAllwithFilter(filterArray) { 
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + BUSINESS_CLIENT_FILTER, POST, "",filterArray);  
        return res.data;    
    
    }
     async getDetails(id) { 
        /*  // console.log('dataObj',dataObj);  
         let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + BUSINESS_CLIENT, GET, "");   
         // console.log('res',res);
         if (res.data.token) {     
             SET_DATA_LOCALSTORAGE("userData",JSON.stringify(res.data.data));
             SET_DATA_LOCALSTORAGE("userToken",res.data.token); 
         }
         return res.data;   */  
     
    }

}

export default new BusinessClientService();
