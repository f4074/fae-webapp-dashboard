import React, { useState,useEffect } from "react";
import axios from "axios";
import { HANDLE_SUCCESS, HANDLE_ERROR, HTTP_SERVICE_CALL } from "../../provider/ApiProvider";
import  {SET_DATA_LOCALSTORAGE,REMEMBER_PASSWORD,GET_DATA_LOCALSTORAGE,REMOVE_ITEM_LOCALSTORAGE} from "../../provider/LocalStorageProvider"
import { BASE_URL,BASE_URL_CORE, POST,GET,PUT,DELETE, IS_LOADING,CITIES } from "../../provider/EndPoints";

class CitiesService {
    async create(dataObj) {             
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + CITIES, POST, "", dataObj);      
        console.log('res',res);     
        return  res.data;  
   
    }
    async updateData(dataObj,id) { 
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + CITIES+'/'+id, PUT, "", dataObj);   
        // console.log('res',res);       
        return res.data;    
    
    }
    async deleteData(id) {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + CITIES+'/'+id, DELETE, "");   
        // console.log('res',res);       
        return res.data;
    
    }
    async getAll() { 
         // console.log('dataObj',dataObj);  
         let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + CITIES, GET, "");   
         // console.log('res',res);         
         return res.data;    
     
     }
     async getDetails(id) { 
         // console.log('dataObj',dataObj);  
         let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + CITIES+'/'+id, GET, "");  
         return res.data;    
     
    }

}

export default new CitiesService();
