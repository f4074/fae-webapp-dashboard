import React, { useState,useEffect } from "react";
import axios from "axios";
import { HANDLE_SUCCESS, HANDLE_ERROR, HTTP_SERVICE_CALL } from "../../provider/ApiProvider";
import  {SET_DATA_LOCALSTORAGE,REMEMBER_PASSWORD,GET_DATA_LOCALSTORAGE,REMOVE_ITEM_LOCALSTORAGE} from "../../provider/LocalStorageProvider"
import { BASE_URL,BASE_URL_CORE, POST,GET,PUT,DELETE, IS_LOADING,HUB,HUB_MULTIPLE_DELETE ,CITYHUB} from "../../provider/EndPoints";

class CitiesService {
    async create(dataObj) {             
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + HUB, POST, "", dataObj);      
        console.log('res',res);     
        return  res.data;  
   
    }
    async updateData(dataObj,id) { 
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + HUB+'/'+id, PUT, "", dataObj); 
        return res.data;    
    
    }
    async deleteData(id) {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + HUB+'/'+id, DELETE, ""); 
        return res.data;
    
    }
    async deleteMultipleData(ids) {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + HUB_MULTIPLE_DELETE, DELETE, "",ids); 
        return res.data;
    
    }
    
    async getAllCityHubs(cityid) {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + CITYHUB+'/'+cityid, GET, ""); 
        return res.data;    
    
    }
    async getAll() {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + HUB, GET, ""); 
        return res.data;    
     
     }
     async getDetails(id) {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + HUB+'/'+id, GET, "");  
        return res.data; 
    }

}

export default new CitiesService();
