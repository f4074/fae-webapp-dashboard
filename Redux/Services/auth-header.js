export default function authHeader() {
  const userToken = localStorage.getItem("userToken");

  if (userToken) {
    // For Spring Boot back-end
    return {  
      "Accept": "application/json",    
      "Content-Type": "application/json;charset=UTF-8",
      // "Authorization": "Bearer " + userToken
      "x-access-token": userToken
    };

    // for Node.js Express back-end
    // return { "x-access-token": userToken };
  } else {
    return {  
      "Accept": "application/json",    
      "Content-Type": "application/json;charset=UTF-8",      
    };
  }
}


  