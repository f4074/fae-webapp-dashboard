import React, { useState,useEffect } from "react";
import axios from "axios";
import { HANDLE_SUCCESS, HANDLE_ERROR, HTTP_SERVICE_CALL } from "../../provider/ApiProvider";
import  {SET_DATA_LOCALSTORAGE,REMEMBER_PASSWORD,GET_DATA_LOCALSTORAGE,REMOVE_ITEM_LOCALSTORAGE} from "../../provider/LocalStorageProvider"
import { BASE_URL,BASE_URL_CORE, POST,GET,PUT,DELETE, IS_LOADING,REFERALS,REFERALS_FILTER} from "../../provider/EndPoints";

class ReferalsService {
    async create(dataObj) {             
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + REFERALS, POST, "", dataObj);      
        console.log('res',res);     
        return  res.data;  
   
    }
    async updateData(dataObj,id) { 
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + REFERALS+'/'+id, PUT, "", dataObj); 
        return res.data;    
    
    }
    async deleteData(id) {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + REFERALS+'/'+id, DELETE, ""); 
        return res.data;
    
    }
    /* async deleteMultipleData(ids) {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + USERS_MULTIPLE_DELETE, DELETE, "",ids); 
        return res.data;
    
    } */
    async getAll(filterArray) {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + REFERALS_FILTER, POST, "",filterArray); 
        return res.data;    
     
    }

     
    async getDetails(id) {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + REFERALS+'/'+id, GET, "");  
        return res.data; 
    }
}

export default new ReferalsService();
