import React, { useState,useEffect } from "react";
import axios from "axios";
import { HANDLE_SUCCESS, HANDLE_ERROR, HTTP_SERVICE_CALL } from "../../provider/ApiProvider";
import  {SET_DATA_LOCALSTORAGE,REMEMBER_PASSWORD,GET_DATA_LOCALSTORAGE,REMOVE_ITEM_LOCALSTORAGE} from "../../provider/LocalStorageProvider"
import { BASE_URL,BASE_URL_CORE, POST,GET,PUT,DELETE, IS_LOADING,FAE_OPRATED_CITIES,CITIES } from "../../provider/EndPoints";


class FaeOperatedCitiesService {
    async create(dataObj) {               
      let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + FAE_OPRATED_CITIES, POST, "", dataObj);      
      console.log('res',res);     
      return  res.data;  
   
    }
    async updateData(dataObj,id) { 
        console.log('dataObj updateData',dataObj);  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + FAE_OPRATED_CITIES+'/'+id, PUT, "", dataObj);   
        // console.log('res',res);       
        return res.data;    
    
    }
    async deleteData(id) {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + FAE_OPRATED_CITIES+'/'+id, DELETE, "");   
        // console.log('res',res);       
        return res.data;
    
    }
    async getAll() { 
         // console.log('dataObj',dataObj);  
         let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + FAE_OPRATED_CITIES, GET, "");   
         // console.log('res',res);         
         return res.data;    
     
     }
     async getDetails(id) { 
        /*  // console.log('dataObj',dataObj);  
         let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + FAE_OPRATED_CITIES, GET, "");   
         // console.log('res',res);
         if (res.data.token) {     
             SET_DATA_LOCALSTORAGE("userData",JSON.stringify(res.data.data));
             SET_DATA_LOCALSTORAGE("userToken",res.data.token); 
         }
         return res.data;   */  
     
    }

}

export default new FaeOperatedCitiesService();
