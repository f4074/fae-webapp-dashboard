import React, { useState,useEffect } from "react";
import axios from "axios";
import { HANDLE_SUCCESS, HANDLE_ERROR, HTTP_SERVICE_CALL } from "../../provider/ApiProvider";
import  {SET_DATA_LOCALSTORAGE,REMEMBER_PASSWORD,GET_DATA_LOCALSTORAGE,REMOVE_ITEM_LOCALSTORAGE} from "../../provider/LocalStorageProvider"
import { BASE_URL,BASE_URL_CORE, POST,GET,PUT,DELETE, IS_LOADING,BOOKINGS, BOOKINGS_FILTER,BOOKINGS_VERIFICATION_CODE ,CHECK_AVAILABILITY} from "../../provider/EndPoints";


class BookingsService {
    async create(dataObj) { 
      let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + BOOKINGS, POST, "", dataObj);      
    //   console.log('res',res);     
      return  res.data;  
   
    }
    async checkvehicleAvailability(dataObj) { 
      let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + CHECK_AVAILABILITY, POST, "", dataObj);      
    //   console.log('res',res);     
      return  res.data; 
    }

    
    async booking_check_verification_code(dataObj) { 
      let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + BOOKINGS_VERIFICATION_CODE, POST, "", dataObj);      
    //   console.log('res',res);     
      return  res.data; 
    }
    async updateData(dataObj,id) { 
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + BOOKINGS+'/'+id, PUT, "", dataObj);   
        console.log('res',res);       
        return res.data;    
    
    }
    async deleteData(id) {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + BOOKINGS+'/'+id, DELETE, "");   
        // console.log('res',res);       
        return res.data;
    
    }
    async getAllWithFilter(filterArray) { 
       console.log('dataObj',BASE_URL_CORE + BOOKINGS_FILTER);  
      let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + BOOKINGS_FILTER, POST, "",filterArray);   
      // console.log('res',res);         
      return res.data;    
  
  }
    async getAll(filterArray) { 
          console.log('dataObj 1',BASE_URL_CORE + BOOKINGS);  
         let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + BOOKINGS, GET, "",filterArray);   
         // console.log('res',res);         
         return res.data;    
     
     }
     async getDetails(id) { 
         // console.log('dataObj',dataObj);  
         let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + BOOKINGS+'/'+id, GET, ""); 
         return res.data;    
     
    }

}

export default new BookingsService();
