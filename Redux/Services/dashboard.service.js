import React, { useState,useEffect } from "react";
import axios from "axios";
import { HANDLE_SUCCESS, HANDLE_ERROR, HTTP_SERVICE_CALL } from "../../provider/ApiProvider";
import  {SET_DATA_LOCALSTORAGE,REMEMBER_PASSWORD,GET_DATA_LOCALSTORAGE,REMOVE_ITEM_LOCALSTORAGE} from "../../provider/LocalStorageProvider"
import { BASE_URL,BASE_URL_CORE, POST,GET,PUT,DELETE, IS_LOADING,ADMIN_DASHBOARD } from "../../provider/EndPoints";

class DashboardService {
    async getAll(filterArray) { 
        // console.log('dataObj',dataObj);  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + ADMIN_DASHBOARD, POST, "",filterArray);   
        // console.log('res',res);         
        return res.data;   
    }

}

export default new DashboardService();
