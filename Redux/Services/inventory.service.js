import React, { useState,useEffect } from "react";
import axios from "axios";
import { HANDLE_SUCCESS, HANDLE_ERROR, HTTP_SERVICE_CALL } from "../../provider/ApiProvider";
import  {SET_DATA_LOCALSTORAGE,REMEMBER_PASSWORD,GET_DATA_LOCALSTORAGE,REMOVE_ITEM_LOCALSTORAGE} from "../../provider/LocalStorageProvider"
import { BASE_URL,BASE_URL_CORE, POST,GET,PUT,DELETE, IS_LOADING,INVENTORY,CITIES,INVENTORYFILTER,TRANSFER_VEHICLE_INVENTORY,INVENTORY_MULTIPLE_DELETE,INVENTORY_TIMELINE } from "../../provider/EndPoints";


class invetoryService {
    async create(dataObj) {  
      console.log('dataObj',dataObj);             
      let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + INVENTORY, POST, "", dataObj);      
      console.log('res',res);     
      return  res.data;  
   
    }
    async updateData(dataObj,id) { 
        console.log('dataObj updateData',dataObj);  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + INVENTORY+'/'+id, PUT, "", dataObj);   
        // console.log('res',res);       
        return res.data;    
    
    }
    async transferInventory(dataObj,id) { 
        // console.log('dataObj updateData',dataObj);  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + TRANSFER_VEHICLE_INVENTORY, POST, "", dataObj);   
        // console.log('res',res);       
        return res.data;    
    
    }
    async deleteData(id) {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + INVENTORY+'/'+id, DELETE, "");   
        // console.log('res',res);       
        return res.data;
    
    }
    async deleteMultiple(record) {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + INVENTORY_MULTIPLE_DELETE, DELETE, "",record);   
        // console.log('res',res);       
        return res.data;
    
    }
    async getAll() { 
         // console.log('dataObj',dataObj);  
        
         let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + INVENTORY, GET,"");   
         // console.log('res',res);         
         return res.data;    
     
    }
    async getAllFilterData(filterInventory) { 
        // console.log('dataObj',dataObj);         
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + INVENTORYFILTER, POST,"", filterInventory);   
        // console.log('res',res);         
        return res.data;    
    
   }
    async getDetails(id) { 
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + INVENTORY+'/'+id, GET, "");         
        return res.data;    
    
    }
    async getVehicleTimeLine(id) { 
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + INVENTORY_TIMELINE+'/'+id, GET, "");         
        return res.data; 
    }
    

}

export default new invetoryService();
