import React, { useState,useEffect } from "react";
import axios from "axios";
import { HANDLE_SUCCESS, HANDLE_ERROR, HTTP_SERVICE_CALL } from "../../provider/ApiProvider";
import  {SET_DATA_LOCALSTORAGE,REMEMBER_PASSWORD,GET_DATA_LOCALSTORAGE,REMOVE_ITEM_LOCALSTORAGE} from "../../provider/LocalStorageProvider"
import { BASE_URL,BASE_URL_AUTH, POST, IS_LOADING,ADMIN_SIGN_IN } from "../../provider/EndPoints";
import Loader from "../../Components/loader";
import Spinner from "../../Components/Spinner/Spinner";

class AuthService {
    async login(dataObj) { 
      // console.log('dataObj',dataObj);  
      let res = await HTTP_SERVICE_CALL(BASE_URL_AUTH + ADMIN_SIGN_IN, POST, "", dataObj);   
      console.log('res login',res);
      if (res.data.token) {     
        SET_DATA_LOCALSTORAGE("userData",JSON.stringify(res.data.data));
        SET_DATA_LOCALSTORAGE("userToken",res.data.token); 
      }
      return res.data;    
   
  }

  logout() {
    REMOVE_ITEM_LOCALSTORAGE("userData");
    REMOVE_ITEM_LOCALSTORAGE("userToken");
    return {"status":true,"message":"You have successfully logged out!"};
  }

  register(username, email, password) {
    // return axios.post(BASE_URL_AUTH + "signup", {
    //   username,
    //   email,
    //   password,
    // });
  }
}

export default new AuthService();
