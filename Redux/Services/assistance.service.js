import React, { useState,useEffect } from "react";
import axios from "axios";
import { HANDLE_SUCCESS, HANDLE_ERROR, HTTP_SERVICE_CALL } from "../../provider/ApiProvider";
import  {SET_DATA_LOCALSTORAGE,REMEMBER_PASSWORD,GET_DATA_LOCALSTORAGE,REMOVE_ITEM_LOCALSTORAGE} from "../../provider/LocalStorageProvider"
import { BASE_URL,BASE_URL_CORE, POST,GET,PUT,DELETE, IS_LOADING,ASSISTANCE,ASSISTANCE_TYPE,ASSISTANCE_FARE,ASSISTANCE_FILTER,PROBLEM_TYPE} from "../../provider/EndPoints";

class AssistanceService {
    async create(dataObj) {             
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + ASSISTANCE, POST, "", dataObj);      
        console.log('res',res);     
        return  res.data;  
   
    }
    async updateData(dataObj,id) { 
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + ASSISTANCE+'/'+id, PUT, "", dataObj); 
        return res.data;    
    
    }
    async deleteData(id) {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + ASSISTANCE+'/'+id, DELETE, ""); 
        return res.data;
    
    }
    async getAllAssistance() {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + ASSISTANCE, GET, ""); 
        return res.data; 
    }
    async getAllAssistanceFilter(filterArray) {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + ASSISTANCE_FILTER, POST, "",filterArray); 
        return res.data; 
    }
     
    async getDetails(id) {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + ASSISTANCE+'/'+id, GET, "");  
        return res.data; 
    }

    // ASSISTANCE TYPE

    async createAssistanceType(dataObj) {             
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + ASSISTANCE_TYPE, POST, "", dataObj);      
        console.log('res',res);     
        return  res.data;  
   
    }
    async updateAssistanceType(dataObj,id) { 
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + ASSISTANCE_TYPE+'/'+id, PUT, "", dataObj); 
        return res.data;    
    
    }
    async deleteAssistanceType(id) {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + ASSISTANCE_TYPE+'/'+id, DELETE, ""); 
        return res.data;
    
    }
   
    async getAllAssistanceType() {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + ASSISTANCE_TYPE, GET, ""); 
        return res.data;    
     
    }
     
    async getDetailsAssistanceType(id) {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + ASSISTANCE_TYPE+'/'+id, GET, "");  
        return res.data; 
    }

    // ASSISTANCE FARE

    async createAssistanceFare(dataObj) {             
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + ASSISTANCE_FARE, POST, "", dataObj);      
        console.log('res',res);     
        return  res.data;  
   
    }
    async updateAssistanceFare(dataObj,id) { 
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + ASSISTANCE_FARE+'/'+id, PUT, "", dataObj); 
        return res.data;    
    
    }
    async deleteAssistanceFare(id) {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + ASSISTANCE_FARE+'/'+id, DELETE, ""); 
        return res.data;
    
    }
    async getAllAssistanceFare() {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + ASSISTANCE_FARE, GET, ""); 
        return res.data;    
     
    }
     
    async getDetailsAssistanceFare(id) {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + ASSISTANCE_FARE+'/'+id, GET, "");  
        return res.data; 
    }
    // PROBLEM FARE

    async deleteProblemType(id) {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + PROBLEM_TYPE+'/'+id, DELETE, ""); 
        return res.data;
    
    }
   
}

export default new AssistanceService();
