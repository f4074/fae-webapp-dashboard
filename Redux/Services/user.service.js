import React, { useState,useEffect } from "react";
import axios from "axios";
import { HANDLE_SUCCESS, HANDLE_ERROR, HTTP_SERVICE_CALL } from "../../provider/ApiProvider";
import  {SET_DATA_LOCALSTORAGE,REMEMBER_PASSWORD,GET_DATA_LOCALSTORAGE,REMOVE_ITEM_LOCALSTORAGE} from "../../provider/LocalStorageProvider"
import { BASE_URL,BASE_URL_CORE,BASE_URL_AUTH, POST,GET,PUT,DELETE, IS_LOADING,USERS,USERS_FILTER,USERS_MULTIPLE_DELETE,USERS_KYC_DETAILS,USERS_KYC_LIST,USERS_KYC_REQUEST_LIST,DASH_USERS,DASH_USERS_FILTER,DASH_USERS_MULTIPLE_DELETE,RESEND_OTP,RESEND_OTP_USER,USERS_FORGOT_PASS,OTP_VERIFY,CHANGE_PASSWORD,CHECK_PASSWORD} from "../../provider/EndPoints";

class UsersService {
    async create(dataObj) {             
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + USERS, POST, "", dataObj);      
        console.log('res',res);     
        return  res.data;  
   
    }
    async Forgot_Password(dataObj) {             
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + USERS_FORGOT_PASS, POST, "", dataObj);      
        console.log('res',res);     
        return  res.data;  
   
    }

    async change_password(dataObj) {             
        let res = await HTTP_SERVICE_CALL(BASE_URL_AUTH + CHANGE_PASSWORD, POST, "", dataObj);      
        // console.log('res',res);     
        return  res.data;  
    }
    async checkOldPassword(dataObj) {             
        // let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + CHECK_PASSWORD, POST, "", dataObj);      
        // console.log('res',res);     
        // return  res.data;  
    }
    
    
    async updateData(dataObj,id) { 
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + USERS+'/'+id, PUT, "", dataObj); 
        return res.data;    
    
    }
    async deleteData(id) {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + USERS+'/'+id, DELETE, ""); 
        return res.data;    
    }
    async deleteMultipleData(ids) {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + USERS_MULTIPLE_DELETE, DELETE, "",ids); 
        return res.data;
    
    }
    async getAll() {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + USERS, GET, ""); 
        return res.data;    
     
    }
    async getAllFilter(filterArray) {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + USERS_FILTER, POST, "",filterArray); 
        return res.data;    
     
    }
     
    async getDetails(id) {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + USERS+'/'+id, GET, "");  
        return res.data; 
    }
    async resendOtp(data) {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + RESEND_OTP_USER, POST, "",data);  
        return res.data; 
    }
    async otp_verify(data) {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + OTP_VERIFY, POST, "",data);  
        return res.data; 
    }
    
    async getKycDetails(id) {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + USERS_KYC_DETAILS+'/'+id, GET, "");  
        return res.data; 
    }

    async getAllKycList(filterArray) {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + USERS_KYC_REQUEST_LIST, POST, "",filterArray);  
        return res.data; 
    }
    /* dash user start */
    async getAllDashUsers(filterArray) {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + DASH_USERS_FILTER, POST, "",filterArray); 
        return res.data;    
     
    }
    async createDashUser(dataObj) {             
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + DASH_USERS, POST, "", dataObj);  
        return  res.data;  
   
    }
    async updateDashUser(dataObj,id) { 
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + DASH_USERS+'/'+id, PUT, "", dataObj); 
        return res.data;    
    
    }
    async deleteDashUser(id) {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + DASH_USERS+'/'+id, DELETE, ""); 
        return res.data;
    
    }
    async deleteMultipleDashData(ids) {  
        let res = await HTTP_SERVICE_CALL(BASE_URL_CORE + DASH_USERS_MULTIPLE_DELETE, DELETE, "",ids); 
        return res.data;
    
    }
}

export default new UsersService();
