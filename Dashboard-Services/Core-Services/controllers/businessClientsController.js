require("dotenv").config();
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const BusinessClients = require("../models/business_clients");
const cities = require("../models/cities");
const ObjectId = require('mongodb').ObjectId;
const auth = require("../middleware/auth");
const bcrypt = require("bcryptjs");
const sendEmail = require("../common/sendMail")
const adminController = require("../controllers/adminController");


const getAllBusinessClients = async (req, res) => {  
    try {       
        const BusinessClientsData = await BusinessClients.find().sort({ created: -1 });
        
        if(BusinessClientsData){           
            // console.log('BusinessClientsData',BusinessClientsData) 
            BusinessClientsData.filter(function(val,i){
                return val.key = i;
            })         
        }

        return res.status(200).send({status: true, message: "Data Found", data: BusinessClientsData});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while getting all data of business clients.",data: []});
    }

}
const getAllBusinessClientsFilter = async (req, res) => {  
    try {       
        // filter start
        const filterArray =  req.body
        var matchArr =[];
        var hex =/[0-9A-Fa-f]{6}/g;
        if(filterArray != null){
            const city_id_filter = filterArray.city_id;
            const hub_id_filter = filterArray.hub_id;
            const searchKey = filterArray.searchKey;
            const start_date =filterArray.start_date;
            const end_date =filterArray.end_date;
            if(searchKey){        
                var searchKey_idmatch =  {$or:[
                    {"business_client":{'$regex':searchKey}},
                ]}
                matchArr.push(searchKey_idmatch);
            }
            if(city_id_filter){
                var city_idmatch =  {"city_id": (hex.test(city_id_filter))? ObjectId(city_id_filter) : city_id_filter}
                matchArr.push(city_idmatch);
            }
        } 

        var matchfilter ={};
        if(matchArr.length >0){
            matchfilter = {$and: matchArr} 
        } 
        // filter end 
        const BusinessClientsData = await BusinessClients.find(matchfilter).sort({ created: -1 });
        
        if(BusinessClientsData){           
            // console.log('BusinessClientsData',BusinessClientsData) 
            BusinessClientsData.filter(function(val,i){
                return val.key = i;
            })         
        }
        let citiesData = await cities.find().sort({ name: 1 });
        if(citiesData){
            citiesData.filter(function(val,i){
                return val.key = i;
            })
        }
        return res.status(200).send({status: true, message: "Data Found", data: BusinessClientsData,citiesData:citiesData});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while getting all data of business clients.",data: []});
    }

}

const getDetailsBusinessClients = async (req, res) => {        
    try {  
        const id = req.params.id;      
        const BusinessClientsDetails = await BusinessClients.findById(id);
        console.log('BusinessClientsDetails',BusinessClientsDetails)
        return res.status(200).send({status: true, message: "Data Found", data: BusinessClientsDetails});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while getting details of business clients.",data: []});
    }

}
const createBusinessClients = async (req, res) => {        
    try { 
        if (!req.body) {
            return res.status(400).send({
                message: "Data to update can not be empty!"
            });
        } 
        /* const businessClients = new BusinessClients({
            name: req.body.name,
            city: req.body.city,
            state: req.body.state,
            country: req.body.country,
            pincode: req.body.pincode,
            lat: req.body.lat,
            lng: req.body.lng,
            status: req.body.status,
            created: req.body.created,
            updated: req.body.updated
        }); */
        // console.log('req.body',req.body);
        
        const businessClients = new BusinessClients(req.body);
             
        const BusinessClientsData = await businessClients.save();
        console.log('BusinessClientsData',BusinessClientsData)
        if (BusinessClientsData) { 
            const BusinessClientsDataNew = await BusinessClients.findById(BusinessClientsData._id);
            const BusinessClientsDataAll = await BusinessClients.find().sort({ created: -1 });
        
            if(BusinessClientsDataAll){           
                // console.log('BusinessClientsData',BusinessClientsData) 
                BusinessClientsDataAll.filter(function(val,i){
                    return val.key = i;
                })         
            }
            return res.status(201).send({status: true, message: "Business Clients successfully created!", data: BusinessClientsDataNew,BusinessClientsDataAll:BusinessClientsDataAll});
        }
        return res.status(404).send({status: false, message: "Some error occurred while creating the business Clients!",data: []});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while creating business Clients.",data: []});
    }

}
const updateBusinessClients = async (req, res) => {        
    try {      
        if (!req.body) {
            return res.status(400).send({
                message: "Data to update can not be empty!"
            });
        } 
        const id = req.params.id;  
        const BusinessClientsData = await BusinessClients.findByIdAndUpdate(id, req.body, { useFindAndModify: false });        
        if (BusinessClientsData) {
            const BusinessClientsdetails = await BusinessClients.findById(id);           
          return res.status(200).send({status: true, message: "Business Clients successfully updated!", data: BusinessClientsdetails});
        }
        return res.status(404).send({status: false, message: "Cannot update business Clients! with id=${id}. Maybe business supplier! was not found!",data: []});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while updating business clients.",data: []});
    }

}
const deleteBusinessClients = async (req, res) => {        
    try {       
        const id = req.params.id;
        const BusinessClientsData = await BusinessClients.findByIdAndRemove(id);
        if (BusinessClientsData) {         
            return res.status(200).send({status: true, message: "Business clients successfully deleted!", data: BusinessClientsData});
          }
          return res.status(404).send({status: false, message: "Cannot delete business clients! with id=${id}. Maybe business supplier! was not found!",data: []});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while removing all business clients.",data: []});
    }

}
const deleteMultipleBusinessClients = async (req, res) => {        
    try { 
        const deleteJson = req.body;
        const data = await BusinessClients.deleteMany(deleteJson);
        if (data) {         
            return res.status(200).send({status: true, message: `${data.deletedCount} Business clients were deleted successfully!`, data: []});
          }
          return res.status(404).send({status: false, message: "Some error occurred while removing all business clients",data: []});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while removing all business supplier.",data: []});
    }
}

module.exports = { getAllBusinessClients,getAllBusinessClientsFilter,getDetailsBusinessClients,createBusinessClients,updateBusinessClients,deleteBusinessClients,deleteMultipleBusinessClients };