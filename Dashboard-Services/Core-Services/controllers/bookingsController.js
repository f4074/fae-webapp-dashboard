require("dotenv").config();
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const bookings = require("../models/bookings");
const cities = require("../models/cities");
const {User} = require("../models/user")
const admin = require("../models/admin")
const vehicles = require("../models/vehicles")
const spares = require("../models/spares");
const iot_devices = require("../models/iot_devices");
const business_clients = require("../models/business_clients")
const business_suppliers = require("../models/business_suppliers")
const inventory = require("../models/inventory");
const inventory_history = require("../models/inventory_history");
const ObjectId = require('mongodb').ObjectId;
const auth = require("../middleware/auth");
const sendEmail = require("../common/sendMail");
/* generate otp */
const otp = Math.floor(100000 + Math.random() * 900000);
const genotp = otp.toString()

var multer  = require('multer');
const { relativeTimeRounding } = require("moment");
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads')
    },
    filename: function (req, file, cb) {
        const fileName = file.originalname.toLowerCase().split(' ').join('-');
        cb(null, Date.now() + '-'+fileName)
    }
})
var upload = multer({ storage: storage })
  

const getAllBookings = async (req, res) => {  
    try { 
        const cityList = await cities.find().sort({ name: 1 });
        var matchfilter ={};  
        const bookingsData = await allBookings(matchfilter);
        return res.status(200).send({status: true, message: "Data Found", data: {'data':bookingsData,'cityList':cityList}});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while getting all data of bookings.",data: []});
    }
}
const getAllBookingsFilters = async (req, res) => {     

    const filterData = req.body;
    // const searchKey =filterData.searchKey;
    const searchKey =filterData.searchKey;
    const searchvehicle_regNo =filterData.vehicle_regNo;
    const city_id =filterData.city_id;
    const hub_id =filterData.hub_id;
    const start_date =filterData.start_date;
    const end_date =filterData.end_date;

    var matchArr = [{'status':true}];    
    // var matchArr = [];    
   
    var hex = /[0-9A-Fa-f]{6}/g;
    if(start_date){
        var startDateSet = start_date+"T00:00:00.000+00:00";
        // var start_date_match =  {from_date: { $gte:  new Date(startDateSet) }}
        var start_date_match =  {from_date: { $gte:  new Date(start_date) }}
        // var start_date_match =  {created:{ $gte:new Date(start_date)}}
        matchArr.push(start_date_match);
    }
    if(end_date){
        var endDateSet = end_date+"T00:00:00.000+00:00";
        // var end_date_match =  {to_date: { $lte:  new Date(endDateSet) }}
        var end_date_match =  {to_date: { $lte:  new Date(end_date) }}
        // var end_date_match =  {created: { $lte:  new Date(end_date) }}
        matchArr.push(end_date_match);
    }
    if(searchKey){        
        // var searchKey_idmatch =  {
        //     "user_id.phone":searchKey
        // }
        var searchKey_idmatch =  {$or:[{"user_id.phone":searchKey},{"user_id.email":searchKey}]}
        matchArr.push(searchKey_idmatch);        
    }  
    if(searchvehicle_regNo){        
        var searchvehicle_regNomatch =  {
            "vehicle_id.vehicle_regNo":searchvehicle_regNo
        }
        matchArr.push(searchvehicle_regNomatch);        
    } 
    
    if(city_id){
        var city_idmatch =  {"inventory_id.city_id": (hex.test(city_id))? ObjectId(city_id) : city_id}
        matchArr.push(city_idmatch);
    }
    if(hub_id){
        var hub_idmatch =  {"inventory_id.hub_id":  (hex.test(hub_id))? ObjectId(hub_id) : hub_id}
        matchArr.push(hub_idmatch);
    }
    
    var matchfilter ={};
    if(matchArr.length >0){
        matchfilter = {$and: matchArr} 
    }
    try { 

        const cityList = await cities.find().sort({ name: 1 }); 
        const userList = await User.find().sort({ name: 1 }); 
        const vehiclesList = await vehicles.find().sort({ vehicle_name : 1 }); 
        const business_clientsList = await business_clients.find().sort({ business_client: 1 }); 
        const business_SupplierList = await business_suppliers.find().sort({ supplier_name: 1 }); 
        
        const bookingsData = await allBookings(matchfilter);

        /* get all spares data */
        var matchArrSpares = [{ type: { $eq: "spares" }}]; 
        const sparesInventoryDataGet = await inventory.aggregate([
            {
                $sort:{created: -1}
            },           
            { $lookup:
                {
                from: "spares",
                localField: "spares_id",
                foreignField: "_id",
                as: "sparesDetails"
                }
            },
            { 
                $unwind: '$sparesDetails'
            },
            {
                $addFields: {
                    "part_number": "$sparesDetails.part_number",
                    "quantity": "$sparesDetails.quantity",
                    "model": "$sparesDetails.model",
                    "supplier_description": "$sparesDetails.supplier_description",
                    "mrp": "$sparesDetails.mrp",
                    "spares_id": "$sparesDetails._id",
                    "dealer_sale_price": "$sparesDetails.dealer_sale_price",
                }
            },
            {
                $project: {
                    _id: 1,
                    city_id:1,
                    hub_id:1,
                    to_city_id:1,
                    to_hub_id:1,
                    spares_id:1,
                    part_number:1,
                    quantity: 1,
                    model: 1,
                    supplier_description: 1,
                    mrp: 1,
                    dealer_sale_price: 1,
                    status: 1,
                    assignedTo: 1,
                    type: 1,
                    updated: 1,
                    created: 1
                }
            },
            {
                $match:{$and:matchArrSpares}
            },
        ])

         /* get all iotdevices data */
         var matchArrSpares = [{ type: { $eq: "iot_devices" }}]; 
         const iotdevicesInventoryData = await inventory.aggregate([
             {
                 $sort:{created: -1}
             },           
             { $lookup:
                 {
                 from: "iot_devices",
                 localField: "iot_device_id",
                 foreignField: "_id",
                 as: "iot_devicesDetails"
                 }
             },
             { 
                 $unwind: '$iot_devicesDetails'
             },
             
             {
                 $match:{$and:matchArrSpares}
             },
         ])

        //  let UpdateAllVehiclesSparesIotdevices = await checkAndUpdateAllVehiclesSparesIotdevices();

        return res.status(200).send({status: true, message: "Data Found",data: {'data':bookingsData,'cityList':cityList,'matchArr':matchArr,'userList':userList,'vehiclesList':vehiclesList,'business_clientsList':business_clientsList,'sparesInventoryData':sparesInventoryDataGet,'iotdevicesInventoryData':iotdevicesInventoryData,'business_SupplierList':business_SupplierList}});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while getting all data of bookings.",data: []});
    }
}
const getBookingDetails = async (req, res) => { 
    const id = ObjectId(req.params.id); 
    try {       
        const bookingData = await bookings.findOne(id).populate('user_id').populate('inventory_id');
        return res.status(200).send({status: true, message: "Data Found", data: bookingData});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while getting all data of business clients.",data: []});
    }

}


const file_upload = async (req, res) => {
    try { 
        if (!req.body) {
            return res.status(400).send({
                message: "Data to update can not be empty!"
            });
        }       
        return res.status(200).send({status: true, message: "file uploded", data: []});
       
        
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while creating booking.",data: []});
    }

}

const createBooking = async (req, res) => {
    try { 
        if (!req.body) {
            return res.status(400).send({
                message: "Data to update can not be empty!"
            });
        } 
        let user_id = req.body.user_id;
        let Uploaded_files = req.files;
        let fileNameSet = [];
        if(Uploaded_files){
            // fileNameSet =Uploaded_files;
            fileNameSet= Uploaded_files.map((val,i)=>{
                return val.filename;
            }).join(",")           
        }
        req.body.images =fileNameSet;
        req.body.imageArr =Uploaded_files;
        delete req.body["_id"];
        delete req.body["booking_id"];

        req.body.created =new Date().toISOString();
        req.body.updated =new Date().toISOString();


        if(req.body.spares){
            let sparesAvailability = await checkSparesAvailability('spares',req.body.spares,null,req.body.city_id,req.body.hub_id,req.body.business_supplier_id);
             // spares
            if(sparesAvailability == null){
                return res.status(200).send({status: false, message: "Invailid Spares!",data: []});
            }
            if(sparesAvailability == 0){
                return res.status(200).send({status: false, message: "Spares Not Available!",data: []});
            }

        }
        let iot_devicesAvailability = await checkSparesAvailability('iot_devices',req.body.iot_devices,null,req.body.city_id,req.body.hub_id,req.body.business_supplier_id);       
        // iot_devicesAvailability
        if(iot_devicesAvailability == null){
            return res.status(200).send({status: false, message: "Invailid IOT Device!",data: []});
        }
        if(iot_devicesAvailability == 0){
            return res.status(200).send({status: false, message: "IOT Device Not Available!",data: []});
        }
        let battery_1Availability = await checkSparesAvailability('battery_1',req.body.battery_1,null,req.body.city_id,req.body.hub_id,req.body.business_supplier_id);

        // battery_1Availability
        if(battery_1Availability == null){
            return res.status(200).send({status: false, message: "Invailid Battery 1!",data: []});
        }
        if(battery_1Availability == 0){
            return res.status(200).send({status: false, message: "Battery 1 Not Available!",data: []});
        }

        if(req.body.battery_2){
            let battery_2Availability = await checkSparesAvailability('battery_2',req.body.battery_2,null,req.body.city_id,req.body.hub_id,req.body.business_supplier_id);
             // battery_1Availability
            if(battery_2Availability == null){
                return res.status(200).send({status: false, message: "Invailid Battery 2!",data: []});
            }
            if(battery_2Availability == 0){
                return res.status(200).send({status: false, message: "Battery 2 Not Available!",data: []});
            }
        }
        
        let charger_1Availability = await checkSparesAvailability('charger_1',req.body.charger_1,null,req.body.city_id,req.body.hub_id,req.body.business_supplier_id);
        // battery_1Availability
        if(charger_1Availability == null){
            return res.status(200).send({status: false, message: "Invailid Charger 1!",data: []});
        }

        if(charger_1Availability == 0){
            return res.status(200).send({status: false, message: "Charger 1 Not Available!",data: []});
        }

        if(req.body.charger_2){
            let charger_2Availability = await checkSparesAvailability('charger_2',req.body.charger_2,null,req.body.city_id,req.body.hub_id,req.body.business_supplier_id);
             // battery_1Availability
            if(charger_2Availability == null){
                return res.status(200).send({status: false, message: "Invailid Charger 2!",data: []});
            }
            if(charger_2Availability == 0){
                return res.status(200).send({status: false, message: "Charger 2 Not Available!",data: []});
            }
        }



        const bookingsSendData = new bookings(req.body); 
        const bookingsData = await bookingsSendData.save();
        if (bookingsData) { 
            /* update otp for booking */
            const user = await User.findOne({_id:bookingsData.user_id});
            if(user){
                const otp = Math.floor(100000 + Math.random() * 900000);
                const genotp = otp.toString()
                const wherecon = {_id:bookingsData.user_id }
                const updvalues = { $set: { otp: genotp } };
                const otpupd = await  User.updateOne(wherecon, updvalues);

                // sendgrid
                sendEmail({
                    sendTo: user.email,
                    subject: 'FaeBikes - Booking OTP Verification',
                    template: `<!DOCTYPE html>
                    <html>
                        <body>
                            <p>Your Booking Verification Code for FaeBikes is ${genotp}</p>
                        </body>
                    </html>`
                })
            }
            /* update otp for booking */            

            return res.status(200).send({status: true, message: "Verified, we will send verification code in your registred phone number/email", data: bookingsData,user:user,genotp:genotp});
        }
        return res.status(404).send({status: false, message: "Some error occurred while creating the booking!",data: []});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while creating booking.",data: []});
    }

}

const updateBooking = async (req, res) => {        
    try {   
        if (!req.body) {
            return res.status(400).send({
                message: "Data to update can not be empty!"
            });
        } 
        
        let Uploaded_files = req.files;
        let fileNameSet = '';
        if(Uploaded_files){
            // fileNameSet =Uploaded_files;
            fileNameSet= Uploaded_files.map((val,i)=>{
                    return val.filename;
            }).join(",")           
        }
        req.body.images =fileNameSet;
        req.body.imageArr =Uploaded_files;    
        const id = req.params.id;  
        // console.log('req.param',id)
        const inventory_id_get = req.body.inventory_id;
        const status_inventory = req.body.status_inventory;

        const bookingsDetails = await bookings.findById(id);
        const getOldImgArr = bookingsDetails.imageArr;
        const inventory_id_old = bookingsDetails.inventory_id;

        var oldInventoryId="";
        if(inventory_id_old !=inventory_id_get){
            oldInventoryId=inventory_id_old;
        }

        const old_images = req.body.old_images;
       
        getOldImgArr.map((val,i)=>{
            if(old_images!=undefined &&old_images.length>0){
                if(old_images.includes(val.filename) == true){
                    Uploaded_files.push(val);
                }
            }else{
                Uploaded_files.push(val);
            }                       
        });

        const sendData ={
            booking_id: req.body.booking_id,
            _id: req.body._id,
            user_id: req.body.user_id,
            spares: req.body.spares,
            battery_1: req.body.battery_1,
            battery_2: req.body.battery_2,
            charger_1: req.body.charger_1,
            charger_2: req.body.charger_2,
            iot_devices: req.body.iot_devices,
            business_supplier_id: req.body.business_supplier_id,
            from_date:req.body.from_date,
            to_date:req.body.to_date,
            images: fileNameSet,
            imageArr:Uploaded_files,
            city_id: req.body.city_id,
            hub_id: req.body.hub_id,
            inventory_id: req.body.inventory_id,
            comment: req.body.comment,
            // created: new Date().toISOString(),
            updated: new Date().toISOString(),
            // status_inventory: req.body.status_inventory,
            vehicle_name: req.body.vehicle_name,
        }
        

        if(req.body.spares){
            let sparesAvailability = await checkSparesAvailability('spares',req.body.spares,req.body._id,req.body.city_id,req.body.hub_id,req.body.business_supplier_id);
             // spares
            if(sparesAvailability == null){
                return res.status(200).send({status: false, message: "Invailid Spares!",data: []});
            }
            if(sparesAvailability == 0){
                return res.status(200).send({status: false, message: "Spares Not Available!",data: []});
            }

        }
        let iot_devicesAvailability = await checkSparesAvailability('iot_devices',req.body.iot_devices,req.body._id,req.body.city_id,req.body.hub_id,req.body.business_supplier_id);       
        // iot_devicesAvailability
        if(iot_devicesAvailability == null){
            return res.status(200).send({status: false, message: "Invailid IOT Device!",data: []});
        }
        if(iot_devicesAvailability == 0){
            return res.status(200).send({status: false, message: "IOT Device Not Available!",data: []});
        }
        let battery_1Availability = await checkSparesAvailability('battery_1',req.body.battery_1,req.body._id,req.body.city_id,req.body.hub_id,req.body.business_supplier_id);

        // battery_1Availability
        if(battery_1Availability == null){
            return res.status(200).send({status: false, message: "Invailid Battery 1!",data: []});
        }
        if(battery_1Availability == 0){
            return res.status(200).send({status: false, message: "Battery 1 Not Available!",data: []});
        }

        if(req.body.battery_2){
            let battery_2Availability = await checkSparesAvailability('battery_2',req.body.battery_2,req.body._id,req.body.city_id,req.body.hub_id,req.body.business_supplier_id);
             // battery_1Availability
            if(battery_2Availability == null){
                return res.status(200).send({status: false, message: "Invailid Battery 2!",data: []});
            }
            if(battery_2Availability == 0){
                return res.status(200).send({status: false, message: "Battery 2 Not Available!",data: []});
            }
        }
        
        let charger_1Availability = await checkSparesAvailability('charger_1',req.body.charger_1,req.body._id,req.body.city_id,req.body.hub_id,req.body.business_supplier_id);
        // battery_1Availability
        if(charger_1Availability == null){
            return res.status(200).send({status: false, message: "Invailid Charger 1!",data: []});
        }

        if(charger_1Availability == 0){
            return res.status(200).send({status: false, message: "Charger 1 Not Available!",data: []});
        }

        if(req.body.charger_2){
            let charger_2Availability = await checkSparesAvailability('charger_2',req.body.charger_2,req.body._id,req.body.city_id,req.body.hub_id,req.body.business_supplier_id);
             // battery_1Availability
            if(charger_2Availability == null){
                return res.status(200).send({status: false, message: "Invailid Charger 2!",data: []});
            }
            if(charger_2Availability == 0){
                return res.status(200).send({status: false, message: "Charger 2 Not Available!",data: []});
            }
        }

        
        const bookingsData = await bookings.findByIdAndUpdate(id, sendData);

        if (bookingsData) {
            /* update otp for booking */
            const user = await User.findOne({_id:bookingsData.user_id});
            if(user){
                const otp = Math.floor(100000 + Math.random() * 900000);
                const genotp = otp.toString()
                const wherecon = {_id:bookingsData.user_id }
                const updvalues = { $set: { otp: genotp } };
                const otpupd = await  User.updateOne(wherecon, updvalues);

                // sendgrid
                sendEmail({
                    sendTo: user.email,
                    subject: 'FaeBikes - Booking OTP Verification',
                    template: `<!DOCTYPE html>
                    <html>
                        <body>
                            <p>Your Booking Verification Code for FaeBikes is ${genotp}</p>
                        </body>
                    </html>`
                })
            }
            /* update otp for booking */            

            return res.status(200).send({status: true, message: "Verified, we will send verification code in your registred phone number/email", data: bookingsData,user:user,genotp:genotp,oldInventoryId:oldInventoryId});

            // const updateInventory1 = await inventory.findByIdAndUpdate(inventory_id_get,{"status":status_inventory});
            // // get all booking list
            // var matchfilter ={};  
            // const allBookingsData = await allBookings(matchfilter);
            // return res.status(200).send({status: true, message: "Booking successfully updated!", data: bookingsData,allBookingsData:allBookingsData});
        }
        return res.status(404).send({status: false, message: "Cannot update Booking! with id=${id}. Maybe Booking! was not found!",data: []});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while updating Booking.",data: []});
    }

}

const check_availability = async (req, res) => {
    try {        
        const { type,value,booking_id,business_supplier_id ,city_id,hub_id} = req.body;
       
        const booking_business_supplier_id = business_supplier_id;
        const booking_city_id = city_id;
        const booking_hub_id = hub_id;


        var checkStatus = 0;
        var getId = 0;
        var message = '';
        if(type == 'spares'){ 
            let bookingFilter=[{'status':true},{ "spares": value}];  
            var matchArrSparesFilter = [{ part_type:"spares"},{part_number:value}]

            if(booking_business_supplier_id){
                bookingFilter.push({'business_supplier_id':ObjectId(booking_business_supplier_id)});
                matchArrSparesFilter.push({'business_supplier':booking_business_supplier_id});

            }
            if(booking_city_id){
                bookingFilter.push({'city_id':ObjectId(booking_city_id)});
                matchArrSparesFilter.push({'city':booking_city_id});

            }
            if(booking_hub_id){
                bookingFilter.push({'hub_id':ObjectId(booking_hub_id)});
                matchArrSparesFilter.push({'hub':booking_hub_id});
            }
            var matchArrSpares = {'$and':matchArrSparesFilter};
            const checkSpare = await spares.findOne(matchArrSpares);
            if(!checkSpare){
                checkStatus = 'invalid';
                message="Invailid data";                

            }else{
                /* get all spares data */                                 
               
                if(booking_id){
                    bookingFilter.push({'_id':{"$ne":ObjectId(booking_id)}});
                    var checkSpareBooking = await bookings.aggregate([
                        { 
                            $lookup:
                            {
                                from: "inventories",
                                localField: "inventory_id",
                                foreignField: "_id",
                                // pipeline: pipeline,
                                as: "inventory_details"
                            }
                        },
                        { 
                            $unwind: '$inventory_details'
                        },
                        {
                            $match:{"$and":bookingFilter}
                        },
                    ])

                    // var checkSpareBooking = await bookings.find({"$and":[{spares:value},{'status':true},{'_id':{"$ne":ObjectId(booking_id)}}]}).populate("invetory_id");
                    
                }else{

                    var checkSpareBooking = await bookings.aggregate([
                        { $lookup:
                            {
                                from: "inventories",
                                localField: "inventory_id",
                                foreignField: "_id",
                                // pipeline: pipeline,
                                as: "inventory_details"
                            }
                        },
                        { 
                            $unwind: '$inventory_details'
                        },
                        {
                            $match:{"$and":bookingFilter}
                        },
                    ])
                    // var checkSpareBooking = await bookings.find({"$and":[{spares:value},{'status':true}]}).populate("invetory_id");                   
                }
                if(checkSpareBooking.length>0 && checkSpare.quantity <= checkSpareBooking.length){
                    checkStatus = 'not_available';
                    message="Not Available";

                }else{
                    getId = checkSpare._id;
                    checkStatus = 'available';
                    message="Available";

                }

            }
            
        }else if(type == 'iot_devices'){ 
            
            let bookingFilter=[{'status':true},{ iot_devices: value}];
            let matchArriot_devicesFilter=[{device_serial_number:value}];
           
            if(booking_business_supplier_id){
                bookingFilter.push({'business_supplier_id':ObjectId(booking_business_supplier_id)});
                matchArriot_devicesFilter.push({'business_supplier':booking_business_supplier_id});

            }
            if(booking_city_id){
                bookingFilter.push({'city_id':ObjectId(booking_city_id)});
                matchArriot_devicesFilter.push({'city':booking_city_id});

            }
            if(booking_hub_id){
                bookingFilter.push({'hub_id':ObjectId(booking_hub_id)});
                matchArriot_devicesFilter.push({'hub':booking_hub_id});
            }
            var matchArriot_devices = {'$and':matchArriot_devicesFilter};

            const checkIotDevices = await iot_devices.find(matchArriot_devices);
            if(checkIotDevices.length<=0){
                checkStatus = 'invalid';
                message="Invailid data";

            }else{
                if(booking_id){
                    bookingFilter.push({'_id':{"$ne":ObjectId(booking_id)}});
                    
                    var checkIotDevicesBooking = await bookings.aggregate([
                        { $lookup:
                            {
                                from: "inventories",
                                localField: "inventory_id",
                                foreignField: "_id",
                                // pipeline: pipeline,
                                as: "inventory_details"
                            }
                        },
                        { 
                            $unwind: '$inventory_details'
                        },
                        {
                            $match:{"$and":bookingFilter}
                        },
                    ])
                    
                }else{

                    var checkIotDevicesBooking = await bookings.aggregate([
                        { $lookup:
                            {
                                from: "inventories",
                                localField: "inventory_id",
                                foreignField: "_id",
                                // pipeline: pipeline,
                                as: "inventory_details"
                            }
                        },
                        { 
                            $unwind: '$inventory_details'
                        },
                        {
                            $match:{"$and":bookingFilter}
                        },
                    ])
                 
                }
                /* get all IotDevices data */                                 
               

                if(checkIotDevicesBooking.length>0){
                    checkStatus = 'not_available';
                    message="Not Available";
                }else{
                    getId = checkIotDevices[0]._id;
                    checkStatus = 'available';
                     message="Available";
                }

            }

        }else if(type == 'battery_1' || type == 'battery_2'){

            let matchArrFilter =[{ 'part_type': { $eq: "battery" }},{'part_number':value}];
            if(booking_business_supplier_id){
                matchArrFilter.push({'business_supplier':booking_business_supplier_id});
            }
            if(booking_city_id){
                matchArrFilter.push({'city':booking_city_id});
            }
            if(booking_hub_id){
                matchArrFilter.push({'hub':booking_hub_id});
            }
            let matchArrSpares = {'$and':matchArrFilter};
           
            let checkSpare = await spares.findOne(matchArrSpares);
            
            if(!checkSpare){
                checkStatus = 'invalid';
                message="Invailid data";

            }else{
                /* get all spares data */                                 
               
                let bookingFilter=[{'status':true}];

                // if(type == "battery_1"){
                //     bookingFilter.push({'battery_1':value});

                // }else  if(type == 'battery_2'){
                //     bookingFilter.push({'battery_2':value});
                // }

                if(booking_business_supplier_id){
                    bookingFilter.push({'business_supplier_id':ObjectId(booking_business_supplier_id)});

                }
                if(booking_city_id){
                    bookingFilter.push({'city_id':ObjectId(booking_city_id)});

                }

                if(booking_hub_id){
                    bookingFilter.push({'hub_id':ObjectId(booking_hub_id)});

                }

                if(type == "battery_1" || type == 'battery_2'){
                    bookingFilter.push({$or:[{'battery_1':value},{'battery_2':value}]});

                }

                if(booking_id){
                    bookingFilter.push({'_id':{"$ne":ObjectId(booking_id)}});                    
                }

                var checkSpareBooking = await bookings.aggregate([
                    { $lookup:
                        {
                            from: "inventories",
                            localField: "inventory_id",
                            foreignField: "_id",
                            // pipeline: pipeline,
                            as: "inventory_details"
                        }
                    },
                    { 
                        $unwind: '$inventory_details'
                    },
                    {
                        $match:{"$and":bookingFilter}
                    },
                ])
                
                if(checkSpareBooking.length>0 && checkSpare.quantity <= checkSpareBooking.length){
                    checkStatus = 'not_available';
                    message="Not Available";
                }else{
                    getId = checkSpare._id;
                    checkStatus = 'available';
                     message="Available";
                }

            }
            
        }else if(type == 'charger_1' || type == 'charger_2'){

            var matchArrFilter =[{ part_type: { $eq: "charger" }},{part_number:value}];
            if(booking_business_supplier_id){
                matchArrFilter.push({'business_supplier':booking_business_supplier_id});
            }
            if(booking_city_id){
                matchArrFilter.push({'city':booking_city_id});
            }
            if(booking_hub_id){
                matchArrFilter.push({'hub':booking_hub_id});
            }

            var matchArrSpares = {'$and':matchArrFilter};
            
            let checkSpare = await spares.findOne(matchArrSpares);
            if(!checkSpare){
                checkStatus = 'invalid';
                message="Invailid data";

            }else{
                /* get all spares data */  
                
                let bookingFilter=[{'status':true}];               
               
                if(booking_business_supplier_id){
                    bookingFilter.push({'business_supplier_id':ObjectId(booking_business_supplier_id)});

                }
                if(booking_city_id){
                    bookingFilter.push({'city_id':ObjectId(booking_city_id)});

                }
                if(booking_hub_id){
                    bookingFilter.push({'hub_id':ObjectId(booking_hub_id)});
                }

                if(booking_id){
                    bookingFilter.push({'_id':{"$ne":ObjectId(booking_id)}});                    
                }

                if(type == "charger_1" || type == 'charger_2'){
                    bookingFilter.push({$or:[{'charger_1':value},{'charger_2':value}]});

                }
                var checkSpareBooking = await bookings.aggregate([
                    { $lookup:
                        {
                            from: "inventories",
                            localField: "inventory_id",
                            foreignField: "_id",
                            // pipeline: pipeline,
                            as: "inventory_details"
                        }
                    },
                    { 
                        $unwind: '$inventory_details'
                    },
                    {
                        $match:{"$and":bookingFilter}
                    },
                ])
               
                if(checkSpareBooking.length>0 && checkSpare.quantity <= checkSpareBooking.length){
                    checkStatus = 'not_available';
                    message="Not Available";
                }else{
                    getId = checkSpare._id;
                    checkStatus = 'available';
                     message="Available";
                }

            }
            
        }
        return res.status(200).send({status: true, message: message, data:checkStatus,type:type,getId:getId});
        
  
    }catch (err) {
        console.log(err);
    }
}
const checkVerificationCode = async (req, res) => {
    try {
        const { booked_by,user_id,booking_id,otp } = req.body;
        if (!(user_id && booking_id && otp)) {
            return res.status(200).send({ status: 'false', message: "Please enter OTP." });
        }
        const user = await User.findOne({$and:[{ "_id":user_id },{'otp':otp}]});
        if(!user){
            return res.status(200).send({status: false, message: "Invalid Otp"});
        }

        const wherecon = {'_id':booking_id }
        const updvalues = { $set: { status: true ,created:new Date().toISOString(),updated:new Date().toISOString()} };
        const bookingUpd = await bookings.updateOne(wherecon, updvalues);

        if(!bookingUpd){
            return res.status(200).send({status: false, message: "Invalid Otp"});
        }
        const bookingData = await bookings.findById(booking_id);
        // console.log('bookingData',bookingData);
        const inventory_id_get = bookingData.inventory_id;
        const business_supplier_id_get = bookingData.business_supplier_id;
        const city_id_get = bookingData.city_id;
        const chub_id_get = bookingData.hub_id;

        const whereconInv = {'_id':inventory_id_get }
        const updvaluesInv = { $set: { status: 4 } };
        const updateInventory1 = await inventory.updateOne(whereconInv, updvaluesInv);

        // update spares 
        console.log('bookingData',bookingData);
        const getSapres = bookingData.spares;
        console.log('getSapres',getSapres);
        let sparesAvailability = await checkSparesAvailability('spares',getSapres,null,city_id_get,chub_id_get,business_supplier_id_get)
        console.log('sparesAvailability',sparesAvailability)
        if(sparesAvailability  == 0){
            const whereconInvSpares = {'part_number':getSapres }
            const updvaluesInvSpares = { $set: { assigned_status: 1 } };
            const updateSpares = await spares.updateOne(whereconInvSpares, updvaluesInvSpares);
        }
        
        // update spares Charger 
        const getSapresCharger = bookingData.charger_1;

        let sparesAvailabilitycharger_1 = await checkSparesAvailability('charger_1',getSapresCharger,null,city_id_get,chub_id_get,business_supplier_id_get)
        console.log('sparesAvailabilitycharger_1',sparesAvailabilitycharger_1)
        if(sparesAvailabilitycharger_1 ==0){
            const whereconInvSparesCharger = {'part_number':getSapresCharger }
            const updvaluesInvSparesCharger = { $set: { assigned_status: 1 } };
            const updateSparesCharger = await spares.updateOne(whereconInvSparesCharger, updvaluesInvSparesCharger);
        }

        const getSapresCharger2 = bookingData.charger_2;
        
        let sparesAvailabilitycharger_2 = await checkSparesAvailability('charger_2',getSapresCharger2,null,city_id_get,chub_id_get,business_supplier_id_get)     
        console.log('sparesAvailabilitycharger_2',sparesAvailabilitycharger_2)

        if(sparesAvailabilitycharger_2  == 0){
            const whereconInvSparesCharger2 = {'part_number':getSapresCharger2 }
            const updvaluesInvSparesCharger2 = { $set: { assigned_status: 1 } };
            const updateSparesCharger2 = await spares.updateOne(whereconInvSparesCharger2, updvaluesInvSparesCharger2);

        }        

        // update spares Battery    
        const getSapresBattery = bookingData.battery_1;
        let sparesAvailabilitybattery_1 = await checkSparesAvailability('battery_1',getSapresBattery,null,city_id_get,chub_id_get,business_supplier_id_get)
        console.log('sparesAvailabilitybattery_1',sparesAvailabilitybattery_1)

        if(sparesAvailabilitybattery_1  == 0){
            const whereconInvSparesBattery = {'part_number':getSapresBattery }
            const updvaluesInvSparesBattery = { $set: { assigned_status: 1 } };
            const updateSparesBattery = await spares.updateOne(whereconInvSparesBattery, updvaluesInvSparesBattery);
        }

        const getSapresBattery2 = bookingData.battery_2;
        let sparesAvailabilitybattery_2 = await checkSparesAvailability('battery_1',getSapresBattery2,null,city_id_get,chub_id_get,business_supplier_id_get)
        if(sparesAvailabilitybattery_2  == 0){
            const whereconInvSparesBattery2 = {'part_number':getSapresBattery2 }
            const updvaluesInvSparesBattery2 = { $set: { assigned_status: 1 } };
            const updateSparesBattery2 = await spares.updateOne(whereconInvSparesBattery2, updvaluesInvSparesBattery2);
        }

        // update iot devices 
        const getiot_devices = bookingData.iot_devices;
        let sparesAvailabilityiot_devices =  await checkSparesAvailability('iot_devices',getiot_devices,null,city_id_get,chub_id_get,business_supplier_id_get)
        if(sparesAvailabilityiot_devices  == 0){
            const whereconInvIot_devices = {'device_serial_number':getiot_devices }
            const updvaluesInvIot_devices = { $set: { assigned_status: 1 } };
            const updateIot_devices = await iot_devices.updateOne(whereconInvIot_devices, updvaluesInvIot_devices);
        }

        let UpdateAllVehiclesSparesIotdevices = await checkAndUpdateAllVehiclesSparesIotdevices();

         /* save data in inventory history start */
        const booked_byUser = await admin.findById(booked_by);
        const postDataHistory = {
            inventory_id:bookingUpd.inventory_id,
            title:'Booked',
            description:'Booked by '+booked_byUser?booked_byUser.first_name+' '+booked_byUser.last_name:'',
            status:3,
            created:new Date().toISOString(),
            updated:new Date().toISOString()
        }
        const inventoryHistory = new inventory_history(postDataHistory); 
        var inventoryHistorylastData = await inventoryHistory.save();            
        /* save data in inventory history end */

        // get all booking list
        var matchfilter ={};  
        const bookingsData = await allBookings(matchfilter);

        return res.status(200).send({status: true, message: "Booking Successfully Assigned!", data:bookingUpd,bookingsData:bookingsData});        
  
    } catch (err) {
        console.log(err);
    }
}

const checkAndUpdateAllVehiclesSparesIotdevices = async () => {
    let getAllBookings = await bookings.aggregate([
        { $lookup:
            {
                from: "inventories",
                localField: "inventory_id",
                foreignField: "_id",
                // pipeline: pipeline,
                as: "inventory_details"
            }
        },
        { 
            $unwind: '$inventory_details'
        },    
        {
            $match:{"$and":[{'status':true}]}
        },
        // {
        //     $match:{"$and":[{'status':true},{"vehicle_details.book_status":1}]}
        // },
       
    ]);
    if(getAllBookings.length>0){
        var bookedInventoryIds=[];
        var bookedSpares=[];
        var bookedIotDevices=[];
        var bookedChargers=[];
        var bookedBatteries=[];
        const getAllDataMap = getAllBookings.map(async(value,index)=>{
            bookedInventoryIds.push(ObjectId(value.inventory_id));            
            if(value.spares != ""){
                bookedSpares.push(value.spares);
            }
            bookedIotDevices.push(value.iot_devices);
            bookedChargers.push(value.charger_1);
            if(value.charger_2 != ""){
                bookedChargers.push(value.charger_2);
            }            
            bookedBatteries.push(value.battery_1);
            if(value.battery_2 != ""){
                bookedBatteries.push(value.battery_2);
            }
           
        })

        let getAll = await Promise.all(getAllDataMap);
        // update status of vehicles
        if(bookedInventoryIds.length >0){
            const updateInventorystatus = await  inventory.updateMany({_id : {$nin: bookedInventoryIds}},{$set : {status : 1}}
            ); 
            const getAllVehiclesIds = await  inventory.find({_id : {$in: bookedInventoryIds}},{"vehicle_id":1}); 
            
            if(getAllVehiclesIds.length >0){
                
                let getAllVehiclesIdsGet = [];
                await Promise.all(getAllVehiclesIds.map(async(value,index)=>{
                    getAllVehiclesIdsGet.push(value.vehicle_id)
                }));

                const updatevehiclestatus = await  vehicles.updateMany({_id : {$in: getAllVehiclesIdsGet}},{$set : {book_status : 1}}); 
                const updatevehiclestatus1 = await  vehicles.updateMany({_id : {$nin: getAllVehiclesIdsGet}},{$set : {book_status : 0}});
            }           

        }
        // update status of spares
        if(bookedSpares.length >0){
            const updateSparesstatus = await  spares.updateMany({$and:[{part_number : {$nin: bookedSpares}},{part_type:"spares"}]},{$set : {assigned_status : 0}}
            ); 
        }
        // update status of battery
        if(bookedBatteries.length >0){
            const updateBatterystatus = await  spares.updateMany({$and:[{part_number : {$nin: bookedBatteries}},{part_type:"battery"}]},{$set : {assigned_status : 0}}
            ); 
        }
        // update status of chargers
        if(bookedChargers.length >0){
            const updatechargerstatus = await  spares.updateMany({$and:[{part_number : {$nin: bookedChargers}},{part_type:"charger"}]},{$set : {assigned_status : 0}}
            ); 
        }
        // update status of iotdevices
        if(bookedIotDevices.length >0){
            const updateIotDevicesstatus = await  iot_devices.updateMany({device_serial_number : {$nin: bookedIotDevices}},{$set : {assigned_status : 0}}
            ); 
        }
    }


    // var checkSpareBooking = await bookings.aggregate([
    //     { $lookup:
    //         {
    //             from: "inventories",
    //             localField: "inventory_id",
    //             foreignField: "_id",
    //             // pipeline: pipeline,
    //             as: "inventory_details"
    //         }
    //     },
    //     { 
    //         $unwind: '$inventory_details'
    //     },
    //     {
    //         $match:{"$and":[{ "spares": value},{'status':true}]}
    //     },
    // ])
    return getAllBookings; 
}
const checkSparesAvailability = async (type,value,booking_id=null,city_id="",hub_id="",business_supplier_id="") => {
    var checkStatus = 0;
    var getId = 0;
    var message = '';
    var status =1;
   
    // const getBookingDetailsAvailability = await bookings.findById(booking_id);
    // console.log('getBookingDetailsAvailability',getBookingDetailsAvailability)
    // let bookignCity = getBookingDetailsAvailability.city_id;
    // let bookignHub = getBookingDetailsAvailability.hub_id;
    // let bookignSupplierId = getBookingDetailsAvailability.business_supplier_id;
    
    if(type == 'spares'){     
        let SparesMatch = [{ 'part_type':'spares'},{'part_number':value}];    
        let BookingMatch = [{ "spares": value},{'status':true}];    
        if(business_supplier_id){
            SparesMatch.push({'business_supplier':typeof(business_supplier_id)=='object'?business_supplier_id.toString():business_supplier_id});
            BookingMatch.push({'inventory_details.supplier_id':ObjectId(business_supplier_id)});
            
        } 
        if(city_id){
            SparesMatch.push({'city':typeof(city_id)=='object'?city_id.toString():city_id});
            BookingMatch.push({'inventory_details.city_id':ObjectId(city_id)});
        }
        if(hub_id){
            SparesMatch.push({'hub':typeof(hub_id)=='object'?hub_id.toString():hub_id});
            BookingMatch.push({'inventory_details.hub_id':ObjectId(hub_id)});
        }  

        let matchArrSpares = {'$and':SparesMatch};
        const checkSpare = await spares.findOne(matchArrSpares);
        if(!checkSpare){
            checkStatus = 'invalid';
            message="Invailid data";
            status =null;
    
        }else{

            if(booking_id != null){
                BookingMatch.push({'_id':{"$ne":ObjectId(booking_id)}})
            }
            /* get all spares data */  
            var checkSpareBooking = await bookings.aggregate([
                { $lookup:
                    {
                        from: "inventories",
                        localField: "inventory_id",
                        foreignField: "_id",
                        // pipeline: pipeline,
                        as: "inventory_details"
                    }
                },
                { 
                    $unwind: '$inventory_details'
                },
                {
                    $match:{"$and":BookingMatch}
                    // $match:{"$and":[{ "spares": value},{'status':true},{'business_supplier_id':bookignSupplierId},{'city_id':bookignCity},{'hub_id':bookignHub}]}
                },
            ])
            if(checkSpareBooking.length>0 && checkSpare.quantity<= checkSpareBooking.length){
                checkStatus = 'not_available';
                message="Not Available";
                status =0;
    
            }else{
                getId = checkSpare._id;
                checkStatus = 'available';
                message="Available";
                status =1;    
            }
        }
        
    }else if(type == 'iot_devices'){   
        let iot_devicesMatch = [{'device_serial_number':value}];  
        let BookingMatch = [{ "iot_devices": value},{'status':true}];    
        if(business_supplier_id){
            iot_devicesMatch.push({'business_supplier':typeof(business_supplier_id)=='object'?business_supplier_id.toString():business_supplier_id});
            BookingMatch.push({'inventory_details.supplier_id':ObjectId(business_supplier_id)});            
        } 
        if(city_id){
            iot_devicesMatch.push({'city':typeof(city_id)=='object'?city_id.toString():city_id});
            BookingMatch.push({'inventory_details.city_id':city_id});
        }
        if(hub_id){
            iot_devicesMatch.push({'hub':typeof(hub_id)=='object'?hub_id.toString():hub_id});
            BookingMatch.push({'inventory_details.hub_id':hub_id});
        }  

        let matchArriot_devices = {'$and':iot_devicesMatch};

        const checkIotDevices = await iot_devices.find(matchArriot_devices);
        if(checkIotDevices.length<=0){
            checkStatus = 'invalid';
            message="Invailid data";
            status =null;

        }else{

            if(booking_id  != null){
                BookingMatch.push({'_id':{"$ne":ObjectId(booking_id)}})
            }
            var checkIotDevicesBooking = await bookings.aggregate([
                { $lookup:
                    {
                        from: "inventories",
                        localField: "inventory_id",
                        foreignField: "_id",
                        // pipeline: pipeline,
                        as: "inventory_details"
                    }
                },
                { 
                    $unwind: '$inventory_details'
                },
                {
                    $match:{"$and":BookingMatch}
                },
            ])
            /* get all IotDevices data */                                
            

            if(checkIotDevicesBooking.length>0){
                checkStatus = 'not_available';
                message="Not Available";
                status =0;
            }else{
                getId = checkIotDevices[0]._id;
                checkStatus = 'available';
                message="Available";
                status =1;
            }

        }

    }else if(type == 'battery_1' || type == 'battery_2'){
        
        let BatteryMatch = [{ 'part_type': { $eq: "battery" }},{'part_number':value}]  
        let BookingMatch = [{'status':true}];    
        if(business_supplier_id){
            BatteryMatch.push({'business_supplier':typeof(business_supplier_id)=='object'?business_supplier_id.toString():business_supplier_id});
            BookingMatch.push({'inventory_details.supplier_id':ObjectId(business_supplier_id)});            
        } 
        if(city_id){
            BatteryMatch.push({'city':typeof(city_id)=='object'?city_id.toString():city_id});
            BookingMatch.push({'inventory_details.city_id':city_id});
        }
        if(hub_id){
            BatteryMatch.push({'hub':typeof(hub_id)=='object'?hub_id.toString():hub_id});
            BookingMatch.push({'inventory_details.hub_id':hub_id});
        }  

        let matchArrBattery = {'$and':BatteryMatch};
        let checkSpare = await spares.findOne(matchArrBattery);
        console.log('checkSpare',checkSpare)
        console.log('matchArrBattery',matchArrBattery)
        if(!checkSpare){
            checkStatus = 'invalid';
            message="Invailid data";
            status =null;

        }else{
            /* get all spares data */                                 
            
            // let bookingFilter=[{'status':true}];
            if(value != ""){
                if(type == "battery_1" || type == 'battery_2'){
                    BookingMatch.push({$or:[{'battery_1':value},{'battery_2':value}]});
                }
    
                if(booking_id != null){
                    BookingMatch.push({'_id':{"$ne":ObjectId(booking_id)}})
                }    
                console.log('BookingMatch',BookingMatch)
                var checkSpareBooking = await bookings.aggregate([
                    { $lookup:
                        {
                            from: "inventories",
                            localField: "inventory_id",
                            foreignField: "_id",
                            // pipeline: pipeline,
                            as: "inventory_details"
                        }
                    },
                    { 
                        $unwind: '$inventory_details'
                    },
                    {
                        $match:{"$and":BookingMatch}
                    },
                ])
                if(checkSpareBooking.length>0 && checkSpare.quantity <= checkSpareBooking.length){
                    checkStatus = 'not_available';
                    message="Not Available";
                    status =0;
                }else{
                    getId = checkSpare._id;
                    checkStatus = 'available';
                    message="Available";
                    status =1;
                }
            }else{
                getId = checkSpare._id;
                checkStatus = 'available';
                message="Available";
                status =1;
            }
            

        }
        
    }else if(type == 'charger_1' || type == 'charger_2'){        
        let ChargerMatch = [{ 'part_type': { $eq: "charger" }},{'part_number':value}]  
        let BookingMatch = [{'status':true}];    
        if(business_supplier_id){
            ChargerMatch.push({'business_supplier':typeof(business_supplier_id)=='object'?business_supplier_id.toString():business_supplier_id});
            BookingMatch.push({'inventory_details.supplier_id':ObjectId(business_supplier_id)});            
        } 
        if(city_id){
            ChargerMatch.push({'city':typeof(city_id)=='object'?city_id.toString():city_id});
            BookingMatch.push({'inventory_details.city_id':city_id});
        }
        if(hub_id){
            ChargerMatch.push({'hub':typeof(hub_id)=='object'?hub_id.toString():hub_id});
            BookingMatch.push({'inventory_details.hub_id':hub_id});
        }  

        let matchArrCharger = {'$and':ChargerMatch};
        let checkSpare = await spares.findOne(matchArrCharger);
        if(!checkSpare){
            checkStatus = 'invalid';
            message="Invailid data";
            status =null;

        }else{
            /* get all spares data */  
            
            //let bookingFilter=[{'status':true}];

            // if(type == "charger_1"){
            //     bookingFilter.push({'charger_1':value});

            // }else  if(type == 'charger_2'){
            //     bookingFilter.push({'charger_2':value});
            // }
            
            if(type == "charger_1" || type == 'charger_2'){
                BookingMatch.push({$or:[{'charger_1':value},{'charger_2':value}]});
            }
            if(booking_id != null){
                BookingMatch.push({'_id':{"$ne":ObjectId(booking_id)}})
            }
            
            var checkSpareBooking = await bookings.aggregate([
                { $lookup:
                    {
                        from: "inventories",
                        localField: "inventory_id",
                        foreignField: "_id",
                        // pipeline: pipeline,
                        as: "inventory_details"
                    }
                },
                { 
                    $unwind: '$inventory_details'
                },
                {
                    $match:{"$and":BookingMatch}
                },
            ])
            
            if(checkSpareBooking.length>0 && (parseInt(checkSpare.quantity) <= checkSpareBooking.length)){
                checkStatus = 'not_available';
                message="Not Available";
                status =0;
            }else{
                getId = checkSpare._id;
                checkStatus = 'available';
                message="Available";
                status =1;
            }

        }
        
    }
    return status;
}    

const deleteBooking = async (req, res) => {        
    try {       
        const id = req.params.id;
        const bookingsData = await bookings.findByIdAndRemove(id);
        if (!bookingsData) { 
            return res.status(404).send({status: false, message: "Cannot delete bookings! with id=${id}. Maybe bookings! was not found!",data: []});
        }

        // get all booking list
        var matchfilter ={};  
        const allBookingsData = await allBookings(matchfilter);  

        return res.status(200).send({status: true, message: "Booking successfully deleted!", data: bookingsData,allBookingsData:allBookingsData});   

    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while removing bookings.",data: []});
    }

}
const allBookings=async(matchfilter)=>{
    const bookingsData = await bookings.aggregate([
        {
            $sort:{created: -1}
        },
        { $lookup:
            {
                from: "users",
                localField: "user_id",
                foreignField: "_id",
                // pipeline: pipeline,
               as: "user_id"
            }
        },
        { 
            $unwind: '$user_id'
        },
        { $lookup:
            {
                from: "inventories",
                localField: "inventory_id",
                foreignField: "_id",
                // pipeline: pipeline,
               as: "inventory_id"
            }
        },
        { 
            $unwind: '$inventory_id'
        },
        { $lookup:
            {
                from: "vehicles",
                localField: "inventory_id.vehicle_id",
                foreignField: "_id",
                // pipeline: pipeline,
               as: "vehicle_id"
            }
        },
        { 
            $unwind: '$vehicle_id'
        },
        { $lookup:
            {
                from: "cities",
                localField: "inventory_id.city_id",
                foreignField: "_id",
                as: "city_id"
            }
        },
        // { 
        //     $unwind: '$city_id'
        // },
        { $lookup:
            {
                from: "hubs",
                localField: "inventory_id.hub_id",
                foreignField: "_id",
                as: "hub_id"
            }
        },
        // { 
        //     $unwind: '$hub_id'
        // },
        { $lookup:
            {
                from: "bookingstransactions",
                localField: "_id",
                foreignField: "booking_id",
                as: "bookingstransactions"
            }
        },
        // { 
        //     $unwind: '$bookingstransactions'
        // },
        { 
            $match:matchfilter 
        },
    ]) 
    // const bookingsData = await bookings.find().populate('user_id').populate('inventory_id').sort({ created: -1 });
    if(bookingsData){
        bookingsData.filter(function(val,index){
            return val['key']=index;
        })
    }

   
    return bookingsData;
}
module.exports = { getAllBookings,getAllBookingsFilters,getBookingDetails,createBooking,updateBooking,deleteBooking,checkVerificationCode,check_availability,file_upload };