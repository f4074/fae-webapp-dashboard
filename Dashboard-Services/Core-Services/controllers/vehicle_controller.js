require("dotenv").config();
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const Hub = require("../models/hub");
const cities = require("../models/cities");
const hub = require("../models/hub");
const vehicle = require("../models/vehicles");
const inventory = require("../models/inventory");
const ObjectId = require('mongodb').ObjectId;
const auth = require("../middleware/auth");

const getAllVehicles = async (req, res) => {        
    try {       
        const allVehicles = await vehicle.find().sort({'vehicle_name':1});        
        return res.status(200).send({status: true, message: "Data Found", data: allVehicles});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while getting all data of Hub.",data: []});
    }

}
const getDetailsVehicle = async (req, res) => {        
    try {  
        const id = req.params.id;      
        const data = await vehicle.findById(id)();
        console.log('data',data)
        return res.status(200).send({status: true, message: "Data Found", data: data});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while getting details of Hub.",data: []});
    }

}
const createVehicle = async (req, res) => { 
           
    try { 
        if (!req.body) {
            return res.status(400).send({
                message: "Data to update can not be empty!"
            });
        } 

        console.log('req.body',req.body);
        const city_id = req.body.city_id;
        const hub_id = req.body.hub_id;
        const type = req.body.type;
        const csv_file_data = req.body.csv_file_data;
        const dataAll = await csv_file_data.map(async function(item, i){
            const keys = Object.keys(item);
            const values = Object.values(item);
            
            const send_data = { business_supplier: values[0],city:values[1] ,hub:values[2] ,vehicle_name:values[3], vehicle_model: values[4],vehicle_color:values[5],vehicle_regNo:values[6],manufacture_date: values[7],date_of_sale:values[8] ,date_of_use: values[9],chassis_no: values[10], moter_engine_no:values[11],battery_no: values[12],battery_no_2:values[13] , controller_number:values[14] ,charger_no:values[15] ,charger_no_2:values[16], key_no:values[17] ,scooter_invoice:values[18], scooter_reg_date:values[19] ,scooter_reg_upto:values[20],insurance_company:values[21],policy_number_insurance:values[22] ,date_policy_expiry:values[23],  permit_no:values[24], exp_date:values[25],sgst:values[26], cgst: values[27],iot_device:values[28], model:values[29],  quantity:2000,type:type,created:new Date(),updated:new Date() }

            const vehicles = new vehicle(send_data);             
            const data = await vehicles.save();    
            return data;        
        });
        const lastData = await Promise.all(dataAll); 
        if(!lastData){
            return res.status(200).send({status: false, message: "Some error occurred while creating the record!",data: []});
        }    
        const inventoryDataAll = lastData.map(async function(item, i){
            const postData = {
                city_id:city_id,
                hub_id:hub_id,
                city_name:item.city,
                hub_name:item.hub,
                vehicle_id:item._id,
                created:new Date(),
                updated:new Date()
            }
            const inventories = new inventory(postData);             
            const inventoryData = await inventories.save(); 
            return inventoryData;
        })
        const inventorylastData = await Promise.all(inventoryDataAll); 

        const getAllInventoryData = getAllInventory();
        
        return res.status(200).send({status: true, message: "Hub successfully created!", data: {'inventorylastData':inventorylastData,'allInventoryData':getAllInventoryData}});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while creating Hub.",data: []});
    }

}
const updateVehicle = async (req, res) => {        
    try {      
        if (!req.body) {
            return res.status(400).send({
                message: "Data to update can not be empty!"
            });
        } 
        const id = req.params.id;  
        const data = await vehicle.findByIdAndUpdate(id, req.body, { useFindAndModify: false });
        console.log('data',data)
        return res.status(200).send({status: true, message: "Hub successfully updated!", data: data});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while updating Hub.",data: []});
    }

}
const deleteVehicle= async (req, res) => {        
    try {       
        const id = req.params.id;
        const data = await vehicle.findByIdAndRemove(id);
        return res.status(200).send({status: true, message: "Hub successfully deleted!", data: data});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while removing Hub.",data: []});
    }

}
const deleteMultipleVehicles = async (req, res) => {        
    try { 
        const deleteJson = req.body;
        const data = await vehicle.deleteMany(deleteJson);
        return res.status(200).send({status: true, message: `${data.deletedCount} hubs were deleted successfully!`, data: []});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while removing all hubs.",data: []});
    }
}

module.exports = { getAllVehicles,getDetailsVehicle,createVehicle,updateVehicle,deleteVehicle,deleteMultipleVehicles };