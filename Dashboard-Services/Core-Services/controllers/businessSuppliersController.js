require("dotenv").config();
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const BusinessSuppliers = require("../models/business_suppliers");
const cities = require("../models/cities");
const ObjectId = require('mongodb').ObjectId;
const auth = require("../middleware/auth");
const bcrypt = require("bcryptjs");
const sendEmail = require("../common/sendMail")


const getAllBusinessSuppliers = async (req, res) => { 
    try {       
        console.log('req.body',req.body);
        const BusinessSuppliersData = await BusinessSuppliers.find().sort({ created: -1 });

        if(BusinessSuppliersData){
            BusinessSuppliersData.filter(function(val,i){
                return val.key = i;
            })
        }       
        return res.status(200).send({status: true, message: "Data Found", data: BusinessSuppliersData});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while getting all data of business supplier.",data: []});
    }
}


const getAllBusinessSuppliersFilter = async (req, res) => { 
    try {    
        
        // filter start
        const filterArray =  req.body
        var matchArr =[];
        var hex =/[0-9A-Fa-f]{6}/g;
        if(filterArray != null){
            const city_id_filter = filterArray.city_id;
            const hub_id_filter = filterArray.hub_id;
            const searchKey = filterArray.searchKey;
            const start_date =filterArray.start_date;
            const end_date =filterArray.end_date;
            if(searchKey){        
                var searchKey_idmatch =  {$or:[
                    {"supplier_name":{'$regex':searchKey}},
                ]}
                matchArr.push(searchKey_idmatch);
            }
            if(city_id_filter){
                var city_idmatch =  {"city_id": (hex.test(city_id_filter))? ObjectId(city_id_filter) : city_id_filter}
                matchArr.push(city_idmatch);
            }
            // if(hub_id){
            //     var hub_idmatch =  {"hub_id":  (hex.test(hub_id))? ObjectId(hub_id) : hub_id}
            //     matchArr.push(hub_idmatch);
            // }
        } 

        var matchfilter ={};
        if(matchArr.length >0){
            matchfilter = {$and: matchArr} 
        } 
        // filter end 
        
        const BusinessSuppliersData = await BusinessSuppliers.find(matchfilter).sort({ created: -1 });
        
        if(BusinessSuppliersData){
            BusinessSuppliersData.filter(function(val,i){
                return val.key = i;
            })
        }      
        
        let citiesData = await cities.find().sort({ name: 1 });
        if(citiesData){
            citiesData.filter(function(val,i){
                return val.key = i;
            })
        }
        return res.status(200).send({status: true, message: "Data Found", data: BusinessSuppliersData,citiesData:citiesData});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while getting all data of business supplier.",data: []});
    }
}


const getDetailsBusinessSuppliers = async (req, res) => {          
    try { 
        const id = req.params.id;       
        const BusinessSuppliersdetails = await BusinessSuppliers.findById(id);
        console.log('BusinessSuppliersdetails',BusinessSuppliersdetails)
        return res.status(200).send({status: true, message: "Data Found", data: BusinessSuppliersdetails});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while getting details of business supplier.",data: []});
    }

}
const createBusinessSuppliers = async (req, res) => {  

    try { 
        if (!req.body) {
            return res.status(400).send({
                message: "Data to update can not be empty!"
            });
        } 

        const businessSuppliers = new BusinessSuppliers(req.body);
             
        const BusinessSuppliersData = await businessSuppliers.save();
        console.log('BusinessSuppliersData',BusinessSuppliersData)
        if (BusinessSuppliersData) {  
            const BusinessSuppliersdetails = await BusinessSuppliers.findById(BusinessSuppliersData._id);
            const BusinessSuppliersDataAll = await BusinessSuppliers.find().sort({ created: -1 });
        
            if(BusinessSuppliersDataAll){
                BusinessSuppliersDataAll.filter(function(val,i){
                    return val.key = i;
                })
            }          
          return res.status(200).send({status: true, message: "Business supplier successfully created!", data: BusinessSuppliersdetails,BusinessSuppliersDataAll:BusinessSuppliersDataAll});
        }
        return res.status(404).send({status: false, message: "Some error occurred while creating the business supplier!",data: []});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while creating business supplier.",data: []});
    }

}
const updateBusinessSuppliers = async (req, res) => {        
    try {      
        if (!req.body) {
            return res.status(400).send({
                message: "Data to update can not be empty!"
            });
        } 
        const id = req.params.id;  
        const BusinessSuppliersData = await BusinessSuppliers.findByIdAndUpdate(id, req.body, { useFindAndModify: false });
        // console.log('BusinessSuppliersData',BusinessSuppliersData)
        if (BusinessSuppliersData) {  
            const BusinessSuppliersdetails = await BusinessSuppliers.findById(id);
            return res.status(200).send({status: true, message: "Business supplier successfully updated!", data: BusinessSuppliersdetails});
        }
        return res.status(404).send({status: false, message: "Cannot update business supplier! with id=${id}. Maybe business supplier! was not found!",data: []});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while updating business supplier.",data: []});
    }

}
const deleteBusinessSuppliers = async (req, res) => {        
    try {       
        const id = req.params.id;
        const BusinessSuppliersData = await BusinessSuppliers.findByIdAndRemove(id);
        if (BusinessSuppliersData) {         
            return res.status(200).send({status: true, message: "Business supplier successfully deleted!", data: BusinessSuppliersData});
          }
          return res.status(404).send({status: false, message: "Cannot delete business supplier! with id=${id}. Maybe business supplier! was not found!",data: []});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while removing all business supplier.",data: []});
    }

}

const deleteMultipleBusinessSuppliers = async (req, res) => {        
    try { 
        const deleteJson = req.body;
        const data = await BusinessSuppliers.deleteMany(deleteJson);
        if (data) {         
            return res.status(200).send({status: true, message: `${data.deletedCount} Business supplier were deleted successfully!`, data: []});
          }
          return res.status(404).send({status: false, message: "Some error occurred while removing all business supplier",data: []});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while removing all business supplier.",data: []});
    }
}

module.exports = { getAllBusinessSuppliers,getAllBusinessSuppliersFilter,getDetailsBusinessSuppliers,createBusinessSuppliers,updateBusinessSuppliers,deleteBusinessSuppliers,deleteMultipleBusinessSuppliers };