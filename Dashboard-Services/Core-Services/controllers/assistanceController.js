require("dotenv").config();
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const cities = require("../models/cities");
const referals = require("../models/referals");
const userRefferal = require("../models/user_refferal");
const {support_assistance} = require("../models/support_assistance");
const assistance_fare = require("../models/assistance_fare");
const assistance_type= require("../models/assistance_type")
const problem_type= require("../models/problem_type")

const ObjectId = require('mongodb').ObjectId;
const auth = require("../middleware/auth");
const sendEmail = require("../common/sendMail")
var hex =/[0-9A-Fa-f]{6}/g;
const getAllProblemType=async()=>{
    const problem_typeData = await problem_type.find().sort({ created: -1 });
    if(problem_typeData){
        problem_typeData.filter(function(val,index){
            return val.key = index;
        })
    }
    return problem_typeData;
}


const getAllassistanceRequest=async(filterArray=null)=>{
    // filter start
    var matchArr =[];
    
    if(filterArray != null){
        const city_id_filter = filterArray.city_id;
        const hub_id_filter = filterArray.hub_id;
        const searchKey = filterArray.searchKey;
        const start_date =filterArray.start_date;
        const end_date =filterArray.end_date;
        console.log('hub_id_filter',hub_id_filter)
        if(searchKey){        
            var searchKey_idmatch =  {$or:[
                {"user_id.phone":searchKey},
                {"user_id.email":searchKey},
                {"booking_id._id":(hex.test(searchKey))? ObjectId(searchKey) : searchKey},
                {"vehicle_id.vehicle_regNo":searchKey},
            ]}
            matchArr.push(searchKey_idmatch);
        }
        if(city_id_filter){        
            var City_id_idmatch =  {"city_id._id":(hex.test(city_id_filter))? ObjectId(city_id_filter) : city_id_filter}
            matchArr.push(City_id_idmatch);        
        }
        if(hub_id_filter){        
            var hub_id_idmatch =  {"hub_id._id":(hex.test(hub_id_filter))? ObjectId(hub_id_filter) : hub_id_filter}
            matchArr.push(hub_id_idmatch);        
        }

        if(start_date){
            var startDateSet = start_date;
            var start_date_match =  {updated: { $gte:  new Date(startDateSet) }}
            matchArr.push(start_date_match);
        }

        if(end_date){
            var endDateSet = end_date;
            var end_date_match =  {updated: { $lte:  new Date(endDateSet) }}
            matchArr.push(end_date_match);
        }
    } 

    var matchfilter ={};
    if(matchArr.length >0){
        matchfilter = {$and: matchArr} 
    } 
    // filter end
    
    let assistanceRequestData = await support_assistance.aggregate([        
        {
            $sort:{created: -1}
        },
        { $lookup:
            {
                from: "users",
                localField: "user_id",
                foreignField: "_id",
                as: "user_id"
            }
        },
        { 
            $unwind: '$user_id'
        },
        { $lookup:
            {
                from: "bookings",
                localField: "booking_id",
                foreignField: "_id",
                as: "booking_id"
            }
        },
        { 
            $unwind: '$booking_id'
        },
        { $lookup:
            {
                from: "inventories",
                localField: "booking_id.inventory_id",
                foreignField: "_id",
                as: "inventory_id"
            }
        },
        { 
            $unwind: '$inventory_id'
        },
        { $lookup:
            {
                from: "vehicles",
                localField: "inventory_id.vehicle_id",
                foreignField: "_id",
                as: "vehicle_id"
            }
        },
        // { 
        //     $unwind: '$vehicle_id'
        // },
        { $lookup:
            {
                from: "assistance_types",
                localField: "assistance_type",
                foreignField: "_id",
                as: "assistance_type"
            }
        },
        { 
            $unwind: '$assistance_type'
        },
        // { $lookup:
        //     {
        //         from: "problem_types",
        //         localField: "problem_type",
        //         foreignField: "_id",
        //         as: "problem_type"
        //     }
        // },
        // { 
        //     $unwind: '$problem_type'
        // },
        { $lookup:
            {
                from: "cities",
                localField: "city_id",
                foreignField: "_id",
                as: "city_id"
            }
        },
        { 
            $unwind: '$city_id'
        },
        { $lookup:
            {
                from: "hubs",
                localField: "hub_id",
                foreignField: "_id",
                as: "hub_id"
            }
        },
        // { 
        //     $unwind: '$hub_id'
        // },
        { 
            $match:matchfilter 
        },
    ]) 

    if(assistanceRequestData.length >0){
        assistanceRequestData.filter((val,index)=>{
            return val.key = index;
        })
        
    }
    return assistanceRequestData;
}


const getAllassistanceFare=async(filterArray=null)=>{
    
    // filter start
    var matchArr =[];
    if(filterArray != null){        
        const city_id_filter = filterArray.city_id;
        const hub_id_filter = filterArray.hub_id;
        const searchKey = filterArray.searchKey;
        if(searchKey){                   
            var searchKey_idmatch =  {$or:[
                {"city_id.name":{ $regex: new RegExp("^" + searchKey.toLowerCase(), "i") }}
            ]}
            matchArr.push(searchKey_idmatch);        
        }
        // if(city_id_filter){                   
        //     var city_id_idmatch =  {"city_id._id":ObjectId(city_id_filter)}
        //     matchArr.push(city_id_idmatch);        
        // }
        // if(hub_id_filter){                   
        //     var hub_id_idmatch =  {"hub_id._id":ObjectId(city_id_filter)}
        //     matchArr.push(hub_id_idmatch);        
        // }
        if(city_id_filter){        
            var City_id_idmatch =  {"city_id._id":(hex.test(city_id_filter))? ObjectId(city_id_filter) : city_id_filter}
            matchArr.push(City_id_idmatch);        
        }
        if(hub_id_filter){        
            var hub_id_idmatch =  {"hub_id._id":(hex.test(hub_id_filter))? ObjectId(hub_id_filter) : hub_id_filter}
            matchArr.push(hub_id_idmatch);        
        }
    } 

    var matchfilter ={};
    if(matchArr.length >0){
        matchfilter = {$and: matchArr} 
    } 
    // filter end
    // const assistance_fareData = await assistance_fare.find().populate('city_id').populate('assistance_type_id').populate('problem_type_id').sort({ created: -1 });
    const assistance_fareData = await assistance_fare.aggregate([        
        {
            $sort:{created: -1}
        },
        { $lookup:
            {
                from: "cities",
                localField: "city_id",
                foreignField: "_id",
                as: "city_id"
            }
        },
        { 
            $unwind: '$city_id'
        },
        { $lookup:
            {
                from: "assistance_types",
                localField: "assistance_type_id",
                foreignField: "_id",
                as: "assistance_type_id"
            }
        },
        { 
            $unwind: '$assistance_type_id'
        },
        // { $lookup:
        //     {
        //         from: "problem_types",
        //         localField: "problem_type_id",
        //         foreignField: "_id",
        //         as: "problem_type_id"
        //     }
        // },
        // { 
        //     $unwind: '$problem_type_id'
        // },
        { 
            $match:matchfilter 
        },
    ]) 
    if(assistance_fareData){
        assistance_fareData.filter(function(val,index){
            return val.key = index;
        })
    }
    return assistance_fareData;
}


const getAllassistanceType=async()=>{
    const assistance_typeData = await assistance_type.find().sort({ created: -1 });
    if(assistance_typeData){
        assistance_typeData.filter(function(val,index){
            return val.key = index;
        })
    }
    return assistance_typeData;
}
const getAllWithFilter = async (req, res) => { 
    try {  
                
        let citiesData = await cities.find().sort({ name: 1 });
        if(citiesData){
            citiesData.filter(function(val,i){
                return val.key = i;
            })
        }
        

        let problem_typeData = await getAllProblemType();      
        
        let assistanceRequestData = await getAllassistanceRequest(req.body);
        
        let assistance_fareData = await getAllassistanceFare(req.body);
        
        let assistance_typeData = await getAllassistanceType();

        return res.status(200).send({status: true, message: "Data Found",citiesData: citiesData ,data:{assistanceRequestData: assistanceRequestData,assistance_fareData:assistance_fareData, assistance_typeData: assistance_typeData,problem_typeData:problem_typeData, }});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while getting all data of assistance.",data: []});
    }
}
const getAll = async (req, res) => {  

    try {       
        let citiesData = await cities.find().sort({ name: 1 });
        if(citiesData){
            citiesData.filter(function(val,i){
                return val.key = i;
            })
        }
        let problem_typeData = await getAllProblemType();        
        let assistanceRequestData = await getAllassistanceRequest();
        let assistance_fareData = await getAllassistanceFare();
        let assistance_typeData = await getAllassistanceType();

        return res.status(200).send({status: true, message: "Data Found", data:{assistanceRequestData: assistanceRequestData,assistance_fareData:assistance_fareData, assistance_typeData: assistance_typeData,problem_typeData:problem_typeData}, citiesData: citiesData});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while getting all data of assistance.",data: []});
    }
}
const getDetails = async (req, res) => {  
    try {       
        const assistanceRequest = await support_assistance.findOne(req._id);
        return res.status(200).send({status: true, message: "Data Found", data: assistanceRequest});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while getting all data of assistance request!.",data: []});
    }

}
const create = async (req, res) => {
    try { 
        if (!req.body) {
            return res.status(400).send({
                message: "Data to update can not be empty!"
            });
        } 
        req.body.created = new Date().toISOString();
        req.body.updated = new Date().toISOString();
        const assistanceRequestSendData = new support_assistance(req.body);             
        const assistanceData = await assistanceRequestSendData.save();
        
        if (assistanceData) {  
            const problem_typeData = await getAllProblemType();        
            const assistanceRequestData = await getAllassistanceRequest();
            const assistance_fareData = await getAllassistanceFare();
            const assistance_typeData = await getAllassistanceType();

            return res.status(200).send({status: true, message: "Support Assistace successfully created!", data:{assistanceRequestData:assistanceRequestData,assistance_fareData:assistance_fareData}});
        }
        return res.status(404).send({status: false, message: "Some error occurred while creating the Support Assistace!",data: []});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while creating Support Assistace.",data: []});
    }

}
const update = async (req, res) => {        
    try {      
        if (!req.body) {
            return res.status(400).send({
                message: "Data to update can not be empty!"
            });
        } 
        const id = req.params.id;  
        req.body.updated = new Date().toISOString();
        const assistanceRequestData = await support_assistance.findByIdAndUpdate(id, req.body, { useFindAndModify: false });
       
        if (assistanceRequestData) {  
            const problem_typeData = await getAllProblemType();        
            const assistanceRequestData = await getAllassistanceRequest();
            const assistance_fareData = await getAllassistanceFare();
            const assistance_typeData = await getAllassistanceType();

            return res.status(200).send({status: true, message: "Support Assistace successfully updated!",data:{assistance_fareData: assistance_fareData,problem_typeData:problem_typeData,assistanceRequestData:assistanceRequestData,assistance_typeData:assistance_typeData}});
        }
        return res.status(404).send({status: false, message: "Cannot update data! with id=${id}. Maybe data! was not found!",data: []});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while updating data.",data: []});
    }

}
const deleteRecord = async (req, res) => {        
    try {       
        const id = req.params.id;
        const assistanceRequestData = await support_assistance.findByIdAndRemove(id);
        if (assistanceRequestData) { 
            const assistanceRequestData = await support_assistance.find().sort({ created: -1 });
            if(assistanceRequestData){
                assistanceRequestData.filter(function(val,index){
                    return val.key = index;
                })
            }

            const assistance_fareData = await assistance_fare.find().sort({ created: -1 });
            if(assistance_fareData){
                assistance_fareData.filter(function(val,index){
                    return val.key = index;
                })
            }
            return res.status(200).send({status: true, message: "Support Assistace successfully deleted!",data:{assistance_fareData: assistance_fareData,problem_typeData:problem_typeData,assistanceRequestData:assistanceRequestData,assistance_typeData:assistance_typeData}});
          }
          return res.status(404).send({status: false, message: "Cannot delete Referal! with id=${id}. Maybe Support Assistace request! was not found!",data: []});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while removing Assistance request.",data: []});
    }

}


// api for assistance type
const getAllAssistanceType = async (req, res) => {  
    try {       
        const citiesData = await cities.find().sort({ name: 1 });
        if(citiesData){
            citiesData.filter(function(val,index){
                return val.key = index;
            })
        }
        const problem_typeData = await getAllProblemType();        
        const assistanceRequestData = await getAllassistanceRequest();
        const assistance_fareData = await getAllassistanceFare();
        const assistance_typeData = await getAllassistanceType();

        return res.status(200).send({status: true, message: "Data Found", data:{assistance_fareData: assistance_fareData,problem_typeData:problem_typeData,assistanceRequestData:assistanceRequestData,assistance_typeData:assistance_typeData, citiesData: citiesData}});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while getting all data of cities.",data: []});
    }
}
const getDetailsAssistanceType = async (req, res) => {  
    try {       
        const assistance_type = await assistance_type.findOne(req._id);
        return res.status(200).send({status: true, message: "Data Found", data: assistance_type});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while getting all data of assistance request!.",data: []});
    }

}
const createAssistanceType = async (req, res) => {
    try { 
        if (!req.body) {
            return res.status(400).send({
                message: "Data to update can not be empty!"
            });
        } 

        if(req.body.type == 'problem_type'){
            let problem_typeSendData = new problem_type(req.body);             
            var assistanceData = await problem_typeSendData.save();
        }else{
            let assistance_typeSendData = new assistance_type(req.body);             
            var assistanceData = await assistance_typeSendData.save();
        }
        
        if (assistanceData) {
            const problem_typeData = await getAllProblemType();        
            const assistanceRequestData = await getAllassistanceRequest();
            const assistance_fareData = await getAllassistanceFare();
            const assistance_typeData = await getAllassistanceType();

            return res.status(201).send({status: true, message: "Record successfully created!", data:{assistance_fareData: assistance_fareData,problem_typeData:problem_typeData,assistanceRequestData:assistanceRequestData,assistance_typeData:assistance_typeData}});
        }
        return res.status(200).send({status: false, message: "Some error occurred while creating the record!",data: []});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while creating record.",data: []});
    }

}
const updateAssistanceType = async (req, res) => {        
    try {      
        if (!req.body) {
            return res.status(400).send({
                message: "Data to update can not be empty!"
            });
        } 
        const id = req.params.id;  

        if(req.body.type == 'problem_type'){
            var assistance_typeDataUpdate = await problem_type.findByIdAndUpdate(id, req.body, { useFindAndModify: false });
        }else{
            var assistance_typeDataUpdate = await assistance_type.findByIdAndUpdate(id, req.body, { useFindAndModify: false });
        }
       
        if (assistance_typeDataUpdate) {             
            const problem_typeData = await getAllProblemType();        
            const assistanceRequestData = await getAllassistanceRequest();
            const assistance_fareData = await getAllassistanceFare();
            const assistance_typeData = await getAllassistanceType();  

            return res.status(200).send({status: true, message: "Data successfully updated!",data:{assistance_fareData: assistance_fareData,problem_typeData:problem_typeData,assistanceRequestData:assistanceRequestData,assistance_typeData:assistance_typeData}});
        }
        return res.status(200).send({status: false, message: "Cannot update record! with id=${id}. Maybe record! was not found!",data: []});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while updating record.",data: []});
    }

}
const deleteAssistanceType = async (req, res) => {        
    try {       
        const id = req.params.id;
        var assistance_typeDataDelete = await assistance_type.findByIdAndRemove(id);
        if (assistance_typeDataDelete) { 

            const problem_typeData = await getAllProblemType();        
            const assistanceRequestData = await getAllassistanceRequest();
            const assistance_fareData = await getAllassistanceFare();
            const assistance_typeData = await getAllassistanceType();

            return res.status(200).send({status: true, message: "Record successfully deleted!", data:{assistance_fareData: assistance_fareData,problem_typeData:problem_typeData,assistanceRequestData:assistanceRequestData,assistance_typeData:assistance_typeData}});
          }
          return res.status(200).send({status: false, message: "Cannot delete Record! with id=${id}. Maybe Record! was not found!",data: []});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while removing Record.",data: []});
    }

}

const deleteProblemType = async (req, res) => {        
    try {       
        const id = req.params.id;
        var problem_typeDataDelete = await problem_type.findByIdAndRemove(id);

        if (problem_typeDataDelete) { 

            const problem_typeData = await getAllProblemType();        
            const assistanceRequestData = await getAllassistanceRequest();
            const assistance_fareData = await getAllassistanceFare();
            const assistance_typeData = await getAllassistanceType();
            return res.status(200).send({status: true, message: "Record successfully deleted!",data:{assistance_fareData: assistance_fareData,problem_typeData:problem_typeData,assistanceRequestData:assistanceRequestData,assistance_typeData:assistance_typeData}});
          }
          return res.status(200).send({status: false, message: "Cannot delete Record! with id=${id}. Maybe Record! was not found!",data: []});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while removing Record.",data: []});
    }

}


// api for assistance Fare
const getAllAssistanceFare = async (req, res) => {  
    try {       
        const citiesData = await cities.find().sort({ name: 1 });
        if(citiesData){
            citiesData.filter(function(val,index){
                return val.key = index;
            })
        }
        
        const assistance_fareData = await assistance_fare.find().populate('city_id').populate('assistance_type_id').populate('problem_type_id').sort({ created: -1 });
        if(assistance_fareData){
            assistance_fareData.filter(function(val,index){
                return val.key = index;
            })
        }

        return res.status(200).send({status: true, message: "Data Found", data: assistance_fareData,citiesData: citiesData});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while getting all data of cities.",data: []});
    }
}
const getDetailsAssistanceFare = async (req, res) => {  
    try {       
        const assistance_fareData = await assistance_fare.findOne(req._id).populate('city_id').populate('assistance_type_id').populate('problem_type_id');
        return res.status(200).send({status: true, message: "Data Found", data: assistance_fareData});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while getting all data of assistance request!.",data: []});
    }

}
const createAssistanceFare = async (req, res) => {
    try { 
        if (!req.body) {
            return res.status(400).send({
                message: "Data to update can not be empty!"
            });
        } 
        const assistance_fareSendData = new assistance_fare(req.body);             
        const assistanceData = await assistance_fareSendData.save();
        
        if (assistanceData) { 
            const problem_typeData = await getAllProblemType();        
            const assistanceRequestData = await getAllassistanceRequest();
            const assistance_fareData = await getAllassistanceFare();
            const assistance_typeData = await getAllassistanceType();

            return res.status(201).send({status: true, message: "Assistance Fare successfully created!", data:{assistance_fareData: assistance_fareData,problem_typeData:problem_typeData,assistanceRequestData:assistanceRequestData,assistance_typeData:assistance_typeData}});
        }
        return res.status(200).send({status: false, message: "Some error occurred while creating the Assistance Fare!",data: []});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while creating Assistance Fare.",data: []});
    }

}
const updateAssistanceFare = async (req, res) => {        
    try {      
        if (!req.body) {
            return res.status(400).send({
                message: "Data to update can not be empty!"
            });
        } 
        const id = req.params.id;  
        const assistance_fareDataUpdate = await assistance_fare.findByIdAndUpdate(id, req.body, { useFindAndModify: false });
       
        if (assistance_fareDataUpdate) { 
            const problem_typeData = await getAllProblemType();        
            const assistanceRequestData = await getAllassistanceRequest();
            const assistance_fareData = await getAllassistanceFare();
            const assistance_typeData = await getAllassistanceType();       
            return res.status(200).send({status: true, message: "Assistance fare successfully updated!", data:{assistance_fareData: assistance_fareData,problem_typeData:problem_typeData,assistanceRequestData:assistanceRequestData,assistance_typeData:assistance_typeData}});
        }
        return res.status(200).send({status: false, message: "Cannot update Assistance fare! with id=${id}. Maybe Assistance! was not found!",data: []});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while updating Assistance fare.",data: []});
    }

}
const deleteAssistanceFare = async (req, res) => {        
    try {       
        const id = req.params.id;
        const assistance_fareDataDelete = await assistance_fare.findByIdAndRemove(id);
        if (assistance_fareDataDelete) { 
            
            const problem_typeData = await getAllProblemType();        
            const assistanceRequestData = await getAllassistanceRequest();
            const assistance_fareData = await getAllassistanceFare();
            const assistance_typeData = await getAllassistanceType();

            return res.status(200).send({status: true, message: "Assistance Fare successfully deleted!", data:{assistance_fareData: assistance_fareData,problem_typeData:problem_typeData,assistanceRequestData:assistanceRequestData,assistance_typeData:assistance_typeData}});
          }
          return res.status(200).send({status: false, message: "Cannot delete Assistance Fare! with id=${id}. Maybe Assistance  fare! was not found!",data: []});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while removing Assistance fare.",data: []});
    }

}

module.exports = { getAll,getAllWithFilter,getDetails,create,update,deleteRecord,getAllAssistanceType,getDetailsAssistanceType,createAssistanceType,updateAssistanceType,deleteAssistanceType,getAllAssistanceFare,getDetailsAssistanceFare,createAssistanceFare,updateAssistanceFare,deleteAssistanceFare ,deleteProblemType};