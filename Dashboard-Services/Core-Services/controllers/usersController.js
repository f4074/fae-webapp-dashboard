require("dotenv").config();
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const {User,User_document} = require("../models/user");
const Hub = require("../models/hub");
const cities = require("../models/cities");
const ObjectId = require('mongodb').ObjectId;
const auth = require("../middleware/auth");
const bcrypt = require("bcryptjs");
const sendEmail = require("../common/sendMail")
var hex = /[0-9A-Fa-f]{6}/g;


const getAllUsers = async (req, res) => {     
  try {       
      const allCities = await cities.find().sort({ name: 1 });;
      // const UsersList = await User.find().sort({ created: -1 });
      const UsersList = await User.aggregate([
        {
            $sort:{created: -1}
        },
        { $lookup:
            {
               from: "user_document",
               localField: "user_id",
               foreignField: "_id",
               as: "userDetails"
            }
        }
       
      ])
      const allHubs = [];
      if(UsersList){
        UsersList.filter(function(val,index){
            return val['key']=index;
        })
      }
      
      return res.status(200).send({status: true, message: "Data Found", data: {'allHubs':allHubs,'citiesList':allCities,'UsersList':UsersList}});
  
  } catch (err) {
      console.log(err);
      return res.status(500).send({status: false, message: err.message || "Some error occurred while getting all data of Users.",data: []});
  }

}
const getAllUsersFilter = async (req, res) => {     
  try {      
    const filterArray = req.body; 
    var matchArr =[];
    if(filterArray != null){
      const city_id_filter = filterArray.city_id;
      const hub_id_filter = filterArray.hub_id;
      const searchKey_filter = filterArray.searchKey;

      if(city_id_filter){        
          var City_id_idmatch =  {"city_id._id":ObjectId(city_id_filter)}
          matchArr.push(City_id_idmatch);
      }
      if(hub_id_filter){        
          var hub_id_idmatch =  {"hub_id._id":ObjectId(hub_id_filter)}
          matchArr.push(hub_id_idmatch); 
      }
      if(searchKey_filter){        
        var searchKey_match =  {$or:[{"name":searchKey_filter},{"phone":searchKey_filter},{"email":searchKey_filter}]}
        matchArr.push(searchKey_match); 
      }
    } 

    var matchfilter ={};
    if(matchArr.length >0){
        matchfilter = {$and: matchArr} 
    } 
    const allCities = await cities.find().sort({ name: 1 });;
    // const UsersList = await User.find().sort({ created: -1 });
    const UsersList = await User.aggregate([
      {
          $sort:{created: -1}
      },
      { $lookup:
        {
            from: "cities",
            localField: "city_id",
            foreignField: "_id",
            as: "city_id"
        }
      },
      { 
          $unwind: '$city_id'
      },
      { $lookup:
        {
            from: "hubs",
            localField: "hub_id",
            foreignField: "_id",
            as: "hub_id"
        }
      },
      // { 
      //     $unwind: '$hub_id'
      // },
      { $lookup:
          {
              from: "user_documents",
              localField: "_id",
              foreignField: "user_id",
              as: "userDetails"
          }
      },
      { 
        $unwind: '$userDetails'
      },
      { 
        $match:matchfilter 
      },
      
    ])
    const allHubs = [];
    if(UsersList){
      UsersList.filter(function(val,index){
          return val['key']=index;
      })
    }
    
    return res.status(200).send({status: true, message: "Data Found", data: {'allHubs':allHubs,'citiesList':allCities,'UsersList':UsersList}});
  
  } catch (err) {
      console.log(err);
      return res.status(500).send({status: false, message: err.message || "Some error occurred while getting all data of Users.",data: []});
  }

}


const getAllKYCRequestUsers = async (req, res) => {     
  try {  
      const kyc_status_filter = req.body.kyc_status_filter;
      const searchKey = req.body.searchKey;
      const city_id_filter = req.body.city_id;
      const hub_id_filter = req.body.hub_id;

      var matchArr =  [{$or:[{kyc_status:{$in:['0','1','2']}}]}];
      // matchArr.push({profile_verified: true});
      if(searchKey != "" ){
        matchArr.push({phone: searchKey});
        
      }
      if(kyc_status_filter != "" ){
        matchArr.push({kyc_status: kyc_status_filter});
        
      }
      if(city_id_filter){
        var City_id_idmatch =  {"city_id._id":ObjectId(city_id_filter)}
        matchArr.push(City_id_idmatch);
      }
      if(hub_id_filter){        
        var hub_id_idmatch =  {"hub_id._id":ObjectId(hub_id_filter)}
        matchArr.push(hub_id_idmatch);        
      }
      // else{
      //   var matchArr = {$or:[{kyc_status:{$in:['0','2']}}]}
      // }
      var matchfilter ={};
      if(matchArr.length >0){
          matchfilter = {$and: matchArr} 
      }
      console.log('matchfilter',matchfilter)
      const requestUsersList = await User.aggregate([
        {
            $sort:{created: -1}
        },
        { $lookup:
            {
               from: "user_documents",
               localField: "_id",
               foreignField: "user_id",
               as: "userDetails"
            }
        },
        { 
          $unwind: '$userDetails'
        },
        { $lookup:
          {
              from: "cities",
              localField: "city_id",
              foreignField: "_id",
              as: "city_id"
          }
        },
        { 
          $unwind: '$city_id'
        },
        { $lookup:
          {
              from: "hubs",
              localField: "hub_id",
              foreignField: "_id",
              as: "hub_id"
          }
        },
        // { 
        //   $unwind: '$hub_id'
        // },
        {
          $match:matchfilter
        }     
      ])
      console.log('requestUsersList',requestUsersList)
      if(requestUsersList){
          requestUsersList.filter(function(val,index){
              return val['key']=index;
          })
      }
      const allCities = await cities.find().sort({ name: 1 });;
      return res.status(200).send({status: true, message: "Data Found", data:requestUsersList,citylist:allCities});
  
  } catch (err) {
      console.log(err);
      return res.status(500).send({status: false, message: err.message || "Some error occurred while getting all data of Users.",data: []});
  }

}
const getUsersKYC = async (req, res) => {     
  try {  
      const user_id = req.params.id;       
      const UsersKYCList = await User_document.findOne({"user_id":user_id}).populate('user_id'); 
           
      return res.status(200).send({status: true, message: "Data Found", data:UsersKYCList});
  
  } catch (err) {
      console.log(err);
      return res.status(500).send({status: false, message: err.message || "Some error occurred while getting all data of Users.",data: []});
  }

}

const updateUserDetails= async (req, res) => {   
  try {      
      if (!req.body) {
          return res.status(400).send({
              message: "Data to update can not be empty!"
          });
      } 
      const id = req.params.id;  
      const data = await User.findByIdAndUpdate(id, req.body);
      
      if(!data) {           
        return res.status(200).send({status: false, message: "Cannot update User details! with id=${id}. Maybe Hub! was not found!",data: []});
      }      
      const allCities = await cities.find().sort({ name: 1 });;
      // const UsersList = await User.find().sort({ created: -1 });
      const UsersList = await User.aggregate([
        {
            $sort:{created: -1}
        },
        { $lookup:
            {
                from: "user_document",
                localField: "user_id",
                foreignField: "_id",
                as: "userDetails"
            }
        }
        
      ])
      if(UsersList){
        UsersList.filter(function(val,index){
            return val['key']=index;
        })
      }
      const allKycRequestData = await User.find({kyc_status: '0'}).sort({created: -1})
     
      return res.status(200).send({status: true, message: "Users successfully updated!", data: {'data':data,'allKycRequestData':allKycRequestData,'citiesList':allCities,'UsersList':UsersList}});
    

  } catch (err) {
      console.log(err);
      return res.status(500).send({status: false, message: err.message || "Some error occurred while updating User.",data: []});
  } 
}
const getAllKYCUsers = async (req, res) => {     
  try {  
      const allCities = await cities.find().sort({ name: 1 });
      const UsersKYCList = await User_document.find().populate('user_id');      
      return res.status(200).send({status: true, message: "Data Found", data:UsersKYCList, allCities:allCities});
  
  } catch (err) {
      console.log(err);
      return res.status(500).send({status: false, message: err.message || "Some error occurred while getting all data of Hub.",data: []});
  }
}
const deleteUser = async (req, res) => { 
  try {      
    
    const id = req.params.id;  
    const data = await User.findByIdAndRemove(id);
    if (!data) {         
        return res.status(200).send({status: false, message: "Cannot delete User ! with id=${id}. Maybe user! was not found!",data: []});
    }

    const allCities = await cities.find().sort({ name: 1 });;
    // const UsersList = await User.find().sort({ created: -1 });
    const UsersList = await User.aggregate([
      {
          $sort:{created: -1}
      },
      { $lookup:
          {
             from: "user_document",
             localField: "user_id",
             foreignField: "_id",
             as: "userDetails"
          }
      }
     
    ])
    if(UsersList){
      UsersList.filter(function(val,index){
          return val['key']=index;
      })
    }
    return res.status(200).send({status: true, message: "Users successfully deleted!", data: {'data':data,'citiesList':allCities,'UsersList':UsersList}});
    

  } catch (err) {
    console.log(err);
    return res.status(500).send({status: false, message: err.message || "Some error occurred while updating User.",data: []});
  }
}
const fotgot_password = async (req, res) => {
  try {
    const { email} = req.body;
    console.log('email',email)
    if (!(email)) {
      return res.status(400).send({ status: 'false', message: "Please Send email." });
    }
    const user = await User.findOne({ email:email });
    if(!user) {
      return res.status(200).send({status: false, message: "User does't Exist!"});
    }
    const otp = Math.floor(100000 + Math.random() * 900000);
    const genotp = otp.toString()
    const wherecon = {'_id':user._id }
    const updvalues = { $set: { otp: genotp } };
    const otpupd = await  User.updateOne(wherecon, updvalues);
    
    if(!otpupd){
      return res.status(200).send({status: false, message: "Something went wrong, please try again.", data: ''});
    }
    // sendgrid
    sendEmail({
      sendTo: user.email,
      subject: 'FaeBikes - Forgot Password OTP Verification',
      template: `<!DOCTYPE html>
      <html>
          <body>
              <p>Your Verification Code for FaeBikes is ${genotp}</p>
          </body>
      </html>`
    })
    
    // send password on mail
    // sendEmail({
    //   sendTo: email,
    //   subject: 'FaeBikes - Forgot Password ',
    //   template: `<!DOCTYPE html>
    //   <html>
    //       <body>
    //           <p>Your Password is ${user.password}</p>
    //       </body>
    //   </html>`
    // })
    return res.status(200).send({status: true, message: "Verification Code send your email.", data: user.password});

  } catch (err) {
    console.log(err);
  }
}

const resendotp = async (req, res) => {
  try {
    const { user_id} = req.body;
    if (!(user_id)) {
      return res.status(400).send({ status: 'false', message: "Please Send user id." });
    }
    const user = await User.findOne({ _id: (hex.test(user_id))? ObjectId(user_id) : user_id});
    if(!user) {
      return res.status(200).send({status: false, message: "User does't Exist!"});
    }
    const otp = Math.floor(100000 + Math.random() * 900000);
    const genotp = otp.toString()
    const wherecon = {'_id':user_id }
    const updvalues = { $set: { otp: genotp } };
    const otpupd = await  User.updateOne(wherecon, updvalues);

    if(!otpupd){
      return res.status(200).send({status: false, message: "Something went wrong, please try again.", data: ''});
    }
    // sendgrid
    sendEmail({
      sendTo: user.email,
      subject: 'FaeBikes - Login OTP Verification',
      template: `<!DOCTYPE html>
      <html>
          <body>
              <p>Your Verification Code for FaeBikes is ${genotp}</p>
          </body>
      </html>`
    })
    return res.status(200).send({status: true, message: "Verification Code send your register phone number/email.", data: genotp});

  } catch (err) {
    console.log(err);
  }
}
const verifyotp = async (req, res) => {
  try {
    console.log('req.body',req.body)
    const { email, otp } = req.body;
    
    if (!(email && otp)) {
      return res.status(400).send({ status: 'false', message: "Please enter OTP." });
    }
    const user = await User.findOne({ otp });
    if (user) {
      return res.status(200).send({status: true, message: "OTP successfully verified!.", data: user});
      // const wherecon = {'email':email, 'otp':otp }
      // const updvalues = { $set: { otp: 1 } };
      // const otpupd = await  User.updateOne(wherecon, updvalues);
      // if(otpupd.modifiedCount==1){
      //   const userrespons = await User.findOne({ email });       
      //   return res.status(200).send({status: true, message: "OTP successfully verified!.", data: userrespons});
      // }else {         
      //   return res.status(200).send({status: false, message: "Invalid OTP", data:user});
      // }           
    }else{
      return res.status(200).send({status: false, message: "Invalid OTP", data:user});
    }
    return res.status(200).send({status: false, message: "Invalid Email Id"});

  } catch (err) {
    console.log(err);
  }
}

const change_password = async (req, res) => {
  try {      
    if(!req.body) {
      return res.status(400).send({
          message: "Data to update can not be empty!"
      });
    } 
    const id = ObjectId(req.body.id);  
    const password = req.body.password;  

    const encryptedPassword = await bcrypt.hash(password, 10);
    console.log('encryptedPassword',encryptedPassword)
    const updateData = {
      password:encryptedPassword,
    }
 
    const data = await User.findByIdAndUpdate(id,updateData);
    
    if (!data) {  
        return res.status(200).send({status: false, message: "Cannot update User details! with id=${id}. Maybe User! was not found!",data: []});
    } 
    return res.status(200).send({status: true, message: "Users successfully updated!", data: {'data':[]}});
  

  } catch (err) {
      console.log(err);
      return res.status(500).send({status: false, message: err.message || "Some error occurred while updating User.",data: []});
  } 
}

module.exports = {getAllUsers,getAllUsersFilter,getUsersKYC,getAllKYCUsers,getAllKYCRequestUsers,updateUserDetails,deleteUser,resendotp,fotgot_password ,change_password,verifyotp};
