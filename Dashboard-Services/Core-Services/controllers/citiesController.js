require("dotenv").config();
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const cities = require("../models/cities");
const Hub = require("../models/hub");
const ObjectId = require('mongodb').ObjectId;
const auth = require("../middleware/auth");
const sendEmail = require("../common/sendMail")
const adminController = require("../controllers/adminController");
const hub = require("../models/hub");

const getAllCities = async (req, res) => {  
    try {  
         /* get current looged in admin details */ 
        const AdminDeatils = await adminController.getUserDetailsWithToken(req.headers["x-access-token"]);
        let admin_city_id  ="";
        let admin_hub_id  ="";
        if(AdminDeatils){
            const user_role  =AdminDeatils.user_role;
            admin_city_id  =AdminDeatils.city_id;
            admin_hub_id  =AdminDeatils.hub_id;
        }     

        // const citiesData = await cities.find({'_id':admin_city_id}).sort({ name: 1 });
        const citiesData = await cities.find().sort({ name: 1 });
        if(citiesData){
            citiesData.filter(function(val,index){
                return val['key']=index;
            })
        }
        return res.status(200).send({status: true, message: "Data Found", data: citiesData});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while getting all data of cities.",data: []});
    }
}
const getCityDetails = async (req, res) => {  
    try {       
        const cities = await cities.findOne(req._id);
        return res.status(200).send({status: true, message: "Data Found", data: cities});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while getting all data of business clients.",data: []});
    }

}

const createCity = async (req, res) => {
    try { 
        if (!req.body) {
            return res.status(400).send({
                message: "Data to update can not be empty!"
            });
        } 
        const checkcities = await cities.find({$or:[{"name":req.body.name},{"short_name":req.body.short_name},{"latitude":req.body.latitude},{"longitude":req.body.longitude}]});
        
        if(checkcities.length>0){
            return res.status(401).send({status: false, message: "City already exist!",data: []});
        }

        const citiesSendData = new cities(req.body);             
        const citiesDataData = await citiesSendData.save();
        if (citiesDataData) {         
          return res.status(200).send({status: true, message: "City successfully created!", data: citiesDataData});
        }
        return res.status(404).send({status: false, message: "Some error occurred while creating the city!",data: []});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while creating city.",data: []});
    }

}
const updateCity = async (req, res) => {        
    try {      
        if (!req.body) {
            return res.status(400).send({
                message: "Data to update can not be empty!"
            });
        } 
        const id = req.params.id;  
        const checkcities = await cities.find({$and:[{$or:[{"name":req.body.name},{"short_name":req.body.short_name},{"latitude":req.body.latitude},{"longitude":req.body.longitude}]},{"_id":{$ne:id}}]});

        if(checkcities.length>0){
            return res.status(401).send({status: false, message: "City already exist!",data: []});
        }

        const citiesData = await cities.findByIdAndUpdate(id, req.body, { useFindAndModify: false });
        if (citiesData) {
         
          return res.status(200).send({status: true, message: "City successfully updated!", data: citiesData});
        }
        return res.status(404).send({status: false, message: "Cannot update city! with id=${id}. Maybe city! was not found!",data: []});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while updating city.",data: []});
    }

}
const deleteCity = async (req, res) => {        
    try {       
        const id = req.params.id;
        const citiesData = await cities.findByIdAndRemove(id);
        if (citiesData) { 
            const hubRemoveData = await hub.remove({'city_id':id});        
            return res.status(200).send({status: true, message: "City successfully deleted!", data: citiesData});
        }
        return res.status(404).send({status: false, message: "Cannot delete city! with id=${id}. Maybe city! was not found!",data: []});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while removing city.",data: []});
    }

}

const deleteMultiple = async (req, res) => {        
    try { 
        const deleteJson = req.body;
        const data = await BusinessSuppliers.deleteMany(deleteJson);
        if (data) {         
            return res.status(200).send({status: true, message: `${data.deletedCount} city were deleted successfully!`, data: []});
          }
          return res.status(404).send({status: false, message: "Some error occurred while removing all city",data: []});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while removing all city.",data: []});
    }
}

module.exports = { getAllCities,getCityDetails,createCity,updateCity,deleteCity };