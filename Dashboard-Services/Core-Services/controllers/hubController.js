require("dotenv").config();
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const Hub = require("../models/hub");
const cities = require("../models/cities");
const ObjectId = require('mongodb').ObjectId;
const auth = require("../middleware/auth");
const bcrypt = require("bcryptjs");
const sendEmail = require("../common/sendMail")


const getAllHub = async (req, res) => {        
    try {       
        const allCities = await cities.find().sort({ name: 1 });        

        const data = await Hub.aggregate([
            {
                $sort:{ created: -1 }
            },
            { $lookup:
                {
                   from: "cities",
                   localField: "city_id",
                   foreignField: "_id",
                   as: "cityDetails"
                }
            },
            { 
                $unwind: '$cityDetails'
            },
            {
                $addFields: {
                    "city_name": "$cityDetails.name",
                    "city_lat": "$cityDetails.latitude",
                    "city_lng": "$cityDetails.longitude",
                    "city_id": "$cityDetails._id",
                }
            },
            {
                $project: {
                    _id: 1,
                    city_id:1,
                    title: 1,
                    description: 1,
                    isCentralHub: 1,
                    city_lat: 1,
                    city_lng: 1,
                    city_name: 1,
                    updated: 1,
                    created: 1
                }
            }
        ])
        
        // data.filter(function(val,index){
        //     return val['key']=index;
        // })
        // allCities.filter(function(val,index){
        //     return val['key']=index;
        // })
        return res.status(200).send({status: true, message: "Data Found", data: {'faeOpratedCities':data,'citiesList':allCities}});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while getting all data of Hub.",data: []});
    }

}

const getAllCityHubs =async(req, res) => {        
    try {  
        const city_id_get = req.params.city_id;      

        const data = await Hub.aggregate([
            {
                $match:{ city_id: ObjectId(city_id_get )}
            },
            {
                $sort:{ created: -1 }
            },
            { $lookup:
                {
                   from: "cities",
                   localField: "city_id",
                   foreignField: "_id",
                   as: "cityDetails"
                }
            },
            { 
                $unwind: '$cityDetails'
            },
            {
                $addFields: {
                    "city_name": "$cityDetails.name",
                    "city_lat": "$cityDetails.latitude",
                    "city_lng": "$cityDetails.longitude",
                    "city_id": "$cityDetails._id",
                }
            },
            {
                $project: {
                    _id: 1,
                    city_id:1,
                    title: 1,
                    description: 1,
                    isCentralHub: 1,
                    city_lat: 1,
                    city_lng: 1,
                    city_name: 1,
                    updated: 1,
                    created: 1
                }
            }
        ])
        if(data){
            data.filter(function(val,index){
                return val['key']=index;
            })
        }
        
        return res.status(200).send({status: true, message: "Data Found", data: data});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while getting details of Hub.",data: []});
    }
}

const getDetailsHub = async (req, res) => {        
    try {  
        const id = req.params.id;      
        const data = await Hub.findById(id);
        console.log('data',data)
        return res.status(200).send({status: true, message: "Data Found", data: data});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while getting details of Hub.",data: []});
    }

}
const createHub = async (req, res) => { 
           
    try { 
        if (!req.body) {
            return res.status(400).send({
                message: "Data to update can not be empty!"
            });
        } 

        const hub = new Hub(req.body);
             
        const data = await hub.save();
        console.log('data',data)
        if (data) { 
            const dataNew = await Hub.aggregate([
                { $match: { _id: data._id } },
                { $lookup:
                    {
                       from: "cities",
                       localField: "city_id",
                       foreignField: "_id",
                       as: "cityDetails"
                    }
                },
                { 
                    $unwind: '$cityDetails'
                },
                {
                    $addFields: {
                        "city_name": "$cityDetails.name",
                        "city_lat": "$cityDetails.latitude",
                        "city_lng": "$cityDetails.longitude",
                        "city_id": "$cityDetails._id",
                    }
                },
                {
                    $project: {
                        _id: 1,
                        city_id:1,
                        title: 1,
                        description: 1,
                        isCentralHub: 1,
                        city_lat: 1,
                        city_lng: 1,
                        city_name: 1,
                        updated: 1,
                        created: 1
                    }
                }
            ])
            return res.status(200).send({status: true, message: "Hub successfully created!", data: dataNew[0]});
        }
        return res.status(200).send({status: false, message: "Some error occurred while creating the Hub!",data: []});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while creating Hub.",data: []});
    }

}
const updateHub = async (req, res) => {        
    try {      
        if (!req.body) {
            return res.status(400).send({
                message: "Data to update can not be empty!"
            });
        } 
        const id = req.params.id;  
        const data = await Hub.findByIdAndUpdate(id, req.body);
        console.log('data',data)
        if (!data) {         
            return res.status(200).send({status: false, message: "Cannot update Hub! with id=${id}. Maybe Hub! was not found!",data: []});
        }

        return res.status(200).send({status: true, message: "Hub successfully updated!", data: data});
       
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while updating Hub.",data: []});
    }

}
const deleteHub = async (req, res) => {        
    try {       
        const id = req.params.id;
        const data = await Hub.findByIdAndRemove(id);
        if (data) {         
            return res.status(200).send({status: true, message: "Hub successfully deleted!", data: data});
          }
          return res.status(404).send({status: false, message: "Cannot delete Hub! with id=${id}. Maybe Hub! was not found!",data: []});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while removing Hub.",data: []});
    }

}
const deleteMultipleHub = async (req, res) => {        
    try { 
        const deleteArray = req.body;
        const data = await Hub.deleteMany({_id: {$in: deleteArray}});
        if (data) {         
            return res.status(200).send({status: true, message: `${data.deletedCount} hubs were deleted successfully!`, data: []});
          }
          return res.status(404).send({status: false, message: "Some error occurred while removing all Hubs",data: []});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while removing all hubs.",data: []});
    }
}

module.exports = { getAllHub,getDetailsHub,getAllCityHubs,createHub,updateHub,deleteHub,deleteMultipleHub };