require("dotenv").config();
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const Hub = require("../models/hub");
const cities = require("../models/cities");
const hub = require("../models/hub");
const vehicle = require("../models/vehicles");
const spares = require("../models/spares");
const iot_devices = require("../models/iot_devices");
const business_suppliers = require("../models/business_suppliers");
const inventory = require("../models/inventory");
const spare = require("../models/spares");
const iot_device = require("../models/iot_devices");
const ObjectId = require('mongodb').ObjectId;
const auth = require("../middleware/auth");

const getAllInventory = async (req, res) => {     
    try {       
        // const allCities = await getAllCities();
        // const businessSupplierList = await getAllBusinessSupplier();
        // // const allHubs = await getAllHubs();
        // const allHubs = [];
        // const allVehicles = await getAllVehicles();
        // if(allVehicles){
        //     allVehicles.filter(function(val,i){
        //         return val.key = i;
        //     })
        // } 

        // const vahicleInventoryData = await inventory.aggregate([
        //     {
        //         $match:{$and:[{type:'vehicle'}]}
        //     },
        //     {
        //         $sort:{created: -1}
        //     },
        //     { $lookup:
        //         {
        //            from: "vehicles",
        //            localField: "vehicle_id",
        //            foreignField: "_id",
        //            as: "vehicleDetails"
        //         }
        //     },
        //     { 
        //         $unwind: '$vehicleDetails'
        //     },
        //     {
        //         $addFields: {
        //             "business_supplier": "$vehicleDetails.business_supplier",
        //             "vehicle_name": "$vehicleDetails.vehicle_name",
        //             "vehicle_model": "$vehicleDetails.vehicle_model",
        //             "vehicle_regNo": "$vehicleDetails.vehicle_regNo",
        //             "vehicle_id": "$vehicleDetails._id",
        //             // "created": "$vehicleDetails.created",
        //             // "updated": "$vehicleDetails.updated"
        //         }
        //     },
        //     {
        //         $project: {
        //             _id: 1,
        //             city_id:1,
        //             hub_id:1,
        //             to_city_id:1,
        //             to_hub_id:1,
        //             vehicle_id:1,
        //             business_supplier:1,
        //             vehicle_name: 1,
        //             vehicle_model: 1,
        //             vehicle_regNo: 1,
        //             status: 1,
        //             assignedTo: 1,
        //             updated: 1,
        //             created: 1
        //         }
        //     }
        // ])

        // const sparesInventoryData = await inventory.aggregate([
        //     {
        //         $match:{type:'spares'}
        //     },
        //     {
        //         $sort:{created: -1}
        //     },
        //     { $lookup:
        //         {
        //            from: "spares",
        //            localField: "spares_id",
        //            foreignField: "_id",
        //            as: "sparesDetails"
        //         }
        //     },
        //     { 
        //         $unwind: '$sparesDetails'
        //     },
        //     {
        //         $addFields: {
        //             "part_number": "$sparesDetails.part_number",
        //             "quantity": "$sparesDetails.quantity",
        //             "model": "$sparesDetails.model",
        //             "supplier_description": "$sparesDetails.supplier_description",
        //             "mrp": "$sparesDetails.mrp",
        //             "spares_id": "$sparesDetails._id",
        //             "dealer_sale_price": "$sparesDetails.dealer_sale_price",
        //         }
        //     },
        //     {
        //         $project: {
        //             _id: 1,
        //             city_id:1,
        //             hub_id:1,
        //             to_city_id:1,
        //             to_hub_id:1,
        //             spares_id:1,
        //             part_number:1,
        //             quantity: 1,
        //             model: 1,
        //             supplier_description: 1,
        //             mrp: 1,
        //             dealer_sale_price: 1,
        //             status: 1,
        //             assignedTo: 1,
        //             updated: 1,
        //             created: 1
        //         }
        //     }
        // ])

        // const iotDevicesInventoryData = await inventory.aggregate([
        //     {
        //         $match:{type:'iot_devices'}
        //     },
        //     {
        //         $sort:{created: -1}
        //     },
        //     { $lookup:
        //         {
        //            from: "iot_devices",
        //            localField: "iot_device_id",
        //            foreignField: "_id",
        //            as: "iot_devicesDetails"
        //         }
        //     },
        //     { 
        //         $unwind: '$iot_devicesDetails'
        //     },
        //     {
        //         $addFields: {
        //             "business_supplier": "$iot_devicesDetails.business_supplier",
        //             "device_serial_number": "$iot_devicesDetails.device_serial_number",
        //             "make": "$iot_devicesDetails.make",
        //             "model_of_device": "$iot_devicesDetails.model_of_device",
        //             "imei": "$iot_devicesDetails.imei",
        //             "provided_by": "$iot_devicesDetails.provided_by",
        //             "consignmentTrackingNumber_CourierDocket": "$iot_devicesDetails.consignmentTrackingNumber_CourierDocket",
        //             "CustomerOrderNumber": "$iot_devicesDetails.CustomerOrderNumber",
        //             "DispatchDetailsRemarks": "$iot_devicesDetails.DispatchDetailsRemarks",
        //             "WarrantyStartdate": "$iot_devicesDetails.WarrantyStartdate",
        //             "Warrantyterms": "$iot_devicesDetails.Warrantyterms",
        //             "WarrantyEnddate": "$iot_devicesDetails.WarrantyEnddate",
        //         }
        //     },
        //     {
        //         $project: {
        //             _id: 1,
        //             city_id:1,
        //             hub_id:1,
        //             to_city_id:1,
        //             to_hub_id:1,
        //             iot_device_id:1,
        //             business_supplier:1,
        //             device_serial_number: 1,
        //             make: 1,
        //             model_of_device: 1,
        //             imei: 1,
        //             provided_by: 1,
        //             consignmentTrackingNumber_CourierDocket: 1,
        //             CustomerOrderNumber: 1,
        //             DispatchDetailsRemarks: 1,
        //             WarrantyStartdate:1,
        //             Warrantyterms: 1,
        //             WarrantyEnddate: 1,
        //             status: 1,
        //             assignedTo: 1,
        //             updated: 1,
        //             created: 1
        //         }
        //     }
        // ])

        // const inventoryData = await inventory.aggregate([
        //     {
        //         $sort:{created: -1}
        //     },
        //     { $lookup:
        //         {
        //            from: "vehicles",
        //            localField: "vehicle_id",
        //            foreignField: "_id",
        //            as: "vehicleDetails"
        //         }
        //     },
        //     { 
        //         $unwind: '$vehicleDetails'
        //     },
        //     {
        //         $addFields: {
        //             "business_supplier": "$vehicleDetails.business_supplier",
        //             "vehicle_name": "$vehicleDetails.vehicle_name",
        //             "vehicle_model": "$vehicleDetails.vehicle_model",
        //             "vehicle_regNo": "$vehicleDetails.vehicle_regNo",
        //             "vehicle_id": "$vehicleDetails._id",
        //             // "created": "$vehicleDetails.created",
        //             // "updated": "$vehicleDetails.updated"
        //         }
        //     },
        //     {
        //         $project: {
        //             _id: 1,
        //             city_id:1,
        //             hub_id:1,
        //             vehicle_id:1,
        //             business_supplier:1,
        //             vehicle_name: 1,
        //             vehicle_model: 1,
        //             vehicle_regNo: 1,
        //             status: 1,
        //             assignedTo: 1,
        //             updated: 1,
        //             created: 1
        //         }
        //     }
        // ])

        // if(vahicleInventoryData){
        //     vahicleInventoryData.filter(function(val,i){
        //         return val.key = i;
        //     });
        // }
        // if(sparesInventoryData){
        //     sparesInventoryData.filter(function(val,i){
        //         return val.key = i;
        //     });
        // }

        // if(iotDevicesInventoryData){
        //     iotDevicesInventoryData.filter(function(val,i){
        //         return val.key = i;
        //     });
        // }
        // if(inventoryData){
        //     inventoryData.filter(function(val,i){
        //         return val.key = i;
        //     })
        // }      
        
        // return res.status(200).send({status: true, message: "Data Found", data: {'allVehicles':allVehicles,'allHubs':allHubs,'citiesList':allCities,'businessSupplierList':businessSupplierList,'inventoryData':inventoryData,'vahicleInventoryData':vahicleInventoryData,'sparesInventoryData':sparesInventoryData,'iotDevicesInventoryData':iotDevicesInventoryData}});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while getting all data of Hub.",data: []});
    }

}

module.exports = { getAllInventory};