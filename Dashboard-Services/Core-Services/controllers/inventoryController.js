require("dotenv").config();
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const Hub = require("../models/hub");
const cities = require("../models/cities");
// const hub = require("../models/hub");
const vehicle = require("../models/vehicles");
//const spares = require("../models/spares");
const iot_devices = require("../models/iot_devices");
const business_suppliers = require("../models/business_suppliers");
const inventory = require("../models/inventory");
const inventory_history = require("../models/inventory_history");
const spare = require("../models/spares");
const bookings = require("../models/bookings");
const ObjectId = require('mongodb').ObjectId;
const auth = require("../middleware/auth");
const adminController = require("../controllers/adminController");
const vehicles = require("../models/vehicles");
var hex = /[0-9A-Fa-f]{6}/g;

const getAllInventory = async (req, res) => {     
    try {       
        const allCities = await getAllCities();
        const businessSupplierList = await getAllBusinessSupplier();
        // const allHubs = await getAllHubs();
        const allHubs = [];
        const allVehicles = await getAllVehicles();
        if(allVehicles){
            allVehicles.filter(function(val,i){
                return val.key = i;
            })
        } 
        var matchArrVehicle = [{ type: { $eq: "vehicle" }}];    
        var matchArrSpares = [{ type: { $eq: "spares" }}];    
        var matchArrIotDevices = [{ type: { $eq: "iot_devices" }}];

         /* get all vehicles list with filter */
         const vahicleInventoryData = await getAllInventoryWithType('vehicle',matchArrVehicle);
            
         /* get all spares list with filter */
         const sparesInventoryData = await getAllInventoryWithType('spares',matchArrSpares);
        
         /* get all iot devices list with filter */           
         const iotDevicesInventoryData = await getAllInventoryWithType('iot_devices',matchArrIotDevices);

        const inventoryData = await inventory.aggregate([
            {
                $sort:{created: -1}
            },
            { $lookup:
                {
                   from: "vehicles",
                   localField: "vehicle_id",
                   foreignField: "_id",
                   as: "vehicleDetails"
                }
            },
            { 
                $unwind: '$vehicleDetails'
            },
            {
                $addFields: {
                    "business_supplier": "$vehicleDetails.business_supplier",
                    "vehicle_name": "$vehicleDetails.vehicle_name",
                    "vehicle_model": "$vehicleDetails.vehicle_model",
                    "vehicle_regNo": "$vehicleDetails.vehicle_regNo",
                    "vehicle_id": "$vehicleDetails._id",
                    // "created": "$vehicleDetails.created",
                    // "updated": "$vehicleDetails.updated"
                }
            },
            {
                $project: {
                    _id: 1,
                    city_id:1,
                    hub_id:1,
                    vehicle_id:1,
                    business_supplier:1,
                    vehicle_name: 1,
                    vehicle_model: 1,
                    vehicle_regNo: 1,
                    status: 1,
                    assignedTo: 1,
                    updated: 1,
                    created: 1
                }
            }
        ])
        if(inventoryData){
            inventoryData.filter(function(val,i){
                return val.key = i;
            })
        } 
        
        return res.status(200).send({status: true, message: "Data Found", data: {'allVehicles':allVehicles,'allHubs':allHubs,'citiesList':allCities,'businessSupplierList':businessSupplierList,'inventoryData':inventoryData,'vahicleInventoryData':vahicleInventoryData,'sparesInventoryData':sparesInventoryData,'iotDevicesInventoryData':iotDevicesInventoryData}});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while getting all data of Hub.",data: []});
    }

}
const getAllInventoryFilter = async (req, res) => { 
    const filterData = req.body;
    const business_supplier_id =filterData.business_supplier_id;
    const city_id =filterData.city_id;
    const hub_id =filterData.hub_id;
    const vehicle_id =filterData.vehicle_id;
    const searchKey =filterData.searchKey;
    const filter_type =filterData.filter_type;  
    const vehicle_name =filterData.vehicle_name;
    const part_type_filter =filterData.part_type_filter;

    /* get current looged in admin details */ 
    const AdminDeatils = await adminController.getUserDetailsWithToken(req.headers["x-access-token"]);
    let admin_city_id  ="";
    let admin_hub_id  ="";
    
    if(AdminDeatils){
        const user_role  =AdminDeatils.user_role;
        admin_city_id  =AdminDeatils.city_id;
        admin_hub_id  =AdminDeatils.hub_id;
    }

    // var matchArrVehicle = [{"type":'vehicle'}];    
    // var matchArrVehicle = [{ book_status: 0}];    
    var matchArrVehicle = [];    
    var matchArrVehicleInventory = [{ type: { $eq: "vehicle" }}];    
    var matchArrSpares = [{ type: { $eq: "spares" }}];    
    var matchArrIotDevices = [{ type: { $eq: "iot_devices" }}];    
    if(searchKey){
        if(filter_type == 'spares'){
            var Keymatch =  {"sparesDetails.part_number": searchKey}
            matchArrSpares.push(Keymatch);  

        }else if(filter_type == 'iot_devices'){
            var Keymatch =  {"iot_devicesDetails.device_serial_number":searchKey}
            matchArrIotDevices.push(Keymatch);

        }else  if(filter_type == 'vehicle'){
            var Keymatch =  {"vehicleDetails.vehicle_regNo": {$eq:searchKey}}
            matchArrVehicleInventory.push(Keymatch);
        }
    }

    if(part_type_filter && filter_type == 'spares'){
        var part_type_filterMatch =  {"sparesDetails.part_type": part_type_filter}
        matchArrSpares.push(part_type_filterMatch);
    }
    

    var hex = /[0-9A-Fa-f]{6}/g;
    if(vehicle_id){
        var vehicle_idmatch =  {vehicle_id: (hex.test(vehicle_id))? ObjectId(vehicle_id) : vehicle_id}
        matchArrVehicleInventory.push(vehicle_idmatch);
        matchArrSpares.push(vehicle_idmatch);
        matchArrIotDevices.push(vehicle_idmatch);
    }
    if(business_supplier_id){        
        var business_supplier_id_idmatch =  {supplier_id: (hex.test(business_supplier_id))? ObjectId(business_supplier_id) : business_supplier_id}
        matchArrVehicleInventory.push(business_supplier_id_idmatch);        
        matchArrSpares.push(business_supplier_id_idmatch);        
        matchArrIotDevices.push(business_supplier_id_idmatch);        
    }   
    if(city_id){
        var city_idmatch =  {city_id: (hex.test(city_id))? ObjectId(city_id) : city_id}
        // var city_idmatch =  {$and:[{city_id: (hex.test(city_id))? ObjectId(city_id) : city_id},{$or:[{to_city_id:{$eq:(hex.test(city_id))? ObjectId(city_id) : city_id}},{to_city_id:null}]}]}
        matchArrVehicleInventory.push(city_idmatch);
        matchArrVehicle.push( {city:city_id});
        matchArrSpares.push(city_idmatch);
        matchArrIotDevices.push(city_idmatch);
    }
    if(hub_id){
        var hub_idmatch =  {hub_id:  (hex.test(hub_id))? ObjectId(hub_id) : hub_id}
        // var hub_idmatch =  {$and:[{hub_id:  (hex.test(hub_id))? ObjectId(hub_id) : hub_id},{$or:[{to_hub_id:{$eq:(hex.test(hub_id))? ObjectId(hub_id) : hub_id}},{to_hub_id:null}]}]}
        matchArrVehicleInventory.push(hub_idmatch);
        matchArrVehicle.push({hub: hub_id});
        matchArrSpares.push(hub_idmatch);
        matchArrIotDevices.push(hub_idmatch);
    }
        
    
    // return res.status(200).send({status: true, message: "Data Found", data: {'allHubs':[],'citiesList':[],'inventoryData':[],'filterData':hex.test(business_supplier_id)}});
    
    try {  

        if(filter_type == 'vehicle_booking'){   
            let matchArrVehicleInventory = [];
            let matchArrVehicleInventoryName = [];
            if(vehicle_name){     
                matchArrVehicleInventory.push({"vehicleDetails.vehicle_name": vehicle_name});
            }     
            
            if(city_id){
                var city_idmatchinv =  {"vehicleDetails.city":city_id}
                var city_idmatch =  {"city":city_id}
                matchArrVehicleInventory.push(city_idmatchinv);
                matchArrVehicleInventoryName.push(city_idmatch);
                
            }
            if(hub_id){
                var hub_idmatchinv =  {"vehicleDetails.hub":hub_id}
                var hub_idmatch =  {"hub": hub_id}
                matchArrVehicleInventory.push(hub_idmatchinv);
                matchArrVehicleInventoryName.push(hub_idmatch);

            }
            /* get all vehicles list with filter */
            const vahicleInventoryData = await getAllInventoryBookingWithType('vehicle',matchArrVehicleInventory);                
            const vahicleNameInventoryData = await getAllInventoryBookingWithType('vehicle_name',matchArrVehicleInventoryName);
            
            return res.status(200).send({status: true, message: "Data Found", data: {vahicleInventoryData:vahicleInventoryData,vahicleNameInventoryData:vahicleNameInventoryData}});

        }else{

            const allCities = await getAllCities();
            const businessSupplierList = await getAllBusinessSupplier();
            const allVehicles = await getAllVehicles();
            
            // const allHubs = await getAllHubs();
            const allHubs = [];
            /* get all vehicles list with filter */
            const vahicleInventoryData = await getAllInventoryWithType('vehicle',matchArrVehicleInventory);
            
            /* get all spares list with filter */
            const sparesInventoryData = await getAllInventoryWithType('spares',matchArrSpares);
           
            /* get all iot devices list with filter */           
            const iotDevicesInventoryData = await getAllInventoryWithType('iot_devices',matchArrIotDevices);

             /* get all invetory list with filter */
             const inventoryData = [];
            /* const inventoryData = await inventory.aggregate([
                {
                    $sort:{created: -1}
                },
                { $lookup:
                    {
                    from: "vehicles",
                    localField: "vehicle_id",
                    foreignField: "_id",
                    as: "vehicleDetails"
                    }
                },
                { 
                    $unwind: '$vehicleDetails'
                },
                {
                    $addFields: {
                        "business_supplier": "$vehicleDetails.business_supplier",
                        "vehicle_name": "$vehicleDetails.vehicle_name",
                        "vehicle_model": "$vehicleDetails.vehicle_model",
                        "vehicle_regNo": "$vehicleDetails.vehicle_regNo",
                        "vehicle_id": "$vehicleDetails._id",
                        // "created": "$vehicleDetails.created",
                        // "updated": "$vehicleDetails.updated"
                    }
                },
                {
                    $project: {
                        _id: 1,
                        city_id:1,
                        hub_id:1,
                        vehicle_id:1,
                        business_supplier:1,
                        vehicle_name: 1,
                        vehicle_model: 1,
                        vehicle_regNo: 1,
                        status: 1,
                        assignedTo: 1,
                        updated: 1,
                        created: 1
                    }
                }
            ])

            if(inventoryData){
                inventoryData.filter(function(val,i){
                    return val.key = i;
                })
            } */  
            
            // let UpdateAllVehiclesSparesIotdevices = await checkAndUpdateAllVehiclesSparesIotdevices(); 

            return res.status(200).send({status: true, message: "Data Found", data: {'allVehicles':allVehicles,'allHubs':allHubs,'citiesList':allCities,'businessSupplierList':businessSupplierList,'inventoryData':inventoryData,'vahicleInventoryData':vahicleInventoryData,'sparesInventoryData':sparesInventoryData,'iotDevicesInventoryData':iotDevicesInventoryData,AdminDeatils:AdminDeatils}});

        }
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while getting all data of Hub.",data: []});
    }

}
const getDetailsInventory = async (req, res) => {        
    try {  
        const id = req.params.id;             
        const data = await inventory.findById(id).populate('vehicle_id').populate('spares_id').populate('iot_device_id').populate('city_id').populate('hub_id').populate('supplier_id'); 
        
        return res.status(200).send({status: true, message: "Data Found", data: data});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while getting details of Hub.",data: []});
    }

}

const getVehicleTimelineInventory = async (req, res) => {        
    try {  
        const id = req.params.id; 
                    // inventory_history
        // const data = await inventory.find({'_id':id}).populate('vehicle_id').populate('spares_id').populate('iot_device_id').populate('city_id').populate('hub_id');        
        // const data = await inventory_history.find({'inventory_id':id}).populate('inventory_id');
        
        const data = await inventory_history.aggregate([
            {
                $sort:{created: -1}
            },
            {
                $match:{inventory_id: ObjectId(id)}
            },
            {   
                $lookup:{
                    from: "inventories",
                    localField: "inventory_id",
                    foreignField: "_id",
                    as: "inventory_id"
                }
            },
            { 
                $unwind: '$inventory_id'
            },            
            { $lookup:
                {
                from: "cities",
                localField: "from_city_id",
                foreignField: "_id",
                as: "from_city_id"
                }
            },
            { 
                $unwind: '$from_city_id'
            },
            { $lookup:
                {
                from: "hubs",
                localField: "from_hub_id",
                foreignField: "_id",
                as: "from_hub_id"
                }
            },
            { 
                $unwind: '$from_hub_id'
            },
            { $lookup:
                {
                from: "cities",
                localField: "to_city_id",
                foreignField: "_id",
                as: "to_city_id"
                }
            },
            { 
                $unwind: '$to_city_id'
            },
            { $lookup:
                {
                from: "hubs",
                localField: "to_hub_id",
                foreignField: "_id",
                as: "to_hub_id"
                }
            },
            { 
                $unwind: '$to_hub_id'
            }
            
        ])

        if(data){
            data.filter(function(val,i){
                return val.key = i;
            })
        }
        return res.status(200).send({status: true, message: "Data Found", data: data});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while getting details of Hub.",data: []});
    }

}


const createInventory = async (req, res) => {            
    try { 
        if (!req.body) {
            return res.status(400).send({
                message: "Data to update can not be empty!"
            });
        } 
        const createType = req.body.createType;
        const business_supplier_id = req.body.business_supplier_id;
        const city_id = req.body.city_id;
        const hub_id = req.body.hub_id;
        const type = req.body.type; 

        if(createType == 'csv'){
            if(type =='spares'){
           
                const csv_file_data = req.body.csv_file_data;

                const fieldCount = Object.keys(csv_file_data[0]).length;

                if(fieldCount != 16)
                {
                    return res.status(200).send({status: false, message: "Invalid spares csv file, please check and upload again",data: []});
                }

                var dataAll = await csv_file_data.map(async function(item, i){
                    
                    const keys = Object.keys(item);
                    const values = Object.values(item);    
                   //  console.log('keys',keys[3])                
                    // console.log('values',values) 
                    
                    const send_data = {     
                        business_supplier: business_supplier_id,
                        // business_supplier: removeDoubleQuotes(values[0]),
                        // city:removeDoubleQuotes(values[1] ),
                        // hub:removeDoubleQuotes(values[2] ),
                        city:city_id,
                        hub:hub_id,
                        part_number:removeDoubleQuotes(values[3]),
                        supplier_description: removeDoubleQuotes(values[4]),
                        image_url:removeDoubleQuotes(values[5]),
                        quantity:removeDoubleQuotes(values[6]),
                        uom: removeDoubleQuotes(values[7]),
                        hsn:removeDoubleQuotes(values[8] ),
                        gst: removeDoubleQuotes(values[9]),
                        dealer_sale_price: removeDoubleQuotes(values[10]),
                        dlp:removeDoubleQuotes(values[11]),
                        sale_rate: removeDoubleQuotes(values[12]),
                        mrp:removeDoubleQuotes(values[13]),
                        model:removeDoubleQuotes(values[14]),
                        part_type:removeDoubleQuotes(values[15]),
                        assigned_status:0,                       
                        created:new Date().toISOString(),
                        updated:new Date().toISOString() 
                    }

                    if(send_data.city != undefined && values[0]!= "")
                    {
                        const checkSpares = await spare.find({$and:[{"part_number":{$eq:removeDoubleQuotes(values[3])}}]});
                           if(checkSpares.length >0){
                                const update_data_spare = {   
                                    _id:checkSpares[0]._id,  
                                    business_supplier: business_supplier_id,
                                    // business_supplier: removeDoubleQuotes(values[0]),
                                    // city:removeDoubleQuotes(values[1] ),
                                    // hub:removeDoubleQuotes(values[2] ),
                                    city:city_id,
                                    hub:hub_id,
                                    part_number:removeDoubleQuotes(values[3]),
                                    supplier_description: removeDoubleQuotes(values[4]),
                                    image_url:removeDoubleQuotes(values[5]),
                                    quantity:removeDoubleQuotes(values[6]),
                                    uom: removeDoubleQuotes(values[7]),
                                    hsn:removeDoubleQuotes(values[8] ),
                                    gst: removeDoubleQuotes(values[9]),
                                    dealer_sale_price: removeDoubleQuotes(values[10]),
                                    dlp:removeDoubleQuotes(values[11]),
                                    sale_rate: removeDoubleQuotes(values[12]),
                                    mrp:removeDoubleQuotes(values[13]),
                                    model:removeDoubleQuotes(values[14]),
                                    part_type:removeDoubleQuotes(values[15]),
                                    updated:new Date().toISOString() 
                                }
                                
                                let whereconSpare = {'$and':[{_id:checkSpares[0]._id},{business_supplier:business_supplier_id},{city:city_id},{hub:hub_id}] }
                                let updvaluesSpare = { $set: update_data_spare };
                                const dataupdateSpare = await  spare.updateOne(whereconSpare, updvaluesSpare);

                                // const dataupdateSpare = await spare.findByIdAndUpdate(checkSpares[0]._id, update_data_spare);   
                                return dataupdateSpare; 
                                
                            }else{
                                const spareSave = new spare(send_data);             
                                const data = await spareSave.save();    
                                return data; 
                            }
                    }                  
                });
            }else if(type =='iot_devices'){
                const csv_file_data = req.body.csv_file_data;

                 const fieldCount = Object.keys(csv_file_data[0]).length;

                if(fieldCount != 21)
                {
                    return res.status(200).send({status: false, message: "Invalid IOT devices csv file, please check and upload again",data: []});
                }


                var dataAll = await csv_file_data.map(async function(item, i){
                    const keys = Object.keys(item);
                    const values = Object.values(item);
                    const send_data = {     
                        business_supplier: business_supplier_id,
                            // business_supplier: removeDoubleQuotes(values[0]),
                            // city:removeDoubleQuotes(values[1] ),
                            // hub:removeDoubleQuotes(values[2] ),
                        city:city_id,
                        hub:hub_id,
                        device_serial_number:removeDoubleQuotes(values[3]),
                        make: removeDoubleQuotes(values[4]),
                        model_of_device:removeDoubleQuotes(values[5]),
                        imei:removeDoubleQuotes(values[6]),
                        provided_by: removeDoubleQuotes(values[7]),
                        consignmentTrackingNumber_CourierDocket:removeDoubleQuotes(values[8] ),
                        CustomerOrderNumber: removeDoubleQuotes(values[9]),
                        DispatchDetailsRemarks: removeDoubleQuotes(values[10]),
                        WarrantyStartdate:removeDoubleQuotes(values[11]),
                        sale_rate: removeDoubleQuotes(values[12]),
                        Warrantyterms:removeDoubleQuotes(values[13]) ,
                        WarrantyEnddate:removeDoubleQuotes(values[14] ),
                        VF_MSISDN:removeDoubleQuotes(values[15]) ,
                        VF_ICCID:removeDoubleQuotes(values[16] ),
                        VF_IMSI:removeDoubleQuotes(values[17] ),
                        circle:removeDoubleQuotes(values[18]) ,
                        history_remark:removeDoubleQuotes(values[19]) ,
                        device_status:removeDoubleQuotes(values[20]) ,
                        sim_status:removeDoubleQuotes(values[21]) ,
                        assigned_status:0, 
                        created:new Date().toISOString(),
                        updated:new Date().toISOString() 
                    }

                     if(send_data.city != undefined)
                       {
                            const checkDeviced = await iot_devices.find({$and:[{"device_serial_number":{$eq:removeDoubleQuotes(values[3])}}]});
                            if(checkDeviced.length >0){
                                const update_data_iot = {   
                                    _id:checkDeviced[0]._id,  
                                    business_supplier: business_supplier_id,
                                    // business_supplier: removeDoubleQuotes(values[0]),
                                    // city:removeDoubleQuotes(values[1] ),
                                    // hub:removeDoubleQuotes(values[2] ),
                                    city:city_id,
                                    hub:hub_id,
                                    device_serial_number:removeDoubleQuotes(values[3]),
                                    make: removeDoubleQuotes(values[4]),
                                    model_of_device:removeDoubleQuotes(values[5]),
                                    imei:removeDoubleQuotes(values[6]),
                                    provided_by: removeDoubleQuotes(values[7]),
                                    consignmentTrackingNumber_CourierDocket:removeDoubleQuotes(values[8] ),
                                    CustomerOrderNumber: removeDoubleQuotes(values[9]),
                                    DispatchDetailsRemarks: removeDoubleQuotes(values[10]),
                                    WarrantyStartdate:removeDoubleQuotes(values[11]),
                                    sale_rate: removeDoubleQuotes(values[12]),
                                    Warrantyterms:removeDoubleQuotes(values[13]) ,
                                    WarrantyEnddate:removeDoubleQuotes(values[14] ),
                                    VF_MSISDN:removeDoubleQuotes(values[15]) ,
                                    VF_ICCID:removeDoubleQuotes(values[16] ),
                                    VF_IMSI:removeDoubleQuotes(values[17] ),
                                    circle:removeDoubleQuotes(values[18]) ,
                                    history_remark:removeDoubleQuotes(values[19]) ,
                                    device_status:removeDoubleQuotes(values[20]) ,
                                    sim_status:removeDoubleQuotes(values[21]) ,
                                    updated:new Date().toISOString() 
                                }
                                let whereconIot= {'$and':[{_id:checkDeviced[0]._id},{business_supplier:business_supplier_id},{city:city_id},{hub:hub_id}] }
                                const updvaluesIot = { $set: update_data_iot };
                                const dataupdateIot = await  iot_devices.updateOne(whereconIot, updvaluesIot);

                                // const dataupdate = await iot_devices.findByIdAndUpdate(checkDeviced[0]._id, update_data);   
                                return dataupdateIot; 
                            }else{
                                const iot_devic = new iot_devices(send_data);             
                                const data = await iot_devic.save();    
                                return data; 
                            }   

                          
                       }  
                        
                });
            }else{

                const csv_file_data = req.body.csv_file_data;


                const fieldCount = Object.keys(csv_file_data[0]).length;
                if(fieldCount != 35)
                {
                    return res.status(200).send({status: false, message: "Invalid vehicle csv file, please check and upload again",data: []});
                }

                var dataAll = await csv_file_data.map(async function(item, i){
                    const keys = Object.keys(item);
                    const values = Object.values(item);

                    // check vehicle by registration number
                    const checkVehicle = await vehicle.find({$and:[{"vehicle_regNo":{$eq:removeDoubleQuotes(values[6])}}]});
                    // console.log('checkVehicle',checkVehicle)
                    if(checkVehicle.length>0){
                        const send_data_vehicle = { 
                            _id:checkVehicle[0]._id,    
                            business_supplier:business_supplier_id,
                            // business_supplier: removeDoubleQuotes(values[0]),
                            // city:removeDoubleQuotes(values[1] ),
                            // hub:removeDoubleQuotes(values[2] ),
                            city:city_id,
                            hub:hub_id,
                            vehicle_name:removeDoubleQuotes(values[3]),
                            vehicle_model: removeDoubleQuotes(values[4]),
                            vehicle_color:removeDoubleQuotes(values[5]),
                            vehicle_regNo:removeDoubleQuotes(values[6]),
                            manufacture_date: removeDoubleQuotes(values[7]),
                            date_of_sale:removeDoubleQuotes(values[8]) ,
                            date_of_use: removeDoubleQuotes(values[9]),
                            chassis_no: removeDoubleQuotes(values[10]),
                            moter_engine_no:removeDoubleQuotes(values[11]),
                            battery_no: removeDoubleQuotes(values[12]),
                            battery_no_2:removeDoubleQuotes(values[13]) ,
                            controller_number:removeDoubleQuotes(values[14] ),
                            charger_no:removeDoubleQuotes(values[15] ),
                            charger_no_2:removeDoubleQuotes(values[16]),
                            key_no:removeDoubleQuotes(values[17] ),
                            scooter_invoice:removeDoubleQuotes(values[18]),
                            scooter_reg_date:removeDoubleQuotes(values[19]) ,
                            scooter_reg_upto:removeDoubleQuotes(values[20]),
                            insurance_company:removeDoubleQuotes(values[21]),
                            policy_number_insurance:removeDoubleQuotes(values[22]) ,
                            date_policy_expiry:removeDoubleQuotes(values[23]),                
                            permit_no:removeDoubleQuotes(values[24]),
                            exp_date:removeDoubleQuotes(values[25]),
                            sgst:removeDoubleQuotes(values[26]),
                            cgst: removeDoubleQuotes(values[27]),
                            iot_device:removeDoubleQuotes(values[28]),
                            model:removeDoubleQuotes(values[29]), 
                            amount:removeDoubleQuotes(values[30],'number'), 
                            advance_amount:removeDoubleQuotes(values[31],'number'),  
                            image1:removeDoubleQuotes(values[32]), 
                            image2:removeDoubleQuotes(values[33]), 
                            image3:removeDoubleQuotes(values[34]), 
                            updated:new Date().toISOString() 
                        }
                         
                        if(send_data_vehicle.city != undefined)
                        {
                            // console.log('checkVehicle[0]._id',checkVehicle[0]._id) 
                            let whereconVehicle= {'$and':[{_id:checkVehicle[0]._id},{business_supplier:business_supplier_id},{city:city_id},{hub:hub_id}] }
                            const updvaluesVehicle = { $set: send_data_vehicle };
                            const dataupdateVehicle = await  vehicle.updateOne(whereconVehicle, updvaluesVehicle);

                            // const dataVehicle = await vehicle.findByIdAndUpdate(checkVehicle[0]._id, send_data);   
                            return dataupdateVehicle; 
                        }

                    }else{
                        const send_data = {     
                            business_supplier: business_supplier_id,
                            // business_supplier: removeDoubleQuotes(values[0]),
                            // city:removeDoubleQuotes(values[1] ),
                            // hub:removeDoubleQuotes(values[2] ),
                            city:city_id,
                            hub:hub_id,
                            vehicle_name:removeDoubleQuotes(values[3]),
                            vehicle_model: removeDoubleQuotes(values[4]),
                            vehicle_color:removeDoubleQuotes(values[5]),
                            vehicle_regNo:removeDoubleQuotes(values[6]),
                            manufacture_date: removeDoubleQuotes(values[7]),
                            date_of_sale:removeDoubleQuotes(values[8]) ,
                            date_of_use: removeDoubleQuotes(values[9]),
                            chassis_no: removeDoubleQuotes(values[10]),
                            moter_engine_no:removeDoubleQuotes(values[11]),
                            battery_no: removeDoubleQuotes(values[12]),
                            battery_no_2:removeDoubleQuotes(values[13]) ,
                            controller_number:removeDoubleQuotes(values[14] ),
                            charger_no:removeDoubleQuotes(values[15] ),
                            charger_no_2:removeDoubleQuotes(values[16]),
                            key_no:removeDoubleQuotes(values[17] ),
                            scooter_invoice:removeDoubleQuotes(values[18]),
                            scooter_reg_date:removeDoubleQuotes(values[19]) ,
                            scooter_reg_upto:removeDoubleQuotes(values[20]),
                            insurance_company:removeDoubleQuotes(values[21]),
                            policy_number_insurance:removeDoubleQuotes(values[22]) ,
                            date_policy_expiry:removeDoubleQuotes(values[23]),                
                            permit_no:removeDoubleQuotes(values[24]),
                            exp_date:removeDoubleQuotes(values[25]),
                            sgst:removeDoubleQuotes(values[26]),
                            cgst: removeDoubleQuotes(values[27]),
                            iot_device:removeDoubleQuotes(values[28]),
                            model:removeDoubleQuotes(values[29]), 
                            amount:removeDoubleQuotes(values[30],'number'), 
                            advance_amount:removeDoubleQuotes(values[31],'number'),  
                            image1:removeDoubleQuotes(values[32]), 
                            image2:removeDoubleQuotes(values[33]), 
                            image3:removeDoubleQuotes(values[34]), 
                            book_status:0, 
                            created:new Date().toISOString(),
                            updated:new Date().toISOString() 
                        }

                        if(send_data.city != undefined)
                            {
                            const vehicles = new vehicle(send_data);             
                            const data = await vehicles.save();    
                            return data;
                       }
                    }
                    // check vehicle by registration number end
                       
                });
            }
            
            const lastData = await Promise.all(dataAll); 
            if(!lastData){
                return res.status(200).send({status: false, message: "Some error occurred while creating the record!",data: []});
            } 
            
            const inventoryDataAll = lastData.map(async function(item, i){
               // console.log('item',item)
                if(item != undefined){
                    if(type == "vehicle"){
                        var filterInventory = {$and:[{"type":"vehicle"},{"vehicle_id":item._id}]};
                    }else if(type == "spares"){
                        var filterInventory = {$and:[{"type":"spares"},{"spares_id":item._id}]};
                    }else if(type == "iot_devices"){
                        var filterInventory = {$and:[{"type":"iot_devices"},{"iot_device_id":item._id}]};
                    }else{
                        var filterInventory = "";
                    }
                    if(filterInventory){                        
                        const checkInventoryOther =  await inventory.find(filterInventory);
                        if(checkInventoryOther.length>0){
                            const postData = {
                                _id:checkInventoryOther[0]._id,
                                city_id:city_id,
                                hub_id:hub_id,
                                supplier_id:business_supplier_id,
                                city_name:item.city,
                                hub_name:item.hub,
                                vehicle_id:(type=='vehicle'?item._id:null),
                                spares_id:(type=='spares'?item._id:null),
                                iot_device_id:(type=='iot_devices'?item._id:null),
                                type:type,
                                created:new Date().toISOString(),
                                updated:new Date().toISOString()
                            }            
                            var inventoryData = await inventory.findByIdAndUpdate(checkInventoryOther[0]._id, postData);
                            return inventoryData;
                        }else{
                            const postData = {
                                city_id:city_id,
                                hub_id:hub_id,
                                supplier_id:business_supplier_id,
                                city_name:item.city,
                                hub_name:item.hub,
                                vehicle_id:(type=='vehicle'?item._id:null),
                                spares_id:(type=='spares'?item._id:null),
                                iot_device_id:(type=='iot_devices'?item._id:null),
                                type:type,
                                created:new Date().toISOString(),
                                updated:new Date().toISOString()
                            }
                            const inventories = new inventory(postData);             
                            var inventoryData = await inventories.save(); 
                            return inventoryData;
                        }
                        
                    }else{

                        const postData = {
                            city_id:city_id,
                            hub_id:hub_id,
                            supplier_id:business_supplier_id,
                            city_name:item.city,
                            hub_name:item.hub,
                            vehicle_id:(type=='vehicle'?item._id:null),
                            spares_id:(type=='spares'?item._id:null),
                            iot_device_id:(type=='iot_devices'?item._id:null),
                            type:type,
                            created:new Date().toISOString(),
                            updated:new Date().toISOString()
                        }
                        const inventories = new inventory(postData);             
                        var inventoryData = await inventories.save(); 
                        return inventoryData;

                    }
                    
                }
               
            })
            var inventorylastData = await Promise.all(inventoryDataAll);  
        }else{
            
            const send_data = req.body; 
            
            const typeSet = req.body.type;
            
            // const type = req.body.type;

            if(type == 'spares'){
                const checkSpares = await spare.find({$and:[{"part_number":{$eq:removeDoubleQuotes(send_data.part_number)}}]});
                if(checkSpares.length >0){                     
                    return res.status(200).send({status: false, message: "Spares Already Exists!",data: []});

                }
                const send_data_spare = {   
                    business_supplier: business_supplier_id,
                    // business_supplier: removeDoubleQuotes(values[0]),
                    // city:removeDoubleQuotes(values[1] ),
                    // hub:removeDoubleQuotes(values[2] ),
                    city:city_id,
                    hub:hub_id,
                    part_number:req.body.part_number,
                    supplier_description:req.body.supplier_description,
                    image_url:req.body.image_url,
                    quantity:req.body.quantity,
                    uom:req.body.uom,
                    hsn:req.body.hsn,
                    gst:req.body.gst,
                    dealer_sale_price:req.body.dealer_sale_price,
                    dlp:req.body.dlp,
                    sale_rate: req.body.sale_rate,
                    mrp:req.body.mrp,
                    model:req.body.model,
                    part_type:req.body.part_type,
                    assigned_status:0, 
                    updated:new Date().toISOString() 
                }
                const spares = new spare(send_data_spare);             
                var submitVehicleData = await spares.save(); 
                
            }else if(type == "iot_devices"){

                const checkDeviced = await iot_devices.find({$and:[{"device_serial_number":{$eq:req.body.device_serial_number}}]});
                if(checkDeviced.length >0){
                    return res.status(200).send({status: false, message: "device serial number Already Exists!",data: []});
                }
                const send_data_devices = {  
                    business_supplier: business_supplier_id,
                    // business_supplier: removeDoubleQuotes(values[0]),
                    // city:removeDoubleQuotes(values[1] ),
                    // hub:removeDoubleQuotes(values[2] ),
                    city:city_id,
                    hub:hub_id,
                    device_serial_number:req.body.device_serial_number,
                    make: req.body.make,
                    model_of_device:req.body.model_of_device,
                    imei:req.body.imei,
                    provided_by: req.body.provided_by,
                    consignmentTrackingNumber_CourierDocket:req.body.consignmentTrackingNumber_CourierDocket,
                    CustomerOrderNumber: req.body.CustomerOrderNumber,
                    DispatchDetailsRemarks: req.body.DispatchDetailsRemarks,
                    WarrantyStartdate:req.body.WarrantyStartdate,
                    sale_rate: req.body.sale_rate,
                    Warrantyterms:req.body.Warrantyterms ,
                    WarrantyEnddate:req.body.WarrantyEnddate,
                    VF_MSISDN:req.body.VF_MSISDN,
                    VF_ICCID:req.body.VF_ICCID,
                    VF_IMSI:req.body.VF_IMSI,
                    circle:req.body.circle ,
                    history_remark:req.body.history_remark,
                    device_status:req.body.device_status,
                    sim_status:req.body.sim_status ,
                    assigned_status:0, 
                    updated:new Date().toISOString() 
                }
                const iot_devicess = new iot_devices(send_data_devices);             
                var submitVehicleData = await iot_devicess.save(); 
            }else{
                // check vehicle by registration number
                const checkVehicle = await vehicle.find({$and:[{"vehicle_regNo":{$eq:req.body.vehicle_regNo}}]});
                if(checkVehicle.length >0){
                    return res.status(401).send({status: false, message: "Vahicle already exist!",data: []});
                }
                // check vehicle by registration number end

                const send_data_vehcle = {     
                    business_supplier: business_supplier_id,
                    // business_supplier: removeDoubleQuotes(values[0]),
                    // city:removeDoubleQuotes(values[1] ),
                    // hub:removeDoubleQuotes(values[2] ),
                    city:city_id,
                    hub:hub_id,
                    vehicle_name:req.body.vehicle_name,
                    vehicle_model: req.body.vehicle_model,
                    vehicle_color:req.body.vehicle_color,
                    vehicle_regNo:req.body.vehicle_regNo,
                    manufacture_date: req.body.manufacture_date,
                    date_of_sale:req.body.date_of_sale,
                    date_of_use:req.body.date_of_use,
                    chassis_no: req.body.chassis_no,
                    moter_engine_no:req.body.moter_engine_no,
                    battery_no: req.body.battery_no,
                    battery_no_2:req.body.battery_no_2,
                    controller_number:req.body.controller_number,
                    charger_no:req.body.charger_no,
                    charger_no_2:req.body.charger_no_2,
                    key_no:req.body.key_no,
                    scooter_invoice:req.body.scooter_invoice,
                    scooter_reg_date:req.body.scooter_reg_date,
                    scooter_reg_upto:req.body.scooter_reg_upto,
                    insurance_company:req.body.insurance_company,
                    policy_number_insurance:req.body.policy_number_insurance,
                    date_policy_expiry:req.body.date_policy_expiry,                
                    permit_no:req.body.permit_no,
                    exp_date:req.body.exp_date,
                    sgst:req.body.sgst,
                    cgst: req.body.cgst,
                    iot_device:req.body.iot_device,
                    model:req.body.model ,
                    amount:req.body.amount ,
                    advance_amount:req.body.advance_amount,  
                    image1:req.body.image1 ,
                    image2:req.body.image2 ,
                    image3:req.body.image3 ,
                    book_status:0, 
                    created:new Date().toISOString(),
                    updated:new Date().toISOString() 
                }

                const vehicles = new vehicle(send_data_vehcle);             
                var submitVehicleData = await vehicles.save(); 
            }
            if(!submitVehicleData){
                return res.status(200).send({status: false, message: "Some error occurred while creating the record!",data: []});
            } 

            
            const postData = {
                city_id:city_id,
                hub_id:hub_id,
                supplier_id:business_supplier_id,
                city_name:submitVehicleData.city,
                hub_name:submitVehicleData.hub,
                vehicle_id:(typeSet=='vehicle'?submitVehicleData._id:null),
                spares_id:(typeSet=='spares'?submitVehicleData._id:null),
                iot_device_id:(typeSet=='iot_devices'?submitVehicleData._id:null),
                type:typeSet,                
                created:new Date().toISOString(),
                updated:new Date().toISOString()
            }
            const inventories = new inventory(postData);             
            var inventorylastData = await inventories.save();           
            
        }

        /* save data in inventory history start */
        if(inventorylastData){ 
            if(createType == 'csv'){
                const inventoryHistoryData = inventorylastData.map(async function(item, i){
                    if(item != undefined){
                        let to_city_name = await getCityNamebyCityId(city_id);
                        let to_hub_name = await getHubNamebyHubId(hub_id);
                        const postDataHistory = {
                            inventory_id:item._id,
                            from_city_id:city_id,
                            from_hub_id:hub_id,
                            to_city_id:city_id,
                            to_hub_id:hub_id,
                            title:'Uploaded',
                            description:'Uploaded to '+to_city_name+' and '+to_hub_name,
                            status:item.status,
                            created:new Date().toISOString(),
                            updated:new Date().toISOString()
                        }
                        const inventoryHistory = new inventory_history(postDataHistory);             
                        return await inventoryHistory.save(); 
                    }
                    
                }) 
    
                var inventoryHistorylastData = await Promise.all(inventoryHistoryData);
            }else{
                let to_city_name = await getCityNamebyCityId(city_id);
                let to_hub_name = await getHubNamebyHubId(hub_id);
                const postDataHistory = {
                    inventory_id:inventorylastData._id,
                    title:'Uploaded',
                    description:'Uploaded to '+to_city_name+' and '+to_hub_name,
                    from_city_id:city_id,
                    from_hub_id:hub_id,
                    to_city_id:city_id,
                    to_hub_id:hub_id,
                    status:inventorylastData.status,
                    created:new Date().toISOString(),
                    updated:new Date().toISOString()
                }
                const inventoryHistory = new inventory_history(postDataHistory);    
                var inventoryHistorylastData = inventoryHistory.save(); 
            }
            
        }
        /* save data in inventory history end */

        var matchArrVehicle = [{ type: { $eq: "vehicle" }}];    
        var matchArrSpares = [{ type: { $eq: "spares" }}];    
        var matchArrIotDevices = [{ type: { $eq: "iot_devices" }}];
        var business_supplier_id_idmatch =  {supplier_id:ObjectId(business_supplier_id)}
        var city_idmatch =  {city_id: ObjectId(city_id)}
        var hub_idmatch =  {hub_id: ObjectId(hub_id) }
        matchArrVehicle.push(business_supplier_id_idmatch);
        matchArrVehicle.push(city_idmatch);
        matchArrVehicle.push(hub_idmatch);

        matchArrSpares.push(business_supplier_id_idmatch);
        matchArrSpares.push(city_idmatch);
        matchArrSpares.push(hub_idmatch);

        matchArrIotDevices.push(business_supplier_id_idmatch);
        matchArrIotDevices.push(city_idmatch);
        matchArrIotDevices.push(hub_idmatch);
        
        /* get all vehicles list with filter */
        const vahicleInventoryData = await getAllInventoryWithType('vehicle',matchArrVehicle);
        
        /* get all spares list with filter */
        const sparesInventoryData = await getAllInventoryWithType('spares',matchArrSpares);
    
        /* get all iot devices list with filter */           
        const iotDevicesInventoryData = await getAllInventoryWithType('iot_devices',matchArrIotDevices);
        
        return res.status(200).send({status: true, message: "Data Uploaded successfully created!", data: {'inventorylastData':inventorylastData,'vahicleInventoryData':vahicleInventoryData,'sparesInventoryData':sparesInventoryData,'iotDevicesInventoryData':iotDevicesInventoryData}});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while creating data.",data: []});
    }

}

const transferVehicleInventory = async (req, res) => {        
    try {      
        if (!req.body) {
            return res.status(400).send({
                message: "Data to update can not be empty!"
            });
        } 

        
        const transferData = req.body; 
        const selectedRowsData = transferData.selectedRowsData;
        const from_city_id = transferData.from_city_id;
        const from_hub_id = transferData.from_hub_id;
        const to_city_id = transferData.to_city_id;
        const to_hub_id = transferData.to_hub_id;
        const type = transferData.type;
        // const quantity =transferData.transferQuantity;
       
        const UpdWtDl = await selectedRowsData.map(async(val,index)=>{
            var Inventory_id = new ObjectId(val._id);
            // get inventory details
            // const getInvetoryDetails = await inventory.findOne({'_id':Inventory_id});
            // let from_city_id = getInvetoryDetails.city_id;
            // let from_hub_id = getInvetoryDetails.hub_id;

            
            const Wquery = {'_id':Inventory_id }  
            
            if(type == 'spares'){
                // var Wvalues = { $set: { city_id:to_city_id, hub_id:to_hub_id,to_city_id:to_city_id, to_hub_id:to_hub_id ,quantity:quantity } };
                var Wvalues = { $set: { city_id:to_city_id, hub_id:to_hub_id,to_city_id:to_city_id, to_hub_id:to_hub_id } };
            }else{
                var Wvalues = { $set: { city_id:to_city_id, hub_id:to_hub_id,to_city_id:to_city_id, to_hub_id:to_hub_id } };                
            }
           
            const UpdWtDl1 = await inventory.updateOne(Wquery, Wvalues);
            const getInventoryDetails = await inventory.findById(Inventory_id);
            // console.log('getInventoryDetails transfered',getInventoryDetails)
            if(getInventoryDetails && type == 'spares'){
                const WquerySpare = {'_id':ObjectId(getInventoryDetails.spares_id)} 
                var WvaluesSpare = { $set: { city:to_city_id, hub:to_hub_id } };
                const UpdWtVehicle = await spare.updateOne(WquerySpare, WvaluesSpare);
               
            }else if(getInventoryDetails && type != 'spares'){
                const WqueryVehicle = {'_id':ObjectId(getInventoryDetails.vehicle_id)} 
                var WvaluesVehicle = { $set: { city:to_city_id, hub:to_hub_id } };
                const UpdWtVehicle = await vehicles.updateOne(WqueryVehicle, WvaluesVehicle);
            }           

            let from_city_name = await getCityNamebyCityId(from_city_id);
            let from_hub_name = await getHubNamebyHubId(from_hub_id);
            let to_city_name = await getCityNamebyCityId(to_city_id);
            let to_hub_name = await getHubNamebyHubId(to_hub_id);
            /* save data in inventory history start */
            const postDataHistory = {
                inventory_id:val._id,
                title:'Transferred',
                description:'Transferred from '+from_city_name+', '+from_hub_name+' To '+to_city_name+', '+to_hub_name,
                from_city_id:from_city_id,
                from_hub_id:from_hub_id,
                to_city_id:to_city_id,
                to_hub_id:to_hub_id,
                status:1,
                created:new Date().toISOString(),
                updated:new Date().toISOString()
            }
            const inventoryHistory = new inventory_history(postDataHistory);             
            var inventoryHistoryData = await inventoryHistory.save(); 
            /* save data in inventory history end */
        })
        const UpdWtDlGet = Promise.all(UpdWtDl);

        /* save data in inventory history start */
        // const inventoryHistoryData = selectedRowsData.map(async function(item, i){
        //     const postDataHistory = {
        //         inventory_id:item._id,
        //         title:'Vehicle Transfered',
        //         description:'',
        //         from_city_id:from_city_id,
        //         from_hub_id:from_hub_id,
        //         to_city_id:to_city_id,
        //         to_hub_id:to_hub_id,
        //         status:3,
        //         created:new Date().toISOString(),
        //         updated:new Date().toISOString()
        //     }
        //     const inventoryHistory = new inventory_history(postDataHistory);             
        //     return await inventoryHistory.save(); 
        // }) 
        // var inventoryHistorylastData = await Promise.all(inventoryHistoryData);
        /* save data in inventory history end */

        // const data = await inventory.findByIdAndUpdate(id, req.body, { useFindAndModify: false });
        // console.log('data',data)
        if (UpdWtDlGet) { 

            var matchArrVehicle = [{ type: { $eq: "vehicle" }},{ city: to_city_id},{ hub: to_hub_id}];    
            var matchArrSpares = [{ type: { $eq: "spares" }},{ city: to_city_id},{ hub: to_hub_id}];    
            var matchArrIotDevices = [{ type: { $eq: "iot_devices" }},{ city: to_city_id},{ hub: to_hub_id}];
    
            /* get all vehicles list with filter */
            const vahicleInventoryData = await getAllInventoryWithType('vehicle',matchArrVehicle);
            
            /* get all spares list with filter */
            const sparesInventoryData = await getAllInventoryWithType('spares',matchArrSpares);
        
            /* get all iot devices list with filter */           
            const iotDevicesInventoryData = await getAllInventoryWithType('iot_devices',matchArrIotDevices);
            
          return res.status(200).send({status: true, message: "Vehicle successfully transfered!", data: {'vahicleInventoryData':vahicleInventoryData,'sparesInventoryData':sparesInventoryData,'iotDevicesInventoryData':iotDevicesInventoryData}});
        }
        return res.status(404).send({status: false, message: "Cannot transfered Vehicle!",data: []});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while updating data.",data: []});
    }

}
const updateInventory = async (req, res) => {        
    try {      
        if (!req.body) {
            return res.status(400).send({
                message: "Data to update can not be empty!"
            });
        } 
        const id = req.params.id;  
        const type = req.body.type;  
        const vehicle_id = req.body.vehicle_id;  
        const business_supplier_id = req.body.business_supplier_id;  
        const city_id = req.body.city_id;  
        const hub_id = req.body.hub_id;  
        const spares_id = req.body.spares_id;  
        const iot_device_id = req.body.iot_device_id;  
        const vehicle_regNo = req.body.vehicle_regNo;  

        delete req.body._id;
       
        const data = await inventory.findByIdAndUpdate(id, req.body);
        if(type == 'vehicle'){
            const checkRegistationNo = await vehicle.find({$and:[{'_id':{$ne:vehicle_id}},{'vehicle_regNo':vehicle_regNo}]});
            if(checkRegistationNo.length>0){
                return res.status(409).send({status: false, message:"Vehicle registration number already exist!",data: []});
            }

            delete req.body.created;
            
            const dataVehicle = await vehicle.findByIdAndUpdate(vehicle_id, req.body);
        }
        if(type == 'spares'){
            const part_type = req.body.part_type;  
            const checkSpares = await spare.find({$and:[{'_id':{$ne:spares_id}},{"part_number":{$eq:req.body.part_number}}]});
            if(checkSpares.length >0){                     
                return res.status(200).send({status: false, message: "Spares Already Exists!",data: []});

            }           

            const send_data_spare = {   
                business_supplier: business_supplier_id,
                // business_supplier: removeDoubleQuotes(values[0]),
                // city:removeDoubleQuotes(values[1] ),
                // hub:removeDoubleQuotes(values[2] ),
                city:city_id,
                hub:hub_id,
                part_number:req.body.part_number,
                supplier_description:req.body.supplier_description,
                image_url:req.body.image_url,
                quantity:req.body.quantity,
                uom:req.body.uom,
                hsn:req.body.hsn,
                gst:req.body.gst,
                dealer_sale_price:req.body.dealer_sale_price,
                dlp:req.body.dlp,
                sale_rate: req.body.sale_rate,
                mrp:req.body.mrp,
                model:req.body.model,
                part_type:req.body.part_type,
                assigned_status:req.body.assigned_status, 
                updated:new Date().toISOString() 
            }
            const dataSpares = await spare.findByIdAndUpdate(spares_id, send_data_spare);

            let getAvailability = await checkAvailability(part_type,req.body.part_number,city_id,hub_id,business_supplier_id);
            
            if(getAvailability == 0){
                var assigned_statusGet=1;
            }else{
                var assigned_statusGet=0;
            }

            const sendDataSpare = {  
                assigned_status:assigned_statusGet, 
                updated:new Date() 
            }
            const dataSpares1 = await spare.findByIdAndUpdate(spares_id, sendDataSpare);


        }

        if(type == 'iot_devices'){
            const checkDeviced = await iot_devices.find({$and:[{'_id':{$ne:iot_device_id}},{"device_serial_number":{$eq:req.body.device_serial_number}}]});
            if(checkDeviced.length >0){
                return res.status(200).send({status: false, message: "device serial number Already Exists!",data: []});
            }

            const send_data_devices = {  
                business_supplier: business_supplier_id,
                // business_supplier: removeDoubleQuotes(values[0]),
                // city:removeDoubleQuotes(values[1] ),
                // hub:removeDoubleQuotes(values[2] ),
                city:city_id,
                hub:hub_id,
                device_serial_number:req.body.device_serial_number,
                make: req.body.make,
                model_of_device:req.body.model_of_device,
                imei:req.body.imei,
                provided_by: req.body.provided_by,
                consignmentTrackingNumber_CourierDocket:req.body.consignmentTrackingNumber_CourierDocket,
                CustomerOrderNumber: req.body.CustomerOrderNumber,
                DispatchDetailsRemarks: req.body.DispatchDetailsRemarks,
                WarrantyStartdate:req.body.WarrantyStartdate,
                sale_rate: req.body.sale_rate,
                Warrantyterms:req.body.Warrantyterms ,
                WarrantyEnddate:req.body.WarrantyEnddate,
                VF_MSISDN:req.body.VF_MSISDN,
                VF_ICCID:req.body.VF_ICCID,
                VF_IMSI:req.body.VF_IMSI,
                circle:req.body.circle ,
                history_remark:req.body.history_remark,
                device_status:req.body.device_status,
                sim_status:req.body.sim_status ,
                assigned_status:req.body.assigned_status, 
                updated:new Date() 
            }
            const dataSpares = await iot_devices.findByIdAndUpdate(iot_device_id, send_data_devices);

            // check and update status
            let getAvailability = await checkAvailability('iot_devices',req.body.device_serial_number);
            if(getAvailability == 0){
                var assigned_statusGet=1;
            }else{
                var assigned_statusGet=0;
            }

            const sendDataDevices = {  
                assigned_status:assigned_statusGet, 
                updated:new Date() 
            }

            const dataSparesIot = await iot_devices.findByIdAndUpdate(iot_device_id, sendDataDevices);

        }


        if (data) {

            var matchArrVehicle = [{ type: { $eq: "vehicle" }}];    
            var matchArrSpares = [{ type: { $eq: "spares" }}];    
            var matchArrIotDevices = [{ type: { $eq: "iot_devices" }}];
            
            var business_supplier_id_idmatch =  {supplier_id:ObjectId(business_supplier_id)}
            var city_idmatch =  {city_id: ObjectId(city_id)}
            var hub_idmatch =  {hub_id: ObjectId(hub_id) }

            matchArrVehicle.push(business_supplier_id_idmatch);
            matchArrVehicle.push(city_idmatch);
            matchArrVehicle.push(hub_idmatch);

            /* get all vehicles list with filter */
            const vahicleInventoryData = await getAllInventoryWithType('vehicle',matchArrVehicle);
            
            /* get all spares list with filter */
            const sparesInventoryData = await getAllInventoryWithType('spares',matchArrSpares);
        
            /* get all iot devices list with filter */           
            const iotDevicesInventoryData = await getAllInventoryWithType('iot_devices',matchArrIotDevices);
         
            return res.status(200).send({status: true, message: "Data successfully updated!",data: {'vahicleInventoryData':vahicleInventoryData,'sparesInventoryData':sparesInventoryData,'iotDevicesInventoryData':iotDevicesInventoryData}});
        }

        // let UpdateAllVehiclesSparesIotdevices = await checkAndUpdateAllVehiclesSparesIotdevices();   

        return res.status(404).send({status: false, message: "Cannot update data! with id=${id}. Maybe data! was not found!",data: []});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while updating data.",data: []});
    }

}

const checkAndUpdateAllVehiclesSparesIotdevices = async () => {
    let getAllBookings = await bookings.aggregate([
        { $lookup:
            {
                from: "inventories",
                localField: "inventory_id",
                foreignField: "_id",
                // pipeline: pipeline,
                as: "inventory_details"
            }
        },
        { 
            $unwind: '$inventory_details'
        },    
        {
            $match:{"$and":[{'status':true}]}
        },
        // {
        //     $match:{"$and":[{'status':true},{"vehicle_details.book_status":1}]}
        // },
       
    ]);
    if(getAllBookings.length>0){
        var bookedInventoryIds=[];
        var bookedSpares=[];
        var bookedIotDevices=[];
        var bookedChargers=[];
        var bookedBatteries=[];
        const getAllDataMap = getAllBookings.map(async(value,index)=>{
            bookedInventoryIds.push(ObjectId(value.inventory_id));            
            if(value.spares != ""){
                bookedSpares.push(value.spares);
            }
            bookedIotDevices.push(value.iot_devices);
            bookedChargers.push(value.charger_1);
            if(value.charger_2 != ""){
                bookedChargers.push(value.charger_2);
            }            
            bookedBatteries.push(value.battery_1);
            if(value.battery_2 != ""){
                bookedBatteries.push(value.battery_2);
            }
           
        })

        let getAll = await Promise.all(getAllDataMap);
        console.log('bookedInventoryIds',bookedInventoryIds)
        // update status of vehicles
        if(bookedInventoryIds.length >0){
            const updateInventorystatus = await  inventory.updateMany({_id : {$nin: bookedInventoryIds}},{$set : {status : 1}}
            ); 
            const getAllVehiclesIds = await  inventory.find({_id : {$in: bookedInventoryIds}},{"vehicle_id":1}); 
            console.log('getAllVehiclesIds',getAllVehiclesIds)

            if(getAllVehiclesIds.length >0){
                
                let getAllVehiclesIdsGet = [];
                await Promise.all(getAllVehiclesIds.map(async(value,index)=>{
                    getAllVehiclesIdsGet.push(value.vehicle_id)
                }));

                const updatevehiclestatus = await  vehicle.updateMany({_id : {$in: getAllVehiclesIdsGet}},{$set : {book_status : 1}}); 
                const updatevehiclestatus1 = await  vehicle.updateMany({_id : {$nin: getAllVehiclesIdsGet}},{$set : {book_status : 0}});
            }           

        }
        // update status of spares
        if(bookedSpares.length >0){
            const updateSparesstatus = await  spare.updateMany({$and:[{part_number : {$nin: bookedSpares}},{part_type:"spares"}]},{$set : {assigned_status : 0}}
            ); 

        }
        // update status of battery
        if(bookedBatteries.length >0){
            const updateBatterystatus = await  spare.updateMany({$and:[{part_number : {$nin: bookedBatteries}},{part_type:"battery"}]},{$set : {assigned_status : 0}}
            ); 
        }
        // update status of chargers
        if(bookedChargers.length >0){
            const updatechargerstatus = await  spare.updateMany({$and:[{part_number : {$nin: bookedChargers}},{part_type:"charger"}]},{$set : {assigned_status : 0}}
            ); 
        }
        // update status of iotdevices
        if(bookedIotDevices.length >0){
            const updateIotDevicesstatus = await  iot_devices.updateMany({device_serial_number : {$nin: bookedIotDevices}},{$set : {assigned_status : 0}}
            ); 
        }
    }


    // var checkSpareBooking = await bookings.aggregate([
    //     { $lookup:
    //         {
    //             from: "inventories",
    //             localField: "inventory_id",
    //             foreignField: "_id",
    //             // pipeline: pipeline,
    //             as: "inventory_details"
    //         }
    //     },
    //     { 
    //         $unwind: '$inventory_details'
    //     },
    //     {
    //         $match:{"$and":[{ "spares": value},{'status':true}]}
    //     },
    // ])
    return getAllBookings; 
}

const deleteInventory= async (req, res) => {        
    try {       
        const id = req.params.id;
        const dataGet = await inventory.findOne({'_id':id});
        const data = await inventory.findByIdAndRemove(id);  
        var data_remove = '';      
        if (data && data.type == 'vehicle') { 
            data_remove = await vehicle.findByIdAndRemove(dataGet.vehicle_id); 

        }else if(data && data.type == 'spares'){
            data_remove = await spares.findByIdAndRemove(dataGet.spares_id); 

        }else if(data &&  data.type == 'iot_devices'){
            data_remove = await spares.findByIdAndRemove(dataGet.iot_device_id); 
        }

        if (data_remove) {
            
            const invenotyHistoryRemove = await inventory_history.remove({'inventory_id':id});
             
            var matchArrVehicle = [{ type: { $eq: "vehicle" }}];    
            var matchArrSpares = [{ type: { $eq: "spares" }}];    
            var matchArrIotDevices = [{ type: { $eq: "iot_devices" }}];
    
            /* get all vehicles list with filter */
            const vahicleInventoryData = await getAllInventoryWithType('vehicle',matchArrVehicle);
            
            /* get all spares list with filter */
            const sparesInventoryData = await getAllInventoryWithType('spares',matchArrSpares);
        
            /* get all iot devices list with filter */           
            const iotDevicesInventoryData = await getAllInventoryWithType('iot_devices',matchArrIotDevices);
          
            return res.status(200).send({status: true, message: "Inventory successfully deleted!",data: {'vahicleInventoryData':vahicleInventoryData,'sparesInventoryData':sparesInventoryData,'iotDevicesInventoryData':iotDevicesInventoryData}});
        }

        return res.status(404).send({status: false, message: "Cannot delete Inventory! with id=${id}. Maybe Inventory! was not found!",data: []});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while removing data.",data: []});
    }

}
const deleteMultipleInventory = async (req, res) => {        
    try { 
        const deleteJson = req.body;
       
        // return res.status(200).send({status: true, message: `data were deleted successfully!`, data: obj});
        const dataGet = await inventory.find({_id: { $in: deleteJson}});
        var vehicleIds =[];
        var sparesIds =[];
        var iotDevicesIds=[];
        if(dataGet){
            dataGet.map((val,i)=>{
                if(val.type == 'vehicle'){
                    vehicleIds.push(val.vehicle_id);
                }
                if(val.type == 'spares'){
                    sparesIds.push(val.spares_id);
                }
                if(val.type == 'iot_devices'){
                    iotDevicesIds.push(val.iot_device_id);
                }
            })
        }
        const data = await inventory.deleteMany({_id: { $in: deleteJson}});
        var data_remove_v = '';      
        var data_remove_s = '';      
        var data_remove_iot_d = '';      
        if(data) { 
            const deleteInvetoryHistory = await inventory_history.deleteMany({inventory_id: { $in: deleteJson}});
            if(vehicleIds.length >0){
                data_remove_v = await vehicle.deleteMany({_id: { $in: vehicleIds}});
            } 
            if(sparesIds.length >0){
                data_remove_s = await spare.deleteMany({_id: { $in:sparesIds}});
            }
            if(iotDevicesIds.length >0){
                data_remove_iot_d = await iot_devices.deleteMany({_id: { $in: iotDevicesIds}});
            }
            
            /* get all invenoty data */
            var matchArrVehicle = [{ type: { $eq: "vehicle" }}];    
            var matchArrSpares = [{ type: { $eq: "spares" }}];    
            var matchArrIotDevices = [{ type: { $eq: "iot_devices" }}];

            /* get all vehicles list with filter */
            const vahicleInventoryData = await getAllInventoryWithType('vehicle',matchArrVehicle);
            
            /* get all spares list with filter */
            const sparesInventoryData = await getAllInventoryWithType('spares',matchArrSpares);
        
            /* get all iot devices list with filter */           
            const iotDevicesInventoryData = await getAllInventoryWithType('iot_devices',matchArrIotDevices);

            return res.status(200).send({status: true, message: `${data.deletedCount} data were deleted successfully!`,data: {'vahicleInventoryData':vahicleInventoryData,'sparesInventoryData':sparesInventoryData,'iotDevicesInventoryData':iotDevicesInventoryData}, data_remove_v: data_remove_v,data_remove_s:data_remove_s,data_remove_iot_d:data_remove_iot_d});
            
        }
        
        return res.status(404).send({status: false, message: "Some error occurred while removing all data",data: []});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while removing all data.",data: []});
    }
}

const getAllCities=(admin_city_id=null)=>{ 
    if(admin_city_id != null){
        return cities.find({'_id':admin_city_id}).sort({ name: 1 }); 
    }else{
        return cities.find().sort({ name: 1 }); 
    }   
}   
 
 const getAllBusinessSupplier=(admin_city_id=null)=>{
    if(admin_city_id != null){
        return business_suppliers.find({'city_id':admin_city_id}).sort({ created: -1 });        
    }else{
        return business_suppliers.find().sort({ created: -1 });
    }
 }
  
 const getAllVehicles=(admin_city_id=null)=>{    
    if(admin_city_id != null){
        return vehicle.find({'_id':admin_city_id}).sort({ created: -1 });        
    }else{
        return vehicle.find().sort({ created: -1 });
    }    
 }

 const getAllSpares=(admin_city_id=null)=>{
    if(admin_city_id != null){
        return spares.find({'city_id':admin_city_id}).sort({ created: -1 });        
    }else{
        return spares.find().sort({ created: -1 });
    }
 }


 const getAllIotDevices=(admin_city_id=null)=>{
    if(admin_city_id != null){
        return iot_devices.find({'city_id':admin_city_id}).sort({ created: -1 });        
    }else{
        return iot_devices.find().sort({ created: -1 });
    }
 }

 const getAllHubs=()=>{
     return Hub.aggregate([
        {
            $sort:{ title: 1 }
        },
        {  $lookup:
            {
            from: "cities",
            localField: "city_id",
            foreignField: "_id",
            as: "cityDetails"
            }
        },
        { 
            $unwind: '$cityDetails'
        },
        {
            $addFields: {
                "city_name": "$cityDetails.name",
                "city_lat": "$cityDetails.latitude",
                "city_lng": "$cityDetails.longitude",
                "city_id": "$cityDetails._id",
            }
        },
        {
            $project: {
                _id: 1,
                city_id:1,
                title: 1,
                description: 1,
                isCentralHub: 1,
                city_lat: 1,
                city_lng: 1,
                city_name: 1,
                updated: 1,
                created: 1
            }
        }
    ]);
}
const getAllInventoryBookingWithType=async(type,filterArray)=>{
    
    let data =[];
    if(type == 'vehicle'){
        let matchArrVehicle = {};
        if(filterArray.length >0){
            matchArrVehicle = {"$and":filterArray};
        }
        /* get all vehicles list with filter */
        let vahicleInventoryData = await inventory.aggregate([ 
            {
                $sort:{created: -1}
            },     
            { $lookup:
                {
                from: "vehicles",
                localField: "vehicle_id",
                foreignField: "_id",
                as: "vehicleDetails"
                }
            },
            { 
                $unwind: '$vehicleDetails'
            },
            {
                $match:matchArrVehicle
            }
        ])
        if(vahicleInventoryData){
            vahicleInventoryData.filter(function(val,i){
                return val.key = i;
            });
        }
        data=vahicleInventoryData;

    }else if(type == 'vehicle_name'){
        let matchArrVehicle = {};
        if(filterArray.length >0){
            matchArrVehicle = {"$and":filterArray};
        }
        
        /* get all vehicles list with filter */
        let vahicleInventoryData = await vehicle.aggregate([ 
            {
                $sort:{created: -1}
            },
            {
                $match:matchArrVehicle
            },         
            { $lookup:
                {
                from: "inventories",
                localField: "_id",
                foreignField: "vehicle_id",
                as: "inventoryDetails"
                }
            },
            { 
                $unwind: '$inventoryDetails'
            },
            { "$group": {
                "_id": "$vehicle_name",count: { $sum: 1 },
                "doc": { "$first": "$$ROOT" },
                count: { $sum: 1 }
            }},
            {
                $replaceRoot: {
                newRoot: { $mergeObjects: [{ count: '$count' }, '$doc'] }
                }
            }
          
        ])
       
        if(vahicleInventoryData){
            vahicleInventoryData.filter(function(val,i){
                return val.key = i;
            });
        }
        
        data=vahicleInventoryData;

    }
    return data;
}

const getAllInventoryWithType=async(type,filterArray)=>{
    console.log('getAllInventoryWithType filterArray',filterArray)
    let data =[];
    if(type == 'vehicle'){
        let matchArrVehicle = {};
        if(filterArray.length >0){
            matchArrVehicle = {$and:filterArray}
        }
       
        /* get all vehicles list with filter */
        let vahicleInventoryData = await inventory.aggregate([ 
            {
                $sort:{created: -1}
            },         
            { $lookup:
                {
                from: "vehicles",
                localField: "vehicle_id",
                foreignField: "_id",
                as: "vehicleDetails"
                }
            },
            { 
                $unwind: '$vehicleDetails'
            },
            {
                $match:matchArrVehicle
            }
        ])
        
        if(vahicleInventoryData){
            vahicleInventoryData.filter(function(val,i){
                return val.key = i;
            });
        }
        data=vahicleInventoryData;

    }else if(type == 'spares'){
        let matchArrSpares = {};
        if(filterArray.length >0){
            matchArrSpares = {$and:filterArray}
        }
        
        /* get all spares list with filter */
        let sparesInventoryData = await inventory.aggregate([
            {
                $sort:{created: -1}
            },           
            { $lookup:
                {
                from: "spares",
                localField: "spares_id",
                foreignField: "_id",
                as: "sparesDetails"
                }
            },
            { 
                $unwind: '$sparesDetails'
            },
          
            {
                $match:matchArrSpares
            },
        ])
        if(sparesInventoryData){
            // sparesInventoryData.filter(function(val,i){
            //     return val.key = i;
            // });

            const spareInvData =  sparesInventoryData.map(async function(val,i){
                
                let BookingMatch = [{$or:[{ "spares": val.sparesDetails.part_number},{ "battery_1": val.sparesDetails.part_number},{ "battery_2": val.sparesDetails.part_number},{ "charger_1": val.sparesDetails.part_number},{ "charger_2": val.sparesDetails.part_number}]},{'status':true}]; 

                if(val.supplier_id){
                    BookingMatch.push({'business_supplier_id':val.supplier_id});
                } 
                if(val.city_id){
                    BookingMatch.push({'city_id':val.city_id});
                }
                if(val.hub_id){
                    BookingMatch.push({'hub_id':val.hub_id});
                } 
                
                const checkSpareBooking = await bookings.aggregate([
                    { $lookup:
                        {
                            from: "inventories",
                            localField: "inventory_id",
                            foreignField: "_id",
                            // pipeline: pipeline,
                            as: "inventory_details"
                        }
                    },
                    { 
                        $unwind: '$inventory_details'
                    },
                    {
                        $match:{"$and":BookingMatch}                       
                    },
                ])
                const total_booking_count = checkSpareBooking.length;  
                return val.total_booking_count = total_booking_count ,val.key = i;
            });
            const lastspareInvData = await Promise.all(spareInvData);
        }

        data=sparesInventoryData;

    }else if(type == 'iot_devices'){
        let matchArrIotDevices = {};
        if(filterArray.length >0){
            matchArrIotDevices = {$and:filterArray}
        }
        
        /* get all iot devices list with filter */
        let iotDevicesInventoryData = await inventory.aggregate([           
            {
                $sort:{created: -1}
            },                      
            { $lookup:
                {
                from: "iot_devices",
                localField: "iot_device_id",
                foreignField: "_id",
                as: "iot_devicesDetails"
                }
            },
            { 
                $unwind: '$iot_devicesDetails'
            },
           
            {
                $match:matchArrIotDevices
            },
        ])

        if(iotDevicesInventoryData){
            iotDevicesInventoryData.filter(function(val,i){
                return val.key = i;
            });
        }
        data=iotDevicesInventoryData;
    }
    return data;
}

const getCityNamebyCityId= async(city_id)=>{
    const citiesNameGet =await cities.findOne({'_id':city_id},{ name: 1,_id: 0 }); 
   
    if(citiesNameGet){
        return citiesNameGet.name;
    }else{
        return '';
    }
    
}

const getHubNamebyHubId= async (hub_id)=>{    
    const HubNameGet = await Hub.findOne({'_id':ObjectId(hub_id)},{title:1,_id:0});
    if(HubNameGet){
        return HubNameGet.title;
    }else{
        return '';
    }
    
}


const removeDoubleQuotes=(str,type=null)=>{
    // console.log('str',str)
    // string = str.substring(1, str.length() - 1) 
    // return str.replace(/^\"(.+)\"$/,"$1");
    // return str;
    if(str != "" && str != undefined){
        // var fIndex = str.indexOf('""');
        // var lIndex = str.lastIndexOf('""');
        // if(fIndex >= 0 && lIndex >= 0){
        //     str = str.substring(fIndex+1, lIndex+1);
        // }
        var replaceStr = str.replace(/^\"(.+)\"$/,"$1");        
        if(type == "number"){
            return parseFloat(replaceStr); 
        }else{
            return replaceStr; 
        }

    }else{
        if(type == "number"){
            return parseFloat(str); 
        }else{
            return str; 
        }
       
    }
       
}
const checkAvailability = async (type,value,city_id="",hub_id="",business_supplier_id="") => {
    var checkStatus = 0;
    var getId = 0;
    var message = '';
    var status =1;
   
    if(type == 'spares'){     
        let SparesMatch = [{ 'part_type':'spares'},{'part_number':value}];    
        let BookingMatch = [{ "spares": value},{'status':true}];    
        if(business_supplier_id){
            SparesMatch.push({'business_supplier':business_supplier_id});
            BookingMatch.push({'inventory_details.supplier_id':ObjectId(business_supplier_id)});
            
        } 
        if(city_id){
            SparesMatch.push({'city':city_id});
            BookingMatch.push({'inventory_details.city_id':ObjectId(city_id)});
        }
        if(hub_id){
            SparesMatch.push({'hub':hub_id});
            BookingMatch.push({'inventory_details.hub_id':ObjectId(hub_id)});
        }  
        let matchArrSpares = {'$and':SparesMatch};
        const checkSpare = await spare.findOne(matchArrSpares);
        if(!checkSpare){
            checkStatus = 'invalid';
            message="Invailid data";
            status =null;
    
        }else{
            /* get all spares data */  
            let checkSpareBooking = await bookings.aggregate([
                { $lookup:
                    {
                        from: "inventories",
                        localField: "inventory_id",
                        foreignField: "_id",
                        // pipeline: pipeline,
                        as: "inventory_details"
                    }
                },
                { 
                    $unwind: '$inventory_details'
                },
                {
                    $match:{"$and":BookingMatch}
                    // $match:{"$and":[{ "spares": value},{'status':true},{'business_supplier_id':bookignSupplierId},{'city_id':bookignCity},{'hub_id':bookignHub}]}
                },
            ])
            if(checkSpareBooking.length>0 && checkSpare.quantity <= checkSpareBooking.length){
                checkStatus = 'not_available';
                message="Not Available";
                status =0;
    
            }else{
                getId = checkSpare._id;
                checkStatus = 'available';
                message="Available";
                status =1;    
            }
    
        }
        
    }else if(type == 'iot_devices'){   
        let iot_devicesMatch = [{'device_serial_number':value}];  
        let BookingMatch = [{ "iot_devices": value},{'status':true}];    
        if(business_supplier_id){
            iot_devicesMatch.push({'business_supplier':business_supplier_id});
            BookingMatch.push({'inventory_details.supplier_id':ObjectId(business_supplier_id)});            
        } 
        if(city_id){
            iot_devicesMatch.push({'city':city_id});
            BookingMatch.push({'inventory_details.city_id':ObjectId(city_id)});
        }
        if(hub_id){
            iot_devicesMatch.push({'hub':hub_id});
            BookingMatch.push({'inventory_details.hub_id':ObjectId(hub_id)});
        }  

        let matchArriot_devices = {'$and':iot_devicesMatch};

        const checkIotDevices = await iot_devices.find(matchArriot_devices);
        if(checkIotDevices.length<=0){
            checkStatus = 'invalid';
            message="Invailid data";
            status =null;

        }else{
            let checkIotDevicesBooking = await bookings.aggregate([
                { $lookup:
                    {
                        from: "inventories",
                        localField: "inventory_id",
                        foreignField: "_id",
                        // pipeline: pipeline,
                        as: "inventory_details"
                    }
                },
                { 
                    $unwind: '$inventory_details'
                },
                {
                    $match:{"$and":BookingMatch}
                },
            ])
            /* get all IotDevices data */                                
            

            if(checkIotDevicesBooking.length>0){
                checkStatus = 'not_available';
                message="Not Available";
                status =0;
            }else{
                getId = checkIotDevices[0]._id;
                checkStatus = 'available';
                message="Available";
                status =1;
            }

        }

    }else if(type == 'battery'){
        
        let BatteryMatch = [{ 'part_type': { $eq: "battery" }},{'part_number':value}]  
        let BookingMatch = [{'status':true}];    
        if(business_supplier_id){
            BatteryMatch.push({'business_supplier':business_supplier_id});
            BookingMatch.push({'inventory_details.supplier_id':ObjectId(business_supplier_id)});            
        } 
        if(city_id){
            BatteryMatch.push({'city':city_id});
            BookingMatch.push({'inventory_details.city_id':ObjectId(city_id)});
        }
        if(hub_id){
            BatteryMatch.push({'hub':hub_id});
            BookingMatch.push({'inventory_details.hub_id':ObjectId(hub_id)});
        }  

        let matchArrBattery = {'$and':BatteryMatch};
        let checkSpare = await spare.findOne(matchArrBattery);
        if(!checkSpare){
            checkStatus = 'invalid';
            message="Invailid data";
            status =null;

        }else{

            if(type == "battery"){
                BookingMatch.push({$or:[{'battery_1':value},{'battery_2':value}]});

            }
            let checkSpareBooking = await bookings.aggregate([
                { $lookup:
                    {
                        from: "inventories",
                        localField: "inventory_id",
                        foreignField: "_id",
                        // pipeline: pipeline,
                        as: "inventory_details"
                    }
                },
                { 
                    $unwind: '$inventory_details'
                },
                {
                    $match:{"$and":BookingMatch}
                },
            ])
            if(checkSpareBooking.length>0 && checkSpare.quantity <= checkSpareBooking.length){
                checkStatus = 'not_available';
                message="Not Available";
                status =0;
            }else{
                getId = checkSpare._id;
                checkStatus = 'available';
                message="Available";
                status =1;
            }

        }
        
    }else if(type == 'charger'){        
        let ChargerMatch = [{ 'part_type': { $eq: "charger" }},{'part_number':value}]  
        let BookingMatch = [{'status':true}];    
        if(business_supplier_id){
            ChargerMatch.push({'business_supplier':business_supplier_id});
            BookingMatch.push({'business_supplier_id':ObjectId(business_supplier_id)});            
        } 
        if(city_id){
            ChargerMatch.push({'city':city_id});
            BookingMatch.push({'city_id':ObjectId(city_id)});
        }
        if(hub_id){
            ChargerMatch.push({'hub':hub_id});
            BookingMatch.push({'hub_id':ObjectId(hub_id)});
        }  

        let matchArrCharger = {'$and':ChargerMatch};
        let checkSpare = await spare.findOne(matchArrCharger);
        if(!checkSpare){
            checkStatus = 'invalid';
            message="Invailid data";
            status =null;

        }else{
            
            if(type == "charger"){
                BookingMatch.push({$or:[{'charger_1':value},{'charger_2':value}]});
            }
            
            let checkSpareBooking = await bookings.aggregate([
                { $lookup:
                    {
                        from: "inventories",
                        localField: "inventory_id",
                        foreignField: "_id",
                        // pipeline: pipeline,
                        as: "inventory_details"
                    }
                },
                { 
                    $unwind: '$inventory_details'
                },
                {
                    $match:{"$and":BookingMatch}
                },
            ])
            
            if(checkSpareBooking.length>0 && (parseInt(checkSpare.quantity) <= checkSpareBooking.length)){
                checkStatus = 'not_available';
                message="Not Available";
                status =0;
            }else{
                getId = checkSpare._id;
                checkStatus = 'available';
                message="Available";
                status =1;
            }

        }
        
    }
    return status;
}    


module.exports = { getAllInventory,getAllInventoryFilter,getDetailsInventory,createInventory,updateInventory,transferVehicleInventory,deleteInventory,deleteMultipleInventory,getVehicleTimelineInventory };