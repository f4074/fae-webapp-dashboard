
require("dotenv").config();
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const Admin = require("../models/admin");
const cities = require("../models/cities");
const business_suppliers = require("../models/business_suppliers");
const business_clients = require("../models/business_clients");
const inventories = require("../models/inventory");
const referals = require("../models/referals");
const bookings = require("../models/bookings");
const userRefferal = require("../models/user_refferal");
const ObjectId = require('mongodb').ObjectId;
const auth = require("../middleware/auth");
const bcrypt = require("bcryptjs");
const sendEmail = require("../common/sendMail")
var hex = /[0-9A-Fa-f]{6}/g;

const createAdmin = async (req, res) => { 
  const errors = {};
    try {
        const { first_name, last_name, email, password,phone_number,user_role,address,city_id,hub_id,pincode } = req.body;
         console.log('req.body',req.body)
        if (!req.body.first_name) {
          return res.status(400).send({ message: "Please enter your first name" });
        }
        if (!req.body.last_name) {
          return res.status(400).send({ message: "Please enter your last name" });
        }
        if (!req.body.phone_number) {
          return res.status(400).send({ message: "Please enter your phone number" });
        }
        if (!req.body.email) {
          return res.status(400).send({ message: "Please enter email address" });
        }
        if (!req.body.password) {
          return res.status(400).send({ message: "Please enter password" });
        }  

        if (!(/^[\-0-9a-zA-Z\.\+_]+@[\-0-9a-zA-Z\.\+_]+\.[a-zA-Z]{2,}$/).test(String(req.body.email))) {
            return res.status(400).send({ message: "Please enter email address" });
        }
        const oldUser = await Admin.findOne({ email });
        if (oldUser) { 
          return res.status(200).send({ status: false, message: 'Email already register, please use another email address.' });
        }

        const checkPhone = await Admin.findOne({ phone_number });
        if (checkPhone) { 
          return res.status(200).send({ status: false, message: 'Phone Number already register, please use another phone number.' });
        }

         const otp = Math.floor(100000 + Math.random() * 900000);
         //user.otp = otp.toString()

        const encryptedPassword = await bcrypt.hash(password, 10);

        var JSonBody = {
          first_name:first_name, 
          last_name:last_name, 
          email:email, 
          password:encryptedPassword, 
          phone_number:phone_number, 
          user_role:user_role, 
          city_id:city_id, 
          hub_id:hub_id, 
          address:address, 
          pincode:pincode,           
        }

        const AdminCreation = await Admin.create(JSonBody);
        /* const token = jwt.sign(
          { user_id: user._id, email },
          process.env.TOKEN_KEY,
          { expiresIn: "30min",}
        );
        user.token = token; */
        if(!AdminCreation){
          
          return res.status(201).json({status:false, message: 'Oops!, something error!' , data:AdminCreation});
        }
        

        const allAdminData = await getAllAdminList();

        return res.status(201).send({status: true, message: "Users successfully created!", data: {'data':AdminCreation,'allAdminData':allAdminData}});

      } catch (err) {
        console.log(err);
      }

}

const getAllDashboard=async(req, res)=>{
  try {
    const filterData = req.body;
    const searchKey =filterData.searchKey;
    const city_id =filterData.city_id;
    const hub_id =filterData.hub_id;
    const start_date =filterData.start_date;
    const end_date =filterData.end_date;
    var hex = /[0-9A-Fa-f]{6}/g;
    var matchArr = []; 
    var matchArrBooking = [{'status':true},{"inventory_id.status":3}]; 
    var matchArrBusinessSupplier = []; 
    var matchArrBusinessClient = []; 
    var matchArrInventory = [{"type":"vehicle"}]; 
    var matchArrRevenueCollection = [{'status':true}]; 
    var matchArrvehicleAssigned = [{"type":"vehicle"},{'status':4}]; 
    var matchArrReferrals = []; 
    if(start_date){
      var startDateSet = start_date+"T00:00:00.000+00:00";
      var start_date_match =  {'from_date': { $gte:  new Date(start_date) }}      
      matchArrBooking.push(start_date_match);
      
      var created_match =  {'created': { $gte:  new Date(start_date) }} 
      matchArrInventory.push(created_match);
      
      matchArrBusinessSupplier.push(created_match);
      matchArrBusinessClient.push(created_match);
      matchArrRevenueCollection.push(created_match);
      matchArrvehicleAssigned.push(created_match);
      matchArrReferrals.push(created_match);
    }
    
    if(end_date){
      var endDateSet = end_date+"T00:00:00.000+00:00";
      var end_date_match =  {to_date: { $lte:  new Date(end_date) }}      
      matchArrBooking.push(end_date_match);
      // matchArrInventory.push(end_date_match);

      var created_match_endDate =  {'created': { $gte:  new Date(end_date) }} 
      matchArrInventory.push(created_match_endDate);                  
      matchArrBusinessSupplier.push(created_match_endDate);
      matchArrBusinessClient.push(created_match_endDate);
      matchArrRevenueCollection.push(created_match_endDate);
      matchArrvehicleAssigned.push(created_match_endDate);
      matchArrReferrals.push(created_match_endDate);
    }
    
    if(city_id){
      var city_idmatch =  {"inventory_id.city_id": (hex.test(city_id))? ObjectId(city_id) : city_id}      
      matchArrBooking.push(city_idmatch);
      matchArrRevenueCollection.push(city_idmatch);

      var city_idmatch_inventory =  {"city_id": (hex.test(city_id))? ObjectId(city_id) : city_id} 
      matchArrInventory.push(city_idmatch_inventory);
      matchArrvehicleAssigned.push(city_idmatch_inventory);
      
      // matchArrBusinessSupplier.push({"city_id":(hex.test(city_id))? ObjectId(city_id) : city_id});
      // matchArrBusinessClient.push({"city_id":(hex.test(city_id))? ObjectId(city_id) : city_id}); 
      matchArrReferrals.push({"city_id":(hex.test(city_id))? ObjectId(city_id) : city_id});
    }

    if(hub_id){
      var hub_idmatch =  {"inventory_id.hub_id":  (hex.test(hub_id))? ObjectId(hub_id) : hub_id}      
      matchArrBooking.push(hub_idmatch);

      var hub_idmatch_inventory =  {"hub_id": (hex.test(hub_id))? ObjectId(hub_id) : hub_id} 
      matchArrInventory.push(hub_idmatch_inventory);
      matchArrvehicleAssigned.push(hub_idmatch_inventory);

      matchArrReferrals.push({"hub_id":(hex.test(hub_id))? ObjectId(hub_id) : hub_id});
    }

    /* if(searchKey){        
        var searchKey_idmatch =  {
            "user_id.phone":searchKey
        }
        matchArr.push(searchKey_idmatch);        
    }  */  
  
    var matchfilter ={};    
    if(matchArr.length >0){
        matchfilter = {$and: matchArr} 
    } 

    var matchfilterBooking ={};    
    if(matchArrBooking.length >0){
      matchfilterBooking = {$and: matchArrBooking} 
    } 

    var matchfilterInvenotory ={};
    if(matchArrInventory.length >0){
      matchfilterInvenotory = {$and: matchArrInventory} 
    }
    var matchfilterBusinessSupplier ={};
    if(matchArrBusinessSupplier.length >0){
      matchfilterBusinessSupplier = {$and: matchArrBusinessSupplier} 
    }
    var matchfilterBusinessClient ={};
    if(matchArrBusinessClient.length >0){
      matchfilterBusinessClient = {$and: matchArrBusinessClient} 
    }

    var matchfilterReferrals ={};
    if(matchArrReferrals.length >0){
      matchfilterReferrals = {$and: matchArrReferrals} 
    }
    
    var matchfiltervehicleAssigned ={};
    if(matchArrvehicleAssigned.length >0){
      matchfiltervehicleAssigned = {$and: matchArrvehicleAssigned} 
    }

    // const NoOfSupplierget = await business_suppliers.count([ { created: { '$gte': '2022-07-01' } } ]);
    const NoOfSupplierGet = await business_suppliers.find(matchfilterBusinessSupplier);
    const NoOfSupplier = NoOfSupplierGet.length;
    // const NoOfSupplier = await business_suppliers.count(matchfilterBusinessSupplier);
    const setNoOfClients = await business_clients.count(matchfilterBusinessClient);    
    let referalsData = await userRefferal.aggregate([        
      {
          $sort:{created: -1}
      },
      { $lookup:
          {
              from: "users",
              localField: "user_id",
              foreignField: "_id",
              as: "user_id"
          }
      },
      { 
          $unwind: '$user_id'
      },
      { 
          $match:matchfilterReferrals 
      },
    ])
    const NoOfReferals = referalsData.length;
    // const NoOfReferals = await userRefferal.count(matchfilterReferrals);
    const NoOfVehicleSupplied = await inventories.count(matchfilterInvenotory);
    const NoOfVehicleAssigned = await inventories.count(matchfiltervehicleAssigned);
    
    const NoOfVehicleBooked = await bookings.aggregate([
      {
        $sort:{created: -1}
      },
      { $lookup:
        {
            from: "users",
            localField: "user_id",
            foreignField: "_id",
            // pipeline: pipeline,
            as: "user_id"
        }
      },
      { 
        $unwind: '$user_id'
      },
      { $lookup:
          {
              from: "inventories",
              localField: "inventory_id",
              foreignField: "_id",
              // pipeline: pipeline,
             as: "inventory_id"
          }
      },
      { 
          $unwind: '$inventory_id'
      },
      { 
          $match:matchfilterBooking 
      },          
    ]);

    const NoOfVehicleBookedAmount = await bookings.aggregate([
      {
        $sort:{created: -1}
      },
      { $lookup:
        {
            from: "users",
            localField: "user_id",
            foreignField: "_id",
            // pipeline: pipeline,
            as: "user_id"
        }
      },
      { 
        $unwind: '$user_id'
      },
      { $lookup:
          {
              from: "inventories",
              localField: "inventory_id",
              foreignField: "_id",
              // pipeline: pipeline,
             as: "inventory_id"
          }
      },
      { 
          $unwind: '$inventory_id'
      },
      { 
          $match:matchfilterBooking 
      }, 
      {
        $group: 
        {_id:null, sum_val:{$sum:"$total_amount"}}
      },
    ]);
    
    let total_amount = 0;
    if(NoOfVehicleBookedAmount.length >0){
      total_amount = NoOfVehicleBookedAmount[0].sum_val;
    }

    const allCities = await cities.find().sort({ name: 1 });

    return res.status(200).send({status: true, message: "Data Found",data:[],citiesList:allCities, NoOfSupplier:NoOfSupplier,setNoOfClients:setNoOfClients,NoOfVehicleSupplied:NoOfVehicleSupplied,NoOfVehicleAssigned:NoOfVehicleAssigned,NoOfReferals:NoOfReferals,NoOfVehicleBooked:NoOfVehicleBooked.length,NoOfVehicleBookedAmount:total_amount});

  } catch (err) {
    console.log(err);
  }
}

const getAllUsers = async (req, res) => { 
  try {
    const filterArray = req.body;
    const searchKey = req.body.searchKey;
    const status = req.body.status;
    const user_role = req.body.user_role;

    const allCities = await cities.find().sort({ name: 1 });
    const filterArr = [];

    if(searchKey != "" ){
      filterArr.push({'phone_number':searchKey})
    }
    if(status !== '' && status != "all"){
      filterArr.push({'status':status})
    }
    if(user_role != "" && user_role != "all"){
      filterArr.push({$and:[{"user_role":{$ne: "FAE_MAIN_ADMIN" }},{"user_role":{$eq: user_role}}]})
    }else{
      filterArr.push({"user_role":{ $ne: "FAE_MAIN_ADMIN"  } })
    }
    
    const adminList = await Admin.find({$and:filterArr}).sort({created: -1}) ;
    if(adminList){
      adminList.filter(function(val,index){
          return val['key']=index;
      })
    }      
    return res.status(200).send({status: true, message: "Data Found", data: adminList, allCities:allCities});

  } catch (err) {
    console.log(err);
  }
}
const updateAdmin = async (req, res) => { 
  try {      
    if (!req.body) {
        return res.status(400).send({
            message: "Data to update can not be empty!"
        });
    } 
    const id = req.params.id;  
    const phone_number = req.body.phone_number;  
    const email = req.body.email;  

    const oldUser = await Admin.findOne({$and:[{"_id":{ $ne: id } },{ "email":email }]});
    if (oldUser) { 
      return res.status(200).send({ status: false, message: 'Email already register, please use another email address.' });
    }

    const checkPhone = await Admin.findOne({$and:[{"_id":{ $ne: id } },{ "phone_number":phone_number }]});
    if (checkPhone) { 
      return res.status(200).send({ status: false, message: 'Phone Number already register, please use another phone number.' });
    }

    const data = await Admin.findByIdAndUpdate(id, req.body);
    console.log('data',data)
    if (!data) {         
        return res.status(200).send({status: false, message: "Cannot update User details! with id=${id}. Maybe user! was not found!",data: []});
    }

    const allAdminData = await getAllAdminList();      

    return res.status(200).send({status: true, message: "Users successfully updated!", data: {'data':data,'allAdminData':allAdminData}});
    

  } catch (err) {
    console.log(err);
    return res.status(500).send({status: false, message: err.message || "Some error occurred while updating User.",data: []});
  }
}

const deleteAdmin = async (req, res) => { 
  try {      
    
    const id = req.params.id;  
    const data = await Admin.findByIdAndRemove(id);
    console.log('data',data)
    if (!data) {         
        return res.status(200).send({status: false, message: "Cannot delete User details! with id=${id}. Maybe user! was not found!",data: []});
    }

    const allAdminData = await getAllAdminList();

    return res.status(200).send({status: true, message: "Users successfully deleted!", data: {'data':data,'allAdminData':allAdminData}});
    

  } catch (err) {
    console.log(err);
    return res.status(500).send({status: false, message: err.message || "Some error occurred while updating User.",data: []});
  }
}

async function getAllAdminList(){
  const allAdminData = await Admin.find({$and:[{"user_role":{ $ne: "FAE_MAIN_ADMIN"  } }]}).sort({created: -1});
  if(allAdminData){
    allAdminData.filter(function(val,index){
        return val['key']=index;
    })
  }
  return allAdminData;

}
const getUserDetailsWithToken=async(access_token)=>{
  const admin_details = await Admin.findOne({"token":access_token});
  return admin_details;
}

const fotgot_password = async (req, res) => {
  try {
    const { email} = req.body;
    if (!(email)) {
      return res.status(400).send({ status: 'false', message: "Please Send email." });
    }
    const user = await Admin.findOne({ email:email });
    if(!user) {
      return res.status(200).send({status: false, message: "User does't Exist!"});
    }
    const otp = Math.floor(100000 + Math.random() * 900000);
    const genotp = otp.toString()
    const wherecon = {'_id':user._id }
    const updvalues = { $set: { otp: genotp } };
    const otpupd = await  Admin.updateOne(wherecon, updvalues);
    
    if(!otpupd){
      return res.status(200).send({status: false, message: "Something went wrong, please try again.", data: ''});
    }
    // sendgrid
    sendEmail({
      sendTo: user.email,
      subject: 'FaeBikes - Forgot Password OTP Verification',
      template: `<!DOCTYPE html>
      <html>
          <body>
              <p>Your Verification Code for FaeBikes is ${genotp}</p>
          </body>
      </html>`
    })
    
    // send password on mail
    // sendEmail({
    //   sendTo: email,
    //   subject: 'FaeBikes - Forgot Password ',
    //   template: `<!DOCTYPE html>
    //   <html>
    //       <body>
    //           <p>Your Password is ${user.password}</p>
    //       </body>
    //   </html>`
    // })
    return res.status(200).send({status: true, message: "Verification Code send your email.", data: user.password});

  } catch (err) {
    console.log(err);
  }
}

const resendotp = async (req, res) => {
  try {
    const { user_id} = req.body;
    console.log('user_id',user_id)
    if (!(user_id)) {
      return res.status(400).send({ status: 'false', message: "Please Send user id." });
    }
    const user = await Admin.findOne({ _id: (hex.test(user_id))? ObjectId(user_id) : user_id});
    if(!user) {
      return res.status(200).send({status: false, message: "User does't Exist!"});
    }
    const otp = Math.floor(100000 + Math.random() * 900000);
    const genotp = otp.toString()
    const wherecon = {'_id':user_id }
    const updvalues = { $set: { otp: genotp } };
    const otpupd = await  Admin.updateOne(wherecon, updvalues);

    if(!otpupd){
      return res.status(200).send({status: false, message: "Something went wrong, please try again.", data: ''});
    }
    // sendgrid
    sendEmail({
      sendTo: user.email,
      subject: 'FaeBikes - Login OTP Verification',
      template: `<!DOCTYPE html>
      <html>
          <body>
              <p>Your Verification Code for FaeBikes is ${genotp}</p>
          </body>
      </html>`
    })
    return res.status(200).send({status: true, message: "Verification Code send your register phone number/email.", data: genotp});

  } catch (err) {
    console.log(err);
  }
}
const verifyotp = async (req, res) => {
  try {
    console.log('req.body',req.body)
    const { email, otp } = req.body;
    
    if (!(email && otp)) {
      return res.status(400).send({ status: 'false', message: "Please enter OTP." });
    }
    const user = await Admin.findOne({ otp });
    if (user) {
      return res.status(200).send({status: true, message: "OTP successfully verified!.", data: user});
               
    }else{
      return res.status(200).send({status: false, message: "Invalid OTP", data:user});
    }
    return res.status(200).send({status: false, message: "Invalid Email Id"});

  } catch (err) {
    console.log(err);
  }
}

const change_password = async (req, res) => {
  try {      
    if(!req.body) {
      return res.status(400).send({
          message: "Data to update can not be empty!"
      });
    } 
    const id = ObjectId(req.body.id);  
    const password = req.body.password;  

    const encryptedPassword = await bcrypt.hash(password, 10);
    console.log('encryptedPassword',encryptedPassword)
    const updateData = {
      password:encryptedPassword,
    }
 
    const data = await Admin.findByIdAndUpdate(id,updateData);
    
    if (!data) {  
        return res.status(200).send({status: false, message: "Cannot update User details! with id=${id}. Maybe User! was not found!",data: []});
    } 
    return res.status(200).send({status: true, message: "Users successfully updated!", data: {'data':[]}});
  

  } catch (err) {
      console.log(err);
      return res.status(500).send({status: false, message: err.message || "Some error occurred while updating User.",data: []});
  } 
}


module.exports = { getAllUsers,createAdmin,updateAdmin,deleteAdmin,getUserDetailsWithToken,getAllDashboard,fotgot_password, resendotp,verifyotp,change_password};
