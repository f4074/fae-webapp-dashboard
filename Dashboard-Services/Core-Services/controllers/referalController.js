require("dotenv").config();
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const cities = require("../models/cities");
const referals = require("../models/referals");
const userRefferal = require("../models/user_refferal");
const ObjectId = require('mongodb').ObjectId;
const auth = require("../middleware/auth");
const sendEmail = require("../common/sendMail")

const getAll = async (req, res) => {  
    try {       
        const citiesData = await cities.find().sort({ name: 1 });
        if(citiesData){
            citiesData.filter(function(val,index){
                return val['key']=index;
            })
        }
        
        const campaignData = await referals.find().sort({ created: -1 });
        if(campaignData){
            campaignData.filter(function(val,index){
                return val['key']=index;
            })
        }

        const referalsData = await userRefferal.find().populate('user_id').populate('reffered_user_id').sort({ created: -1 });
        if(referalsData){
            referalsData.filter(function(val,index){
                return val['key']=index;
            })
        }

        return res.status(200).send({status: true, message: "Data Found", data: campaignData,referalsDataAll:referalsData, citiesData: citiesData});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while getting all data of cities.",data: []});
    }
}
const getAllReferalsFilters = async (req, res) => {  
    try { 
        const filterArray = req.body;  
        var matchArr =[];
        var matchArrRef =[];
        if(filterArray != null){
            const city_id_filter = filterArray.city_id;
            const hub_id_filter = filterArray.hub_id;

            if(city_id_filter){        
                var City_id_idmatch =  {"city_id._id":ObjectId(city_id_filter)}
                matchArr.push(City_id_idmatch);
                
                var City_id_idmatchRef =  {"user_id.city_id":ObjectId(city_id_filter)}
                matchArrRef.push(City_id_idmatchRef);
            }
            if(hub_id_filter){        
                var hub_id_idmatch =  {"hub_id._id":ObjectId(hub_id_filter)}
                matchArr.push(hub_id_idmatch); 
                
                var hub_id_idmatchRef =  {"user_id.hub_id":ObjectId(hub_id_filter)}
                matchArrRef.push(hub_id_idmatchRef);
            }
        } 

        var matchfilter ={};
        if(matchArr.length >0){
            matchfilter = {$and: matchArr} 
        } 

        var matchfilterRef ={};
        if(matchArrRef.length >0){
            matchfilterRef = {$and: matchArrRef} 
        }

        // GET ALL CITY DATA   
        const citiesData = await cities.find().sort({ name: 1 });
        if(citiesData){
            citiesData.filter(function(val,index){
                return val['key']=index;
            })
        }
        
        // GET ALL CAMPAIGN DATA       
        let campaignData = await referals.aggregate([        
            {
                $sort:{created: -1}
            },
            { $lookup:
                {
                    from: "cities",
                    localField: "city_id",
                    foreignField: "_id",
                    as: "city_id"
                }
            },
            { 
                $unwind: '$city_id'
            },
            { 
                $match:matchfilter 
            },
        ]) 
    
        if(campaignData){
            campaignData.filter((val,index)=>{
                return val.key = index;
            })
        }

        // GET ALL USER REFERALS DATA
        // const referalsData = await userRefferal.find().populate('user_id').populate('reffered_user_id').sort({ created: -1 });
        let referalsData = await userRefferal.aggregate([        
            {
                $sort:{created: -1}
            },
            { $lookup:
                {
                    from: "users",
                    localField: "user_id",
                    foreignField: "_id",
                    as: "user_id"
                }
            },
            { 
                $unwind: '$user_id'
            },
            { 
                $match:matchfilterRef 
            },
        ]) 
        if(referalsData){
            referalsData.filter(function(val,index){
                return val.key = index;
            })
        }

        return res.status(200).send({status: true, message: "Data Found", data: campaignData,referalsDataAll:referalsData, citiesData: citiesData});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while getting all data of cities.",data: []});
    }
}

const getDetails = async (req, res) => {  
    try {       
        const referals = await referals.findOne(req._id);
        return res.status(200).send({status: true, message: "Data Found", data: referals});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while getting all data of business clients.",data: []});
    }

}

const create = async (req, res) => {
    try { 
        if (!req.body) {
            return res.status(400).send({
                message: "Data to update can not be empty!"
            });
        } 
        const referalsSendData = new referals(req.body);             
        const referalsData = await referalsSendData.save();
        console.log('referalsData',referalsData)
        if (referalsData) {  

            const campaignDataAll = await referals.aggregate([        
                {
                    $sort:{created: -1}
                },
                { $lookup:
                    {
                        from: "cities",
                        localField: "city_id",
                        foreignField: "_id",
                        as: "city_id"
                    }
                },
                { 
                    $unwind: '$city_id'
                },               
            ]);
            if(campaignDataAll){
                campaignDataAll.filter(function(val,index){
                    return val['key']=index;
                })
            }

            const referalsDataAll = await userRefferal.aggregate([        
                {
                    $sort:{created: -1}
                },
                { $lookup:
                    {
                        from: "users",
                        localField: "user_id",
                        foreignField: "_id",
                        as: "user_id"
                    }
                },
                { 
                    $unwind: '$user_id'
                },                
            ])
            if(referalsDataAll){
                referalsDataAll.filter(function(val,index){
                    return val['key']=index;
                })
            }

            return res.status(200).send({status: true, message: "City successfully created!", data: referalsData,campaignDataAll:campaignDataAll,referalsDataAll:referalsDataAll});
        }
        return res.status(404).send({status: false, message: "Some error occurred while creating the referal!",data: []});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while creating city.",data: []});
    }

}
const update = async (req, res) => {        
    try {      
        if (!req.body) {
            return res.status(400).send({
                message: "Data to update can not be empty!"
            });
        } 
        const id = req.params.id;  
        const referalsData = await referals.findByIdAndUpdate(id, req.body, { useFindAndModify: false });
        console.log('referalsData',referalsData)
        if (referalsData) {  
            
            const campaignDataAll = await referals.aggregate([        
                {
                    $sort:{created: -1}
                },
                { $lookup:
                    {
                        from: "cities",
                        localField: "city_id",
                        foreignField: "_id",
                        as: "city_id"
                    }
                },
                { 
                    $unwind: '$city_id'
                },               
            ]);
            if(campaignDataAll){
                campaignDataAll.filter(function(val,index){
                    return val['key']=index;
                })
            }

            const referalsDataAll = await userRefferal.aggregate([        
                {
                    $sort:{created: -1}
                },
                { $lookup:
                    {
                        from: "users",
                        localField: "user_id",
                        foreignField: "_id",
                        as: "user_id"
                    }
                },
                { 
                    $unwind: '$user_id'
                },                
            ]);
            
            if(referalsDataAll){
                referalsDataAll.filter(function(val,index){
                    return val['key']=index;
                })
            }

            return res.status(200).send({status: true, message: "Referal successfully updated!", data: referalsData,campaignDataAll:campaignDataAll,referalsDataAll:referalsDataAll});
        }
        return res.status(404).send({status: false, message: "Cannot update Referal! with id=${id}. Maybe Referal! was not found!",data: []});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while updating Referal.",data: []});
    }

}
const deleteRecord = async (req, res) => {        
    try {       
        const id = req.params.id;
        const referalsData = await referals.findByIdAndRemove(id);
        if (referalsData) { 
            const campaignDataAll = await referals.find().sort({ created: -1 });
            if(campaignDataAll){
                campaignDataAll.filter(function(val,index){
                    return val['key']=index;
                })
            } 
            
            const referalsDataAll = await referals.find().sort({ created: -1 });
            if(referalsDataAll){
                referalsDataAll.filter(function(val,index){
                    return val['key']=index;
                })
            } 
            return res.status(200).send({status: true, message: "Referal successfully deleted!", data: referalsData,campaignDataAll:campaignDataAll,referalsDataAll:referalsDataAll});
          }
          return res.status(404).send({status: false, message: "Cannot delete Referal! with id=${id}. Maybe Referal! was not found!",data: []});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while removing Referal.",data: []});
    }

}

const deleteMultiple = async (req, res) => {        
    try { 
        const deleteJson = req.body;
        const data = await referals.deleteMany(deleteJson);
        if (data) {         
            return res.status(200).send({status: true, message: `${data.deletedCount} Referal were deleted successfully!`, data: []});
          }
          return res.status(404).send({status: false, message: "Some error occurred while removing all Referal",data: []});
    
    } catch (err) {
        console.log(err);
        return res.status(500).send({status: false, message: err.message || "Some error occurred while removing all Referal.",data: []});
    }
}

module.exports = { getAll,getAllReferalsFilters,getDetails,create,update,deleteRecord,deleteMultiple };