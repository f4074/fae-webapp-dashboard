const http = require("http");
const app = require("./app");
const server = http.createServer(app);
const express = require("express");
const bodyParser= require('body-parser');

global.__basedir = __dirname;

const { API_PORT } = process.env;
const port = process.env.PORT_CORE || API_PORT;


const initRoutes = require("./routes");
app.use(express.urlencoded({ extended: true }));

initRoutes(app);
app.use('/uploads/', express.static(__dirname+'/uploads/'));

// server listening 
server.listen(port, () => {
  console.log(`Server running on port ${port}`);
});

// Parses the text as url encoded data
// app.use(bodyParser.urlencoded({extended: true})); 
  
// Parses the text as json
// app.use(bodyParser.json()); 