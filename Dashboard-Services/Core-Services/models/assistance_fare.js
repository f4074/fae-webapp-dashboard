const mongoose = require("mongoose");
const validator = require("validator");

const assistanceFareSchema = new mongoose.Schema({  
    city_id: { 
        type: mongoose.Schema.Types.ObjectId,ref:'cities' , 
    },
    assistance_type_id:{ 
        type: mongoose.Schema.Types.ObjectId,ref:'assistance_type' , 
    },
    problem_type_id:{ 
        type: mongoose.Schema.Types.ObjectId,ref:'problem_type' , 
    },
    price: { type: String,trim: true  },
    gst: { type: String,required: true,trim: true  },    
    term_conditions: { type:String,trim: true  },       
    status: { type: Boolean,default:true},
    created: { type: Date, default: Date.now },
    updated: { type: Date, default: Date.now }   

},{ timestamps: true });
module.exports = mongoose.model("assistance_fare", assistanceFareSchema);
