const mongoose = require("mongoose");
const validator = require("validator");
const SupportassistanceSchema = new mongoose.Schema({    
    user_id: {
         type: mongoose.Schema.Types.ObjectId,ref:'users'
         },
    booking_id:{ 
        type: mongoose.Schema.Types.ObjectId,ref:'bookings' ,  
    } ,  
    assistance_type:{ 
        type: mongoose.Schema.Types.ObjectId,ref:'assistance_type' , 
    }, 
    problem_type:{ 
        type: mongoose.Schema.Types.ObjectId,ref:'problem_type' , 
    }, 
    city_id: { 
        type: mongoose.Schema.Types.ObjectId,ref:'cities' , 
    },
    hub_id: { 
        type: mongoose.Schema.Types.ObjectId,ref:'hubs' ,  
    },
    current_location: { type: String, required: false },
    location: {
        type: { type: String },
        coordinates: [Number],
      },
    image_file: { type: String, required: true },
    amount: { type: Number, default: 0  },
    additional_amount: { type: Number, default: 0},
    comment: { type: String},
    additional_comment: { type: String},
    status: { type: Number, default: 0 },
    assistance_status: {
        type: String,
        trim: true,
        enum : ["open","picked","resolved","invalid","payment_requested"],
        default: "open"
    },
    created: { type: Date },
    updated: { type: Date }    
});

SupportassistanceSchema.index({ "location": "2dsphere" });



const Supportass_fileSchema = new mongoose.Schema({    
    support_assistances_id:{ 
        type: mongoose.Schema.Types.ObjectId,ref:'support_assistances' , 
    }, 
    file_name: { type: String, default: '' },
    mimetype: { type: String, default: '' },
    destination: { type: String, default: '' },
    created: { type: Date },
    updated: { type: Date }    
});

//module.exports = mongoose.model("support_assistance", SupportassistanceSchema);
const support_assistance = mongoose.model('support_assistance', SupportassistanceSchema);
const supportass_file = mongoose.model('Supportass_file', Supportass_fileSchema);


module.exports = { support_assistance, supportass_file };