const mongoose = require("mongoose");
const validator = require("validator");

const problemTypeSchema = new mongoose.Schema({ 
    problem_type: { type: String,trim: true  },
    created: { type: Date, default: Date.now },
    updated: { type: Date, default: Date.now }   

},{ timestamps: true });
module.exports = mongoose.model("problem_type", problemTypeSchema);
