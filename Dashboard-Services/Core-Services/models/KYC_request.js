const mongoose = require("mongoose");
const validator = require("validator");

const KYC_requestSchema = new mongoose.Schema({  
    user_id: { 
        type: mongoose.Schema.Types.ObjectId,ref:'users' , 
        default:null
    },
    aadhar_front: { type: String,trim: true  },
    aadhar_back: { type: String,trim: true  }, 
    pan_card: { type: String,trim: true  },
    driving_licence: { type: String,trim: true  },
    other: { type: String,trim: true  },    
    reject_reason: { type: String,trim: true  },       
    status: { type: Number,default:1},
    created: { type: Date, default: Date.now },
    updated: { type: Date, default: Date.now }   

},{ timestamps: true });
module.exports = mongoose.model("KYC_request", KYC_requestSchema);
