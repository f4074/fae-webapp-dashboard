const mongoose = require("mongoose");
const validator = require("validator");
const userRefferalSchema = new mongoose.Schema({    
    user_id: { type: mongoose.Schema.Types.ObjectId,ref:'user' },
    reffered_user_id: {  type: mongoose.Schema.Types.ObjectId,ref:'user' },
    referral_amount: { type: String, default: 0 },
    referral_type: { type: String, default: null },
    status: { type: Boolean, default: 0 },
    created: { type: Date, default: Date.now },
    updated: { type: Date, default: Date.now }    
});
module.exports = mongoose.model("userRefferal", userRefferalSchema);