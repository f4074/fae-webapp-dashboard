const mongoose = require("mongoose");
const validator = require("validator");
const adminSchema = new mongoose.Schema({  
    first_name: { type: String,trim: true ,minLength: 2, maxlength: 50 },
    last_name: { type: String, trim: true,minLength: 2, maxlength: 50},
    email: {type: String, unique: true,trim: true},
    password: {  type: String},
    phone_number: { type: String, min: 10 ,trim: true},
    phone_code:{type:String, default: '91',trim: true},
    token:{type:String, default:null,trim: true},
    profile_photo: { type: String },
    user_role: { 
        type: String ,
        enum : ['FAE_MAIN_ADMIN','FAE_ADMIN','FAE_CITY_ADMIN','FAE_HUB_ADMIN','FAE_HUB_MANAGER','FAE_STAFF'],
        default: null
    },    
    address: { type: String },
    lat: { type: String },
    lng: { type: String },
    // city_id: { 
    //     type: mongoose.Schema.Types.ObjectId,ref:'cities' , 
    //     default:null
    // },
    // hub_id: { 
    //     type: mongoose.Schema.Types.ObjectId,ref:'hub' , 
    //     default:null 
    // },
    city_id: { 
        type:Array, 
        default: [] 
    },
    hub_id: { 
        type:Array, 
        default: []
    },
    city: { type: String },
    state: { type: String },
    country: { type: String },
    pincode: { type: String },
    status: { type: Boolean,default: true},
    created: { type: Date, default: Date.now },
    updated: { type: Date, default: Date.now }  

},{ timestamps: true });

module.exports = mongoose.model("admin", adminSchema);
