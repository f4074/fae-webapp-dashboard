const mongoose = require("mongoose");
const validator = require("validator");

const referalsSchema = new mongoose.Schema({  
    campaign_name: { type: String,trim: true  },
    campaign_description: { type: String,trim: true  },
    city_id: { 
        type: mongoose.Schema.Types.ObjectId,ref:'cities' ,
    },   
    discount_amount: { type:Number,trim: true ,default:0 },
    start_date: { type: Date,trim: true  },     
    end_date: { type: Date,trim: true  },    
    active_status: { type: Boolean,default:true},
    created: { type: Date, default: Date.now },
    updated: { type: Date, default: Date.now }   

},{ timestamps: true });
module.exports = mongoose.model("referals", referalsSchema);
