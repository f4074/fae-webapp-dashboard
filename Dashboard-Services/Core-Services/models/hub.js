const mongoose = require("mongoose");
const validator = require("validator");
const hubSchema = new mongoose.Schema({ 
    city_id: { 
        type: mongoose.Schema.Types.ObjectId,ref:'cities' ,
    },   
    title: { type: String,trim: true  },   
    description: { type: String,trim: true },   
    isCentralHub: { type: String , default: 'no'},
    status: { type: Boolean ,trim: true, default: true},
    created: { type: Date, default: Date.now },
    updated: { type: Date, default: Date.now }   

},{ timestamps: true });

module.exports = mongoose.model("hub", hubSchema);
