const mongoose = require("mongoose");
const validator = require("validator");

const businessSuppliersSchema = new mongoose.Schema({  
    supplier_name: { type: String,trim: true  },
    type: { type: Number, trim: true},
    contact_person: { type: String,trim: true},
    phone_number: { type: String ,trim: true},
    email: { type: String ,trim: true},
    address: { type: String ,trim: true},
    city: { type: String ,trim: true},
    city_id: { 
        type: mongoose.Schema.Types.ObjectId,ref:'cities' , 
    },
    additional: { type: String ,trim: true},   
    status: { type: String ,trim: true},
    created: { type: Date, default: Date.now },
    updated: { type: Date, default: Date.now }  

},{ timestamps: true });

module.exports = mongoose.model("business_suppliers", businessSuppliersSchema);
