const mongoose = require("mongoose");
const validator = require("validator");
const InventoryHistorySchema = new mongoose.Schema({
    inventory_id: { 
        type: mongoose.Schema.Types.ObjectId,ref:'inventories' ,
    },
    title: { type: String, default: null },
    description:{ type: String, default: null },
    from_city_id:{ 
        type: mongoose.Schema.Types.ObjectId,ref:'cities' , 
    },
    from_hub_id:{ 
        type: mongoose.Schema.Types.ObjectId,ref:'hubs' , 
    },
    to_city_id:{ 
        type: mongoose.Schema.Types.ObjectId,ref:'cities' , 
    },
    to_hub_id:{ 
        type: mongoose.Schema.Types.ObjectId,ref:'hubs' , 
    },
    status: { type: String, default: 1 } ,
    created: { type: Date, default: Date.now },
    updated: { type: Date, default: Date.now }   
});
module.exports = mongoose.model("inventory_history", InventoryHistorySchema);