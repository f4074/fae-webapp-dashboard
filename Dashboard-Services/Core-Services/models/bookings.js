const mongoose = require("mongoose");
const validator = require("validator");
const BookingsSchema = new mongoose.Schema({
    inventory_id: { 
        type: mongoose.Schema.Types.ObjectId,ref:'inventories' ,
    },
    user_id: { 
        type: mongoose.Schema.Types.ObjectId,ref:'user' ,
    },
    booked_by: { 
        type: mongoose.Schema.Types.ObjectId,ref:'user' ,
    },
    from_date: { type: Date, default: null },
    to_date: { type: Date, default: null },
    from_time_slot: { type: String, default: null },
    amount: { type: Number, default: "0" },
    discount_amount: { type: Number, default: "0" },
    total_amount: { type: Number, default: "0" },
    tax_amount: { type: Number, default: "0" },
    city_id: { 
        type: mongoose.Schema.Types.ObjectId,ref:'cities' , 
    },
    hub_id: { 
        type: mongoose.Schema.Types.ObjectId,ref:'hub' ,  
    },
    // business_client_id: { 
    //     type: mongoose.Schema.Types.ObjectId,ref:'business_clients' ,  
    // },
    business_supplier_id: { 
        type: mongoose.Schema.Types.ObjectId,ref:'business_suppliers' ,  
    },
    images:{ type: String, default: null },
    imageArr:{ type: Array, default: [] },
    battery_health:{ type: String, default: null },
    battery_1:{ type: String, default: null },
    battery_2:{ type: String, default: null },
    charger_1:{ type: String, default: null },
    charger_2:{ type: String, default: null },   
    // spares:{  type: Array, default: [] },
    spares:{  type: String, default: null },
    iot_devices:{ type: String, default: null },
    comment:{ type: String, default: null },
    advance_amount: { type: Number, default: "0" },
    rental_amount: { type: Number, default: "0" },
    // booking_status: { 
    //     type: String,  
    //     enum : ['intiated','assigned'],
    //     default: 'intiated'
    // },
    status: { type: Boolean, default: 0 } ,
    created: { type: Date, default: Date.now },
    updated: { type: Date, default: Date.now }   
});
module.exports = mongoose.model("bookings", BookingsSchema);