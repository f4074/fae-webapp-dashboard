const mongoose = require("mongoose");
const validator = require("validator");

const assistanceTypeSchema = new mongoose.Schema({ 
    assistance_type: { type: String,trim: true  },
    created: { type: Date, default: Date.now },
    updated: { type: Date, default: Date.now }   

},{ timestamps: true });
module.exports = mongoose.model("assistance_type", assistanceTypeSchema);
