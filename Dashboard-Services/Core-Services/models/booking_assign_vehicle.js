const mongoose = require("mongoose");
const validator = require("validator");

const bookingAssignVehicleSchema = new mongoose.Schema({  
    booking_id: { 
        type: mongoose.Schema.Types.ObjectId,ref:'bookings' 
    },
    user_id: { 
        type: mongoose.Schema.Types.ObjectId,ref:'user' ,
    },
    vehicle_model: { type: String,trim: true  },    
    entity_name: { type: String,trim: true  },
    battery_health: { type: String,trim: true  },     
    spares_id: {  
        type: mongoose.Schema.Types.ObjectId,ref:'spares' , 
    },
    photos_videos: { type: String,trim: true  },    
    city_id: { 
        type: mongoose.Schema.Types.ObjectId,ref:'cities' , 
    },
    hub_id: { 
        type: mongoose.Schema.Types.ObjectId,ref:'hub' ,  
    }, 
    vehicle_id: { 
        type: mongoose.Schema.Types.ObjectId,ref:'vehicles' ,  
    },      
    comment: { type: String,trim: true  },      
    otp: { type: Number},
    status: { type: Boolean,default:false},
    created: { type: Date, default: Date.now },
    updated: { type: Date, default: Date.now }   

},{ timestamps: true });
module.exports = mongoose.model("bookingAssignVehicle", bookingAssignVehicleSchema);
