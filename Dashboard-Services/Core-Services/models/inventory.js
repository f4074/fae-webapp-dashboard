const mongoose = require("mongoose");
const validator = require("validator");

const inventorySchema = new mongoose.Schema({  
    supplier_id: { 
        type: mongoose.Schema.Types.ObjectId,ref:'business_suppliers' ,
        default: null,
    },
    city_id: { 
        type: mongoose.Schema.Types.ObjectId,ref:'cities' , 
        default:null
    },
    hub_id: { 
        type: mongoose.Schema.Types.ObjectId,ref:'hub' , 
        default:null 
    },
    to_city_id: { 
        type: mongoose.Schema.Types.ObjectId,ref:'cities' , 
        default:null
    },
    to_hub_id: { 
        type: mongoose.Schema.Types.ObjectId,ref:'hub' ,  
        default:null
    }, 
    supplier_name: { type: String,trim: true  },
    city_name: { type: String,trim: true  },
    hub_name: { type: String,trim: true  },    
    type: { type: String,trim: true  }, 
    vehicle_id: {  
        type: mongoose.Schema.Types.ObjectId,ref:'vehicles' , 
        default:null
    },    
    spares_id: {  
        type: mongoose.Schema.Types.ObjectId,ref:'spare' , 
        default:null
    },    
    iot_device_id: { 
        type: mongoose.Schema.Types.ObjectId,ref:'iot_devices' ,
        default:null
    }, 
    quantity: { type: Number,default:0},
    status: { type: Number,default:1},
    assignedTo: { type: String,default:null},
    created: { type: Date, default: Date.now },
    updated: { type: Date, default: Date.now }   

},{ timestamps: true });
module.exports = mongoose.model("inventories", inventorySchema);
