const mongoose = require("mongoose");
const validator = require("validator");

const sparesSchema = new mongoose.Schema({ 
    business_supplier: {type: String},
    city: {  type: String},
    hub: {  type: String},    
    part_number:{type: String, unique: true},
    supplier_description: { type: String, trim: true},
    image_url:{type:String,trim: true},
    quantity: { type: Number},
    uom: { type: String },
    hsn: { type: String},
    gst: { type: String},
    dealer_sale_price: { type: String},
    dlp: { type: String},
    sale_rate: { type: String },
    mrp: { type: String},
    model: { type: String},     
    part_type:{
        type: String,
        trim: true,
        enum : ["spares","battery","charger"],
        default: "spares"
    },  
    assigned_status: { type: Number ,default:0}, 
    status: { type: Boolean ,default:true},
    created: { type: Date, default: Date.now },
    updated: { type: Date, default: Date.now }   

},{ timestamps: true });
module.exports = mongoose.model("spare", sparesSchema);
