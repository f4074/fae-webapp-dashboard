const mongoose = require("mongoose");
const validator = require("validator");
const BookingTransactionSchema = new mongoose.Schema({
    booking_id: { 
        type: mongoose.Schema.Types.ObjectId,ref:'bookings' 
    },
    user_id: { 
        type: mongoose.Schema.Types.ObjectId,ref:'user' ,
    },
    txn_id: { type: String, default: null },
    amount: { type: String, default: "0" },
    payment_mode: { type: String, default: null },    
    status: { type: Boolean, default: 0 },
    created: { type: Date, default: Date.now },
    updated: { type: Date, default: Date.now }    
});
module.exports = mongoose.model("bookingsTransaction", BookingTransactionSchema);