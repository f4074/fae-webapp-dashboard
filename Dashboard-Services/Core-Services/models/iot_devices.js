const mongoose = require("mongoose");
const validator = require("validator");

const iotDevicesSchema = new mongoose.Schema({   
    business_supplier: {  type: String},
    city: {  type: String},
    hub: {  type: String},   
    device_serial_number: {type: String, unique: true},
    make: { type: String},
    model_of_device:{type:String,trim: true},
    imei: { type: String},
    provided_by: { type: String },
    consignmentTrackingNumber_CourierDocket: { type: String},
    CustomerOrderNumber: { type: String},
    DispatchDetailsRemarks: { type: String},
    WarrantyStartdate: { type: String},
    Warrantyterms: { type: String },
    WarrantyEnddate: { type: String},
    VF_MSISDN: { type: String},     
    VF_ICCID: { type: String},
    VF_IMSI: { type: String},
    circle: { type: String},
    history_remark: { type: String},
    device_status: { type: String},
    sim_status: { type: String},
    status: { type: Boolean ,default:true},
    assigned_status: { type: Number ,default:0}, 
    created: { type: Date, default: Date.now },
    updated: { type: Date, default: Date.now }   

},{ timestamps: true });
module.exports = mongoose.model("iot_devices", iotDevicesSchema);
