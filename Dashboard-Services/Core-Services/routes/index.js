const express = require("express");
const router = express.Router();
const usercontroller = require("../controllers/usersController");
const admincontroller = require("../controllers/adminController");
const businessClientsController = require("../controllers/businessClientsController");
const businessSuppliersController = require("../controllers/businessSuppliersController");
const hubController = require("../controllers/hubController");
const citiesController = require("../controllers/citiesController");
const inventoryController = require("../controllers/inventoryController");
const bookingsController = require("../controllers/bookingsController");
const referalController = require("../controllers/referalController");
const assistanceController = require("../controllers/assistanceController")
const verifyToken = require("../middleware/auth.js");
var multer  = require('multer');
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './uploads')
  },
  filename: function (req, file, cb) {
    const fileName = file.originalname.toLowerCase().split(' ').join('-');
        cb(null, Date.now() + '-'+fileName)
  }
})
var upload = multer({ storage: storage })
const AUTH_API= process.env.AUTH_API;
const CORE_API= process.env.CORE_API;

// user routes
let routes = (app) => {  
  //user routes 
  router.get("/users", usercontroller.getAllUsers); 
  router.post("/users_filter", usercontroller.getAllUsersFilter); 
  router.put("/users/:id", usercontroller.updateUserDetails); 
  router.post("/getAllKycRequest", usercontroller.getAllKYCRequestUsers); 
  router.get("/usersKycDetails/:id", usercontroller.getUsersKYC); 
  router.get("/getAllKycList", usercontroller.getAllKYCUsers); 
  router.delete("/users/:id", usercontroller.deleteUser);
  // router.post("/resend_otp", usercontroller.resendotp);
  // router.post("/fotgot_password", usercontroller.fotgot_password);
  // router.post("/otp_verify", usercontroller.verifyotp);
  // router.post("/change_password", usercontroller.change_password);
  router.post("/resend_otp_user", usercontroller.resendotp);
  
  // admin routes 
  router.get("/dash-users", admincontroller.getAllUsers);
  router.post("/dash-users-filter", admincontroller.getAllUsers);
  router.post("/dash-users", admincontroller.createAdmin);
  router.put("/dash-users/:id", admincontroller.updateAdmin);
  router.delete("/dash-users/:id", admincontroller.deleteAdmin);
  router.post("/dashboard", admincontroller.getAllDashboard);
  router.post("/resend_otp", admincontroller.resendotp);  
  router.post("/fotgot_password", admincontroller.fotgot_password);
  router.post("/otp_verify", admincontroller.verifyotp);
  router.post("/change_password", admincontroller.change_password);


  // businessClients routes
  router.get("/business-clients", businessClientsController.getAllBusinessClients);
  router.post("/business-clients-filter", businessClientsController.getAllBusinessClientsFilter);  
  router.post("/business-clients", businessClientsController.createBusinessClients);  
  router.get("/business-clients/:id", businessClientsController.getDetailsBusinessClients);  
  router.put("/business-clients/:id", businessClientsController.updateBusinessClients);  
  router.delete("/business-clients/:id", businessClientsController.deleteBusinessClients);  

  // business suppliers routes
  router.get("/business-suppliers", businessSuppliersController.getAllBusinessSuppliers); 
  router.post("/business-suppliers-filter", businessSuppliersController.getAllBusinessSuppliersFilter); 
  router.get("/business-suppliers/:id", businessSuppliersController.getDetailsBusinessSuppliers);  
  router.post("/business-suppliers", businessSuppliersController.createBusinessSuppliers);  
  router.put("/business-suppliers/:id", businessSuppliersController.updateBusinessSuppliers);  
  router.delete("/business-suppliers/:id", businessSuppliersController.deleteBusinessSuppliers);

  // hub routes 
  router.get("/hub", hubController.getAllHub);
  router.get("/cityhub/:city_id", hubController.getAllCityHubs);  
  router.get("/hub:id", hubController.getDetailsHub);  
  router.post("/hub", hubController.createHub);  
  router.put("/hub/:id", hubController.updateHub);  
  router.delete("/hub/:id", hubController.deleteHub);
  router.delete("/cityhubmultiple", hubController.deleteMultipleHub);
  

  // cities routes 
  router.get("/cities", citiesController.getAllCities);  
  router.get("/cities:id", citiesController.getCityDetails);  
  router.post("/cities", citiesController.createCity);  
  router.put("/cities/:id", citiesController.updateCity);  
  router.delete("/cities/:id", citiesController.deleteCity); 
  // router.delete("/cities/:id", citiesController.deleteMultiple);

// inventory routes 
  router.get("/inventory", inventoryController.getAllInventory);
  router.post("/inventory_filter", inventoryController.getAllInventoryFilter); 
  router.post("/inventory_transfer_vehicle", inventoryController.transferVehicleInventory); 
  router.get("/inventory/:id", inventoryController.getDetailsInventory);  
  router.post("/inventory", inventoryController.createInventory);  
  router.put("/inventory/:id", inventoryController.updateInventory);  
  router.delete("/inventory/:id", inventoryController.deleteInventory); 
  router.delete("/inventory_multiple_delete", inventoryController.deleteMultipleInventory);
  router.get("/inventory_timeline/:id", inventoryController.getVehicleTimelineInventory);

  // bookings routes 
  router.get("/bookings", bookingsController.getAllBookings);  
  router.post("/bookings_filter", bookingsController.getAllBookingsFilters);  
  router.get("/bookings/:id", bookingsController.getBookingDetails);  
  router.post("/bookings",upload.array('images',100), bookingsController.createBooking);  
  router.put("/bookings/:id",upload.array('images',100), bookingsController.updateBooking);  
  router.delete("/bookings/:id", bookingsController.deleteBooking); 
  router.post("/checkVerificationCode", bookingsController.checkVerificationCode); 
  router.post("/check_availability", bookingsController.check_availability); 
  router.post("/file_upload", bookingsController.file_upload); 
  
  // router.get("/upload" ,(req,res)=> {
  //   return  res.send("Please add api name in url.");
  // }); 
  


  // referals routes 
  router.get("/referals", referalController.getAll);  
  router.post("/referals_filter", referalController.getAllReferalsFilters);  
  router.get("/referals/:id", referalController.getDetails);  
  router.post("/referals", referalController.create);  
  router.put("/referals/:id", referalController.update);  
  router.delete("/referals/:id", referalController.deleteRecord); 

  // assistance routes 
  router.get("/assistance", assistanceController.getAll);  
  router.post("/assistance_filter", assistanceController.getAllWithFilter);  
  router.get("/assistance/:id", assistanceController.getDetails);  
  router.post("/assistance", assistanceController.create);  
  router.put("/assistance/:id", assistanceController.update);  
  router.delete("/assistance/:id", assistanceController.deleteRecord);

  // assistance type routes 
  router.get("/assistance_type", assistanceController.getAllAssistanceType);  
  router.get("/assistance_type/:id", assistanceController.getDetailsAssistanceType);  
  router.post("/assistance_type", assistanceController.createAssistanceType);  
  router.put("/assistance_type/:id", assistanceController.updateAssistanceType);  
  router.delete("/assistance_type/:id", assistanceController.deleteAssistanceType);

  // problem_type type routes 
  /* router.get("/problem_type", assistanceController.getAllAssistanceType);  
  router.get("/problem_type/:id", assistanceController.getDetailsAssistanceType);  
  router.post("/problem_type", assistanceController.createAssistanceType);  
  router.put("/problem_type/:id", assistanceController.updateAssistanceType);   */
  router.delete("/problem_type/:id", assistanceController.deleteProblemType);


  // assistance_fare routes 
  router.get("/assistance_fare", assistanceController.getAllAssistanceFare); 
  router.get("/assistance_fare/:id", assistanceController.getDetailsAssistanceFare);  
  router.post("/assistance_fare", assistanceController.createAssistanceFare);  
  router.put("/assistance_fare/:id", assistanceController.updateAssistanceFare);  
  router.delete("/assistance_fare/:id", assistanceController.deleteAssistanceFare);

  app.use(router);
};
module.exports = routes;
