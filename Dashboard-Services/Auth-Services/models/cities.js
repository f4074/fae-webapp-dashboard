const mongoose = require("mongoose");
const validator = require("validator");

const citiesSchema = new mongoose.Schema({ 
    name: { type: String,trim: true  },   
    short_name: { type: String,trim: true },   
    latitude: { type: String ,trim: true, default: 0},
    longitude: { type: String ,trim: true, default: 0},    
    status: { type: Boolean ,trim: true, default: true},
    created: { type: Date, default: Date.now },
    updated: { type: Date, default: Date.now }   

},{ timestamps: true });

module.exports = mongoose.model("cities", citiesSchema);
