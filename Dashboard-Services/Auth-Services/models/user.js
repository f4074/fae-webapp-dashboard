const mongoose = require("mongoose");
const validator = require("validator");


const userSchema = new mongoose.Schema({  
  name: { type: String, required: true},
  email: {type: String, default: "" },
  password: {  type: String, default: ""},
  phonecode: { type: String },
  phone: { type: String, min: 10 },
  otp: { type: String },
  referral_code: {type: String},
  latitude: { type: String, default: "" } ,
  longitude: { type: String, default: "" } , 
  address1: { type: String, default: "" },
  address2: { type: String, default: "" },
  city_id: { 
      type: mongoose.Schema.Types.ObjectId,ref:'cities' , 
      default:null
  },
  hub_id: { 
    type: mongoose.Schema.Types.ObjectId,ref:'hubs' , 
    default:null 
  },
  city: { type: String, default: "" },
  state: { type: String, default: "" },
  country: { type: String, default: "" },
  zipcode: { type: String, default: "" },
  wallet_balance: { type: String, default: "0" } ,
  profile_image: { type: String, default: "no.png" } ,
  status: {type: Boolean, default:false },
  profile_verified: {type: Boolean, default: false },
  active_status:{type: Boolean, default: true },
  kyc_status: {type: String, default: '0' },
  reject_reason: { type: String,trim: true,default: null }, 
  delete_status: { type: String, default: "" } ,
  delete_date: { type: String },
  created: { type: Date, default: Date.now },
  updated: { type: Date, default: Date.now }  
},{
  timestamps: true
});

  // Refreal Schema
  const topicSchema =  new mongoose.Schema({
    referral_by_code: { type: String, default: ""},
    referral_id: {type: String, default: "" },
    status: {type: Boolean, default: "0" },
  },{
    timestamps: true
  });

  const documentSchema =  new mongoose.Schema({
    user_id: { 
      type: mongoose.Schema.Types.ObjectId,ref:'user' , 
      default:null
    },
    aadhar_front: {type: String, default: "" },
    aadhar_back: {type: String, default: "" },
    pan_card: {type: String, default: "" },
    driving_licence: {type: String, default: "" },
    other: {type: String, default: "" },
    business_status: {type: Boolean, default: "0" },
    reject_reason: { type: String,trim: true  },
    entity: {type: String, default: "" },
    business_partner_id: {type: String, default: "" },
    status: {type: Boolean, default: "0" },
    created: { type: Date, default: Date.now },
    updated: { type: Date, default: Date.now }  
  },{
    timestamps: true
  });


const User = mongoose.model('user', userSchema);
const Topic = mongoose.model('referral', topicSchema);
const User_document = mongoose.model('user_documents', documentSchema);


module.exports = {User, Topic, User_document};