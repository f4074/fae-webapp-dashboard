const express = require("express");
const router = express.Router();
const usercontroller = require("../controllers/usersController");
const admincontroller = require("../controllers/adminController");

const verifyToken = require("../middleware/auth.js");
var multer  = require('multer');
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './uploads')
  },
  filename: function (req, file, cb) {
    const fileName = file.originalname.toLowerCase().split(' ').join('-');
        cb(null, Date.now() + '-'+fileName)
  }
})
var upload = multer({ storage: storage })
const AUTH_API= process.env.AUTH_API;

// user routes
let routes = (app) => {  
  //user routes 
  router.post("/sign-up", usercontroller.signup);
  router.post("/sign-in", usercontroller.signin); 
  
  // admin routes 
  router.post("/signup", admincontroller.createAdmin);
  router.post("/login", admincontroller.adminLogin);
  router.post("/change-password", admincontroller.changePassword);
  
  app.use(router);
};
module.exports = routes;
