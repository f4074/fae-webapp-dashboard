
require("dotenv").config();
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const Admin = require("../models/admin");
const cities = require("../models/cities");
const Hub = require("../models/hub");

const ObjectId = require('mongodb').ObjectId;
const auth = require("../middleware/auth");
const bcrypt = require("bcryptjs");
const sendEmail = require("../common/sendMail")


const createAdmin = async (req, res) => { 
  const errors = {};
    try {
        const { first_name, last_name, email, password,phone_number,user_role,address,city_id,hub_id,pincode } = req.body;
         
        if (!req.body.first_name) {
          return res.status(400).send({ message: "Please enter your first name" });
        }
        if (!req.body.last_name) {
          return res.status(400).send({ message: "Please enter your last name" });
        }
        if (!req.body.phone_number) {
          return res.status(400).send({ message: "Please enter your phone number" });
        }
        if (!req.body.email) {
          return res.status(400).send({ message: "Please enter email address" });
        }
        if (!req.body.password) {
          return res.status(400).send({ message: "Please enter password" });
        }  

        if (!(/^[\-0-9a-zA-Z\.\+_]+@[\-0-9a-zA-Z\.\+_]+\.[a-zA-Z]{2,}$/).test(String(req.body.email))) {
            return res.status(400).send({ message: "Please enter email address" });
        }
        const oldUser = await Admin.findOne({ email });
        if (oldUser) { 
          return res.status(200).send({ status: false, message: 'Email already register, please use another email address.' });
        }

        const checkPhone = await Admin.findOne({ phone_number });
        if (checkPhone) { 
          return res.status(200).send({ status: false, message: 'Phone Number already register, please use another phone number.' });
        }

         const otp = Math.floor(100000 + Math.random() * 900000);
         //user.otp = otp.toString()

         console.log("otp.toString()", otp.toString());

        encryptedPassword = await bcrypt.hash(password, 10);

        var JSonBody = {
          first_name:first_name, 
          last_name:last_name, 
          email:email, 
          password:encryptedPassword, 
          phone_number:phone_number, 
          user_role:user_role, 
          city_id:city_id, 
          hub_id:hub_id, 
          address:address, 
          pincode:pincode,           
        }

        const AdminCreation = await Admin.create(JSonBody);
        /* const token = jwt.sign(
          { user_id: user._id, email },
          process.env.TOKEN_KEY,
          { expiresIn: "30min",}
        );
        user.token = token; */
        if(!AdminCreation){
          
          return res.status(201).json({status:false, message: 'Oops!, something error!' , data:AdminCreation});
        }
        const allAdminData = await getAllAdminList();

        return res.status(201).send({status: true, message: "Users successfully created!", data: {'data':AdminCreation,'allAdminData':allAdminData}});

      } catch (err) {
        console.log(err);
      }

}


async function getAllAdminList(){
  const allAdminData = await Admin.find({$and:[{"user_role":{ $ne: "FAE_MAIN_ADMIN"  } }]}).sort({created: -1});
  if(allAdminData){
    allAdminData.filter(function(val,index){
        return val['key']=index;
    })
  }
  return allAdminData;

}

const adminLogin = async (req, res) => { 
  try {
    const { email, password } = req.body;
    console.log("req.email", req.body)
    if (!(email && password)) {
      return res.status(400).send({ status: false, message: "Please enter email address and password" });
    }
    // const admin = await Admin.findOne({ email });
    const admin = await Admin.findOne({$or:[{ email: { $eq:email } },{ phone_number: { $eq:email } }]});
    
    if(!admin){
      return res.status(401).send({status: false, message: "Invalid Credentials"});
    }        
    if(admin  && admin.password != undefined  && (await bcrypt.compare(password, admin.password))) {
      const checkUserActivation = await checkUserActivateDeactivate(admin._id);
      if(!checkUserActivation){
        return res.status(202).send({status: false, message: "Account Deacivated by Admin!"});
      }
      const token = jwt.sign(
        { user_id: admin._id, email },
        process.env.TOKEN_KEY,
        { expiresIn: "1Hour",}
      );
      admin.token = token;
      const data = await Admin.findByIdAndUpdate(admin._id, {'token':token});
      //res.status(200).send().json(admin);
      const city_data = await cities.find({ _id : { $in : admin.city_id } } ,{name:1}).sort({ name: 1 });
      const hub_data = await Hub.find({ _id : { $in : admin.hub_id } } ,{title:1}).sort({ title: 1 });
      // console.log('city_data',city_data)
      admin.citiesList = city_data;
      admin.hubsList = hub_data;
      var citiesName = "";
      if(city_data.length >0){
        var citiesArr = city_data.map(function(item) {
          return item['name'];
        });
        citiesName =  citiesArr.join(', ')
      }

      var hubsName = "";
      if(hub_data.length >0){
        var hubsArr = hub_data.map(function(item) {
          return item['title'];
        });
        hubsName =  hubsArr.join(', ')
      }
   
      return res.status(200).send({status: true, message: "Login successfully", data: admin, token: token,city_data:citiesName,hub_data:hubsName});
    }else{
      return res.status(401).send({status: false, message: "Invalid Credentials"});
    }

  } catch (err) {
    console.log(err);
  }
}

const checkUserActivateDeactivate = async (user_id) => { 
  const admin = await Admin.findOne({$and:[{ _id:ObjectId(user_id)},{ status: { $eq:true } }]});
  if(!admin){
    return false;
  }
  return true;
}

const changePassword=async(req, res)=>{
  const id = req.body._id;  
  const oldpassword = req.body.oldpassword;  
  const newpassword = req.body.newpassword;

  const encryptedNewPassword = await bcrypt.hash(newpassword, 10);
 
  const admin = await Admin.findOne({ _id: ObjectId(id) });
    
  if(!admin){
    return res.status(401).send({status: false, message: "User does not exist!"});
  }        
  
  if(admin  && admin.password != undefined  && (await bcrypt.compare(oldpassword, admin.password))) {    
    const data = await Admin.findByIdAndUpdate(admin._id, {'password':encryptedNewPassword});    
    return res.status(200).send({status: true, message: "Password Changed successfully"});
    
  }else{
    return res.status(202).send({status: false, message: "Please enter correct old password!"});
  }
  
}

const updateAdmin = async (req, res) => { 
  try {      
    if (!req.body) {
        return res.status(400).send({
            message: "Data to update can not be empty!"
        });
    } 
    const id = req.params.id;  
    const phone_number = req.body.phone_number;  
    const email = req.body.email;  

    const oldUser = await Admin.findOne({$and:[{"_id":{ $ne: id } },{ "email":email }]});
    if (oldUser) { 
      return res.status(200).send({ status: false, message: 'Email already register, please use another email address.' });
    }

    const checkPhone = await Admin.findOne({$and:[{"_id":{ $ne: id } },{ "phone_number":phone_number }]});
    if (checkPhone) { 
      return res.status(200).send({ status: false, message: 'Phone Number already register, please use another phone number.' });
    }

    const data = await Admin.findByIdAndUpdate(id, req.body);
    console.log('data',data)
    if (!data) {         
        return res.status(200).send({status: false, message: "Cannot update User details! with id=${id}. Maybe user! was not found!",data: []});
    }

    const allAdminData = await getAllAdminList();      

    return res.status(200).send({status: true, message: "Users successfully updated!", data: {'data':data,'allAdminData':allAdminData}});
    

  } catch (err) {
    console.log(err);
    return res.status(500).send({status: false, message: err.message || "Some error occurred while updating User.",data: []});
  }
}

const deleteAdmin = async (req, res) => { 
  try {      
    
    const id = req.params.id;  
    const data = await Admin.findByIdAndRemove(id);
    console.log('data',data)
    if (!data) {         
        return res.status(200).send({status: false, message: "Cannot delete User details! with id=${id}. Maybe user! was not found!",data: []});
    }

    const allAdminData = await getAllAdminList();

    return res.status(200).send({status: true, message: "Users successfully deleted!", data: {'data':data,'allAdminData':allAdminData}});
    

  } catch (err) {
    console.log(err);
    return res.status(500).send({status: false, message: err.message || "Some error occurred while updating User.",data: []});
  }
}

const getUserDetailsWithToken=async(access_token)=>{
  const admin_details = await Admin.findOne({"token":access_token});
  return admin_details;
}







module.exports = { adminLogin,createAdmin,updateAdmin,deleteAdmin,changePassword,getUserDetailsWithToken };
