require("dotenv").config();
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const {User,User_document} = require("../models/user");
const cities = require("../models/cities");
// const Hub = require("../models/hub");
const ObjectId = require('mongodb').ObjectId;
const auth = require("../middleware/auth");
const bcrypt = require("bcryptjs");
const sendEmail = require("../common/sendMail")



const signup = async (req, res) => {
    const errors = {};
    try {
        const { first_name, last_name, email, password,phone,address1, address2,city,state,country,zipcode } = req.body;

          if (!req.body.phone) {
            return res.status(400).send({ message: "Please enter your phone number" });
          }
          if (!req.body.first_name) {
            return res.status(400).send({ message: "Please enter your first name" });
          }
          if (!req.body.last_name) {
            return res.status(400).send({ message: "Please enter your last name" });
          }
          if (!req.body.email) {
            return res.status(400).send({ message: "Please enter email address" });
          }
          if (!req.body.password) {
            return res.status(400).send({ message: "Please enter password" });
          }  
          if (!(/^[\-0-9a-zA-Z\.\+_]+@[\-0-9a-zA-Z\.\+_]+\.[a-zA-Z]{2,}$/).test(String(req.body.email))) {
             return res.status(400).send({ message: "Please enter email address" });
          }



        const oldUser = await User.findOne({ email });
        if (oldUser) { 
          return res.status(409).send({ status: 'false', message: 'Email already register, please use another email address.' });
        }

        const checkPhone = await User.findOne({ phone});
        if (checkPhone) { 
          return res.status(409).send({ status: false, message: 'Phone Number already register, please use another phone number.' });
        }

        const otp = Math.floor(100000 + Math.random() * 900000);
      //  console.log("otp.toString()", otp.toString());

        encryptedPassword = await bcrypt.hash(password, 10);
        const user = await User.create(req.body);
        /* const token = jwt.sign(
          { user_id: user._id, email },
          process.env.TOKEN_KEY,
          { expiresIn: "30min",}
        );
        user.token = token; */

        const allCities = await cities.find().sort({ name: 1 });
        // const UsersList = await User.find().sort({ created: -1 });
        const UsersList = await User.aggregate([
          {
              $sort:{created: -1}
          },
          { $lookup:
              {
                 from: "user_document",
                 localField: "user_id",
                 foreignField: "_id",
                 as: "userDetails"
              }
          }
         
        ])
        if(UsersList){
          UsersList.filter(function(val,index){
              return val['key']=index;
          })
        }
        
        return res.status(201).json({status: 'true', message: 'User created successfully!' , data: {'data':user,'citiesList':allCities,'UsersList':UsersList}});

      } catch (err) {
        console.log(err);
      }

}

const signin = async (req, res) => {
    
    try {
        const { email, password } = req.body;
        if (!(email && password)) {
          return res.status(400).send({ status: 'false', message: "Please enter email address and password" });
        }
        const user = await User.findOne({ email });
        if (user && (await bcrypt.compare(password, user.password))) {
          const token = jwt.sign(
            { user_id: user._id, email },
            process.env.TOKEN_KEY,
            { expiresIn: "30min",}
          );
          user.token = token;
          //res.status(200).send().json(user);
          // const user_city_id= user.city_id;
          // console.log('user_city_id',user_city_id)
          

          return res.status(200).send({status: true, message: "Login successfully", data: user, token: token});
        }
        return res.status(200).send({status: false, message: "Invalid Credentials"});
    
      } catch (err) {
        console.log(err);
      }

}


module.exports = { signup, signin};
