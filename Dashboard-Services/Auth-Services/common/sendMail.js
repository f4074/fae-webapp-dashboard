const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

module.exports = async ({ sendTo, subject, template }) => {
    const email = process.env.SENDGRID_FROM_EMAIL || ''
    const name = process.env.SENDGRID_FROM_NAME || ''
    
    const msg = {
        to: sendTo,
        from: {
            email,
            name
        },
        subject: subject,
        // text: `Click here t`,
        html: template, // html body
    };
    try {
        await sgMail.send(msg);
        console.log('Email sent to ', sendTo)
    } catch (error) {
        console.error(error);
        if (error.response) {
            console.error(error.response.body)
        }
    }
}