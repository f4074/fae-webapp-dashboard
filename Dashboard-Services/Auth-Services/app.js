require("dotenv").config();
require("./config/database").connect();
const express = require("express");
const auth = require("./middleware/auth");
const jwt = require("jsonwebtoken");
const app = express();
const cors = require("cors");

app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({limit: "50mb", extended: true, parameterLimit:50000}));

// importing user context
const User = require("./models/user");
const date = require('date-and-time')

app.get("/",(req,res)=> {
  // res.setHeader("Access-Control-Allow-Origin", "*")
  return  res.send("Please add api name in url.");
});

const corsOptions ={
  origin: '*',
  methods: ['GET','POST','DELETE','UPDATE','PUT','PATCH'],
  credentials:true,            //access-control-allow-credentials:true
  optionSuccessStatus:200
}
app.use(cors());

module.exports = app;